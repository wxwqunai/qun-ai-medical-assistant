//
//  QWRegPassTipsView.m
//  qwm
//
//  Created by kevin on 2023/3/27.
//

#import "QWRegPassTipsView.h"
@interface QWRegPassTipsView()
@property (nonatomic,strong)UILabel *label;
@end

@implementation QWRegPassTipsView

- (instancetype)init
{
    self =[super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self label];
    }
    return self;
}

- (UILabel *)label{
    if(!_label){
        _label = [[UILabel alloc]init];
        _label.font=[UIFont fontWithName:@"PingFangSC-Light" size:14];
        _label.text=@"请设置至少6位数的密码";
        [self addSubview:_label];
        
        [_label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(18);
            make.centerY.equalTo(self);
        }];
    }
    return _label;
}


@end
