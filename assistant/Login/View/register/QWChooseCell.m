//
//  QWChooseCell.m
//  assistant
//
//  Created by kevin on 2023/6/9.
//

#import "QWChooseCell.h"
@interface QWChooseCell()
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *chooseLabel;
@end

@implementation QWChooseCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        [self bgView];
    }
    return self;
}

#pragma mark - bgview
- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(self.contentView);
            make.left.mas_offset(18);
            make.right.mas_offset(-18);
            make.height.mas_offset(100);
        }];
        
        [self nameLabel];
        [self chooseLabel];
        
        
        //分割线
        UIImageView *lineImageView = [[UIImageView alloc]init];
        lineImageView.backgroundColor = [UIColor lightGrayColor];
        [_bgView addSubview:lineImageView];
        [lineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.chooseLabel.mas_bottom).offset(10);
            make.left.mas_offset(8);
            make.right.mas_offset(-8);
            make.height.mas_offset(0.5);
        }];
    }
    return _bgView;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.text = @"用户类型";
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.font = [UIFont systemFontOfSize:16];
        [_bgView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_offset(10);
            make.left.mas_offset(8);
            make.right.mas_offset(-8);
        }];
    }
    return _nameLabel;
}

- (UILabel *)chooseLabel{
    if(!_chooseLabel){
        _chooseLabel = [[UILabel alloc]init];
        _chooseLabel.textColor = [UIColor blackColor];
        _chooseLabel.font = [UIFont systemFontOfSize:14];
        [_bgView addSubview:_chooseLabel];
                
        [_chooseLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_nameLabel.mas_bottom);
            make.left.mas_offset(8);
            make.height.mas_offset(50);
        }];
        
        
        //箭头
        UIImageView *imageView = [[UIImageView alloc]init];
        imageView.image = [UIImage imageNamed:@"ic_arrow_down.png"];
        [_bgView addSubview:imageView];
        
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_offset(-8);
            make.centerY.equalTo(_chooseLabel);
        }];
    }
    return _chooseLabel;
}

- (void)setType:(NSString *)type
{
    _type = type;
    _chooseLabel.text = _type;
}

@end
