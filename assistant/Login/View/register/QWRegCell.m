//
//  QWRegCell.m
//  qwm
//
//  Created by kevin on 2023/3/27.
//

#import "QWRegCell.h"

@implementation QWRegCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lineImgaeView.backgroundColor = [UIColor lightGrayColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
