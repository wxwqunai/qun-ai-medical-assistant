//
//  QWChooseCell.h
//  assistant
//
//  Created by kevin on 2023/6/9.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWChooseCell : QWBaseTableViewCell
@property (nonatomic,copy) NSString *type;
@end

NS_ASSUME_NONNULL_END
