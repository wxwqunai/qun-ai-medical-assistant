//
//  QWLoginCell.h
//  qwm
//
//  Created by kevin on 2023/3/26.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWLoginCell : QWBaseTableViewCell

@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@end

NS_ASSUME_NONNULL_END
