//
//  QWAgreementController.h
//  qwm
//
//  Created by kevin on 2023/5/8.
//

#import "QWBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWAgreementController : QWBaseController
@property (nonatomic,copy) NSString *url;
@end

NS_ASSUME_NONNULL_END
