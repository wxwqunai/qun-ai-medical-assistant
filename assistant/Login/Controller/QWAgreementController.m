//
//  QWAgreementController.m
//  qwm
//
//  Created by kevin on 2023/5/8.
//

#import "QWAgreementController.h"
#import <WebKit/WebKit.h>

@interface QWAgreementController ()<WKNavigationDelegate>
@property (nonatomic,strong) WKWebView *webView;
@property (nonatomic,strong) UIProgressView *progressView;//进度条

@end

@implementation QWAgreementController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self webView];
    [self progressView];
}

#pragma mark -网页
-(WKWebView *)webView
{
    if (!_webView) {
        _webView =[[WKWebView alloc]init];
        _webView.navigationDelegate=self;
        [_webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];//监听进度条
        _webView.scrollView.showsVerticalScrollIndicator=NO;
        _webView.scrollView.showsHorizontalScrollIndicator=YES;
        NSURLRequest *request =[[NSURLRequest alloc]initWithURL:[NSURL URLWithString:self.url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:5.0];
        [_webView loadRequest:request];
//        [NSURLSession.sharedSession dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//            if (error == nil && data != nil && data.length > 0) {
////                self->_webView.hidden=NO;
//                [self->_webView loadRequest:request];
//            } else {
////                self->_webView.hidden=YES;
////                [SWHUDUtil hideHudView];
//            }
//        }];
        
        [self.view addSubview:_webView];
        
        [_webView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
    }
    return _webView;
}
// 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{
//    [SWHUDUtil showHudViewInSuperView:self.view withMessage:@""];
}
// 当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation
{
}
// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
//    [SWHUDUtil hideHudView];
}
// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation
{
//    [SWHUDUtil hideHudView];
}

#pragma mark -进度条
-(UIProgressView *)progressView
{
    if (!_progressView) {
        _progressView=[[UIProgressView alloc]initWithFrame:CGRectMake(0, [UIDevice navigationFullHeight], kScreenWidth, 1)];
        _progressView.progressTintColor=[UIColor colorFromHexString:@"#97e070"];
        _progressView.trackTintColor=[UIColor whiteColor];
        [_webView addSubview:_progressView];
    }
    return _progressView;
}
// 计算wkWebView进度条
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (object == self.webView && [keyPath isEqualToString:@"estimatedProgress"]) {
        CGFloat newprogress = [[change objectForKey:NSKeyValueChangeNewKey] doubleValue];
        if (newprogress == 1) {
            [self.progressView setProgress:1 animated:NO];
            self.progressView.hidden = YES;
        }else {
            [self.progressView setProgress:newprogress animated:YES];
            self.progressView.hidden = NO;
        }
    }
}



@end
