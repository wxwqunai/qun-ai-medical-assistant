//
//  QWForgetController.h
//  qwm
//
//  Created by kevin on 2023/3/26.
//

#import "QWBaseController.h"

NS_ASSUME_NONNULL_BEGIN
typedef void (^ForgetSuccessBlock)(NSString *phone, NSString *pwd);

@interface QWForgetController : QWBaseController

@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) ForgetSuccessBlock forgetSuccessBlock;

@end

NS_ASSUME_NONNULL_END
