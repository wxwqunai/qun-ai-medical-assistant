//
//  QWRegisterController.h
//  qwm
//
//  Created by kevin on 2023/3/26.
//

#import "QWBaseController.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^RegistSuccessBlock)(NSString *phone, NSString *pwd);

@interface QWRegisterController : QWBaseController
@property (nonatomic, copy) RegistSuccessBlock registSuccessBlock;
@end

NS_ASSUME_NONNULL_END
