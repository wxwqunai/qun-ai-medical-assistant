//
//  QWLoginModel.h
//  qwm
//
//  Created by kevin on 2023/3/26.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWLoginModel : NSObject
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *password;
@end


@interface QWUseRreserve1Model : NSObject
@property (nonatomic, copy) NSString *is_attestation; //1：已认证 0 待认证
@property (nonatomic, copy) NSString *make_appointment; //是否可以发出预约
@property (nonatomic, copy) NSString *bg_appointment; //是否可以接受预约
@property (nonatomic, copy) NSString *isshow; //是否显示到首页
@end

@interface QWqaUserModel : NSObject
@property (nonatomic, assign) NSInteger timeSecond;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, copy) NSString *companyId;
@property (nonatomic, copy) NSString *createBy;
@property (nonatomic, copy) NSString *delFlag;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *extprop;
@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *level;
@property (nonatomic, copy) NSString *loginDate;
@property (nonatomic, copy) NSString *loginFlag;
@property (nonatomic, copy) NSString *loginIp;
@property (nonatomic, copy) NSString *loginName;
@property (nonatomic, copy) NSString *major;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *no;
@property (nonatomic, copy) NSString *officeId;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *photo;
@property (nonatomic, copy) NSString *qrcode;
@property (nonatomic, copy) NSString *remarks;
@property (nonatomic, strong) QWUseRreserve1Model *reserve1;
@property (nonatomic, copy) NSString *reserve2;
@property (nonatomic, copy) NSString *resume;
@property (nonatomic, copy) NSString *sign;
@property (nonatomic, copy) NSString *updateBy;
@property (nonatomic, copy) NSString *updateDate;
@property (nonatomic, copy) NSString *usertype;
@property (nonatomic, copy) NSString *wechat;
@end


@interface QWRegTableInfoModel : NSObject

@end

@interface SWLoginTimeModel : NSObject
@property (nonatomic, assign) NSInteger timeSecond;
@property (nonatomic, strong) NSTimer *timer;
@end



NS_ASSUME_NONNULL_END
