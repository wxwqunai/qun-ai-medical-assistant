//
//  QWConsultationWebController.m
//  assistant
//
//  Created by kevin on 2023/6/30.
//

#import "QWConsultationWebController.h"
#import "QWWebviewController.h"
#import "QWNavgationController.h"

@interface QWConsultationWebController ()
@property (nonatomic,copy) NSString *url;
@property (nonatomic, strong) QWWebviewController *webviewVC;
@end

@implementation QWConsultationWebController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self reloadWebview];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"会诊大厅";
    self.isHiddenLeftBarButtonItem = YES;
    [self.rightBarButtonItem setImage:[UIImage imageNamed:@"ic_web_home.png"]];

    self.url = [NSString stringWithFormat:@"%@/bokehall/hall/allHall",QWM_BASE_API_URL];

    [self webviewVC];
}

- (QWWebviewController *)webviewVC{
    if(!_webviewVC){
        _webviewVC =[[QWWebviewController alloc]init];
        _webviewVC.url = self.url;
        [self addChildViewController:_webviewVC]; //将webviewVC 添加到navigation队列
        [self.view addSubview:_webviewVC.view];
        [_webviewVC didMoveToParentViewController:self];
    }
    return _webviewVC;
}

#pragma mark-导航条右边按钮
-(void)navRightBaseButtonClick
{
    NSString *reloadurlStr = [self.webviewVC wrapUrl];
    NSURLRequest *request =[[NSURLRequest alloc]initWithURL:[NSURL URLWithString:reloadurlStr] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:5.0];
    [self.webviewVC.webView loadRequest:request];
}

#pragma mark - 每次进入会诊重新加载web
- (void) reloadWebview{
    static NSInteger num = 0;
    if(num>0){
        if(_webviewVC){
            [_webviewVC willMoveToParentViewController:nil];
            [_webviewVC.view removeFromSuperview];
            [_webviewVC removeFromParentViewController];
            _webviewVC = nil;
        }
        
        [self webviewVC];
    }
    num = num+1;
}


@end

