//
//  AppDelegate.h
//  assistant
//
//  Created by kevin on 2023/4/12.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@end

