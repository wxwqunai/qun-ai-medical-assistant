//
//  HXPostRequest.m
//  qwm
//
//  Created by kevin on 2023/4/3.
//

#import "HXPostRequest.h"
 
@implementation HXPostRequest
 
- (id)initWithRequestUrl:(NSString *)url argument:(id)argument {
    
    return [self initWithRequestUrl:url argument:argument constructingBodyBlock:nil];
}
 
- (id)initWithRequestUrl:(NSString *)url argument:(id)argument constructingBodyBlock:(nullable AFConstructingBlock)block{
    
    self = [super init];
    if (self) {
        self.requestUrl = url;
        self.requestArgument = argument;
        self.constructingBodyBlock = block;//直接赋值过去即可
    }
    return self;
}
 
- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

//列子：
//HXPostRequest *uploadRequest = [[HXPostRequest alloc] initWithRequestUrl:@"api/uploadfile" argument:nil constructingBodyBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
//    //把所有要上传的文件都加进去
//    for (NSURL * tempFileUrl in self.fileUrls) {
//        [formData appendPartWithFileURL:tempFileUrl name:@"files" error:nil];
//    }
//}];
//
//[uploadRequest startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
//    if (success) {
//        //上传成功
//    }else
//    {
//        //失败
//    }
//} failure:^(__kindof HXRequest *request, NSString *errorInfo) {
//    //报错
//    NSLog(@"%@",errorInfo);
//}];
 
@end
