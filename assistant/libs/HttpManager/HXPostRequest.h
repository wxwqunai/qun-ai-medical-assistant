//
//  HXPostRequest.h
//  qwm
//
//  Created by kevin on 2023/4/3.
//

#import "HXRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface HXPostRequest : HXRequest
/**
 POST 请求
 
 @param url 网址
 @param argument 参数
 @return HXRequest
 */
- (id)initWithRequestUrl:(NSString *)url argument:(id)argument;
 
/**
 POST 请求
 @param url 网址
 @param argument 参数
 @param block 支持附件
 @return HXRequest
 */
- (id)initWithRequestUrl:(NSString *)url argument:(id)argument constructingBodyBlock:(nullable AFConstructingBlock)block;


@end

NS_ASSUME_NONNULL_END
