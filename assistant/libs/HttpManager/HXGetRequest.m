//
//  HXGetRequest.m
//  qwm
//
//  Created by kevin on 2023/3/31.
//

#import "HXGetRequest.h"
 
@implementation HXGetRequest
 
- (id)initWithRequestUrl:(NSString *)url argument:(NSDictionary *)argument {
    
    self = [super init];
    if (self) {
        self.requestUrl = url;
        self.requestArgument = argument;
    }
    return self;
}
 
- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}



#pragma mark - 下载
+ (void)downloadPach:(NSString *)url
        withLocalPath:(nullable NSString *) downloadPath
            withPress:(AFURLSessionTaskProgressBlock)resumableDownloadProgressBlock
          withSuccess:(nullable HXRequestCompletionBlock)success
              failure:(nullable HXRequestCompletionFailureBlock)failure{
    HXGetRequest * downloadRequest = [[HXGetRequest alloc] initWithRequestUrl:url argument:[NSDictionary dictionary]];
    
    //缓存路径
    if(IsStringEmpty(downloadPath)) downloadPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    downloadRequest.resumableDownloadPath = downloadPath;
    
    //缓存进度
    downloadRequest.resumableDownloadProgressBlock = resumableDownloadProgressBlock;
    
    //启动下载
    [downloadRequest startWithCompletionBlockWithSuccess:success failure:failure];
}

//下载列子：
//NSString *url = @"";
//HXGetRequest * downloadRequest = [[HXGetRequest alloc] initWithRequestUrl:url argument:nil];
//
////缓存路径和下载进度
//downloadRequest.resumableDownloadPath = @"";  //设置缓存路径
//downloadRequest.resumableDownloadProgressBlock = ^(NSProgress * _Nonnull progress) {
//    dispatch_async(dispatch_get_main_queue(), ^{
//        //progress.localizedDescription       //需要在主线程中更新下载进度
//    });
//};
//
////启动下载
//[downloadRequest startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
//    //下载完毕
//    //这里的success并不能代表文件真的下载成功，需要校验文件的有效性
//
//} failure:^(__kindof HXRequest *request, NSString *errorInfo) {
//    //下载失败
//}];
 
@end
