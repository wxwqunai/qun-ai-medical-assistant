//
//  HXRequest.h
//  qwm
//
//  Created by kevin on 2023/3/31.
//

#import "YTKRequest.h"

NS_ASSUME_NONNULL_BEGIN
 
@class HXRequest;
 
typedef void(^HXRequestCompletionBlock)(__kindof HXRequest *request,NSDictionary *result, BOOL success);
typedef void(^HXRequestCompletionFailureBlock)(__kindof HXRequest *request,NSString *errorInfo);
 
@interface HXRequest : YTKRequest
 
/**
 请求URL地址
 */
@property(nonatomic, strong) NSString * requestUrl;
 
/**
 请求参数
 */
@property(nonatomic, strong) id requestArgument;
 
/**
 请求类型
 */
@property(nonatomic, assign) YTKRequestSerializerType requestSerializerType;
 
/**
 是否校验json数据格式，默认yes
 */
@property(nonatomic, assign) BOOL verifyJSONFormat;
 
/**
 开始请求数据
 @param success 成功回调
 @param failure 失败回调，返回error信息
 */
- (void)startWithCompletionBlockWithSuccess:(nullable HXRequestCompletionBlock)success
                                    failure:(nullable HXRequestCompletionFailureBlock)failure;
 
@end

NS_ASSUME_NONNULL_END
