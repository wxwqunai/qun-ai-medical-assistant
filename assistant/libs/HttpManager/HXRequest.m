//
//  HXRequest.m
//  qwm
//
//  Created by kevin on 2023/3/31.
//

#import "HXRequest.h"
#import "YTKNetworkConfig.h"
 
@implementation HXRequest
 
- (instancetype)init
{
    self = [super init];
    if (self) {
//        [YTKNetworkConfig sharedConfig].debugLogEnabled = YES;  //开启Debug模式
        
        self.verifyJSONFormat = NO;
    }
    return self;
}
 
- (NSString *)baseUrl {
    
    return QWM_BASE_API_URL;
}
 
- (NSTimeInterval)requestTimeoutInterval {
    return 30;
}
 
- (id)jsonValidator {
    if (self.verifyJSONFormat) {
        return @{
                 @"data": [NSObject class],
                 @"code": [NSNumber class],
                 @"msg": [NSString class]
                 };
    }else
    {
        return nil;
    }
}
 
- (void)startWithCompletionBlockWithSuccess:(nullable HXRequestCompletionBlock)success
                                    failure:(nullable HXRequestCompletionFailureBlock)failure
{
    //网络判断
    Reachability *reach = [Reachability reachabilityForInternetConnection];
    if (reach.currentReachabilityStatus == NotReachable) {
        [SWHUDUtil hideHudViewWithFailureMessage:@"网络不给力，请检查网络设置。"];
        return;
    }
    
    //请求链接拦截更替
    NSArray * configArr= [[NSUserDefaults standardUserDefaults]objectForKey:NSUserDefaults_myjson_dev_config];
    [configArr enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *config_url = obj[@"url"];
        if(!IsStringEmpty(config_url) && ( [config_url isEqualToString:self.requestUrl] || [config_url containsString:self.requestUrl] ||[self.requestUrl containsString:config_url])){
            NSString *data_type = obj[@"data_type"];
            if([data_type isEqualToString:@"dev_tmp_req"] ||[data_type isEqualToString:@"real_req"]){
                self.requestUrl =obj[data_type];
                *stop = YES;
            }else if([data_type isEqualToString:@"local_assets_data"] ||[data_type isEqualToString:@"local_assets_file"]){
                NSString *fileName = obj[data_type];
                NSArray *arr = [fileName componentsSeparatedByString:@"."];
                if(arr.count == 2){
                    NSDictionary *dic = [[QWCommonMethod Instance] fetchBundleFile:arr[0] withType:arr[1]];
                    success([HXRequest new],dic,YES);
                }else{
                    failure([HXRequest new],@"");
                }
                return;
            }
        }
    }];
    
    [super startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSDictionary *result = [request responseJSONObject];
        
        BOOL isSuccess = YES;
        
        //校验格式
        if (self.verifyJSONFormat) {
            isSuccess = [[result objectForKey:@"code"] intValue] == 0;
        }
        
        success(request,result,isSuccess);
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@",self.description);
 
        failure(request,[self errorInfo:request]);
    }];
}


//无用
- (NSArray *)fetchDevConfigJson{
    NSArray * configArr= [[NSUserDefaults standardUserDefaults]objectForKey:NSUserDefaults_myjson_dev_config];
    if(!configArr){
        HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_myjson_dev_config argument:@{}];
        [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success1) {
            NSLog(@"");
            NSArray *dataArr = (NSArray *)result;
            [[NSUserDefaults standardUserDefaults] setObject:dataArr forKey:NSUserDefaults_myjson_dev_config];

        } failure:^(__kindof HXRequest *request, NSString *errorInfo) {

            NSString * filePath = [[NSBundle mainBundle] pathForResource:@"dev_config" ofType:@"json"];
            NSData *data = [NSData dataWithContentsOfFile:filePath];//获取指定路径的data文件
            NSArray *suggestsArr = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            [[NSUserDefaults standardUserDefaults] setObject:suggestsArr forKey:NSUserDefaults_myjson_dev_config];
           
            //错误
            NSLog(@"%@",errorInfo);
        }];
    }
    return configArr;
}



 
- (NSString *)description {
    //打印自己认为重要的信息
    return [NSString stringWithFormat:@"%@ \nstatusCode:%ld\nresponseJSONObject:\n%@",super.description,self.responseStatusCode,self.responseJSONObject];
}
 
- (NSString *)errorInfo:( YTKBaseRequest *)request{
    NSString * info = @"";
    if (request && request.error) {
        if (request.error.code==NSURLErrorNotConnectedToInternet) {
            info = @"请检查网络!";
        }else if (request.error.code==NSURLErrorTimedOut) {
            info = @"请求超时,请重试!";
        }else if (request.responseStatusCode == 401) {
            info = @"401";
        }else if (request.responseStatusCode == 403) {
            info = @"403";
        }else if (request.responseStatusCode == 404) {
            info = @"404";
        }else if (request.responseStatusCode == 500) {
            info = @"服务器报错,请稍后再试!";
        }else
        {
            info = @"网络异常,请重试!";
        }
    }
    return info;
}

//- (void)fetchDataGuideWayWithSuccess:(nullable HXRequestCompletionBlock)success
//                             failure:(nullable HXRequestCompletionFailureBlock)failure{
//    NSString *oldUrl = self.requestUrl;
//    NSString *oldArgument = self.requestArgument;
//
//    self.requestUrl =URL_myjson_dev_config;
//    self.requestArgument = @{};
//
//    [super startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
//        NSArray *result = [request responseJSONObject];
//        [[NSUserDefaults standardUserDefaults] setObject:result forKey:NSUserDefaults_myjson_dev_config];
//
//        self.requestUrl =oldUrl;
//        self.requestArgument = oldArgument;
//        [self startWithCompletionBlockWithSuccess:success failure:failure];
//
//
//    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
//        NSString * filePath = [[NSBundle mainBundle] pathForResource:@"dev_config" ofType:@"json"];
//        NSData *data = [NSData dataWithContentsOfFile:filePath];//获取指定路径的data文件
//        NSArray *suggestsArr = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
//        [[NSUserDefaults standardUserDefaults] setObject:suggestsArr forKey:NSUserDefaults_myjson_dev_config];
//
//        self.requestUrl =oldUrl;
//        self.requestArgument = oldArgument;
//        [self startWithCompletionBlockWithSuccess:success failure:failure];
//    }];
//}
 
@end
