//
//  XWVerticalScrollText.m
//  assistant
//
//  Created by qunai on 2024/1/5.
//

#import "XWVerticalScrollText.h"

@interface XWVerticalScrollText()
@property(assign, nonatomic)int titleIndex;
@property(assign, nonatomic)int index;
@property (nonatomic) NSMutableArray *titles;
/**第一个*/
@property(nonatomic)UIButton *firstBtn;
/**更多个*/
@property(nonatomic)UIButton *moreBtn;

@property (nonatomic, strong) UIView *scrollView;
@end

@implementation XWVerticalScrollText

-(instancetype)initWithFrame:(CGRect)frame{

    if (self = [super initWithFrame:frame]) {
        self.titleColor = [UIColor blackColor];
        self.titleFont =  [UIFont systemFontOfSize:14];
        self.clipsToBounds = YES;
        self.index = 1;
        self.firstBtn = [self btnframe:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)  titleColor:_titleColor action:@selector(clickBtn:)];
        self.firstBtn .tag = self.index;
        [self addSubview:self.firstBtn];
        [NSTimer scheduledTimerWithTimeInterval:4.0 target:self selector:@selector(nextAd) userInfo:nil repeats:YES];
        
    }
    
    return self;
}

- (void)setTitleArr:(NSArray *)titleArr{
    _titleArr = titleArr;
    
    NSMutableArray *MutableTitles = [NSMutableArray arrayWithArray:_titleArr];
    NSString *str = @"";
    self.titles = MutableTitles;
    [self.titles addObject:str]; //加一个空的,防止数组为空奔溃
    
    [self.firstBtn  setTitle:self.titles[0] forState:UIControlStateNormal];
}

#pragma mark - SEL Methods
-(void)nextAd{
    UIButton *firstBtn = [self viewWithTag:self.index];
    self.moreBtn = [self btnframe: CGRectMake(0, self.bounds.size.height,self.bounds.size.width, self.bounds.size.height)  titleColor:_titleColor action:@selector(clickBtn:)];
    self.moreBtn.tag = self.index + 1;
    if ([self.titles[self.titleIndex+1] isEqualToString:@""]) {
        self.titleIndex = -1;
        self.index = 0;
    }
    if (self.moreBtn.tag == self.titles.count) {
        
        self.moreBtn.tag = 1;
    }
    [self.moreBtn setTitle:self.titles[self.titleIndex+1] forState:UIControlStateNormal];
    [self addSubview:self.moreBtn];
    
    [UIView animateWithDuration:0.25 animations:^{
//        firstBtn.y = -self.bounds.size.height;
//        self.moreBtn.y = 0;
        
        CGRect firstBtnFrame = firstBtn.frame;
        firstBtnFrame.origin.y = -self.bounds.size.height;
        firstBtn.frame =firstBtnFrame;
        
        CGRect moreBtnFrame = self.moreBtn.frame;
        moreBtnFrame.origin.y = 0;
        self.moreBtn.frame =moreBtnFrame;
    } completion:^(BOOL finished) {
        [firstBtn removeFromSuperview];
        
    } ];
    self.index++;
    self.titleIndex++;
}
-(void)clickBtn:(UIButton *)btn{
    
    if (self.handlerTitleClickCallBack) {
        self.handlerTitleClickCallBack(btn.tag);
    }
}

#pragma mark - Custom Methods
- (UIButton *)btnframe:(CGRect)frame  titleColor:(UIColor *)titleColor action:(SEL)action{
    
    UIButton *btn = [[UIButton alloc]init];
    btn.frame = frame;
    btn.titleLabel.font = _titleFont;
      //靠左 不居中显示
    btn.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    btn.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;////文字多出部分 在右侧显示点点
    [btn setTitleColor:titleColor forState:UIControlStateNormal];
    [btn addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    
    return btn;
}
#pragma mark - Setter && Getter Methods
- (void)setTitleColor:(UIColor *)titleColor{
    _titleColor = titleColor;
     [self.firstBtn setTitleColor:titleColor forState:UIControlStateNormal];
     [self.moreBtn setTitleColor:titleColor forState:UIControlStateNormal];

}
- (void)setTitleFont:(UIFont *)titleFont{

    _titleFont = titleFont;
   self.firstBtn.titleLabel.font = titleFont;
   self.moreBtn.titleLabel.font = titleFont;
    
}
@end
