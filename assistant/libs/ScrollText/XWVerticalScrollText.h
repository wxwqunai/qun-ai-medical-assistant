//
//  XWVerticalScrollText.h
//  assistant
//
//  Created by qunai on 2024/1/5.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XWVerticalScrollText : UIView
/** 标题的字体 默认为14 */
@property(nonatomic)UIFont *titleFont;
/**标题的颜色 默认红色*/
@property(nonatomic)UIColor *titleColor;
/**存放titles的数组 和初始化的数组一致*/
@property(nonatomic)NSArray *titleArr;
//回调
@property(nonatomic,copy)void(^handlerTitleClickCallBack)(NSInteger index);

@end

NS_ASSUME_NONNULL_END
