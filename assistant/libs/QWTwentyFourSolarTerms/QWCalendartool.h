#ifndef CALENDARTOOL_H
#define CALENDARTOOL_H


class CalendarTool
{
private:
    CalendarTool();

public:
    //  must be same with in calendar_const.h
    enum {
        VERNAL_EQUINOX      = 0,    // 春分
        CLEAR_AND_BRIGHT    = 1,    // 清明
        GRAIN_RAIN          = 2,    // 谷雨
        SUMMER_BEGINS       = 3,    // 立夏
        GRAIN_BUDS          = 4,    // 小满
        GRAIN_IN_EAR        = 5,    // 芒种
        SUMMER_SOLSTICE     = 6,    // 夏至
        SLIGHT_HEAT         = 7,    // 小暑
        GREAT_HEAT          = 8,    // 大暑
        AUTUMN_BEGINS       = 9,    // 立秋
        STOPPING_THE_HEAT   = 10,   // 处暑
        WHITE_DEWS          = 11,   // 白露
        AUTUMN_EQUINOX      = 12,   // 秋分
        COLD_DEWS           = 13,   // 寒露
        HOAR_FROST_FALLS    = 14,   // 霜降
        WINTER_BEGINS       = 15,   // 立冬
        LIGHT_SNOW          = 16,   // 小雪
        HEAVY_SNOW          = 17,   // 大雪
        WINTER_SOLSTICE     = 18,   // 冬至
        SLIGHT_COLD         = 19,   // 小寒
        GREAT_COLD          = 20,   // 大寒
        SPRING_BEGINS       = 21,   // 立春
        THE_RAINS           = 22,   // 雨水
        INSECTS_AWAKEN      = 23   // 惊蛰
    };


    static double   calculateJieqiTimeById(int year, int jieqiId);
    static void     jdTimeToString(double jdTime, char* buffer, int bufferSize);
    static void     calculateAllJieqiTime(int year, double *jieqiTime, int start); // jieTime is 25 elements of array.
    static const char*    jieqiNameById(int jieqiId); // don't need to free the returned buffer.
};

#endif // CALENDARTOOL_H
