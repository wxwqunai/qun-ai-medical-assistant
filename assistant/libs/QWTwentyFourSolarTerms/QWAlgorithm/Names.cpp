﻿#include "Names.h"
#include "calendar_const.h"

#pragma execution_character_set("utf-8")

TCHAR *nameOfMonth[MONTHES_FOR_YEAR] = { "January", "February", "March", "April", "May",
                                        "June", "July", "August", "September", "October",
                                        "November", "December" };
TCHAR *nameOfWeek[DAYS_FOR_WEEK] = { "星期日","星期一","星期二","星期三","星期四","星期五","星期六" };

TCHAR *nameOfStems[HEAVENLY_STEMS] = { "甲","乙","丙","丁","戊","己","庚","辛","壬","癸" };
TCHAR *nameOfBranches[EARTHLY_BRANCHES] = { "子","丑","寅","卯","辰","巳","午","未","申","酉","戌","亥" };
TCHAR *nameOfShengXiao[CHINESE_SHENGXIAO] = { "鼠","牛","虎","兔","龙","蛇","马","羊","猴","鸡","狗","猪" };
TCHAR *nameOfChnDay[MAX_CHINESE_MONTH_DAYS] = { "初一","初二","初三","初四","初五","初六","初七","初八","初九","初十",
                                              "十一","十二","十三","十四","十五","十六","十七","十八","十九","二十",
                                              "廿一","廿二","廿三","廿四","廿五","廿六","廿七","廿八","廿九","三十"
                                            };
TCHAR *nameOfChnMonth[MONTHES_FOR_YEAR] = { "正","二","三","四","五","六","七","八","九","十","十一","腊" };
TCHAR *nameOfJieQi[SOLAR_TERMS_COUNT] = { "春分", "清明", "谷雨", "立夏", "小满", "芒种", "夏至", "小暑",
                                        "大暑", "立秋", "处暑", "白露", "秋分", "寒露", "霜降", "立冬",
                                        "小雪", "大雪", "冬至", "小寒", "大寒", "立春", "雨水", "惊蛰" };

