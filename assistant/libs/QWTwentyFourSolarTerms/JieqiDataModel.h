//
//  JieqiData.h
//  qwm
//
//  Created by kevin on 2023/3/24.
//

#import <Foundation/Foundation.h>

@interface JieqiDataModel : NSObject
@property (strong, nonatomic) NSString* jieqiName;
@property (strong, nonatomic) NSString* jieqiTime;
@property (strong, nonatomic) NSDate* jieqiDate;
@property (assign, nonatomic) BOOL currentJieqi;
@property (assign, nonatomic) CGFloat percent;
@property (assign, nonatomic) NSInteger numOfAllJieqi;
@end
