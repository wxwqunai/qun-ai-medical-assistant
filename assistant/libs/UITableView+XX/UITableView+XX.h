//
//  UITableView+XX.h
//  Jumploo2
//
//  Created by 许洵 on 15/6/26.
//  Copyright (c) 2015年 吴荆东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJRefresh.h"
@interface UITableView (XX)

/**
 上拉加载
 */
- (void)addLoadMoreFooterWithTarget:(id)target action:(SEL)action;

/**
 下拉刷新
 */
- (void)addRefreshHeaderWithTarget:(id)target action:(SEL)action;

/**
 首次进controller自动刷新
 */
- (void)beginRefresh;

/**
 设置下拉刷新的提示文字
 */
-(void) setHeadRefreshTip:(NSString*) pull2Refresh release2Refresh:(NSString*) release2Refresh refreshing:(NSString*) refreshing;

/**
 结束动画
 */
- (void)endWRefreshing;

- (void)removeHeader;
- (void)removeFooter;

- (void)addFooter;

#pragma mark - 刷新加载(消除“点击加载更多”)
//-(void)MJRefresh:(UITableView *)table begin:(NSInteger)begin count:(NSInteger)count mutableArray:(NSMutableArray *)arr;

#pragma mark -判断删除/添加上拉刷新
- (void)judgeFooterState:(BOOL)isShouldRemove;

@end
