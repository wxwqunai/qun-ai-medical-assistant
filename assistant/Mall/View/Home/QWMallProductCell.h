//
//  QWMallProductCell.h
//  assistant
//
//  Created by qunai on 2024/3/28.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN
@interface QWProjectStateView : UIView
@property (nonatomic, strong) UILabel *stateLabel;
@end

@interface QWProjectPriceView : UIView
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *sellNumLabel;
@end

@interface QWProjectTypeView : UIView
@property (nonatomic, strong) NSArray *dateArr;
@end

@interface QWMallProductCell : QWBaseTableViewCell
@property (nonatomic, strong) UIImageView *picImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) QWProjectStateView *stateView;

@property (nonatomic, strong) QWProjectPriceView *priceView;

@property (nonatomic, strong) QWProjectTypeView *typeView;


@property (nonatomic, strong) NSDictionary *dataDic;
@end

NS_ASSUME_NONNULL_END
