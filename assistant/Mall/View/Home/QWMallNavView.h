//
//  QWMallNavView.h
//  assistant
//
//  Created by qunai on 2024/3/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWMallNavView : UIView
@property (nonatomic, strong) UIView *navBgView;
@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic,strong) UIView *rightView;
@property (nonatomic,strong) UIButton *shopCart;


@property (nonatomic, copy) void (^shopCartBlock)(void);

@end

NS_ASSUME_NONNULL_END
