//
//  QWMallNavView.m
//  assistant
//
//  Created by qunai on 2024/3/27.
//

#import "QWMallNavView.h"

@implementation QWMallNavView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = Color_Main_Green;
        
        [self navBgView];
        [self titleLabel];
        
        [self rightView];
        [self shopCart];
    }
    return self;
}

- (UIView *)navBgView{
    if(!_navBgView){
        _navBgView = [[UIView alloc]init];
        _navBgView.backgroundColor = [UIColor clearColor];
        [self addSubview:_navBgView];
        
        [_navBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset([UIDevice statusBarHeight]);
            make.bottom.equalTo(self);
            make.left.equalTo(self);
            make.right.equalTo(self);
        }];
    }
    return _navBgView;
}

- (UILabel *)titleLabel{
    if(!_titleLabel){
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.text = @"商城";
        _titleLabel.font = [UIFont boldSystemFontOfSize:17];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.navBgView addSubview:_titleLabel];
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.navBgView);
            make.top.equalTo(self.navBgView);
            make.bottom.equalTo(self.navBgView);
            make.width.equalTo(self.navBgView.mas_width).multipliedBy(1.0/3.0);
        }];
    }
    return _titleLabel;
}

#pragma mark - 右边操作View
- (UIView *)rightView{
    if(!_rightView){
        _rightView = [[UIView alloc]init];
        [self addSubview:_rightView];
        
        [_rightView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo([UIDevice safeDistanceTop]);
            make.right.equalTo(self).offset(-14.0);
            make.bottom.equalTo(self);
        }];
    }
    return _rightView;
}

#pragma mark - 消息
- (UIButton *)shopCart{
    if(!_shopCart){
        _shopCart = [UIButton buttonWithType:UIButtonTypeCustom];
        [_shopCart setImage:[UIImage imageNamed:@"mall_shopCart_empty.png"] forState:UIControlStateNormal];
        [_shopCart addTarget:self action:@selector(shopCartClick) forControlEvents:UIControlEventTouchUpInside];
        [self.rightView addSubview:_shopCart];
        
        [_shopCart mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.rightView);
            make.left.equalTo(self.rightView);
            make.top.equalTo(self.rightView);
            make.bottom.equalTo(self.rightView);
            make.width.equalTo(_shopCart.mas_height);
           
        }];
    }
    return _shopCart;
}

-(void)shopCartClick{
    if(self.shopCartBlock)self.shopCartBlock();
}


@end
