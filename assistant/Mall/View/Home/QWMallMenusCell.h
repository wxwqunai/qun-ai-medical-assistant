//
//  QWMallMenusCell.h
//  assistant
//
//  Created by qunai on 2024/3/27.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWMallMenusCell : QWBaseTableViewCell
@property (nonatomic, copy) void (^menusSelectBlock)(NSInteger itemNum);
@end


@interface QWmenuItem : UIView
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UIButton *button;
@end

NS_ASSUME_NONNULL_END
