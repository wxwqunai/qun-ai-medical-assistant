//
//  QWMallProductCell.m
//  assistant
//
//  Created by qunai on 2024/3/28.
//

#import "QWMallProductCell.h"
@interface QWMallProductCell()
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIView *contentRightView;
@end

@implementation QWMallProductCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDataDic:(NSDictionary *)dataDic{
    _dataDic = dataDic;
    NSString *pic = _dataDic[@"pic"];
    NSString *name = _dataDic[@"productName"];
    NSString *state = _dataDic[@"state"];
    NSString *price = _dataDic[@"price"];
    NSString *sellNum = _dataDic[@"sellNum"];
    NSArray *typeArr = _dataDic[@"type"];
    
    
    self.picImageView.image = [UIImage imageNamed:pic];
    self.nameLabel.text = name;
    self.stateView.stateLabel.text = state;
    self.priceView.priceLabel.text = price;
    self.priceView.sellNumLabel.text = sellNum;
    
    self.typeView.dateArr = typeArr;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self bgView];
        [self picImageView];
        
        [self contentRightView];
        [self nameLabel];
        [self stateView];
        
        [self typeView];
        [self priceView];
    }
    return self;
}
- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_bgView];
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(5.0);
            make.bottom.equalTo(self.contentView).offset(-5.0);
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
        }];

    }
    return _bgView;
}

#pragma mark - 商品图片
- (UIImageView *)picImageView{
    if(!_picImageView){
        _picImageView = [[UIImageView alloc]init];
        _picImageView.layer.cornerRadius = 8.0;
        _picImageView.layer.masksToBounds = YES;
        [self.bgView addSubview:_picImageView];
        [_picImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView).offset(10.0);
            make.top.equalTo(self.bgView);
            make.bottom.equalTo(self.bgView);
            make.height.mas_equalTo(120.0);
            make.width.equalTo(_picImageView.mas_height);
        }];
    }
    return _picImageView;
}

- (UIView *)contentRightView{
    if(!_contentRightView){
        _contentRightView = [[UIView alloc]init];
        [self.bgView addSubview:_contentRightView];
        [_contentRightView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.picImageView.mas_right).offset(5.0);
            make.right.equalTo(self.bgView).offset(-10.0);
            make.top.equalTo(self.bgView);
            make.bottom.equalTo(self.bgView);
        }];
    }
    return _contentRightView;
}

#pragma mark - 商品名称
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.numberOfLines = 2;
        _nameLabel.font = [UIFont systemFontOfSize:16 weight:(UIFontWeightMedium)];
        [self.contentRightView addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentRightView);
            make.top.equalTo(self.contentRightView);
            make.right.equalTo(self.contentRightView);
        }];
    }
    return _nameLabel;
}

#pragma mark - 商品状态
- (QWProjectStateView *)stateView{
    if(!_stateView){
        _stateView = [[QWProjectStateView alloc]init];
        [self.contentRightView addSubview:_stateView];
        [_stateView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentRightView);
            make.right.equalTo(self.contentRightView);
            make.top.equalTo(self.nameLabel.mas_bottom);
            make.height.mas_equalTo(20);
        }];
    }
    return _stateView;
}

#pragma mark - 商品价格
- (QWProjectPriceView *)priceView{
    if(!_priceView){
        _priceView = [[QWProjectPriceView alloc]init];
        [self.contentRightView addSubview:_priceView];
        [_priceView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentRightView);
            make.right.equalTo(self.contentRightView);
            make.bottom.equalTo(self.typeView.mas_top);
            make.height.mas_equalTo(30);
        }];
    }
    return _priceView;
}

#pragma mark - 商品状态
- (QWProjectTypeView *)typeView{
    if(!_typeView){
        _typeView = [[QWProjectTypeView alloc]init];
        [self.contentRightView addSubview:_typeView];
        [_typeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentRightView);
            make.right.equalTo(self.contentRightView);
            make.bottom.equalTo(self.contentRightView);
        }];
    }
    return _typeView;
}
@end


#pragma mark - 商品状态 - view
@implementation QWProjectStateView
- (instancetype)init{
    self = [super init];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        [self stateLabel];
    }
    return self;
}

- (UILabel *)stateLabel{
    if(!_stateLabel){
        _stateLabel = [[UILabel alloc]init];
        _stateLabel.numberOfLines = 1;
        _stateLabel.textColor = [UIColor colorFromHexString:@"#E7B597"];
        _stateLabel.font = [UIFont systemFontOfSize:13];
        [self addSubview:_stateLabel];
        [_stateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.centerY.equalTo(self);
        }];
    }
    return _stateLabel;
}
@end


#pragma mark - 商品价格、购买量 - view
@implementation QWProjectPriceView
- (instancetype)init{
    self = [super init];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        [self sellNumLabel];
        [self priceLabel];
    }
    return self;
}
- (UILabel *)priceLabel{
    if(!_priceLabel){
        _priceLabel = [[UILabel alloc]init];
        _priceLabel.numberOfLines = 1;
        _priceLabel.textColor = [UIColor colorFromHexString:@"#EB5D2A"];
        _priceLabel.font = [UIFont systemFontOfSize:18 weight:(UIFontWeightMedium)];
        [self addSubview:_priceLabel];
        [_priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.centerY.equalTo(self);
            make.right.equalTo(self.sellNumLabel.mas_left).offset(-4);
        }];
    }
    return _priceLabel;
}

- (UILabel *)sellNumLabel{
    if(!_sellNumLabel){
        _sellNumLabel = [[UILabel alloc]init];
        _sellNumLabel.numberOfLines = 1;
        _sellNumLabel.textColor = [UIColor colorFromHexString:@"#C5C5C5"];
        _sellNumLabel.font = [UIFont systemFontOfSize:13];
        [self addSubview:_sellNumLabel];
        [_sellNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self);
            make.centerY.equalTo(self);
        }];
    }
    return _sellNumLabel;
}

@end

#pragma mark - 商品状态 - view
@implementation QWProjectTypeView
- (instancetype)init{
    self = [super init];
    if(self){
        self.backgroundColor = [UIColor clearColor];
       
    }
    return self;
}

- (void)setDateArr:(NSArray *)dateArr{
    _dateArr = dateArr;
    
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    
    UIView __block *lastView;
    [_dateArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *text = (NSString *)obj;
        
        UIView *bgView = [[UIView alloc]init];
        bgView.layer.borderColor = [UIColor colorFromHexString:@"#F18F6B"].CGColor;
        bgView.layer.borderWidth = 0.5;
        bgView.layer.cornerRadius = 2.0;
        bgView.layer.masksToBounds = YES;
        [self addSubview:bgView];
        
        
        UILabel *label = [[UILabel alloc]init];
        label.text = text;
        label.font = [UIFont systemFontOfSize:11];
        label.textColor = [UIColor colorFromHexString:@"#F18F6B"];
        [bgView addSubview:label];
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(bgView).offset(4.0);
            make.right.equalTo(bgView).offset(-4.0);
            make.top.equalTo(bgView).offset(3.0);
            make.bottom.equalTo(bgView).offset(-3.0);
        }];
        
        if(lastView){
            [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(lastView.mas_right).offset(4.0);
                make.top.equalTo(self);
                make.bottom.equalTo(self);
            }];
        }else{
            [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self);
                make.top.equalTo(self);
                make.bottom.equalTo(self);
            }];
        }
       
        lastView = bgView;
    }];
    
}

@end
