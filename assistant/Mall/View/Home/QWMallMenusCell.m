//
//  QWMallMenusCell.m
//  assistant
//
//  Created by qunai on 2024/3/27.
//

#import "QWMallMenusCell.h"

@implementation QWMallMenusCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self loadData];
        
    }
    return self;
}
- (void)loadData{
    NSArray *dataArr = @[
        @{@"name":@"学术会议",@"pic":@"mall_meeting"},
        @{@"name":@"健康科普",@"pic":@"mall_healthy"},
        @{@"name":@"附近商超",@"pic":@"mall_supermarket.png"},
    ];

    NSMutableArray *tolAry = [NSMutableArray new];
    for (int i = 0; i< dataArr.count; i ++) {
        NSDictionary *dic = dataArr[i];
        QWmenuItem *itemView =[[QWmenuItem alloc]init];
        itemView.imageView.image = [UIImage imageNamed:dic[@"pic"]];
        itemView.nameLabel.text = dic[@"name"];
        itemView.button.tag = 10000+i;
        [itemView.button addTarget:self action:@selector(bottonOnClick:) forControlEvents:UIControlEventTouchUpInside];
        [tolAry addObject:itemView];
    }
    
    UIView *allView = [[QWCommonMethod Instance] mas_HorizontalBisection:tolAry withNum:3 withFixedSpacing:5 leadSpacing:14.0 tailSpacing:14.0];
    [self.contentView addSubview:allView];
    
    [allView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
}

- (void)bottonOnClick:(UIButton *)sender{
    if(self.menusSelectBlock)self.menusSelectBlock(sender.tag - 10000);
}

@end


@implementation QWmenuItem
- (instancetype)init{
    self = [super init];
    if(self){
        //图片
        [self imageView];
        //文本
        [self nameLabel];
        //按钮
        [self button];
    }
    return self;
}

- (UIImageView *)imageView{
    if(!_imageView){
        _imageView = [[UIImageView alloc]init];

        _imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_imageView];
        [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.equalTo(self).offset(5);
            make.width.mas_equalTo(40);
            make.height.mas_equalTo(40);
        }];
    }
    return _imageView;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.font = [UIFont systemFontOfSize:15];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.top.equalTo(self.imageView.mas_bottom);
            make.height.mas_equalTo(25);
            make.bottom.equalTo(self);
        }];
    }
    return _nameLabel;
}

- (UIButton *)button{
    if(!_button){
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:_button];
        [_button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return _button;
}

@end
