//
//  QWMallMeetingCell.h
//  assistant
//
//  Created by qunai on 2024/3/28.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWMallMeetingCell : QWBaseTableViewCell
@property (nonatomic, strong) UIImageView *picImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *dateLabel;
@end

NS_ASSUME_NONNULL_END
