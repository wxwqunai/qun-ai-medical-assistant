//
//  QWMallHealthyCell.m
//  assistant
//
//  Created by qunai on 2024/3/28.
//

#import "QWMallHealthyCell.h"
@interface QWMallHealthyCell()
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIView *contentRightView;
@end
@implementation QWMallHealthyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self bgView];
        
        [self picImageView];
        [self playTimeView];
        
        [self contentRightView];
        [self nameLabel];
        [self dateLabel];
  
    }
    return self;
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 8.0;
        _bgView.layer.masksToBounds = YES;
        [self.contentView addSubview:_bgView];
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(5.0);
            make.bottom.equalTo(self.contentView).offset(-5.0);
            make.left.equalTo(self.contentView).offset(10.0);
            make.right.equalTo(self.contentView).offset(-10.0);
        }];

    }
    return _bgView;
}

#pragma mark - 图片
- (UIImageView *)picImageView{
    if(!_picImageView){
        _picImageView = [[UIImageView alloc]init];
        _picImageView.backgroundColor = Color_Main_Green;
        _picImageView.layer.cornerRadius = 8.0;
        _picImageView.layer.masksToBounds = YES;
        [self.bgView addSubview:_picImageView];
        [_picImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView);
            make.top.equalTo(self.bgView);
            make.bottom.equalTo(self.bgView);
            make.height.mas_equalTo(90.0);
            make.width.equalTo(_picImageView.mas_height).multipliedBy(1.5);
        }];
    }
    return _picImageView;
}

#pragma mark - 时间
- (QWMallPlayTimeView *)playTimeView{
    if(!_playTimeView){
        _playTimeView = [[QWMallPlayTimeView alloc]init];
        _playTimeView.hidden = YES;
        [self.picImageView addSubview:_playTimeView];
        
        [_playTimeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.picImageView);
            make.bottom.equalTo(self.picImageView);
        }];
    }
    return _playTimeView;
}

- (UIView *)contentRightView{
    if(!_contentRightView){
        _contentRightView = [[UIView alloc]init];
        [self.bgView addSubview:_contentRightView];
        [_contentRightView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.picImageView.mas_right).offset(5.0);
            make.right.equalTo(self.bgView);
            make.top.equalTo(self.bgView);
            make.bottom.equalTo(self.bgView);
        }];
    }
    return _contentRightView;
}

#pragma mark - 名称
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.numberOfLines = 2;
        _nameLabel.font = [UIFont systemFontOfSize:16 weight:(UIFontWeightMedium)];
        [self.contentRightView addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentRightView);
            make.top.equalTo(self.contentRightView);
            make.right.equalTo(self.contentRightView);
        }];
    }
    return _nameLabel;
}

#pragma mark - 时间
- (UILabel *)dateLabel{
    if(!_dateLabel){
        _dateLabel = [[UILabel alloc]init];
        _dateLabel.font = [UIFont systemFontOfSize:14];
        _dateLabel.textColor = [UIColor lightGrayColor];
        [self.contentRightView addSubview:_dateLabel];
        [_dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentRightView);
            make.bottom.equalTo(self.contentRightView);
        }];
    }
    return _dateLabel;
}

#pragma mark - 赋值
- (void)setDateDic:(NSDictionary *)dateDic{
    _dateDic = dateDic;
    self.picImageView.image = [UIImage imageNamed:_dateDic[@"pic"]];
    self.nameLabel.text =_dateDic[@"name"];
    self.dateLabel.text =_dateDic[@"date"];
    
    NSString *type = _dateDic[@"type"];
    self.playTimeView.hidden = ![type isEqualToString:@"video"];
    self.playTimeView.timeLabel.text = _dateDic[@"videoTime"];
}

@end

@implementation QWMallPlayTimeView
- (instancetype)init{
    self = [super init];
    if(self){
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
        [self iconImageView];
        [self timeLabel];
    }
    return self;
}

- (UIImageView *)iconImageView{
    if(!_iconImageView){
        _iconImageView = [[UIImageView alloc]init];
        _iconImageView.image = [UIImage imageNamed:@"mall_play.png"];
        [self addSubview:_iconImageView];
        [_iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(2.0);
            make.centerY.equalTo(self);
            make.height.mas_equalTo(8.0);
        }];
    }
    return _iconImageView;
}

- (UILabel *)timeLabel{
    if(!_timeLabel){
        _timeLabel = [[UILabel alloc]init];
        _timeLabel.font = [UIFont systemFontOfSize:12];
        _timeLabel.textColor = [UIColor whiteColor];
        [self addSubview:_timeLabel];
        [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.iconImageView.mas_right).offset(2.0);
            make.right.equalTo(self).offset(-2.0);
            make.top.equalTo(self).offset(2.0);
            make.bottom.equalTo(self).offset(-2.0);
        }];
    }
    return _timeLabel;
}

@end
