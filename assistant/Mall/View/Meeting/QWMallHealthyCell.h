//
//  QWMallHealthyCell.h
//  assistant
//
//  Created by qunai on 2024/3/28.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWMallPlayTimeView : UIView
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *timeLabel;
@end

@interface QWMallHealthyCell : QWBaseTableViewCell
@property (nonatomic, strong) UIImageView *picImageView;
@property (nonatomic, strong) QWMallPlayTimeView *playTimeView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *dateLabel;

@property (nonatomic, strong) NSDictionary *dateDic;
@end

NS_ASSUME_NONNULL_END
