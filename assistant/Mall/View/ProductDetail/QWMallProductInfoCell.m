//
//  QWMallProductInfoCell.m
//  assistant
//
//  Created by qunai on 2024/4/1.
//

#import "QWMallProductInfoCell.h"

@implementation QWMallProductInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self bgView];
        
        [self priceLabel];
        [self sellNumLabel];
        
        [self nameLabel];
        [self stateLabel];
    }
    return self;
}
- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_bgView];
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(14.0);
            make.right.equalTo(self.contentView).offset(-14.0);
            make.top.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
        }];
    }
    return _bgView;
}

- (UILabel *)priceLabel{
    if(!_priceLabel){
        _priceLabel = [[UILabel alloc]init];
        _priceLabel.numberOfLines = 1;
        _priceLabel.textColor = [UIColor colorFromHexString:@"#EB5D2A"];
        _priceLabel.font = [UIFont systemFontOfSize:18 weight:(UIFontWeightMedium)];
        [self.bgView addSubview:_priceLabel];
        [_priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView);
            make.top.equalTo(self.bgView).offset(10.0);
        }];
    }
    return _priceLabel;
}

- (UILabel *)sellNumLabel{
    if(!_sellNumLabel){
        _sellNumLabel = [[UILabel alloc]init];
        _sellNumLabel.numberOfLines = 1;
        _sellNumLabel.textColor = [UIColor colorFromHexString:@"#C5C5C5"];
        _sellNumLabel.font = [UIFont systemFontOfSize:13];
        [self.bgView  addSubview:_sellNumLabel];
        [_sellNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.bgView);
            make.centerY.equalTo(self.priceLabel);
        }];
    }
    return _sellNumLabel;
}

#pragma mark - 商品名称
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.numberOfLines = 2;
        _nameLabel.font = [UIFont systemFontOfSize:16 weight:(UIFontWeightMedium)];
        [self.bgView addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView);
            make.top.equalTo(self.priceLabel.mas_bottom).offset(10.0);
            make.right.equalTo(self.bgView);
        }];
    }
    return _nameLabel;
}

- (UILabel *)stateLabel{
    if(!_stateLabel){
        _stateLabel = [[UILabel alloc]init];
        _stateLabel.numberOfLines = 1;
        _stateLabel.textColor = [UIColor colorFromHexString:@"#E7B597"];
        _stateLabel.font = [UIFont systemFontOfSize:13];
        [self.bgView addSubview:_stateLabel];
        [_stateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView);
            make.right.equalTo(self.bgView);
            make.top.equalTo(self.nameLabel.mas_bottom).offset(4.0);
            make.bottom.equalTo(self.bgView).offset(-10);
        }];
    }
    return _stateLabel;
}

@end
