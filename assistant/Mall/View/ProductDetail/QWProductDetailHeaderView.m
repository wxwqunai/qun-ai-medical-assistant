//
//  QWProductDetailHeaderView.m
//  assistant
//
//  Created by qunai on 2024/4/1.
//

#import "QWProductDetailHeaderView.h"
@interface QWProductDetailHeaderView()
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UIImageView *leftLine;
@property (nonatomic, strong) UIImageView *rightLine;
@end
@implementation QWProductDetailHeaderView
- (instancetype)initWithReuseIdentifier:(nullable NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if(self){
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self nameLabel];
        [self leftLine];
        [self rightLine];
    }
    return self;
}
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.text = @"宝贝详情";
        _nameLabel.font = [UIFont systemFontOfSize:14 weight:(UIFontWeightLight)];
        [self addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.equalTo(self);
            make.bottom.equalTo(self);
        }];
    }
    return _nameLabel;
}

- (UIImageView *)leftLine{
    if(!_leftLine){
        _leftLine = [[UIImageView alloc]init];
        _leftLine.backgroundColor = [UIColor lightGrayColor];
        [self addSubview:_leftLine];
        
        [_leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.nameLabel.mas_left).offset(-4.0);
            make.centerY.equalTo(self.nameLabel);
            make.width.equalTo(self).multipliedBy(0.15);
            make.height.mas_equalTo(0.5);
        }];
    }
    return _leftLine;
}

- (UIImageView *)rightLine{
    if(!_rightLine){
        _rightLine = [[UIImageView alloc]init];
        _rightLine.backgroundColor = [UIColor lightGrayColor];
        [self addSubview:_rightLine];
        
        [_rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.nameLabel.mas_right).offset(4.0);
            make.centerY.equalTo(self.nameLabel);
            make.width.equalTo(self).multipliedBy(0.15);
            make.height.mas_equalTo(0.5);
        }];
    }
    return _rightLine;
}

@end
