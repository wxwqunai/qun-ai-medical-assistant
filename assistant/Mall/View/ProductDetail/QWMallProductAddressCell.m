//
//  QWMallProductAddressCell.m
//  assistant
//
//  Created by qunai on 2024/4/1.
//

#import "QWMallProductAddressCell.h"
@interface QWMallProductAddressCell()
@property (nonatomic, strong ) UIView *adBgView;
@property (nonatomic, strong ) UILabel *adNameLabel;
@property (nonatomic, strong ) UILabel *freightLabel;
@property (nonatomic, strong ) UILabel *addressLabel;
@property (nonatomic, strong ) UIImageView *addressNext;

@property (nonatomic, strong ) UIView *tipBgView;
@property (nonatomic, strong ) UILabel *tipNameLabel;
@property (nonatomic, strong ) UILabel *tipLabel;
@property (nonatomic, strong ) UILabel *tipDesLabel;
@end
@implementation QWMallProductAddressCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self adBgView];
        [self adNameLabel];
        [self freightLabel];
        [self addressLabel];
        [self addressNext];
        
        [self tipBgView];
        [self tipNameLabel];
        [self tipLabel];
        [self tipDesLabel];
    }
    return self;
}

- (UIView *)adBgView{
    if(!_adBgView){
        _adBgView = [[UIView alloc]init];
        _adBgView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_adBgView];
        
        [_adBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(14.0);
            make.right.equalTo(self.contentView).offset(-14.0);
            make.top.equalTo(self.contentView).offset(10.0);
        }];
    }
    return _adBgView;
}

- (UILabel *)adNameLabel{
    if(!_adNameLabel){
        _adNameLabel = [[UILabel alloc]init];
        _adNameLabel.text = @"发货";
        _adNameLabel.textColor = [UIColor blackColor];
        _adNameLabel.font = [UIFont systemFontOfSize:15.0 weight:UIFontWeightLight];
        [self.adBgView addSubview:_adNameLabel];
        
        [_adNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.adBgView);
            make.top.equalTo(self.adBgView);
            make.width.mas_equalTo(50);
        }];
    }
    return _adNameLabel;
}

//运费
- (UILabel *)freightLabel{
    if(!_freightLabel){
        _freightLabel = [[UILabel alloc]init];
        _freightLabel.text = @"运费：免运费";
        _freightLabel.textColor = [UIColor blackColor];
        _freightLabel.font = [UIFont systemFontOfSize:15.0 weight:UIFontWeightLight];
        [self.adBgView addSubview:_freightLabel];
        
        [_freightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.adBgView);
            make.top.equalTo(self.adNameLabel.mas_top);
            make.left.equalTo(self.adNameLabel.mas_right);
        }];
    }
    return _freightLabel;
}
//地址
- (UILabel *)addressLabel{
    if(!_addressLabel){
        _addressLabel = [[UILabel alloc]init];
        _addressLabel.text = @"暂未设置收获地址，点击前往设置";
        _addressLabel.textColor = [UIColor grayColor];
        _addressLabel.font = [UIFont systemFontOfSize:15.0 weight:UIFontWeightLight];
        [self.adBgView addSubview:_addressLabel];
        
        [_addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.freightLabel);
            make.top.equalTo(self.freightLabel.mas_bottom).offset(4.0);
            make.bottom.equalTo(self.adBgView);
        }];
    }
    return _addressLabel;
}
- (UIImageView *)addressNext{
    if(!_addressNext){
        _addressNext = [[UIImageView alloc]init];
        _addressNext.image = [UIImage imageNamed:@"next_arrow"];
        [self.adBgView addSubview:_addressNext];
        [_addressNext mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.adBgView);
            make.centerY.equalTo(self.addressLabel);
            make.left.equalTo(self.addressLabel.mas_right);
        }];
    }
    return _addressNext;
}


- (UIView *)tipBgView{
    if(!_tipBgView){
        _tipBgView = [[UIView alloc]init];
        _tipBgView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_tipBgView];
        
        [_tipBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.adBgView);
            make.right.equalTo(self.adBgView);
            make.top.equalTo(self.adBgView.mas_bottom).offset(10.0);
            make.bottom.equalTo(self.contentView).offset(-10.0);
        }];
    }
    return _tipBgView;
}
- (UILabel *)tipNameLabel{
    if(!_tipNameLabel){
        _tipNameLabel = [[UILabel alloc]init];
        _tipNameLabel.text = @"保障";
        _tipNameLabel.textColor = [UIColor blackColor];
        _tipNameLabel.font = [UIFont systemFontOfSize:15.0 weight:UIFontWeightLight];
        [self.tipBgView addSubview:_tipNameLabel];
        
        [_tipNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.tipBgView);
            make.top.equalTo(self.tipBgView);
            make.width.mas_equalTo(50);
        }];
    }
    return _tipNameLabel;
}

- (UILabel *)tipLabel{
    if(!_tipLabel){
        _tipLabel = [[UILabel alloc]init];
        _tipLabel.text = @"支持申请退款";
        _tipLabel.textColor = [UIColor blackColor];
        _tipLabel.font = [UIFont systemFontOfSize:15.0 weight:UIFontWeightLight];
        [self.tipBgView addSubview:_tipLabel];
        
        [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.tipBgView);
            make.top.equalTo(self.tipNameLabel.mas_top);
            make.left.equalTo(self.tipNameLabel.mas_right);
        }];
    }
    return _tipLabel;
}

- (UILabel *)tipDesLabel{
    if(!_tipDesLabel){
        _tipDesLabel = [[UILabel alloc]init];
        _tipDesLabel.text = @" ";
        _tipDesLabel.textColor = [UIColor grayColor];
        _tipDesLabel.font = [UIFont systemFontOfSize:15.0 weight:UIFontWeightLight];
        [self.tipBgView addSubview:_tipDesLabel];
        
        [_tipDesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.tipLabel);
            make.top.equalTo(self.tipLabel.mas_bottom).offset(4.0);
            make.bottom.equalTo(self.tipBgView);
        }];
    }
    return _tipDesLabel;
}

@end
