//
//  QWMallProductlBanner.h
//  assistant
//
//  Created by qunai on 2024/4/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWMallProductlBanner : UIView
@property (nonatomic,strong) NSArray *showArr;

@property (nonatomic,copy) void (^bannerClickBlock)(NSInteger index);
@end

NS_ASSUME_NONNULL_END
