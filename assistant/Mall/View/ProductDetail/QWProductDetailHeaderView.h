//
//  QWProductDetailHeaderView.h
//  assistant
//
//  Created by qunai on 2024/4/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWProductDetailHeaderView : UITableViewHeaderFooterView

@end

NS_ASSUME_NONNULL_END
