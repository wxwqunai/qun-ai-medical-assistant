//
//  QWMallProductlBanner.m
//  assistant
//
//  Created by qunai on 2024/4/1.
//

#import "QWMallProductlBanner.h"

@interface QWMallProductlBanner()<UIScrollViewDelegate>
@property (nonatomic,strong) UIScrollView *bannerScrollView;
@property (nonatomic,strong) NSMutableArray *showBannerImageArr;
@property (nonatomic,assign) NSInteger timerPage;
@end
@implementation QWMallProductlBanner

-(instancetype)initWithFrame:(CGRect)frame
{
    self =[super initWithFrame:frame];
    if (self) {
        self.backgroundColor = Color_Main_Green;
        [self bannerScrollView];
    }
    return self;
}

#pragma mark- ScrollView
-(UIScrollView *)bannerScrollView
{
    if (!_bannerScrollView) {
        _bannerScrollView=[[UIScrollView alloc]init];
        _bannerScrollView.backgroundColor =[UIColor clearColor];
        _bannerScrollView.delegate =self;
        _bannerScrollView.pagingEnabled =YES;
        _bannerScrollView.showsVerticalScrollIndicator = NO;
        _bannerScrollView.showsHorizontalScrollIndicator = NO;
        [self addSubview:_bannerScrollView];
        [self sendSubviewToBack:_bannerScrollView];
        [_bannerScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.top.equalTo(self);
            make.right.equalTo(self);
            make.bottom.equalTo(self);
        }];
        
        //默认背景图
        UIImageView *imageView =[[UIImageView alloc]init];
        imageView.backgroundColor=[UIColor whiteColor];
        imageView.image=[UIImage imageNamed:@"WHeaderBack"];
        [self addSubview:imageView];
        [self sendSubviewToBack:imageView];
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.top.equalTo(self);
            make.right.equalTo(self);
            make.bottom.equalTo(self);
        }];
    }
    return _bannerScrollView;
}

#pragma mark - 数据
- (void)setShowArr:(NSArray *)showArr
{
    _showArr = showArr;
    [self.showBannerImageArr removeAllObjects];
    [self.showBannerImageArr addObjectsFromArray:_showArr];
    [self loadBannerImageScrollView];
}

-(void)loadBannerImageScrollView
{
    NSArray *arr =[_bannerScrollView subviews];
    for (UIView *view in arr) {
        [view removeFromSuperview];
    }
    
    if (self.showBannerImageArr.count) {
        UIView *lastView=nil;
        for (int i = 0; i<_showBannerImageArr.count; i++) {
            UIView *bgShadowView = [[UIView alloc]init];
            [_bannerScrollView addSubview:bgShadowView];
            
            [bgShadowView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(self.bannerScrollView.mas_width);
                make.top.equalTo(self.mas_top);
                make.height.mas_equalTo(kScreenWidth);
                
                if (lastView) {
                    make.left.equalTo(lastView.mas_right);
                } else {
                    make.left.equalTo(self.bannerScrollView);
                }
            }];
            lastView = bgShadowView;
            
            UIImageView *imageView =[[UIImageView alloc]init];
            imageView.backgroundColor=[UIColor whiteColor];
            NSString *picStr = _showBannerImageArr[i];
            imageView.image = [UIImage imageNamed:picStr];
            imageView.tag=10000+i;
            [bgShadowView addSubview:imageView];
            [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(bgShadowView);
            }];
        }
        [_bannerScrollView mas_updateConstraints:^(MASConstraintMaker *make) {
            //让scrollview的contentSize随着内容的增多而变化
            make.right.mas_equalTo(lastView.mas_right);
        }];
        _bannerScrollView.pagingEnabled=YES;
        _bannerScrollView.alwaysBounceHorizontal=YES;
        _bannerScrollView.contentSize = CGSizeMake(_showBannerImageArr.count*kScreenWidth, 0);
    }
}

#pragma mark- 点击
-(void)imageButtonClick:(UIButton *)sender
{
//    QABannerModel *bannerModel =_showBannerImageArr[sender.tag-10000];
//    if(self.bannerClickBlock)self.bannerClickBlock(bannerModel);
}

-(NSMutableArray *)showBannerImageArr
{
    if (!_showBannerImageArr) {
        _showBannerImageArr =[[NSMutableArray alloc]init];
    }
    return _showBannerImageArr;
}
@end
