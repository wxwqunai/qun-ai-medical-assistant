//
//  QWMallProductPicCell.h
//  assistant
//
//  Created by qunai on 2024/4/1.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWMallProductPicCell : QWBaseTableViewCell
@property (nonatomic, strong) UIImageView *picImageView;
@end

NS_ASSUME_NONNULL_END
