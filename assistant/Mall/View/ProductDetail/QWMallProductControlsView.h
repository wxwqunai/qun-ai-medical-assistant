//
//  QWMallProductControlsView.h
//  assistant
//
//  Created by qunai on 2024/3/29.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWMallProductControlsView : UIView
@property (nonatomic,strong) UIButton *shopCart;
@property (nonatomic,strong) UIButton *store;
@property (nonatomic,strong) UIButton *customerService;

@property (nonatomic,strong) UIButton *addToCart; //加入购物车
@property (nonatomic,strong) UIButton *buy; //购买

@property (nonatomic, copy) void (^shopCartBlock)(void);
@property (nonatomic, copy) void (^storeBlock)(void);
@property (nonatomic, copy) void (^customerServiceBlock)(void);
@property (nonatomic, copy) void (^addToCartBlock)(void);
@property (nonatomic, copy) void (^buyBlock)(void);
@end

NS_ASSUME_NONNULL_END
