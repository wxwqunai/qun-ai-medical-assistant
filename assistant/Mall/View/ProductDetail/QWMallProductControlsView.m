//
//  QWMallProductControlsView.m
//  assistant
//
//  Created by qunai on 2024/3/29.
//

#import "QWMallProductControlsView.h"
@interface QWMallProductControlsView()
@property (nonatomic,strong) UIView *contentView;

@property (nonatomic,strong) UIView *rightView;
@end

@implementation QWMallProductControlsView
- (instancetype)init{
    self = [super init];
    if(self){
        self.backgroundColor = [UIColor whiteColor];
        
        [self contentView];
        [self shopCart];
        [self store];
        [self customerService];
        
        [self rightView];
        [self addToCart];
        [self buy];
    }
    return self;
}

- (UIView *)contentView{
    if(!_contentView){
        _contentView = [[UIView alloc]init];
        _contentView.backgroundColor = [UIColor clearColor];
        [self addSubview:_contentView];
        
        [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.top.equalTo(self);
            make.bottom.equalTo(self).mas_offset(- [UIDevice safeDistanceBottom]);
        }];
    }
    return _contentView;
}

#pragma mark - 购物车
- (UIButton *)shopCart{
    if(!_shopCart){
        _shopCart = [UIButton buttonWithType:UIButtonTypeCustom];
        [_shopCart setTitle:@"购物车" forState:UIControlStateNormal];
        [_shopCart setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _shopCart.titleLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightMedium];
        [_shopCart setImage:[UIImage imageNamed:@"mall_shopCart_black.png"] forState:UIControlStateNormal];
        [self.contentView addSubview:_shopCart];
        _shopCart.frame = CGRectMake(0, 0, 70, [UIDevice tabBarHeight]);
        CGSize imageSize = _shopCart.imageView.frame.size;
        CGSize titleSize = _shopCart.titleLabel.frame.size;
        [_shopCart setTitleEdgeInsets:UIEdgeInsetsMake(0, -imageSize.width, -imageSize.height, 0)];
        [_shopCart setImageEdgeInsets:UIEdgeInsetsMake(-titleSize.height, 0, 0, -titleSize.width)];
        [_shopCart addTarget:self action:@selector(shopCartOnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shopCart;
}

- (void)shopCartOnClick{
    if(self.shopCartBlock)self.shopCartBlock();
}

#pragma mark - 店铺
- (UIButton *)store{
    if(!_store){
        _store = [UIButton buttonWithType:UIButtonTypeCustom];
        [_store setTitle:@"店铺" forState:UIControlStateNormal];
        [_store setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _store.titleLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightMedium];
        [_store setImage:[UIImage imageNamed:@"mall_store.png"] forState:UIControlStateNormal];
        [self.contentView addSubview:_store];
        _store.frame = CGRectMake(CGRectGetMaxX(self.shopCart.frame), 0, 55, [UIDevice tabBarHeight]);
        CGSize imageSize = _store.imageView.frame.size;
        CGSize titleSize = _store.titleLabel.frame.size;
        [_store setTitleEdgeInsets:UIEdgeInsetsMake(2, -imageSize.width, -imageSize.height, 0)];
        [_store setImageEdgeInsets:UIEdgeInsetsMake(-titleSize.height, 0, 0, -titleSize.width)];
        [_store addTarget:self action:@selector(storeOnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _store;
}

- (void)storeOnClick{
    if(self.storeBlock)self.storeBlock();
}

#pragma mark - 客服
- (UIButton *)customerService{
    if(!_customerService){
        _customerService = [UIButton buttonWithType:UIButtonTypeCustom];
        [_customerService setTitle:@"客服" forState:UIControlStateNormal];
        [_customerService setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _customerService.titleLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightMedium];
        [_customerService setImage:[UIImage imageNamed:@"mall_customerService.png"] forState:UIControlStateNormal];
        [self.contentView addSubview:_customerService];
        _customerService.frame = CGRectMake(CGRectGetMaxX(self.store.frame), 0, 55, [UIDevice tabBarHeight]);
        CGSize imageSize = _customerService.imageView.frame.size;
        CGSize titleSize = _customerService.titleLabel.frame.size;
        [_customerService setTitleEdgeInsets:UIEdgeInsetsMake(2, -imageSize.width, -imageSize.height, 0)];
        [_customerService setImageEdgeInsets:UIEdgeInsetsMake(-titleSize.height, 0, 0, -titleSize.width)];
        [_customerService addTarget:self action:@selector(customerServiceOnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _customerService;
}

- (void)customerServiceOnClick{
    if(self.customerServiceBlock)self.customerServiceBlock();
}


- (UIView *)rightView{
    if(!_rightView){
        _rightView = [[UIView alloc]init];
        _rightView.layer.cornerRadius = 20.0;
        _rightView.layer.masksToBounds = YES;
        [self.contentView addSubview:_rightView];
        [_rightView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.customerService.mas_right);
            make.right.equalTo(self.contentView).offset(-10.0);
            make.centerY.equalTo(self.contentView);
            make.height.mas_equalTo(40);
        }];
    }
    return _rightView;
}
#pragma mark - 加入购物车
- (UIButton *)addToCart{
    if(!_addToCart){
        _addToCart = [UIButton buttonWithType:UIButtonTypeCustom];
        [_addToCart setTitle:@"加入购物车" forState:UIControlStateNormal];
        _addToCart.backgroundColor = [UIColor orangeColor];
        [_addToCart setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _addToCart.titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
        [_addToCart addTarget:self action:@selector(addToCartOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.rightView addSubview:_addToCart];
        
        [_addToCart mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.rightView);
            make.bottom.equalTo(self.rightView);
            make.left.equalTo(self.rightView);
            make.right.equalTo(self.rightView.mas_centerX);
        }];
    }
    return _addToCart;
}
- (void)addToCartOnClick{
    if(self.addToCartBlock)self.addToCartBlock();
}
#pragma mark - 立即购买
- (UIButton *)buy{
    if(!_buy){
        _buy = [UIButton buttonWithType:UIButtonTypeCustom];
        [_buy setTitle:@"立即购买" forState:UIControlStateNormal];
        _buy.backgroundColor = [UIColor redColor];
        [_buy setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _buy.titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
        [_buy addTarget:self action:@selector(buyOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.rightView addSubview:_buy];
        
        [_buy mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.rightView);
            make.bottom.equalTo(self.rightView);
            make.right.equalTo(self.rightView);
            make.left.equalTo(self.rightView.mas_centerX);
        }];
    }
    return _buy;
}
- (void)buyOnClick{
    if(self.buyBlock)self.buyBlock();
}
@end
