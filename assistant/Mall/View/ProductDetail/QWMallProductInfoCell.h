//
//  QWMallProductInfoCell.h
//  assistant
//
//  Created by qunai on 2024/4/1.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWMallProductInfoCell : QWBaseTableViewCell
@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *sellNumLabel;

@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *stateLabel;
@end

NS_ASSUME_NONNULL_END
