//
//  QWMallSCCell.h
//  assistant
//
//  Created by qunai on 2024/4/7.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWMallSCCell : QWBaseTableViewCell
@property (nonatomic, assign) BOOL isChoose;

@property (nonatomic, strong) UIImageView *picImageView;
@property (nonatomic, strong) UILabel *productNameLabel;
@property (nonatomic, strong) UILabel *realPriceLabel;

@property (nonatomic, strong) UILabel *numLabel;
@property (nonatomic, strong) UIButton *numAddBtn;
@property (nonatomic, strong) UIButton *numSubBtn;
@property (nonatomic, assign) NSInteger num;

@property (nonatomic, copy) void (^chooseBlock)(void);
@property (nonatomic, copy) void (^numChangeBlock)(NSInteger num);
@end

NS_ASSUME_NONNULL_END
