//
//  QWMallSCControlsView.h
//  assistant
//
//  Created by qunai on 2024/4/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWMallSCControlsView : UIView
@property (nonatomic, strong) UILabel *totalLabel;
@property (nonatomic, strong) UIButton *submitBtn;
@property (nonatomic, assign) NSInteger chooseCount;

@property (nonatomic, copy) void (^submitBlock)(void);
@property (nonatomic, copy) void (^chooseAllBlock)(BOOL isChooseAll);
@property (nonatomic, assign) BOOL isChooseAll;

@end

NS_ASSUME_NONNULL_END
