//
//  QWMallSCControlsView.m
//  assistant
//
//  Created by qunai on 2024/4/8.
//

#import "QWMallSCControlsView.h"

@interface QWMallSCControlsView()
@property (nonatomic,strong) UIView *bgView;
@property (nonatomic, strong) UIButton *chooseBtn;
@property (nonatomic,strong) UIView *rightView;
@end

@implementation QWMallSCControlsView

- (instancetype)init{
    self = [super init];
    if(self){
        self.backgroundColor = [UIColor whiteColor];
        
        [self bgView];
        [self chooseBtn];
        
        [self rightView];
        [self submitBtn];
        [self totalLabel];
        
        self.chooseCount = 0;
    }
    return self;
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor clearColor];
        [self addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.top.equalTo(self);
            make.bottom.equalTo(self).mas_offset(- [UIDevice safeDistanceBottom]);
        }];
    }
    return _bgView;
}

#pragma mark - 全选
- (UIButton *)chooseBtn{
    if(!_chooseBtn){
        _chooseBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_chooseBtn setImage:[UIImage imageNamed:self.isChooseAll?@"mall_choose":@"mall_choose_not"] forState:UIControlStateNormal];
        [_chooseBtn addTarget:self action:@selector(chooseOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.bgView addSubview:_chooseBtn];
        
        [_chooseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView).offset(14.0);
            make.top.equalTo(self.bgView);
            make.bottom.equalTo(self.bgView);
            make.width.mas_equalTo(30);
        }];
        
        UILabel *label = [[UILabel alloc]init];
        label.textColor = [UIColor blackColor];
        label.font = [UIFont systemFontOfSize:15.0 weight:UIFontWeightRegular];
        label.text = @"全选";
        [self.bgView addSubview:label];
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_chooseBtn.mas_right);
            make.centerY.equalTo(_chooseBtn);
        }];
    }
    return _chooseBtn;
}
- (void)chooseOnClick{
    self.isChooseAll = !self.isChooseAll;
    if(self.chooseAllBlock)self.chooseAllBlock(self.isChooseAll);
}
- (void)setIsChooseAll:(BOOL)isChooseAll{
    _isChooseAll = isChooseAll;
    [self.chooseBtn setImage:[UIImage imageNamed:_isChooseAll?@"mall_choose":@"mall_choose_not"] forState:UIControlStateNormal];
}
#pragma mark - 价格
- (UILabel *)totalLabel{
    if(!_totalLabel){
        _totalLabel = [[UILabel alloc]init];
        _totalLabel.numberOfLines = 1;
        _totalLabel.text = @"¥0.00";
        _totalLabel.textColor = [UIColor colorFromHexString:@"#EB5D2A"];
        _totalLabel.font = [UIFont systemFontOfSize:15 weight:(UIFontWeightMedium)];
        [self.bgView addSubview:_totalLabel];
        [_totalLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.rightView.mas_left).offset(-10.0);
            make.centerY.equalTo(self.bgView);
        }];
        
        UILabel *nameLabel = [[UILabel alloc]init];
        nameLabel.textColor = [UIColor blackColor];
        nameLabel.font = [UIFont systemFontOfSize:12.0 weight:UIFontWeightRegular];
        nameLabel.text = @"合计";
        [self.bgView addSubview:nameLabel];
        
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_totalLabel.mas_left).offset(-4.0);
            make.centerY.equalTo(self.bgView);
        }];
    }
    return _totalLabel;
}

- (UIView *)rightView{
    if(!_rightView){
        _rightView = [[UIView alloc]init];
        _rightView.layer.cornerRadius = 20.0;
        _rightView.layer.masksToBounds = YES;
        _rightView.backgroundColor = Color_Main_Green;
        [self.bgView addSubview:_rightView];
        [_rightView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.bgView).offset(-14.0);
            make.centerY.equalTo(self.bgView);
            make.height.mas_equalTo(40.0);
        }];
    }
    return _rightView;
}

#pragma mark - 提交订单
- (UIButton *)submitBtn{
    if(!_submitBtn){
        _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_submitBtn setTitle:@"去结算(0)" forState:UIControlStateNormal];
        [_submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _submitBtn.titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
        [_submitBtn addTarget:self action:@selector(submitOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.rightView addSubview:_submitBtn];
        
        [_submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.rightView).with.insets(UIEdgeInsetsMake(0, 20, 0, 20));
        }];
    }
    return _submitBtn;
}
- (void)submitOnClick{
    if(self.submitBlock)self.submitBlock();
}
- (void)setChooseCount:(NSInteger)chooseCount{
    _chooseCount = chooseCount;
    [self.submitBtn setTitle:[NSString stringWithFormat:@"去结算(%ld)",_chooseCount] forState:UIControlStateNormal];
}

@end
