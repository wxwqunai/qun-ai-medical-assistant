//
//  QWMallSCHeaderView.m
//  assistant
//
//  Created by qunai on 2024/4/7.
//

#import "QWMallSCHeaderView.h"

@interface QWMallSCHeaderView()
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIImageView *storeLogo;
@end

@implementation QWMallSCHeaderView

- (instancetype)initWithReuseIdentifier:(nullable NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if(self){
        [self bgView];
    }
    return self;
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_bgView];
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView).with.insets(UIEdgeInsetsMake(0, 8, 0, 8));
        }];
    }
    return _bgView;
}

- (UIImageView *)storeLogo{
    if (!_storeLogo) {
        _storeLogo = [[UIImageView alloc]init];
        _storeLogo.image = [UIImage imageNamed:@"mall_store"];
        [self.bgView addSubview:_storeLogo];
        [_storeLogo mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView).offset(8.0);
            make.centerY.equalTo(self.bgView);
            make.width.mas_equalTo(16);
            make.height.equalTo(_storeLogo.mas_width);
        }];
    }
    return _storeLogo;
}
- (UILabel *)storeNameLabel{
    if(!_storeNameLabel){
        _storeNameLabel = [[UILabel alloc]init];
        _storeNameLabel.textColor = [UIColor blackColor];
        _storeNameLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightSemibold];
        [self.bgView addSubview:_storeNameLabel];
        [_storeNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.storeLogo.mas_right).offset(4.0);
            make.centerY.equalTo(self.storeLogo);
            make.right.equalTo(self.bgView).offset(-8.0);
        }];
    }
    return _storeNameLabel;
}

@end
