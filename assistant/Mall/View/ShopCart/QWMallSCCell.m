//
//  QWMallSCCell.m
//  assistant
//
//  Created by qunai on 2024/4/7.
//

#import "QWMallSCCell.h"

@interface QWMallSCCell()
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIButton *chooseBtn;
@property (nonatomic, strong) UIView *numView;
@end

@implementation QWMallSCCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self bgView];
        
        [self chooseBtn];
        
        [self picImageView];
        [self productNameLabel];
        [self realPriceLabel];
        
        [self numView];
        [self numAddBtn];
        [self numLabel];
        [self numSubBtn];
    }
    return self;
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 6.0;
        _bgView.layer.masksToBounds = YES;
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView).with.insets(UIEdgeInsetsMake(0, 8, 0, 8));
        }];
    }
    return _bgView;
}

#pragma mark - 选择
- (UIButton *)chooseBtn{
    if(!_chooseBtn){
        _chooseBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_chooseBtn setImage:[UIImage imageNamed:self.isChoose?@"mall_choose":@"mall_choose_not"] forState:UIControlStateNormal];
        [_chooseBtn addTarget:self action:@selector(chooseOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.bgView addSubview:_chooseBtn];
        
        [_chooseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView).offset(6.0);
            make.top.equalTo(self.bgView);
            make.bottom.equalTo(self.bgView);
            make.width.mas_equalTo(30);
        }];
    }
    return _chooseBtn;
}
- (void)chooseOnClick{
//    self.isChoose = !self.isChoose;
    if(self.chooseBlock)self.chooseBlock();
}


- (void)setIsChoose:(BOOL)isChoose{
    _isChoose = isChoose;
    [self.chooseBtn setImage:[UIImage imageNamed:_isChoose?@"mall_choose":@"mall_choose_not"] forState:UIControlStateNormal];
}


#pragma mark - 商品图片
- (UIImageView *)picImageView{
    if(!_picImageView){
        _picImageView = [[UIImageView alloc]init];
        _picImageView.layer.cornerRadius = 6.0;
        _picImageView.layer.masksToBounds = YES;
        [self.bgView addSubview:_picImageView];
        [_picImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.chooseBtn.mas_right).offset(6.0);
            make.top.equalTo(self.bgView).offset(15.0);
            make.bottom.equalTo(self.bgView).offset(-15.0);
            make.height.mas_equalTo(80.0);
            make.width.equalTo(_picImageView.mas_height);
        }];
    }
    return _picImageView;
}

#pragma mark - 商品名称
- (UILabel *)productNameLabel{
    if(!_productNameLabel){
        _productNameLabel = [[UILabel alloc]init];
        _productNameLabel.numberOfLines = 2;
        _productNameLabel.font = [UIFont systemFontOfSize:15 weight:(UIFontWeightRegular)];
        [self.bgView addSubview:_productNameLabel];
        [_productNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.picImageView.mas_right).offset(8.0);
            make.top.equalTo(self.picImageView.mas_top);
            make.right.equalTo(self.bgView).offset(-6.0);
        }];
    }
    return _productNameLabel;
}
#pragma mark - 价格
- (UILabel *)realPriceLabel{
    if(!_realPriceLabel){
        _realPriceLabel = [[UILabel alloc]init];
        _realPriceLabel.numberOfLines = 1;
        _realPriceLabel.textColor = [UIColor colorFromHexString:@"#EB5D2A"];
        _realPriceLabel.font = [UIFont systemFontOfSize:15 weight:(UIFontWeightMedium)];
        [self.bgView  addSubview:_realPriceLabel];
        [_realPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.picImageView.mas_right).offset(8.0);
            make.bottom.equalTo(self.picImageView);
        }];
    }
    return _realPriceLabel;
}

#pragma mark - 购买数量
- (UIView *)numView{
    if(!_numView){
        _numView = [[UIView alloc]init];
        _numView.layer.borderWidth = 0.5;
        _numView.layer.borderColor = [UIColor grayColor].CGColor;
        _numView.layer.cornerRadius = 4.0;
        _numView.layer.masksToBounds =YES;
        [self.bgView addSubview:_numView];
        
        [_numView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.bgView).offset(-6.0);
            make.bottom.equalTo(self.picImageView);
            make.height.mas_equalTo(20.0);
        }];
    }
    return _numView;
}

- (UIButton *)numAddBtn{
    if(!_numAddBtn){
        _numAddBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_numAddBtn setTitle:@"+" forState:UIControlStateNormal];
        [_numAddBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_numAddBtn addTarget:self action:@selector(numChangeOnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.numView addSubview:_numAddBtn];
        [_numAddBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.numView);
            make.top.equalTo(self.numView);
            make.bottom.equalTo(self.numView);
            make.width.equalTo(_numAddBtn.mas_height);
        }];
        
        UIImageView *line = [[UIImageView alloc]init];
        line.backgroundColor = [UIColor grayColor];
        [self.numView addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.numView);
            make.bottom.equalTo(self.numView);
            make.centerX.equalTo(_numAddBtn.mas_left);
            make.width.mas_equalTo(0.5);
        }];
    }
    return _numAddBtn;
}

- (UILabel *)numLabel{
    if(!_numLabel){
        _numLabel = [[UILabel alloc]init];
        _numLabel.textColor = [UIColor blackColor];
        _numLabel.font = [UIFont systemFontOfSize:13];
        _numLabel.text = [NSString stringWithFormat:@"%ld",self.num];
        _numLabel.textAlignment = NSTextAlignmentCenter;
        [self.numView addSubview:_numLabel];
        [_numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.numView);
            make.bottom.equalTo(self.numView);
            make.right.equalTo(self.numAddBtn.mas_left);
            make.width.mas_greaterThanOrEqualTo(40);
        }];
    }
    return _numLabel;
}

- (UIButton *)numSubBtn{
    if(!_numSubBtn){
        _numSubBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_numSubBtn setTitle:@"-" forState:UIControlStateNormal];
        [_numSubBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_numSubBtn addTarget:self action:@selector(numChangeOnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.numView addSubview:_numSubBtn];
        [_numSubBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.numLabel.mas_left);
            make.left.equalTo(self.numView);
            make.top.equalTo(self.numView);
            make.bottom.equalTo(self.numView);
            make.width.equalTo(_numSubBtn.mas_height);
        }];
        
        UIImageView *line = [[UIImageView alloc]init];
        line.backgroundColor = [UIColor grayColor];
        [self.numView addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.numView);
            make.bottom.equalTo(self.numView);
            make.centerX.equalTo(_numSubBtn.mas_right);
            make.width.mas_equalTo(0.5);
        }];
    }
    return _numSubBtn;
}

- (void)numChangeOnClick:(UIButton *)sender{
    NSInteger changeNum = self.num;

    if(sender == self.numAddBtn) changeNum = changeNum +1;
    if(sender == self.numSubBtn) changeNum = changeNum -1;
    
    if(changeNum <1) changeNum = 1;
    if(changeNum >99) changeNum = 99;
    
    self.num = changeNum;
    if(self.numChangeBlock)self.numChangeBlock(self.num);
}

- (void)setNum:(NSInteger)num{
    _num = num;
    self.numLabel.text = [NSString stringWithFormat:@"%ld",_num];
}


@end
