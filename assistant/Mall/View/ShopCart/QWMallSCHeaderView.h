//
//  QWMallSCHeaderView.h
//  assistant
//
//  Created by qunai on 2024/4/7.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWMallSCHeaderView : UITableViewHeaderFooterView
@property (nonatomic, strong) UILabel *storeNameLabel;
@end

NS_ASSUME_NONNULL_END
