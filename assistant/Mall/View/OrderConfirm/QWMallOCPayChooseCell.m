//
//  QWMallOCPayChooseCell.m
//  assistant
//
//  Created by qunai on 2024/4/3.
//

#import "QWMallOCPayChooseCell.h"

@interface QWMallOCPayChooseCell()
@property (nonatomic, strong) UIView *bgView;
@end

@implementation QWMallOCPayChooseCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self bgView];
        [self iconImageView];
        [self nameLabel];
        [self chooseBtn];
    }
    return self;
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView).with.insets(UIEdgeInsetsMake(0, 14, 0, 14));
            make.height.mas_equalTo(44.0);
        }];
    }
    return _bgView;
}

- (UIImageView *)iconImageView{
    if(!_iconImageView){
        _iconImageView = [[UIImageView alloc]init];
        [self.bgView addSubview:_iconImageView];
        [_iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView).offset(8.0);
            make.centerY.equalTo(self.bgView);
        }];
    }
    return _iconImageView;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.font = [UIFont systemFontOfSize:15.0 weight:UIFontWeightLight];
        [self.bgView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.iconImageView.mas_right).offset(6.0);
            make.centerY.equalTo(self.bgView);
        }];
    }
    return _nameLabel;
}

- (UIButton *)chooseBtn{
    if(!_chooseBtn){
        _chooseBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_chooseBtn setImage:[UIImage imageNamed:self.isChoose?@"mall_choose":@"mall_choose_not"] forState:UIControlStateNormal];
        [_chooseBtn addTarget:self action:@selector(chooseOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.bgView addSubview:_chooseBtn];
        
        [_chooseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.bgView);
            make.top.equalTo(self.bgView);
            make.bottom.equalTo(self.bgView);
            make.width.mas_equalTo(_chooseBtn.mas_height);
        }];
    }
    return _chooseBtn;
}

- (void)chooseOnClick{
//    self.isChoose = !self.isChoose;
    if(self.chooseBlock)self.chooseBlock();
}


- (void)setIsChoose:(BOOL)isChoose{
    _isChoose = isChoose;
    [self.chooseBtn setImage:[UIImage imageNamed:_isChoose?@"mall_choose":@"mall_choose_not"] forState:UIControlStateNormal];
}

@end
