//
//  QWMallOCAddressCell.h
//  assistant
//
//  Created by qunai on 2024/4/2.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWAddressEmptyView : UIView
@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) UILabel *tipLabel;
@property (nonatomic, strong) UIImageView *addressNext;
@end

@interface QWMallOCAddressCell : QWBaseTableViewCell

@end

NS_ASSUME_NONNULL_END
