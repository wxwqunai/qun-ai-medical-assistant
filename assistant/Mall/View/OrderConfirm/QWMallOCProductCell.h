//
//  QWMallOCProductCell.h
//  assistant
//
//  Created by qunai on 2024/4/2.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWMallOCProductCell : QWBaseTableViewCell
@property (nonatomic, strong) UILabel *storeNameLabel;
@property (nonatomic, strong) UIImageView *picImageView;
@property (nonatomic, strong) UILabel *productNameLabel;
@property (nonatomic, strong) UILabel *realPriceLabel;
@property (nonatomic, strong) UILabel *originalPriceLabel;
@property (nonatomic, strong) UILabel *numLabel;
@end

NS_ASSUME_NONNULL_END
