//
//  QWMallOCControlsView.m
//  assistant
//
//  Created by qunai on 2024/4/2.
//

#import "QWMallOCControlsView.h"
@interface QWMallOCControlsView()
@property (nonatomic,strong) UIView *contentView;

@property (nonatomic,strong) UIView *rightView;
@end
@implementation QWMallOCControlsView
- (instancetype)init{
    self = [super init];
    if(self){
        self.backgroundColor = [UIColor whiteColor];
        
        [self contentView];
        [self nameLabel];
        [self totalLabel];
        
        [self rightView];
        [self submitBtn];
    }
    return self;
}

- (UIView *)contentView{
    if(!_contentView){
        _contentView = [[UIView alloc]init];
        _contentView.backgroundColor = [UIColor clearColor];
        [self addSubview:_contentView];
        
        [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.top.equalTo(self);
            make.bottom.equalTo(self).mas_offset(- [UIDevice safeDistanceBottom]);
        }];
    }
    return _contentView;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.font = [UIFont systemFontOfSize:15.0 weight:UIFontWeightRegular];
        _nameLabel.text = @"合计";
        [self.contentView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(14.0);
            make.centerY.equalTo(self.contentView);
        }];
    }
    return _nameLabel;
}

#pragma mark - 价格
- (UILabel *)totalLabel{
    if(!_totalLabel){
        _totalLabel = [[UILabel alloc]init];
        _totalLabel.numberOfLines = 1;
        _totalLabel.textColor = [UIColor colorFromHexString:@"#EB5D2A"];
        _totalLabel.font = [UIFont systemFontOfSize:18 weight:(UIFontWeightMedium)];
        [self.contentView addSubview:_totalLabel];
        [_totalLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.nameLabel.mas_right).offset(4.0);
            make.bottom.equalTo(self.nameLabel.mas_bottom);
        }];
    }
    return _totalLabel;
}


- (UIView *)rightView{
    if(!_rightView){
        _rightView = [[UIView alloc]init];
        _rightView.layer.cornerRadius = 8.0;
        _rightView.layer.masksToBounds = YES;
        _rightView.backgroundColor = [UIColor redColor];
        [self.contentView addSubview:_rightView];
        [_rightView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-10.0);
            make.centerY.equalTo(self.contentView);
            make.height.mas_equalTo(40.0);
        }];
    }
    return _rightView;
}

#pragma mark - 提交订单
- (UIButton *)submitBtn{
    if(!_submitBtn){
        _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_submitBtn setTitle:@"提交订单" forState:UIControlStateNormal];
        [_submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _submitBtn.titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
        [_submitBtn addTarget:self action:@selector(submitOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.rightView addSubview:_submitBtn];
        
        [_submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.rightView).with.insets(UIEdgeInsetsMake(0, 20, 0, 20));
        }];
    }
    return _submitBtn;
}
- (void)submitOnClick{
    if(self.submitBlock)self.submitBlock();
}

@end
