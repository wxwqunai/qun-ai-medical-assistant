//
//  QWMallOCAddressCell.m
//  assistant
//
//  Created by qunai on 2024/4/2.
//

#import "QWMallOCAddressCell.h"

@interface QWMallOCAddressCell()
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) QWAddressEmptyView *emptyView;

@end
@implementation QWMallOCAddressCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self bgView];
        
        [self emptyView];
    }
    return self;
}
- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 6.0;
        _bgView.layer.masksToBounds = YES;
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView).with.insets(UIEdgeInsetsMake(0, 14, 0, 14));
        }];
        
        UIImageView *bgImageView = [[UIImageView alloc]init];
        bgImageView.contentMode = UIViewContentModeScaleAspectFill;
        bgImageView.image = [UIImage imageNamed:@"mall_address_bg"];
        [_bgView addSubview:bgImageView];
        [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_bgView);
        }];
    }
    return _bgView;
}

- (QWAddressEmptyView *)emptyView{
    if(!_emptyView){
        _emptyView = [[QWAddressEmptyView alloc]init];
        [self.bgView addSubview:_emptyView];
        
        [_emptyView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.bgView).with.insets(UIEdgeInsetsMake(0, 8, 0, 8));
        }];
    }
    return _emptyView;
}

@end


@implementation QWAddressEmptyView
- (instancetype)init{
    self = [super init];
    if(self){
        [self addressNext];
        [self addressLabel];
        [self tipLabel];
        
    }
    return self;
}
- (UIImageView *)addressNext{
    if(!_addressNext){
        _addressNext = [[UIImageView alloc]init];
        UIImage * image = [UIImage imageNamed:@"next_arrow"];
        _addressNext.image = image;
        [self addSubview:_addressNext];
        [_addressNext mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self);
            make.centerY.equalTo(self);
            make.width.mas_equalTo(image.size.width);
        }];
    }
    return _addressNext;
}

//地址
- (UILabel *)addressLabel{
    if(!_addressLabel){
        _addressLabel = [[UILabel alloc]init];
        _addressLabel.text = @"设置完收货信息才能下单哦";
        _addressLabel.textColor = [UIColor blackColor];
        _addressLabel.font = [UIFont systemFontOfSize:17.0 weight:UIFontWeightBold];
        [self addSubview:_addressLabel];
        
        [_addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self.addressNext.mas_left);
            make.bottom.equalTo(self.mas_centerY).offset(-4.0);
        }];
    }
    return _addressLabel;
}

- (UILabel *)tipLabel{
    if(!_tipLabel){
        _tipLabel = [[UILabel alloc]init];
        _tipLabel.text = @"点击前往设置";
        _tipLabel.textColor = [UIColor grayColor];
        _tipLabel.font = [UIFont systemFontOfSize:14.0 weight:UIFontWeightRegular];
        [self addSubview:_tipLabel];
        
        [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.addressLabel.mas_left);
            make.right.equalTo(self.addressLabel.mas_right);
            make.top.equalTo(self.mas_centerY).offset(4.0);
        }];
    }
    return _tipLabel;
}


@end
