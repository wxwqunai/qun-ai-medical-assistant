//
//  QWMallOCControlsView.h
//  assistant
//
//  Created by qunai on 2024/4/2.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWMallOCControlsView : UIView
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *totalLabel;

@property (nonatomic, strong) UIButton *submitBtn;
@property (nonatomic, copy) void (^submitBlock)(void);
@end

NS_ASSUME_NONNULL_END
