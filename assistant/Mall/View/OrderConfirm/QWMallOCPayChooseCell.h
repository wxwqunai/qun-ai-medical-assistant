//
//  QWMallOCPayChooseCell.h
//  assistant
//
//  Created by qunai on 2024/4/3.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWMallOCPayChooseCell : QWBaseTableViewCell
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UIButton *chooseBtn;

@property (nonatomic, copy) void (^chooseBlock)(void);
@property (nonatomic, assign) BOOL isChoose;
@end

NS_ASSUME_NONNULL_END
