//
//  QWMallOCHeaderView.h
//  assistant
//
//  Created by qunai on 2024/4/2.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWMallOCHeaderView : UITableViewHeaderFooterView
@property (nonatomic, strong) UILabel *nameLabel;
@end

NS_ASSUME_NONNULL_END
