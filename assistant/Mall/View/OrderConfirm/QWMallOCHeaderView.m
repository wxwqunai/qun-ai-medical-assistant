//
//  QWMallOCHeaderView.m
//  assistant
//
//  Created by qunai on 2024/4/2.
//

#import "QWMallOCHeaderView.h"

@interface QWMallOCHeaderView()
@property (nonatomic, strong) UIView *bgView;
@end

@implementation QWMallOCHeaderView
- (instancetype)initWithReuseIdentifier:(nullable NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if(self){
        [self bgView];
    }
    return self;
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_bgView];
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView).with.insets(UIEdgeInsetsMake(0, 14, 0, 14));
        }];
    }
    return _bgView;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightBold];
        [self.bgView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView).offset(8.0);
            make.top.equalTo(self.bgView).offset(8.0);
            make.bottom.equalTo(self.bgView);
        }];
    }
    return _nameLabel;
}

@end
