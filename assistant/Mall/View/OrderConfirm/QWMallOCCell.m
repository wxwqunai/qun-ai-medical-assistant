//
//  QWMallOCCell.m
//  assistant
//
//  Created by qunai on 2024/4/2.
//

#import "QWMallOCCell.h"

@interface QWMallOCCell()
@property (nonatomic, strong) UIView *bgView;

@end
@implementation QWMallOCCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self bgView];
        
        [self nameLabel];
        [self desLabel];
    }
    return self;
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_bgView];
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView).with.insets(UIEdgeInsetsMake(0, 14, 0, 14));
        }];
    }
    return _bgView;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.textColor = [UIColor grayColor];
        _nameLabel.font = [UIFont systemFontOfSize:15];
        [self.bgView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView).offset(8.0);
            make.top.equalTo(self.bgView);
            make.bottom.equalTo(self.bgView);
            make.height.mas_equalTo(44.0);
        }];
    }
    return _nameLabel;
}

- (UILabel *)desLabel{
    if(!_desLabel){
        _desLabel = [[UILabel alloc]init];
        _desLabel.textColor = [UIColor blackColor];
        _desLabel.font = [UIFont systemFontOfSize:15];
        [self.bgView addSubview:_desLabel];
        
        [_desLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.bgView).offset(-8.0);
            make.top.equalTo(self.bgView);
            make.bottom.equalTo(self.bgView);
        }];
    }
    return _desLabel;
}


@end
