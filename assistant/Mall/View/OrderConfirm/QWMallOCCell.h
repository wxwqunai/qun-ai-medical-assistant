//
//  QWMallOCCell.h
//  assistant
//
//  Created by qunai on 2024/4/2.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWMallOCCell : QWBaseTableViewCell

@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *desLabel;
@end

NS_ASSUME_NONNULL_END
