//
//  QWMallModel.h
//  assistant
//
//  Created by qunai on 2024/4/8.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWMallModel : NSObject

@end

@interface QWMallShopCartModel : NSObject
@property (nonatomic, copy) NSString *storeName;
@property (nonatomic, copy) NSString *productName;
@property (nonatomic, copy) NSString *productPrice;
@property (nonatomic, copy) NSString *productPic;
@property (nonatomic, assign) NSInteger count;
@property (nonatomic, assign) BOOL isChoose;
@end

NS_ASSUME_NONNULL_END
