//
//  QWMallCarTConfirmController.m
//  assistant
//
//  Created by qunai on 2024/4/7.
//

#import "QWMallCarTConfirmController.h"

@interface QWMallCarTConfirmController ()<UIGestureRecognizerDelegate>
@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UIImageView *picImageView;
@property (nonatomic, strong) UILabel *productNameLabel;
@property (nonatomic, strong) UILabel *realPriceLabel;
@property (nonatomic, strong) UILabel *originalPriceLabel;

@property (nonatomic, strong) UIView *numView;
@property (nonatomic, strong) UILabel *numNameLabel;
@property (nonatomic, strong) UILabel *numLabel;
@property (nonatomic, strong) UIButton *numAddBtn;
@property (nonatomic, strong) UIButton *numSubBtn;
@property (nonatomic, assign) NSInteger num;

@property (nonatomic, strong) UIButton *submintBtn;

@end

@implementation QWMallCarTConfirmController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openMeetOptionsTap:)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
    
    self.num = 1;
    
    [self bgView];
    
    [self picImageView];
    [self productNameLabel];
    [self realPriceLabel];
    [self originalPriceLabel];
    
    [self numView];
    [self numNameLabel];
    [self numAddBtn];
    [self numLabel];
    [self numSubBtn];
    
    [self submintBtn];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.04 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self show];
    });
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.equalTo(self.view.mas_bottom);
        }];
    }
    return _bgView;
}

#pragma mark - 商品图片
- (UIImageView *)picImageView{
    if(!_picImageView){
        _picImageView = [[UIImageView alloc]init];
        _picImageView.image =[UIImage imageNamed:@"dev_toothpaste.png"];
        _picImageView.layer.cornerRadius = 6.0;
        _picImageView.layer.masksToBounds = YES;
        [self.bgView addSubview:_picImageView];
        [_picImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView).offset(14.0);
            make.top.equalTo(self.bgView).offset(15.0);
            make.height.mas_equalTo(80.0);
            make.width.equalTo(_picImageView.mas_height);
        }];
    }
    return _picImageView;
}

#pragma mark - 商品名称
- (UILabel *)productNameLabel{
    if(!_productNameLabel){
        _productNameLabel = [[UILabel alloc]init];
        _productNameLabel.text = @"敖东大幽小幽抗菌肽口腔抑菌膏牙膏，护理抗菌男女成人清洁口气清新，专利抗菌肽，口腔深护理！";
        _productNameLabel.numberOfLines = 2;
        _productNameLabel.font = [UIFont systemFontOfSize:15 weight:(UIFontWeightRegular)];
        [self.bgView addSubview:_productNameLabel];
        [_productNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.picImageView.mas_right).offset(8.0);
            make.top.equalTo(self.picImageView.mas_top);
            make.right.equalTo(self.bgView).offset(-14.0);
        }];
    }
    return _productNameLabel;
}
#pragma mark - 价格
- (UILabel *)realPriceLabel{
    if(!_realPriceLabel){
        _realPriceLabel = [[UILabel alloc]init];
        _realPriceLabel.text = @"¥24.9";
        _realPriceLabel.numberOfLines = 1;
        _realPriceLabel.textColor = [UIColor colorFromHexString:@"#EB5D2A"];
        _realPriceLabel.font = [UIFont systemFontOfSize:15 weight:(UIFontWeightMedium)];
        [self.bgView  addSubview:_realPriceLabel];
        [_realPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.picImageView.mas_right).offset(8.0);
            make.bottom.equalTo(self.picImageView);
        }];
    }
    return _realPriceLabel;
}

- (UILabel *)originalPriceLabel{
    if(!_originalPriceLabel){
        _originalPriceLabel = [[UILabel alloc]init];
        _originalPriceLabel.text = @"¥99.9";
        _originalPriceLabel.numberOfLines = 1;
        _originalPriceLabel.textColor = [UIColor grayColor];
        _originalPriceLabel.font = [UIFont systemFontOfSize:12 weight:(UIFontWeightLight)];
        [self.bgView  addSubview:_originalPriceLabel];
        [_originalPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.realPriceLabel.mas_right).offset(2.0);
            make.bottom.equalTo(self.realPriceLabel);
        }];
        
        UIImageView *line = [[UIImageView alloc]init];
        line.backgroundColor = [UIColor grayColor];
        [_originalPriceLabel addSubview:line];
        
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_originalPriceLabel);
            make.right.equalTo(_originalPriceLabel);
            make.centerY.equalTo(_originalPriceLabel);
            make.height.mas_equalTo(0.5);
        }];
    }
    return _originalPriceLabel;
}

#pragma mark - 购买数量
- (UIView *)numView{
    if(!_numView){
        _numView = [[UIView alloc]init];
        [self.bgView addSubview:_numView];
        
        [_numView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.picImageView.mas_left);
            make.right.equalTo(self.bgView).offset(-14.0);
            make.top.equalTo(self.picImageView.mas_bottom).offset(25);
            make.height.mas_equalTo(30.0);
        }];
    }
    return _numView;
}
- (UILabel *)numNameLabel{
    if(!_numNameLabel){
        _numNameLabel = [[UILabel alloc]init];
        _numNameLabel.font = [UIFont boldSystemFontOfSize:15];
        _numNameLabel.text = @"购买数量";
        [self.numView addSubview:_numNameLabel];
        [_numNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.numView);
            make.bottom.equalTo(self.numView);
            make.left.equalTo(self.numView);
        }];
    }
    return _numNameLabel;
}

- (UIButton *)numAddBtn{
    if(!_numAddBtn){
        _numAddBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_numAddBtn setTitle:@"+" forState:UIControlStateNormal];
        [_numAddBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _numAddBtn.layer.borderWidth = 0.5;
        _numAddBtn.layer.borderColor = [UIColor blackColor].CGColor;
        [_numAddBtn addTarget:self action:@selector(numChangeOnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.numView addSubview:_numAddBtn];
        [_numAddBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.numView);
            make.top.equalTo(self.numView);
            make.bottom.equalTo(self.numView);
            make.width.equalTo(_numAddBtn.mas_height);
        }];
    }
    return _numAddBtn;
}

- (UILabel *)numLabel{
    if(!_numLabel){
        _numLabel = [[UILabel alloc]init];
        _numLabel.textColor = [UIColor grayColor];
        _numLabel.font = [UIFont systemFontOfSize:17];
        _numLabel.text = [NSString stringWithFormat:@"%ld",self.num];
        _numLabel.textAlignment = NSTextAlignmentCenter;
        [self.numView addSubview:_numLabel];
        [_numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.numView);
            make.bottom.equalTo(self.numView);
            make.right.equalTo(self.numAddBtn.mas_left);
            make.width.mas_greaterThanOrEqualTo(40);
        }];
    }
    return _numLabel;
}

- (UIButton *)numSubBtn{
    if(!_numSubBtn){
        _numSubBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_numSubBtn setTitle:@"-" forState:UIControlStateNormal];
        [_numSubBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _numSubBtn.layer.borderWidth = 0.5;
        _numSubBtn.layer.borderColor = [UIColor blackColor].CGColor;
        [_numSubBtn addTarget:self action:@selector(numChangeOnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.numView addSubview:_numSubBtn];
        [_numSubBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.numLabel.mas_left);
            make.top.equalTo(self.numView);
            make.bottom.equalTo(self.numView);
            make.width.equalTo(_numSubBtn.mas_height);
        }];
    }
    return _numSubBtn;
}

- (void)numChangeOnClick:(UIButton *)sender{
    NSInteger changeNum = self.num;

    if(sender == self.numAddBtn) changeNum = changeNum +1;
    if(sender == self.numSubBtn) changeNum = changeNum -1;
    
    if(changeNum <1) changeNum = 1;
    if(changeNum >99) changeNum = 99;
    
    self.num = changeNum;
    
    self.numLabel.text = [NSString stringWithFormat:@"%ld",self.num];
}

#pragma mark - 加入购物车
- (UIButton *)submintBtn{
    if(!_submintBtn){
        _submintBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _submintBtn.backgroundColor = [UIColor orangeColor];
        [_submintBtn setTitle:@"加入购物车" forState:UIControlStateNormal];
        _submintBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        _submintBtn.layer.cornerRadius= 18.0;
        _submintBtn.layer.masksToBounds = YES;
        [_submintBtn addTarget:self action:@selector(submintClick) forControlEvents:UIControlEventTouchUpInside];
        [self.bgView addSubview:_submintBtn];
        
        [_submintBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView).offset(25);
            make.right.equalTo(self.bgView).offset(-25);
            make.top.equalTo(self.numView.mas_bottom).offset(25);
            make.bottom.equalTo(self.bgView).offset(-25);
            make.height.mas_equalTo(36.0);
        }];
    }
    return _submintBtn;
}
- (void)submintClick{
    [SWHUDUtil hideHudViewWithMessageSuperView:self.view withMessage:@"购物车添加成功～"];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self close];
    });
}


#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isDescendantOfView:self.bgView]) {
        return NO;
    }
    return YES;
}
#pragma mark - 手势 - 打开/关闭控制页面
- (void)openMeetOptionsTap:(UIGestureRecognizer *)ges{
    if (ges.state == UIGestureRecognizerStateEnded ) {
        [self close];
    }
}

#pragma mark - 动画弹出
- (void)show{
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
    // 告诉self.view约束需要更新
    [self.view setNeedsUpdateConstraints];
    // 调用此方法告诉self.view检测是否需要更新约束，若需要则更新，下面添加动画效果才起作用
    [self.view updateConstraintsIfNeeded];
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}
#pragma mark - 动画关闭
- (void)close{
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.top.equalTo(self.view.mas_bottom);
    }];
    
    // 告诉self.view约束需要更新
    [self.view setNeedsUpdateConstraints];
    // 调用此方法告诉self.view检测是否需要更新约束，若需要则更新，下面添加动画效果才起作用
    [self.view updateConstraintsIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:NO completion:nil];
    }];
}

@end
