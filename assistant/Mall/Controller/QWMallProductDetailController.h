//
//  QWMallProductDetailController.h
//  assistant
//
//  Created by qunai on 2024/3/29.
//

#import "QWBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWMallProductDetailController : QWBaseController
@property (nonatomic, strong) NSDictionary *dataDic;
@end

NS_ASSUME_NONNULL_END
