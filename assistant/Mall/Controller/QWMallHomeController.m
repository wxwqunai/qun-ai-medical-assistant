//
//  QWMallHomeController.m
//  assistant
//
//  Created by qunai on 2024/3/25.
//

#import "QWMallHomeController.h"
#import "QWMallNavView.h"
#import "QWMallMenusCell.h"
#import "QWMallProductCell.h"
#import "QWShopCartHomeController.h"
#import "QWMallHealthPopularizationController.h"
#import "QWmallMeetingController.h"
#import "QWMallProductDetailController.h"

@interface QWMallHomeController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showDataArr;
@property (nonatomic, strong) QWMallNavView *navView;
@end

@implementation QWMallHomeController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"商城";
    
    [self navView];
    [self tableView];
    [self loadData];
}

#pragma mark - 导航
- (QWMallNavView *)navView{
    if(!_navView){
        _navView = [[QWMallNavView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, [UIDevice navigationFullHeight])];
        [self.view addSubview:_navView];
        __weak __typeof(self) weakSelf = self;
        _navView.shopCartBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf jumpToShopCart];
        };
    }
    return _navView;
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        //        [_tableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
        //        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
        [_tableView registerClass:[QWMallMenusCell class] forCellReuseIdentifier:@"QWMallMenusCell"];
        [_tableView registerClass:[QWMallProductCell class] forCellReuseIdentifier:@"QWMallProductCell"];

        
        _tableView.backgroundColor=Color_TableView_Gray;
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        //        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        //        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
                
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.navView.mas_bottom);
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.view).offset(-[UIDevice tabBarFullHeight]);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0000001;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger num = 0;
    if (section == 0) num = 1;
    if (section == 1) num = self.showDataArr.count;
    return num;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        QWMallMenusCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWMallMenusCell"];
        __weak __typeof(self) weakSelf = self;
        cell.menusSelectBlock = ^(NSInteger itemNum) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if (itemNum == 0) {
                [strongSelf jumpToAcademicConference];
            }else if(itemNum == 1){
                [strongSelf jumpToHealthPopularization];
            }else if(itemNum == 2){
                [strongSelf jumpToNearbySupermarket];
            }
        };
        return cell;
    }
    
    if (indexPath.section == 1) {
        QWMallProductCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWMallProductCell"];
        NSDictionary *dic = self.showDataArr[indexPath.row];
        cell.dataDic = dic;
        return cell;
    }
    
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"cell"];
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *dic = self.showDataArr[indexPath.row];
    [self jumpToProjectDetail:dic];
}



#pragma mark - 加载数据
- (void)loadData{
    self.showDataArr = [NSMutableArray arrayWithArray:@[
        @{@"productName":@"敖东大幽小幽抗菌肽口腔抑菌膏牙膏，护理抗菌男女成人清洁口气清新，专利抗菌肽，口腔深护理！",@"pic":@"dev_toothpaste.png",@"state":@"本店牙膏热销第一名",@"price":@"¥24.9",@"sellNum":@"100+人付款 包邮",@"type":@[@"先用后付",@"赠送费险",@"包邮"]},
        @{@"productName":@"曲特恪 盐酸曲唑酮缓释片",@"pic":@"dev_quteke.png",@"state":@"本店热销第一名",@"price":@"¥135",@"sellNum":@"1000+人付款 包邮",@"type":@[@"先用后付",@"赠送费险",@"包邮"]},
        @{@"productName":@"速乐涓 蛇毒血凝酶注射液",@"pic":@"dev_sulejuan.png",@"state":@"本店热销第一名",@"price":@"¥42.5",@"sellNum":@"1000+人付款 包邮",@"type":@[@"先用后付",@"赠送费险",@"包邮"]},
        @{@"productName":@"盐酸伊伐布雷定片(可兰特)",@"pic":@"dev_yifabuleiding.png",@"state":@"本店热销第一名",@"price":@"¥88.9",@"sellNum":@"1000+人付款 包邮",@"type":@[@"先用后付",@"赠送费险",@"包邮"]},
        @{@"productName":@"憩昙平 阿齐沙坦片",@"pic":@"dev_qitanping.png",@"state":@"本店热销第一名",@"price":@"¥61",@"sellNum":@"100+人付款 包邮",@"type":@[@"先用后付",@"赠送费险",@"包邮"]},
    ]];
    [self.tableView reloadData];
}

#pragma mark - 下拉刷新
- (void)loadPullDownRefreshDataInfo{
    [_tableView endWRefreshing];
}

#pragma mark - 商品详情 - 跳转
- (void)jumpToProjectDetail:(NSDictionary *)dic{
    QWMallProductDetailController *projectDetailVC = [[QWMallProductDetailController alloc]init];
    projectDetailVC.dataDic = dic;
    projectDetailVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:projectDetailVC animated:YES];
}

#pragma mark - 购物车 - 跳转
- (void)jumpToShopCart{
    QWShopCartHomeController *shopCartVC = [[QWShopCartHomeController alloc]init];
    shopCartVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:shopCartVC animated:YES];
}

#pragma mark - 学术会议 - 跳转
- (void)jumpToAcademicConference{
    QWmallMeetingController *meetingVC = [[QWmallMeetingController alloc]init];
    meetingVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:meetingVC animated:YES];
}
#pragma mark - 健康科普 - 跳转
- (void)jumpToHealthPopularization{
    QWMallHealthPopularizationController *healthyVC = [[QWMallHealthPopularizationController alloc]init];
    healthyVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:healthyVC animated:YES];
}
#pragma mark - 附近商超 - 跳转
- (void)jumpToNearbySupermarket{
    [SWHUDUtil hideHudViewWithMessageSuperView:self.view withMessage:@"开发中，敬请期待"];
}

@end
