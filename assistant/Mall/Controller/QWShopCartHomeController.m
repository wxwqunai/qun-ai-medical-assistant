//
//  QWShopCartHomeController.m
//  assistant
//
//  Created by qunai on 2024/3/27.
//

#import "QWShopCartHomeController.h"
#import "QWMallSCHeaderView.h"
#import "QWMallSCCell.h"
#import "QWMallSCControlsView.h"
#import "QWMallModel.h"

@interface QWShopCartHomeController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showDataArr;
@property (nonatomic, strong) QWMallSCControlsView *controlsView;

@end

@implementation QWShopCartHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"购物车";
    
    [self controlsView];
    [self tableView];
    [self loadData];
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        [_tableView registerClass:[QWMallSCHeaderView class] forHeaderFooterViewReuseIdentifier:@"QWMallSCHeaderView"];
        [_tableView registerClass:[QWMallSCCell class] forCellReuseIdentifier:@"QWMallSCCell"];
        
        _tableView.backgroundColor=Color_TableView_Gray;
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        //        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        //        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        //        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        //        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.equalTo(self.view);
            make.bottom.equalTo(self.controlsView.mas_top);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.showDataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *arr = self.showDataArr[section];
    return arr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QWMallSCCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWMallSCCell"];
    NSArray *arr = self.showDataArr[indexPath.section];
    QWMallShopCartModel *model = arr[indexPath.row];
    cell.isChoose = model.isChoose;
    cell.realPriceLabel.text = [NSString stringWithFormat:@"¥%@",model.productPrice];
    cell.productNameLabel.text = model.productName;
    cell.picImageView.image =[UIImage imageNamed:model.productPic];
    cell.num = model.count;
    cell.chooseBlock = ^{
        model.isChoose = !model.isChoose;
        [tableView reloadData];
        [self checkIsAllChoose];
    };
    cell.numChangeBlock = ^(NSInteger num) {
        model.count = num;
        [tableView reloadData];
        [self checkIsAllChoose];
    };
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    QWMallSCHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"QWMallSCHeaderView"];
    QWMallShopCartModel *model = self.showDataArr[section][0];
    headerView.storeNameLabel.text = model.storeName;
    return headerView;
}

#pragma mark - 底部操作
- (QWMallSCControlsView *)controlsView{
    if(!_controlsView){
        _controlsView = [[QWMallSCControlsView alloc]init];
        [self.view addSubview:_controlsView];
        
        [_controlsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.view);
            make.height.mas_equalTo([UIDevice tabBarFullHeight]);
        }];
        
        __weak __typeof(self) weakSelf = self;
        _controlsView.submitBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf submit];
        };
        
        _controlsView.chooseAllBlock = ^(BOOL isChooseAll) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf chooseAll:isChooseAll];
        };
    }
    return _controlsView;
}

- (void)submit{
    [SWHUDUtil hideHudViewWithMessageSuperView:self.view withMessage:@"结算成功"];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController popToRootViewControllerAnimated:YES];
    });
}

#pragma mark -  全选 - 操作
- (void)chooseAll:(BOOL)isChooseAll{
    
    CGFloat __block priceAll = 0;
    NSInteger __block isChooseCount = 0;
    [self.showDataArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSArray *arr = (NSArray *)obj;
        [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            QWMallShopCartModel *model = (QWMallShopCartModel *)obj;
            model.isChoose = isChooseAll;
            if(model.isChoose){
                CGFloat price = [model.productPrice floatValue] * model.count;
                priceAll = priceAll +price;
                isChooseCount = isChooseCount+1;
            }
        }];
    }];
    [self.tableView reloadData];
    self.controlsView.totalLabel.text = [NSString stringWithFormat:@"¥%.2f",priceAll];
    self.controlsView.chooseCount = isChooseCount;
}

#pragma mark - 检查是否全选/合计选中价格
- (void)checkIsAllChoose{
    BOOL __block isChooseAll = YES;
    CGFloat __block priceAll = 0;
    NSInteger __block isChooseCount = 0;
    [self.showDataArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSArray *arr = (NSArray *)obj;
        [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            QWMallShopCartModel *model = (QWMallShopCartModel *)obj;
            if(!model.isChoose){
                isChooseAll = NO;
            }else{
                CGFloat price = [model.productPrice floatValue] * model.count;
                priceAll = priceAll +price;
                isChooseCount = isChooseCount+1;
            }
        }];
    }];
    
    self.controlsView.isChooseAll = isChooseAll;
    self.controlsView.totalLabel.text = [NSString stringWithFormat:@"¥%.2f",priceAll];
    self.controlsView.chooseCount = isChooseCount;
}


#pragma mark - 加载数据
- (void)loadData{
    NSArray *arr = @[@[@{@"storeName":@"群爱智慧医疗",@"productName":@"敖东大幽小幽抗菌肽口腔抑菌膏牙膏，护理抗菌男女成人清洁口气清新，专利抗菌肽，口腔深护理！",@"productPrice":@"19.9",@"productPic":@"dev_toothpaste.png",@"count":@2,@"isChoose":@NO},@{@"storeName":@"群爱智慧医疗",@"productName":@"敖东大幽小幽抗菌肽口腔抑菌膏牙膏，护理抗菌男女成人清洁口气清新，专利抗菌肽，口腔深护理！",@"productPrice":@"24.9",@"productPic":@"dev_toothpaste.png",@"count":@1,@"isChoose":@NO}]];
    self.showDataArr = [QWMallShopCartModel mj_objectArrayWithKeyValuesArray:arr];
    [self.tableView reloadData];
}



@end
