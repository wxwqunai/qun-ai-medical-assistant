//
//  QWMallProductDetailController.m
//  assistant
//
//  Created by qunai on 2024/3/29.
//

#import "QWMallProductDetailController.h"
#import "QWMallProductControlsView.h"
#import "QWShopCartHomeController.h"
#import "QWAddressHomeController.h"
#import "QWMallProductlBanner.h"
#import "QWMallProductInfoCell.h"
#import "QWMallProductPicCell.h"
#import "QWMallProductAddressCell.h"
#import "QWProductDetailHeaderView.h"
#import "QWMallOrderConfirmController.h"
#import "QWMallCarTConfirmController.h"

@interface QWMallProductDetailController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showDataArr;

@property (nonatomic,strong) QWMallProductControlsView *controlsView;
@property (nonatomic,strong) QWMallProductlBanner *bannerView;

@property (nonatomic, strong) NSArray *picArr;
@end

@implementation QWMallProductDetailController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden = YES;
    
    [self customNavigationBarWithBgColor:[UIColor clearColor] withTitleColor:[UIColor clearColor]];
    self.leftBarButtonItem.tintColor = [UIColor blackColor];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"商品详情";

    [self controlsView];
    [self tableView];
    [self loadData];
}

- (QWMallProductlBanner *)bannerView{
    if(!_bannerView){
        _bannerView =[[QWMallProductlBanner alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenWidth)];
        _bannerView.showArr = @[@"dev_product_1.png",@"dev_product_2.png",@"dev_product_3.png",@"dev_product_4.png",@"dev_product_5.png"];
//        __weak __typeof(self) weakSelf = self;
        _bannerView.bannerClickBlock = ^(NSInteger index) {
//            __strong __typeof(weakSelf)strongSelf = weakSelf;
        };
    }
    return _bannerView;
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        //        [_tableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
        [_tableView registerClass:[QWProductDetailHeaderView class] forHeaderFooterViewReuseIdentifier:@"QWProductDetailHeaderView"];
        [_tableView registerClass:[QWMallProductInfoCell class] forCellReuseIdentifier:@"QWMallProductInfoCell"];
        [_tableView registerClass:[QWMallProductPicCell class] forCellReuseIdentifier:@"QWMallProductPicCell"];
        [_tableView registerClass:[QWMallProductAddressCell class] forCellReuseIdentifier:@"QWMallProductAddressCell"];
        
        _tableView.backgroundColor=Color_TableView_Gray;
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        //        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        //        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        //        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        _tableView.tableHeaderView = [self bannerView];
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.equalTo(self.view);
            make.bottom.equalTo(self.controlsView.mas_top);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat height = 0.0000001;
    if(section == 2) height = 40;
    return height;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger num = 0;
    if(section == 0 || section == 1) num = 1;
    if(section == 2) num = self.picArr.count;
    return num;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        QWMallProductInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWMallProductInfoCell"];
        cell.priceLabel.text = @"¥24.9";
        cell.sellNumLabel.text = @"已售100+";
        cell.nameLabel.text = @"敖东大幽小幽抗菌肽口腔抑菌膏牙膏，护理抗菌男女成人清洁口气清新，专利抗菌肽，口腔深护理！";
        cell.stateLabel.text = @"本店牙膏热销第一名";
        return cell;
    }
    if(indexPath.section == 1){
        QWMallProductAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWMallProductAddressCell"];
        return cell;
    }
    
    if(indexPath.section == 2){
        QWMallProductPicCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWMallProductPicCell"];
        NSString *picStr = self.picArr[indexPath.row];
        cell.picImageView.image = [UIImage imageNamed:picStr];
        return cell;
    }
    
    
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"cell"];
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.section == 1){
        [self jumpToaddressVC];
    }
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    QWMallProductlBanner *bannerView =[[QWMallProductlBanner alloc]init];
//    bannerView.showArr = @[@"dev_product_1.png",@"dev_product_2.png",@"dev_product_3.png",@"dev_product_4.png",@"dev_product_5.png"];
//    __weak __typeof(self) weakSelf = self;
//    bannerView.bannerClickBlock = ^(NSInteger index) {
//        __strong __typeof(weakSelf)strongSelf = weakSelf;
//    };
//    return bannerView;
    
    if(section == 2){
        QWProductDetailHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"QWProductDetailHeaderView"];
        return headerView;
    }
    
    return [UIView new];
}

#pragma mark - 加载数据
- (void)loadData{
    
    self.picArr = @[@"dev_IMG_4352.png",@"dev_IMG_4353.png",@"dev_IMG_4354.png",@"dev_IMG_4355.png",@"dev_IMG_4356.png",@"dev_IMG_4357.png",@"dev_IMG_4358.png",@"dev_IMG_4359.png",@"dev_IMG_4360.png",@"dev_IMG_4361.png",@"dev_IMG_4362.png"];
    [self.tableView reloadData];
}


#pragma mark - 底部操作 - 购物车、客服、加入购物车、立即购买
- (QWMallProductControlsView *)controlsView{
    if(!_controlsView){
        _controlsView = [[QWMallProductControlsView alloc]init];
        [self.view addSubview:_controlsView];
        
        [_controlsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.view);
            make.height.mas_equalTo([UIDevice tabBarFullHeight]);
        }];
        
        __weak __typeof(self) weakSelf = self;
        _controlsView.shopCartBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf jumpToShopCartVC];
        };
        
        _controlsView.storeBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf jumpToStoreVC];
        };
        
        _controlsView.customerServiceBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf jumpTocustomerServiceVC];
        };
        
        _controlsView.addToCartBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf jumpToAddToCartVC];
        };
        
        _controlsView.buyBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf jumpToBuyVC];
        };
    }
    return _controlsView;
}


#pragma mark - 购物车 - 跳转
- (void)jumpToShopCartVC{
    QWShopCartHomeController *shopCartVC = [[QWShopCartHomeController alloc]init];
    shopCartVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:shopCartVC animated:YES];
}

#pragma mark - 店铺 - 跳转
- (void)jumpToStoreVC{
    [SWHUDUtil hideHudViewWithMessageSuperView:self.view withMessage:@"开发中，敬请期待"];
}

#pragma mark - 客服 - 跳转
- (void)jumpTocustomerServiceVC{
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"mqq://"]]){
        NSURL *url = [NSURL URLWithString:CustomerService_OpenQQ_Url];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {
            [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
        }
    }else{
        [SWHUDUtil showHudViewTipInSuperView:self.view withMessage:@"请检查是否安装QQ软件"];
    }
}
#pragma mark - 加入购物车 - 跳转
- (void)jumpToAddToCartVC{
    QWMallCarTConfirmController *cartConfirmVC = [[QWMallCarTConfirmController alloc]init];
    cartConfirmVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:cartConfirmVC animated:NO completion:nil];
}

#pragma mark - 立即购买 - 跳转
- (void)jumpToBuyVC{
    QWMallOrderConfirmController *orderConfirmVC = [[QWMallOrderConfirmController alloc]init];
    [self.navigationController pushViewController:orderConfirmVC animated:YES];
}

#pragma mark - 收货地址 - 跳转
-  (void)jumpToaddressVC{
    if(![AppProfile isLoginedAlert]) return;
    QWAddressHomeController *addressVC = [[QWAddressHomeController alloc]init];
    addressVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:addressVC animated:YES];
}

@end
