//
//  QWMallHealthPopularizationController.m
//  assistant
//
//  Created by qunai on 2024/3/28.
//

#import "QWMallHealthPopularizationController.h"
#import "QWMallHealthyCell.h"

@interface QWMallHealthPopularizationController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showDataArr;
@end

@implementation QWMallHealthPopularizationController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"健康科普";
    
    [self tableView];
    [self loadData];
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        //        [_tableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
        //        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
        [_tableView registerClass:[QWMallHealthyCell class] forCellReuseIdentifier:@"QWMallHealthyCell"];
        
        _tableView.backgroundColor=Color_TableView_Gray;
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        //        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        //        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        //        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        //        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0000001;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.showDataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QWMallHealthyCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWMallHealthyCell"];
    NSDictionary *dic = self.showDataArr[indexPath.row];
    cell.dateDic = dic;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



#pragma mark - 加载数据
- (void)loadData{
    self.showDataArr = [NSMutableArray arrayWithArray:@[
                        @{@"name":@"你每天都刷对了吗？",@"pic":@"dev_healthy",@"date":@"2023-09-20",@"type":@"video",@"videoTime":@"01:30"},
                        @{@"name":@"正确的刷牙",@"pic":@"dev_healthy",@"date":@"2023-09-19",@"type":@"article",@"videoTime":@""},
                        @{@"name":@"牙膏的选择",@"pic":@"dev_healthy",@"date":@"2023-09-19",@"type":@"video",@"videoTime":@"03:25"},
                        @{@"name":@"幽门螺旋杆菌",@"pic":@"dev_healthy",@"date":@"2023-03-12",@"type":@"article",@"videoTime":@""}]];
    [self.tableView reloadData];
}


@end
