//
//  QWMallOrderConfirmController.m
//  assistant
//
//  Created by qunai on 2024/4/2.
//

#import "QWMallOrderConfirmController.h"
#import "QWMallOCControlsView.h"
#import "QWMallOCAddressCell.h"
#import "QWMallOCProductCell.h"
#import "QWMallOCCell.h"
#import "QWMallOCHeaderView.h"
#import "QWMallOCPayChooseCell.h"
#import "QWAddressHomeController.h"

@interface QWMallOrderConfirmController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) QWMallOCControlsView *controlsView;
@property (nonatomic, strong) NSMutableArray *showDataArr;
@property (nonatomic, strong) NSArray *priceDetailArr;
@property (nonatomic, strong) NSArray *payChooseArr;

@property (nonatomic, assign) NSInteger paySelect_row;
@end

@implementation QWMallOrderConfirmController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"确认订单";
    
    [self controlsView];
    [self tableView];
    [self loadData];
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        [_tableView registerClass:[QWMallOCHeaderView class] forHeaderFooterViewReuseIdentifier:@"QWMallOCHeaderView"];
        //        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
        [_tableView registerClass:[QWMallOCAddressCell class] forCellReuseIdentifier:@"QWMallOCAddressCell"];
        [_tableView registerClass:[QWMallOCProductCell class] forCellReuseIdentifier:@"QWMallOCProductCell"];
        [_tableView registerClass:[QWMallOCCell class] forCellReuseIdentifier:@"QWMallOCCell"];
        [_tableView registerClass:[QWMallOCPayChooseCell class] forCellReuseIdentifier:@"QWMallOCPayChooseCell"];
        
        _tableView.backgroundColor= Color_TableView_Gray;
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        //        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        //        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        //        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        //        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.equalTo(self.view);
            make.bottom.equalTo(self.controlsView.mas_top);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    CGFloat height = 0.0000001;
    if(section ==1 ) height = 10;
    return height;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat height = 10;
    if(section ==2) height = 30;
    return height;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger num = 0;
    if(section == 0 ||section ==1) num =1;
    if(section == 2) num = self.priceDetailArr.count;
    if(section == 3) num = self.payChooseArr.count;
    return num;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        QWMallOCAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWMallOCAddressCell"];
        return cell;
    }
    
    if (indexPath.section == 1) {
        QWMallOCProductCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWMallOCProductCell"];
        cell.storeNameLabel.text = @"群爱智慧医疗";
        cell.numLabel.text = @"x1";
        cell.picImageView.image =[UIImage imageNamed:@"dev_toothpaste.png"];
        cell.productNameLabel.text = @"敖东大幽小幽抗菌肽口腔抑菌膏牙膏，护理抗菌男女成人清洁口气清新，专利抗菌肽，口腔深护理！";
        cell.realPriceLabel.text = @"¥24.9";
        cell.originalPriceLabel.text = @"¥99.9";
        return cell;
    }
    
    if (indexPath.section == 2) {
        QWMallOCCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWMallOCCell"];
        NSDictionary *dic = self.priceDetailArr[indexPath.row];
        cell.nameLabel.text =dic[@"name"];
        cell.desLabel.text = dic[@"desc"];
        return cell;
    }
    
    if (indexPath.section == 3) {
        QWMallOCPayChooseCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWMallOCPayChooseCell"];
        NSDictionary *dic = self.payChooseArr[indexPath.row];
        cell.iconImageView.image = [UIImage imageNamed:dic[@"icon"]];
        cell.nameLabel.text = dic[@"name"];
        cell.isChoose = self.paySelect_row == indexPath.row;
        cell.chooseBlock = ^{
            self.paySelect_row = indexPath.row;
            [tableView reloadData];
        };
        return cell;
    }
    
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"cell"];
    }
    cell.textLabel.text =[NSString stringWithFormat:@"cell：%ld",indexPath.row];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(section == 2){
        QWMallOCHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"QWMallOCHeaderView"];
        headerView.nameLabel.text = @"价格明细";
        return headerView;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.section == 0){
        [self jumpToaddressVC];
    }
}

#pragma mark - 底部操作
- (QWMallOCControlsView *)controlsView{
    if(!_controlsView){
        _controlsView = [[QWMallOCControlsView alloc]init];
        _controlsView.totalLabel.text = @"¥24.9";
        [self.view addSubview:_controlsView];
        
        [_controlsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.view);
            make.height.mas_equalTo([UIDevice tabBarFullHeight]);
        }];
        
        __weak __typeof(self) weakSelf = self;
        _controlsView.submitBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf submit];
        };
    }
    return _controlsView;
}

#pragma mark - 加载数据
- (void)loadData{
    self.priceDetailArr = @[@{@"name":@"商品金额",@"desc":@"¥24.9"},@{@"name":@"配送费",@"desc":@"免运费"},@{@"name":@"配送方式",@"desc":@"物流派送"}];
    self.payChooseArr = @[@{@"icon":@"mall_zhifubao",@"name":@"支付宝"},@{@"icon":@"mall_weixin",@"name":@"微信"},@{@"icon":@"mall_yinhangka",@"name":@"银行卡"}];
    [self.tableView reloadData];
    
}

#pragma mark - 提交订单
- (void)submit{
    NSDictionary *payChooseDic = self.payChooseArr[self.paySelect_row];
    NSString *payNameStr = payChooseDic[@"name"];
    
    [SWHUDUtil hideHudViewWithMessageSuperView:self.view withMessage:[NSString stringWithFormat:@"%@支付成功！",payNameStr]];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController popToRootViewControllerAnimated:YES];
    });
}

#pragma mark - 收货地址 - 跳转
-  (void)jumpToaddressVC{
    if(![AppProfile isLoginedAlert]) return;
    QWAddressHomeController *addressVC = [[QWAddressHomeController alloc]init];
    addressVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:addressVC animated:YES];
}


@end
