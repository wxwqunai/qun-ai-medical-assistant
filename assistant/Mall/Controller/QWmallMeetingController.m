//
//  QWmallMeetingController.m
//  assistant
//
//  Created by qunai on 2024/3/28.
//

#import "QWmallMeetingController.h"
#import "QWMallMeetingCell.h"

@interface QWmallMeetingController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showDataArr;
@end

@implementation QWmallMeetingController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"学术会议";
    
    [self tableView];
    [self loadData];
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        //        [_tableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
        //        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
        [_tableView registerClass:[QWMallMeetingCell class] forCellReuseIdentifier:@"QWMallMeetingCell"];
        
        _tableView.backgroundColor=Color_TableView_Gray;
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
//        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        //        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        //        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        //        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0000001;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.showDataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QWMallMeetingCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWMallMeetingCell"];
    NSDictionary *dic = self.showDataArr[indexPath.row];
    cell.picImageView.image = [UIImage imageNamed:dic[@"pic"]];
    cell.nameLabel.text =dic[@"name"];
    cell.dateLabel.text =dic[@"date"];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



#pragma mark - 加载数据
- (void)loadData{
    self.showDataArr = [NSMutableArray arrayWithArray:@[
                        @{@"name":@"健齿中国行-口腔健康大会",@"pic":@"dev_meeting.png",@"date":@"2023-09-20"},
                        @{@"name":@"2023年健康口腔大会",@"pic":@"dev_meeting.png",@"date":@"2023-09-19"},
                        @{@"name":@"牙病防治大会",@"pic":@"dev_meeting.png",@"date":@"2023-09-19"},
                        @{@"name":@"中华口腔医学会召开第十六次专委会（分会）工作会议",@"pic":@"dev_meeting.png",@"date":@"2023-03-12"}]];
    [self.tableView reloadData];
}


@end
