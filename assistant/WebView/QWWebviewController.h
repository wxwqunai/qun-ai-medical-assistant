//
//  QWWebviewController.h
//  qwm
//
//  Created by kevin on 2023/3/30.
//

#import "QWBaseController.h"
#import <WebKit/WebKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface QWWebviewController : QWBaseController
@property (nonatomic,copy) NSString *url;
@property (nonatomic,strong) WKWebView *webView;

- (NSString *)wrapUrl;
@end

NS_ASSUME_NONNULL_END
