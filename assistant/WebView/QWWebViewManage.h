//
//  QWWebViewManage.h
//  assistant
//
//  Created by qunai on 2023/9/27.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWWebViewManage : NSObject
+(instancetype)sharedInstance;

#pragma mark - 拦截/解析数据
- (NSDictionary *)handleURLData:(NSURL*)objURL;
- (void)handleURLAndjump:(NSURL*)objURL;

//快速视频会议
- (void)jumpToMeetQuickJoinInfo:(NSString*)url;
@end

NS_ASSUME_NONNULL_END
