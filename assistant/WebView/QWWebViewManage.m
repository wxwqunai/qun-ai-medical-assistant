//
//  QWWebViewManage.m
//  assistant
//
//  Created by qunai on 2023/9/27.
//

#import "QWWebViewManage.h"
#import "EMChatViewController.h"
#import "QWMeetQuickJoinController.h"

@implementation QWWebViewManage

static QWWebViewManage *_sharedInstance = nil;
static dispatch_once_t once_token = 0;
+(instancetype)sharedInstance {
    dispatch_once(&once_token, ^{
        if (_sharedInstance == nil) {
            _sharedInstance = [[super allocWithZone:NULL] init] ;
        }
    });
    return _sharedInstance;
}




#pragma mark - 拦截/解析数据
-  (NSDictionary *)handleURLData:(NSURL*)objURL{
//    NSURL *objURL =navigationAction.request.URL;
    
    //url处理
    NSMutableDictionary *resuleDic = [NSMutableDictionary new];
    [resuleDic setObject:objURL.scheme ? objURL.scheme : @"" forKey:@"PROTOCOL"];
    [resuleDic setObject:objURL.host ? objURL.host : @"" forKey:@"HOST"];
    [resuleDic setObject:objURL.path ? objURL.path : @"" forKey:@"PATH"];
    [resuleDic setObject:objURL.fragment ? objURL.fragment : @"" forKey:@"FRAGMENT"];
    
    if(objURL.query.length > 0)
    {
        NSArray *queryList = [objURL.query componentsSeparatedByString:@"&"];
        [queryList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            NSString *oneQueryString = (NSString *)obj;
            if(oneQueryString.length > 0)
            {
                NSUInteger location = [oneQueryString rangeOfString:@"="].location;
                if (location != NSNotFound) {
                    NSString *key = [oneQueryString substringToIndex:location];
                    NSString *value = [oneQueryString substringFromIndex:location+1];
                    if (key.length>0 && value.length>0) {
                        NSString *decodeValue = [self decodeString:value].length>0 ? [self decodeString:value] : @"";
                        NSData *jsonData = [decodeValue dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                                            options:NSJSONReadingMutableContainers
                                                                              error:nil];
                        if(dic)[resuleDic setObject:dic forKey:@"PARAMS"];
                    }
                }
            }
        }];
    }
    NSLog(@"%@",resuleDic);

    return resuleDic;
}

- (void)handleURLAndjump:(NSURL*)objURL{
    NSDictionary * resuleDic = [self handleURLData:objURL];
    
    //进入聊天
    if([resuleDic[@"HOST"] isEqualToString:@"openIMChat"]){
        [self jumpToChat:resuleDic[@"PARAMS"][@"toID"]];
    }
    
    //文件下载
    if([resuleDic[@"HOST"] isEqualToString:@"downloadFile"]){
        [self downLoadFile:resuleDic[@"PARAMS"]];
    }
    
    //返回根目录
    if([resuleDic[@"HOST"] isEqualToString:@"webPageChange"]){
        [[[QWCommonMethod Instance] getCurrentVC].navigationController popToRootViewControllerAnimated:YES];
    }
}

#pragma mark - 聊天 - 跳转
- (void)jumpToChat:(NSString *)conversationId{
    if([AppProfile isLoginedAlert] && !IsStringEmpty(conversationId)){
        // ConversationId 接收消息方的环信ID:@"user2"
        // type 聊天类型:EMConversationTypeChat    单聊类型
        // createIfNotExist 如果会话不存在是否创建会话：YES
        [[UserInfoStore sharedInstance] fetchUserInfosFromServer:@[[conversationId lowercaseString]]];
         EMChatViewController *chatViewController = [[EMChatViewController alloc] initWithConversationId:conversationId conversationType:EMConversationTypeChat];
        chatViewController.hidesBottomBarWhenPushed = YES;
        [[[QWCommonMethod Instance] getCurrentVC].navigationController pushViewController:chatViewController animated:YES];
    }
}

#pragma mark - 快速视频会议
- (void)jumpToMeetQuickJoinInfo:(NSString*)url{
    NSDictionary * resuleDic = [QWCommonMethod parameterWithURL:url];
    NSString *toUserId = resuleDic[@"id"];
    if(IsStringEmpty(toUserId)) return;
    QWMeetQuickJoinController *quickJoinInfoVC = [[QWMeetQuickJoinController alloc]init];
    quickJoinInfoVC.toUserId = toUserId;
    quickJoinInfoVC.hidesBottomBarWhenPushed = YES;
    [[[QWCommonMethod Instance] getCurrentVC].navigationController pushViewController:quickJoinInfoVC animated:YES];
}

#pragma mark - 文件下载
- (void)downLoadFile:(NSDictionary *)fileDic{
    NSString *fileUrl = [self encodeString:fileDic[@"file"]];
    NSString *fileName = fileDic[@"fileName"];
    if(IsStringEmpty(fileUrl)) return;
    
    [SWHUDUtil showHudViewInSuperView:[[QWCommonMethod Instance] getCurrentVC].view withMessage:@"开始下载"];
    [HXGetRequest downloadPach:fileUrl withLocalPath:nil withPress:^(NSProgress * _Nonnull progress) {
        //需要在主线程中更新下载进度
        dispatch_async(dispatch_get_main_queue(), ^{
            [SWHUDUtil updateLabelText:progress.localizedDescription];
        });
    } withSuccess:^(__kindof HXRequest * _Nonnull request, NSDictionary * _Nonnull result, BOOL success) {
        [SWHUDUtil hideHudView];
        NSURL *cachePath = (NSURL *)request.responseObject;
        [[QWCommonMethod Instance] sharedFromSystem:fileName withUrl:cachePath withImage:nil success:^{
            [self saveFileSuccessAlert];
        } failure:^{
            NSLog(@"文件失败");
        }];
    } failure:^(__kindof HXRequest * _Nonnull request, NSString * _Nonnull errorInfo) {
        [SWHUDUtil hideHudView];
    }];
}

//系统文件存储成功
- (void)saveFileSuccessAlert{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"存储成功" message:@"请前往手机软件“文件”查看,或是否打开文件所在地?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *sureAction = [UIAlertAction actionWithTitle:@"是" style: UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[QWCommonMethod Instance] openSystemAppFile];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"否" style: UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alertController addAction:sureAction];
    [alertController addAction:cancelAction];
    [[[QWCommonMethod Instance] getCurrentVC] presentViewController:alertController animated:YES completion:nil];
}


#pragma mark - 编码
- (NSString *)encodeString:(NSString *)string{
    NSString *encodeString = string;
    if([[QWCommonMethod Instance] isHasChineseWithStr:string]){//是否包含中文
        encodeString = [string stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    }
    return encodeString;
}
#pragma mark - 解码
- (NSString *)decodeString:(NSString *)string{
    NSString *decodeString = string.stringByRemovingPercentEncoding;
    return decodeString;
}





@end
