//
//  QWWebviewController.m
//  qwm
//
//  Created by kevin on 2023/3/30.
//

#import "QWWebviewController.h"
#import "QWWebViewManage.h"
#import "YCXMenu.h"
#import "QWShareHomeController.h"

#define USER_DETAIL  @"boke/user_findone.do" //用户详情

@interface QWWebviewController ()<WKNavigationDelegate>

@property (nonatomic,strong) UIProgressView *progressView;//进度条
@property (nonatomic,copy) NSString *curLoadUrl;
@end

@implementation QWWebviewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = YES;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.rightBarButtonItem setImage:[UIImage imageNamed:@"ic_web_more.png"]];
    [self webView];
    [self progressView];
}
#pragma mark-导航条左边按钮
-(void)navLeftBaseButtonClick
{
    if([self.webView canGoBack]){
        [self.webView goBack];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}
#pragma mark-导航条右边按钮
-(void)navRightBaseButtonClick
{
    if([YCXMenu isShow]){
        [YCXMenu dismissMenu];
        return;
    }
    
    NSMutableArray * menuArray = [[NSMutableArray alloc]initWithArray:@[[YCXMenuItem menuItem:@"回到首页" image:[UIImage imageNamed:@"ic_web_home.png"] tag:100 userInfo:nil]]];
    [menuArray addObject: [YCXMenuItem menuItem:@"分享" image:[UIImage imageNamed:@"ic_web_share.png"] tag:101 userInfo:nil]];
    //用户详情-添加视频通话
    if(!IsStringEmpty(self.url) && [self.url containsString:USER_DETAIL]) [menuArray addObject: [YCXMenuItem menuItem:@"视频通话" image:[UIImage imageNamed:@"ic_web_videomeeting.png"] tag:102 userInfo:nil]];
    
    CGRect rect=CGRectMake(kScreenWidth-25, 0, 0, 0);
    [YCXMenu setTintColor:Color_Main_Green];
    [YCXMenu setSelectedColor:Color_Main_Green];
    [YCXMenu setSeparatorColor:[UIColor whiteColor]];
    [YCXMenu showMenuInView:self.webView fromRect:CGRectMake(rect.origin.x, rect.origin.y+4, rect.size.width, rect.size.height) menuItems:menuArray selected:^(NSInteger index, YCXMenuItem *item) {
        if(index == 0){
            [self.navigationController popViewControllerAnimated:YES];
        }
        if(index ==1){
            [self fetchShareData];
        }
        if(index == 2){
            [[QWWebViewManage sharedInstance] jumpToMeetQuickJoinInfo:self.url];
        }
    }];
}

#pragma mark -网页
-(WKWebView *)webView
{
    if (!_webView) {
        _webView =[[WKWebView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
        _webView.navigationDelegate=self;
        [_webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];//监听进度条
        _webView.scrollView.showsVerticalScrollIndicator=NO;
        _webView.scrollView.showsHorizontalScrollIndicator=YES;
        NSURLRequest *request =[[NSURLRequest alloc]initWithURL:[NSURL URLWithString:[self wrapUrl]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:5.0];
        [_webView loadRequest:request];
//        [NSURLSession.sharedSession dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//            if (error == nil && data != nil && data.length > 0) {
////                self->_webView.hidden=NO;
//                [self->_webView loadRequest:request];
//            } else {
////                self->_webView.hidden=YES;
////                [SWHUDUtil hideHudView];
//            }
//        }];
        
        //如果你导入的MJRefresh库是最新的库，就用下面的方法创建下拉刷新和上拉加载事件
        _webView.scrollView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRefresh)];
//        _webView.scrollView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
        
        [self.view addSubview:_webView];
        
        if ([_webView.scrollView respondsToSelector:@selector(setContentInsetAdjustmentBehavior:)]) {
            _webView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        
        [_webView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
        }];
    }
    return _webView;
}

-  (void)headerRefresh{
    [_webView reload];
    [_webView.scrollView.mj_header endRefreshing];
}
// 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{
//    [SWHUDUtil showHudViewInSuperView:self.view withMessage:@""];
    self.curLoadUrl = webView.URL.absoluteString;
}
// 当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation
{
}
// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    self.title = webView.title;
//    [SWHUDUtil hideHudView];
}
// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation
{
//    [SWHUDUtil hideHudView];
}

- (void)webView:(WKWebView*)webView decidePolicyForNavigationAction:(WKNavigationAction*)navigationAction
decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    
    if([navigationAction.request.URL.absoluteString containsString:@"https://meeting.tencent"]){
        [[QWCommonMethod Instance] openAppExternalBrowser:navigationAction.request.URL.absoluteString];
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
    
    if([navigationAction.request.URL.absoluteString containsString:@"qalive.belovedlive.com"]){
        [[QWCommonMethod Instance] openAppExternalBrowser:navigationAction.request.URL.absoluteString];
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
    
    if([navigationAction.request.URL.absoluteString containsString:@"qab://"]){
        decisionHandler(WKNavigationActionPolicyCancel);
        [[QWWebViewManage sharedInstance] handleURLAndjump:navigationAction.request.URL];
        return;
    }

    decisionHandler(WKNavigationActionPolicyAllow);
}

#pragma mark - url
- (NSString *)wrapUrl{
    NSString *url = self.url;
    if(!IsStringEmpty(url) && [url containsString:QWM_DOMAIN]){
        NSString * timestamp = [[QWCommonMethod Instance]nowTimeInterval];
        url = [url stringByAppendingFormat:@"%@%@&timestamp=%@",[url containsString:@"?"]?@"&":@"?",@"from=app&operatingSystem=ios",timestamp];
//        url = [url stringByAppendingFormat:@"%@%@",[url containsString:@"?"]?@"&":@"?",@"from=app&operatingSystem=ios"];
        if(AppProfile.isLogined){
            url = [url stringByAppendingFormat:@"&autoTest=autoLogin&userName=%@&password=%@",AppProfile.qaUserInfo.phone,AppProfile.qaUserInfo.password];
        }
    }
    return url;
}


#pragma mark -进度条
-(UIProgressView *)progressView
{
    if (!_progressView) {
        _progressView=[[UIProgressView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 1)];
        _progressView.progressTintColor=[UIColor colorFromHexString:@"#97e070"];
        _progressView.trackTintColor=[UIColor whiteColor];
        [_webView addSubview:_progressView];
    }
    return _progressView;
}
// 计算wkWebView进度条
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (object == self.webView && [keyPath isEqualToString:@"estimatedProgress"]) {
        CGFloat newprogress = [[change objectForKey:NSKeyValueChangeNewKey] doubleValue];
        if (newprogress == 1) {
            [self.progressView setProgress:1 animated:NO];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.progressView.hidden = YES;
            });
        }else {
            [self.progressView setProgress:newprogress animated:YES];
            self.progressView.hidden = NO;
        }
    }
}

#pragma mark - 分享
- (void)fetchShareData{
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_share_config argument:@{}];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success1) {
        NSString *__block shareUrl = @"";
        NSArray *dataArr = (NSArray *)result;
        [dataArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSDictionary *dic = (NSDictionary *)obj;
            NSString *url =dic[@"url"];
            if([self.curLoadUrl containsString:url]){
                shareUrl = self.curLoadUrl;
                
                NSString *share_type =dic[@"share_type"];
                NSString *real_url = dic[share_type];
                if(!IsStringEmpty(real_url)){
                    shareUrl = [shareUrl stringByReplacingOccurrencesOfString:url withString:real_url];
                }
                
                *stop = YES;
            }
        }];
        
        if(!IsStringEmpty(shareUrl)){
            [self jumpToShare:shareUrl];
        }else{
            [SWHUDUtil hideHudViewWithMessageSuperView:self.view withMessage:@"当前页面暂不支持分享！"];
        }
        
        NSLog(@"");
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        //错误
        NSLog(@"%@",errorInfo);
    }];
}

#pragma mark - 分享选择
- (void)jumpToShare:(NSString *)shareUrl{
    QWShareHomeController *shareVC = [[QWShareHomeController alloc]init];
    shareVC.shareUrl = shareUrl;
    shareVC.shareTitle = self.title;
    shareVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:shareVC animated:NO completion:nil];
}


@end
