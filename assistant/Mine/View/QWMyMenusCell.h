//
//  QWMyMenusCell.h
//  assistant
//
//  Created by kevin on 2023/6/30.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN
typedef void (^MenuItemClickBlock)(QWMyMenuItem *menuItem);

@interface QWMyMenusCell : QWBaseTableViewCell
@property (nonatomic,copy) MenuItemClickBlock menuItemClickBlock;

- (void)loadData;
@end

NS_ASSUME_NONNULL_END
