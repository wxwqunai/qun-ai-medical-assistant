//
//  QWMineMenusCell.h
//  qwm
//
//  Created by kevin on 2023/3/26.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWMineMenusCell : QWBaseTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *desLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrImageView;

- (void)changeRightShow:(BOOL)isShowDes;
@end

NS_ASSUME_NONNULL_END
