//
//  QWAdsHomeCell.h
//  assistant
//
//  Created by kevin on 2024/4/6.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWAdsHomeCell : QWBaseTableViewCell
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *phoneLabel;
@property (nonatomic, strong) UILabel *defaultLabel;
@property (nonatomic, strong) UILabel *addressLabel;

@property (nonatomic, strong) UIButton *editedBtn;

@property (nonatomic, copy) void (^editedBlock)(void);
@end

NS_ASSUME_NONNULL_END
