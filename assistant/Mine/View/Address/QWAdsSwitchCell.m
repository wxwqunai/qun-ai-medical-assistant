//
//  QWAdsSwitchCell.m
//  assistant
//
//  Created by kevin on 2024/4/6.
//

#import "QWAdsSwitchCell.h"

@implementation QWAdsSwitchCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self nameLabel];
        [self customSwitch];
    }
    return self;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.font = [UIFont systemFontOfSize:16.0];
        [self.contentView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(14.0);
            make.top.equalTo(self.contentView).offset(20.0);
        }];
    }
    return _nameLabel;
}

- (UISwitch *)customSwitch{
    if(!_customSwitch){
        _customSwitch = [[UISwitch alloc]init];
        _customSwitch.transform = CGAffineTransformMakeScale(0.7, 0.7);
        _customSwitch.onTintColor = Color_Main_Green;
        _customSwitch.thumbTintColor = [UIColor whiteColor];
        _customSwitch.on = YES;
        [self.contentView addSubview:_customSwitch];
        
        [_customSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-14.0);
            make.centerY.equalTo(self.nameLabel);
            make.width.mas_equalTo(50);
        }];
    }
    return _customSwitch;
}

@end
