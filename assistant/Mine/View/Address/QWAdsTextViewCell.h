//
//  QWAdsTextViewCell.h
//  assistant
//
//  Created by kevin on 2024/4/6.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWAdsTextViewCell : QWBaseTableViewCell<UITextViewDelegate>
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UITextView *textView;
@property (nonatomic,strong) UILabel *placeholderLabel;
@end

NS_ASSUME_NONNULL_END
