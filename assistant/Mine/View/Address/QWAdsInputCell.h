//
//  QWAdsInputCell.h
//  assistant
//
//  Created by qunai on 2024/4/3.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWAdsInputCell : QWBaseTableViewCell
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UITextField *inputField;
@end

NS_ASSUME_NONNULL_END
