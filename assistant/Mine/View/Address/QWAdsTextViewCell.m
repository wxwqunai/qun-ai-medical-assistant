//
//  QWAdsTextViewCell.m
//  assistant
//
//  Created by kevin on 2024/4/6.
//

#import "QWAdsTextViewCell.h"

@implementation QWAdsTextViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self nameLabel];
        [self textView];
    }
    return self;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.font = [UIFont systemFontOfSize:16.0];
        [self.contentView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(14.0);
            make.top.equalTo(self.contentView).offset(20.0);
        }];
    }
    return _nameLabel;
}

-(UITextView *)textView
{
    if (!_textView) {
        _textView =[[UITextView alloc]init];
        _textView.text = @"";
        _textView.returnKeyType=UIReturnKeyDone;
        _textView.delegate=self;
        _textView.font=[UIFont fontWithName:@"PingFangSC-Medium" size:15];
        _textView.textColor=[UIColor colorFromHexString:@"#707070"];
        _textView.backgroundColor=[UIColor clearColor];
//        _textView.scrollEnabled = NO;
        
        _textView.layer.borderWidth=1;
        _textView.layer.borderColor=Color_Main_Green.CGColor;
        
        [self.contentView addSubview:_textView];
        
        [_textView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.nameLabel);
            make.right.equalTo(self.contentView.mas_right).offset(-14.0);
            make.top.equalTo(self.nameLabel.mas_bottom).offset(8.0);
            make.bottom.equalTo(self.contentView);
            make.height.mas_offset(120);
        }];
        
        _placeholderLabel =[[UILabel alloc]init];
        _placeholderLabel.numberOfLines = 0;
//        _placeholderLabel.hidden = !IsStringEmpty(self.answerModel.rengong1);
        _placeholderLabel.font = [UIFont systemFontOfSize:15];
        _placeholderLabel.textColor = [UIColor colorFromHexString:@"#CBCBCB"];
        [_textView addSubview:_placeholderLabel];
        
        [_placeholderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_textView).offset(4);
            make.right.equalTo(_textView).offset(-4.0);
            make.top.equalTo(_textView).offset(8);
        }];
    }
    return _textView;
}

#pragma mark- UITextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if(textView == _textView) _placeholderLabel.hidden = YES;
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    if(textView == _textView) _placeholderLabel.hidden = !IsStringEmpty(textView.text);
    return YES;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if( [ @"\n" isEqualToString: text]){
        //你的响应方法
        [self endEditing:YES];
        return NO;
    }
    
    if (range.length + range.location > textView.text.length) {
        return NO;
    }
    
    return YES;
}
@end
