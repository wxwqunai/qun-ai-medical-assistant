//
//  QWAdsInputCell.m
//  assistant
//
//  Created by qunai on 2024/4/3.
//

#import "QWAdsInputCell.h"

@interface QWAdsInputCell()
@property (nonatomic, strong) UILabel *starLabel;
@end

@implementation QWAdsInputCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self nameLabel];
        [self starLabel];
        [self inputField];
    }
    return self;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.font = [UIFont systemFontOfSize:16.0];
        [self.contentView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(14.0);
            make.top.equalTo(self.contentView).offset(20.0);
        }];
    }
    return _nameLabel;
}

- (UILabel *)starLabel{
    if(!_starLabel){
        _starLabel = [[UILabel alloc]init];
        _starLabel.textColor = [UIColor orangeColor];
        _starLabel.font = [UIFont systemFontOfSize:16.0];
        _starLabel.text = @"*";
        [self.contentView addSubview:_starLabel];
        
        [_starLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.nameLabel.mas_right);
            make.centerY.equalTo(self.nameLabel);
        }];
    }
    return _starLabel;
}

- (UITextField *)inputField{
    if(!_inputField){
        _inputField = [[UITextField alloc]init];
        _inputField.borderStyle = UITextBorderStyleNone;
        _inputField.font =[UIFont systemFontOfSize:16];
        
        //边框
        _inputField.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _inputField.layer.borderWidth = 0.5;
        
        //左边距
        _inputField.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, 0)];
        _inputField.leftViewMode = UITextFieldViewModeAlways;
        
        //清除样式
        _inputField.clearButtonMode = UITextFieldViewModeWhileEditing;
        
        [self.contentView addSubview:_inputField];
        
        [_inputField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.nameLabel);
            make.right.equalTo(self.contentView.mas_right).offset(-14.0);
            make.top.equalTo(self.nameLabel.mas_bottom).offset(8.0);
            make.bottom.equalTo(self.contentView);
            make.height.mas_equalTo(40.0);
        }];
    }
    return _inputField;
}



@end
