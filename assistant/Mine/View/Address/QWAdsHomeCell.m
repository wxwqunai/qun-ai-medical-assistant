//
//  QWAdsHomeCell.m
//  assistant
//
//  Created by kevin on 2024/4/6.
//

#import "QWAdsHomeCell.h"

@interface QWAdsHomeCell()
@property (nonatomic, strong) UIView *bgView;
@end
@implementation QWAdsHomeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];

        [self bgView];
        
        [self editedBtn];

        [self nameLabel];
        [self phoneLabel];
        [self defaultLabel];
        [self addressLabel];
    }
    return self;
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 6.0;
        _bgView.layer.masksToBounds = YES;
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView).with.insets(UIEdgeInsetsMake(5, 14, 5, 14));
            make.height.mas_greaterThanOrEqualTo(80);
        }];
    }
    return _bgView;
}

- (UIButton *)editedBtn{
    if(!_editedBtn){
        _editedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_editedBtn setImage:[UIImage imageNamed:@"mine_address_edited"] forState:UIControlStateNormal];
        [_editedBtn addTarget:self action:@selector(editedOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.bgView addSubview:_editedBtn];
        [_editedBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.bgView).offset(-6.0);
            make.centerY.equalTo(self.bgView);
        }];
    }
    return _editedBtn;
}
- (void)editedOnClick{
    if(self.editedBlock)self.editedBlock();
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.font = [UIFont boldSystemFontOfSize:16.0];
        [self.bgView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView).offset(8.0);
            make.top.equalTo(self.bgView).offset(10.0);
        }];
    }
    return _nameLabel;
}

- (UILabel *)phoneLabel{
    if(!_phoneLabel){
        _phoneLabel = [[UILabel alloc]init];
        _phoneLabel.textColor = [UIColor grayColor];
        _phoneLabel.font = [UIFont systemFontOfSize:16.0];
        [self.bgView addSubview:_phoneLabel];
        
        [_phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.nameLabel.mas_right).offset(8.0);
            make.centerY.equalTo(self.nameLabel);
        }];
    }
    return _phoneLabel;
}

- (UILabel *)defaultLabel{
    if(!_defaultLabel){
        _defaultLabel = [[UILabel alloc]init];
        _defaultLabel.text = @"默认";
        _defaultLabel.textColor = [UIColor whiteColor];
        _defaultLabel.font = [UIFont systemFontOfSize:13.0];
        _defaultLabel.backgroundColor = [UIColor blackColor];
        _defaultLabel.layer.cornerRadius = 10.0;
        _defaultLabel.layer.masksToBounds = YES;
        _defaultLabel.textAlignment = NSTextAlignmentCenter;
        [self.bgView addSubview:_defaultLabel];
        
        [_defaultLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.phoneLabel.mas_right).offset(8.0);
            make.centerY.equalTo(self.nameLabel);
            make.width.mas_equalTo(44);
            make.height.mas_equalTo(20);
        }];
    }
    return _defaultLabel;
}

- (UILabel *)addressLabel{
    if(!_addressLabel){
        _addressLabel = [[UILabel alloc]init];
        _addressLabel.textColor = [UIColor grayColor];
        _addressLabel.numberOfLines = 0;
        _addressLabel.font = [UIFont systemFontOfSize:16.0];
        [self.bgView addSubview:_addressLabel];
        
        [_addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.nameLabel);
            make.right.equalTo(self.bgView).offset(-20);
            make.top.equalTo(self.nameLabel.mas_bottom).offset(10);
            make.bottom.equalTo(self.bgView).offset(-10);
        }];
    }
    return _addressLabel;
}



@end
