//
//  QWAdsSwitchCell.h
//  assistant
//
//  Created by kevin on 2024/4/6.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWAdsSwitchCell : QWBaseTableViewCell
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UISwitch *customSwitch;

@end

NS_ASSUME_NONNULL_END
