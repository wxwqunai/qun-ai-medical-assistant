//
//  QWMineInfoView.h
//  qwm
//
//  Created by kevin on 2023/3/24.
//

#import <UIKit/UIKit.h>
#define Mine_Header_Height 220

NS_ASSUME_NONNULL_BEGIN

typedef void (^MineInfoBlock)(BOOL isLogin); //为登录

@interface QWMineInfoView : UIView

@property (nonatomic,copy)MineInfoBlock mineInfoBlock;

@end

NS_ASSUME_NONNULL_END
