//
//  QWMineInfoView.m
//  qwm
//
//  Created by kevin on 2023/3/24.
//

#import "QWMineInfoView.h"

@interface QWMineInfoView ()

@property (nonatomic, strong) UIImageView *bgImageView;

@property (nonatomic, strong) UIView *loginedView;
@property (nonatomic, strong) UIImageView *headImageView;
@property (nonatomic, strong) UIImageView *nextImageView;
@property (nonatomic, strong) UILabel *userNameLabel;//用户名
@property (nonatomic, strong) UILabel *userTypeLabel;
@property (nonatomic, strong) UILabel *attestationLabel; //是否已认证

@property (nonatomic, strong) UIButton *logRegButtun;

@end

@implementation QWMineInfoView

-(instancetype)init
{
    self =[super init];
    if (self) {
        self.backgroundColor = Color_Main_Green;
        self.clipsToBounds = YES;

        [self bgImageView];
        
        [self loginedView];
        [self logRegButtun];
        
        [self loginStateChangeNotification];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginStateChangeNotification) name:QWLogin_Change_State object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(qaUserInfoStateChangeNotification) name:QWUserInfo_Change_State object:nil];
    }
    return self;
}
#pragma mark - 背景图
- (UIImageView *)bgImageView{
    if(!_bgImageView){
        _bgImageView = [[UIImageView alloc]init];
        _bgImageView.image = [UIImage imageNamed:@"bg_header.png"];
        _bgImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_bgImageView];
        
        [_bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return _bgImageView;
}

#pragma mark - 已登录-view
- (UIView *)loginedView{
    if(!_loginedView){
        _loginedView = [[UIView alloc]init];
        _loginedView.backgroundColor = [UIColor clearColor];
        _loginedView.hidden = YES;
        [self addSubview:_loginedView];
        
        [_loginedView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.mas_equalTo(20);
            make.right.mas_equalTo(-20);
        }];
        
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickLoginedView)];
        [_loginedView addGestureRecognizer:singleTap];
        
        [self headImageView];
        [self userNameLabel];
        [self nextImageView];
        
        [self userTypeLabel];
        [self attestationLabel];
    }
    return _loginedView;
}
-  (void)clickLoginedView{
    if(self.mineInfoBlock) self.mineInfoBlock(YES);
}

#pragma mark - 已登录- 头像
- (UIImageView *)headImageView{
    if(!_headImageView){
        _headImageView = [[UIImageView alloc]init];
        _headImageView.tintColor = [UIColor whiteColor];
//        NSString *shichenName=[[QWCommonMethod Instance] calculateOneOfTheTwelveHourPeriods];
//        NSString *imageNameStr =[NSString stringWithFormat:@"qw_%@.png",shichenName];
//        UIImage *image =[UIImage imageNamed:imageNameStr];
//        _headImageView.image =image;
        _headImageView.layer.masksToBounds = YES;
        _headImageView.layer.cornerRadius = 40.0;
        [_loginedView addSubview:_headImageView];
        
        [_headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_loginedView);
            make.bottom.equalTo(_loginedView);
            make.left.equalTo(_loginedView);
            make.width.mas_equalTo(80);
            make.height.mas_equalTo(80);
        }];
    }
    return _headImageView;
}

#pragma mark - 已登录- 用户名
- (UILabel *)userNameLabel{
    if(!_userNameLabel){
        _userNameLabel = [[UILabel alloc]init];
        _userNameLabel.text =@"xxx";
        _userNameLabel.font =[UIFont boldSystemFontOfSize:24];
        _userNameLabel.textColor =[UIColor whiteColor];
        
        [_loginedView addSubview:_userNameLabel];
        
        [_userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_loginedView.mas_centerY).offset(-4.0);
            make.left.equalTo(_headImageView.mas_right).offset(8);
        }];
    }
    return _userNameLabel;
}

#pragma mark - 已登录- next
- (UIImageView *)nextImageView{
    if(!_nextImageView){
        _nextImageView = [[UIImageView alloc]init];
        UIImage *image =[UIImage imageNamed:@"next_white.png"];
        _nextImageView.image =image;
        [_loginedView addSubview:_nextImageView];
        
        [_nextImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_loginedView);
            make.right.equalTo(_loginedView);
        }];
    }
    return _nextImageView;
}

#pragma mark - 已登陆-用户类型
- (UILabel *)userTypeLabel{
    if(!_userTypeLabel){
        
        UIView *view = [[UIView alloc]init];
        view.layer.borderWidth = 1.0;
        view.layer.borderColor = [UIColor whiteColor].CGColor;
        view.layer.cornerRadius = 6.0;
        view.layer.masksToBounds = YES;
        [_loginedView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_loginedView.mas_centerY).offset(4.0);
            make.left.equalTo(_userNameLabel.mas_left);
            make.height.mas_offset(28.0);
        }];
        
        _userTypeLabel = [[UILabel alloc]init];
        _userTypeLabel.font =[UIFont systemFontOfSize:17];
        _userTypeLabel.textColor =[UIColor whiteColor];
        [view addSubview:_userTypeLabel];
        
        [_userTypeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(view);
            make.bottom.equalTo(view);
            make.left.equalTo(view).offset(10.0);
            make.right.equalTo(view).offset(-10.0);
        }];
    }
    return _userTypeLabel;
}

#pragma mark - 已登陆-认证状态
- (UILabel *)attestationLabel{
    if(!_attestationLabel){
        
        UIView *view = [[UIView alloc]init];
        view.layer.borderWidth = 1.0;
        view.layer.borderColor = [UIColor whiteColor].CGColor;
        view.layer.cornerRadius = 6.0;
        view.layer.masksToBounds = YES;
        [_loginedView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_userTypeLabel.mas_centerY);
            make.left.equalTo(_userTypeLabel.superview.mas_right).offset(8.0);
            make.height.equalTo(_userTypeLabel.mas_height);
        }];
        
        _attestationLabel = [[UILabel alloc]init];
        _attestationLabel.font =[UIFont systemFontOfSize:17];
        _attestationLabel.textColor =[UIColor whiteColor];
        [view addSubview:_attestationLabel];
        
        [_attestationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(view);
            make.bottom.equalTo(view);
            make.left.equalTo(view).offset(10.0);
            make.right.equalTo(view).offset(-10.0);
        }];
    }
    return _attestationLabel;
}

#pragma mark - 未登录
-(UIButton *)logRegButtun
{
    if (!_logRegButtun) {
        _logRegButtun =[UIButton buttonWithType:UIButtonTypeCustom];
        [_logRegButtun setTitle:@"登陆/注册" forState:UIControlStateNormal];
        [_logRegButtun setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _logRegButtun.titleLabel.font = [UIFont systemFontOfSize:24];
        _logRegButtun.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _logRegButtun.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 6);
        [_logRegButtun addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_logRegButtun];
        
        [_logRegButtun mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.mas_equalTo(20);
            make.width.equalTo(self).offset(-40);
        }];
    }
    return _logRegButtun;
}
-(void)buttonClick:(UIButton *)sender
{
    if(self.mineInfoBlock){
        self.mineInfoBlock(NO);
    }
}


#pragma mark - 接受通知

- (void)loginStateChangeNotification {
 
//    NSLog(@"接受通知==%@",AppProfile.qaUserInfo);
    
    if(AppProfile.qaUserInfo){
        self.loginedView.hidden =NO;
        self.logRegButtun.hidden = YES;
        
        [self userInfoDidChange];
    }else{
        self.loginedView.hidden = YES;
        self.logRegButtun.hidden = NO;
    }
}
- (void)qaUserInfoStateChangeNotification{
    [self userInfoDidChange];
}

#pragma mark - 用户信息改变
- (void)userInfoDidChange{
    if(AppProfile.qaUserInfo){
        QWqaUserModel *qaUserInfo = AppProfile.qaUserInfo;
        self.userNameLabel.text = qaUserInfo.name;
        
        
        NSDictionary *userTypeDic = @{
            @"assistant":@"医助",
            @"doctor":@"医生",
            @"patient":@"患者",
            @"admin":@"管理员"
        };
        self.userTypeLabel.text = userTypeDic[qaUserInfo.usertype];
        self.userTypeLabel.superview.hidden = IsStringEmpty(self.userTypeLabel.text);

        
        if(qaUserInfo.reserve1){
            self.attestationLabel.text =[qaUserInfo.reserve1.is_attestation isEqualToString:@"1"]?@"认证通过":@"待认证";
        }
        self.attestationLabel.superview.hidden = IsStringEmpty(self.attestationLabel.text);
        
        
        NSString *url = [QWM_BASE_API_URL stringByAppendingFormat:@"/bokeupload%@",qaUserInfo.photo];
        [_headImageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"ic_header_default.png"]];
    }
}


- (void)dealloc{
    //移除观察者 self
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
