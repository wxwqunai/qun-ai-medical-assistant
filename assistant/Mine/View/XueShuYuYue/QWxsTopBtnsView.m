//
//  QWxsTopBtnsView.m
//  assistant
//
//  Created by qunai on 2023/10/8.
//

#import "QWxsTopBtnsView.h"
@interface QWxsTopBtnsView ()
@property (nonatomic,strong) UIView *leftView;
@property (nonatomic,strong) UIView *rightView;

@property (nonatomic,strong) UIImageView *leftImageView;
@property (nonatomic,strong) UIImageView *rightImageView;

@property (nonatomic,strong) UIButton *leftBtn;
@property (nonatomic,strong) UIButton *rightBtn;
@end
@implementation QWxsTopBtnsView

- (instancetype)init{
    self = [super init];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        
        [self leftView];
        [self rightView];
        
        
        [self leftBtn];
        [self rightBtn];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    
    [self createLeftBgColor];
    
    [self createRightBgColor];
}

- (UIView *)leftView{
    if(!_leftView){
        _leftView = [[UIView alloc]init];
        _leftView.backgroundColor = [UIColor clearColor];
        [self addSubview:_leftView];
        
        [_leftView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self.mas_centerX).offset(-10.0);
            make.top.equalTo(self);
            make.bottom.equalTo(self);
        }];
        
        UIImageView *bgImageView = [[UIImageView alloc]init];
        self.leftImageView = bgImageView;
        [_leftView addSubview:bgImageView];
        [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_leftView);
        }];
        
        UILabel *nameLabel = [[UILabel alloc]init];
        nameLabel.text = @"会议预告";
        nameLabel.font = [UIFont systemFontOfSize:18];
        nameLabel.textColor = [UIColor whiteColor];
        [_leftView addSubview:nameLabel];
        
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_leftView).offset(4.0);
            make.top.equalTo(_leftView).offset(4.0);
        }];
        
        UIImageView *imageView = [[UIImageView alloc]init];
        imageView.image = [UIImage imageNamed:@"yy_meetingNotice.png"];
        [_leftView addSubview:imageView];
        
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(nameLabel.mas_bottom).offset(-8.0);
            make.right.equalTo(_leftView).offset(-4.0);
            make.bottom.equalTo(_leftView).offset(-4.0);
            make.height.mas_equalTo(40);
            make.width.mas_equalTo(40);
        }];
    }
    return _leftView;
}



- (UIView *)rightView{
    if(!_rightView){
        _rightView = [[UIView alloc]init];
        _rightView.backgroundColor = [UIColor clearColor];
        [self addSubview:_rightView];
        
        [_rightView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self);
            make.left.equalTo(self.mas_centerX).offset(10.0);
            make.top.equalTo(self);
            make.bottom.equalTo(self);
        }];
        
        UIImageView *bgImageView = [[UIImageView alloc]init];
        self.rightImageView = bgImageView;
        [_rightView addSubview:bgImageView];
        [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_rightView);
        }];
        
        UILabel *nameLabel = [[UILabel alloc]init];
        nameLabel.text = @"会议预约";
        nameLabel.font = [UIFont systemFontOfSize:18];
        nameLabel.textColor = [UIColor whiteColor];
        [_rightView addSubview:nameLabel];
        
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_rightView).offset(4.0);
            make.top.equalTo(_rightView).offset(4.0);
        }];
        
        UIImageView *imageView = [[UIImageView alloc]init];
        imageView.image = [UIImage imageNamed:@"yy_meetingreservation.png"];
        [_rightView addSubview:imageView];
        
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(nameLabel.mas_bottom).offset(-8.0);
            make.right.equalTo(_rightView).offset(-4.0);
            make.bottom.equalTo(_rightView).offset(-4.0);
            make.height.mas_equalTo(40);
            make.width.mas_equalTo(40);
        }];
    }
    return _rightView;
}

-(UIButton *)leftBtn
{
    if (!_leftBtn) {
        _leftBtn =[UIButton buttonWithType:UIButtonTypeCustom];
        [_leftBtn addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [_leftView addSubview:_leftBtn];
        [_leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_leftView);
        }];
    }
    return _leftBtn;
}

-(UIButton *)rightBtn
{
    if (!_rightBtn) {
        _rightBtn =[UIButton buttonWithType:UIButtonTypeCustom];
        [_rightBtn addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [_rightView addSubview:_rightBtn];
        [_rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_rightView);
        }];
    }
    return _rightBtn;
}

- (void)buttonClick:(UIButton *)sender{
    if(self.btnsClickBlock)self.btnsClickBlock(sender == self.leftBtn?@"notice":@"reservation");
}


#pragma mark - 绘制渐变色背景
- (void)createLeftBgColor{
    [self.leftImageView layoutIfNeeded];
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.colors = @[(__bridge id)[UIColor colorFromHexString:@"#93DBFD"].CGColor, (__bridge id)[UIColor colorFromHexString:@"#8C96FD"].CGColor];
    gradientLayer.locations = @[@0.0, @1.0];
    gradientLayer.startPoint = CGPointMake(0, 0.5);
    gradientLayer.endPoint = CGPointMake(1.0, 0.5);
    gradientLayer.frame = CGRectMake(0, 0, self.leftImageView.frame.size.width, self.leftImageView.frame.size.height);
    gradientLayer.shadowColor= [UIColor colorFromHexString:@"#8C96FD"].CGColor;
    gradientLayer.masksToBounds = YES;
    gradientLayer.cornerRadius = 6.0;
    [self.leftImageView.layer addSublayer:gradientLayer];
}

- (void)createRightBgColor{
    [self.rightImageView layoutIfNeeded];
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.colors = @[(__bridge id)[UIColor colorFromHexString:@"#E186BB"].CGColor, (__bridge id)[UIColor colorFromHexString:@"#D7296B"].CGColor];
    gradientLayer.locations = @[@0.0, @1.0];
    gradientLayer.startPoint = CGPointMake(0, 0.5);
    gradientLayer.endPoint = CGPointMake(1.0, 0.5);
    gradientLayer.frame = CGRectMake(0, 0, self.rightImageView.frame.size.width, self.rightImageView.frame.size.height);
    gradientLayer.shadowColor= [UIColor colorFromHexString:@"#D7296B"].CGColor;
    gradientLayer.masksToBounds = YES;
    gradientLayer.cornerRadius = 6.0;
    [self.rightImageView.layer addSublayer:gradientLayer];
}

@end
