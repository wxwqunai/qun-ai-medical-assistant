//
//  QWCalendarView.m
//  assistant
//
//  Created by qunai on 2023/10/8.
//

#import "QWCalendarView.h"
#import "FSCalendar.h"

@interface QWCalendarView() <FSCalendarDataSource,FSCalendarDelegate>

@property (nonatomic, strong) UIView *bgView;
@property (strong, nonatomic) NSDateFormatter *dateFormatter1;
@property (strong, nonatomic) NSDateFormatter *dateFormatter2;
@property (weak, nonatomic) FSCalendar *calendar;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;
@property (strong, nonatomic) NSCalendar *gregorian;

@property (nonatomic, strong) UIView *colorsTipsView;
@property (nonatomic,strong) UIButton *yuyueButton;

@property (nonatomic, strong) NSDate *selDate; //选中日期


@end

@implementation QWCalendarView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self bgView];
        [self calendar];
        [self colorsTipsView];//颜色提示
    }
    return self;
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]initWithFrame:self.frame];
        _bgView.backgroundColor = [UIColor clearColor];
        [self addSubview:_bgView];

        //圆角阴影
        CALayer *subLayer = [CALayer layer];
           subLayer.frame = CGRectMake(4.0, 2.0, CGRectGetWidth(_bgView.frame)-8, CGRectGetHeight(_bgView.frame)-4);
           subLayer.cornerRadius = 8;
           subLayer.masksToBounds = NO;
           subLayer.backgroundColor = [UIColor whiteColor].CGColor;
           subLayer.shadowColor = [UIColor colorFromHexString:@"#3CB371"].CGColor;//shadowColor阴影颜色
           subLayer.shadowOffset = CGSizeMake(0,0);
           subLayer.shadowOpacity = 0.4;//阴影透明度，默认0
           subLayer.shadowRadius = 3;//阴影半径，默认3
        [_bgView.layer addSublayer:subLayer];
    }
    return _bgView;
}


- (FSCalendar *)calendar{
    if(!_calendar){
        self.gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        
        self.dateFormatter2 = [[NSDateFormatter alloc] init];
        self.dateFormatter2.dateFormat = @"YYYY-MM-dd";
        self.selDate = [NSDate date];
        
        FSCalendar *calendar = [[FSCalendar alloc] initWithFrame:CGRectMake(4, 2, self.frame.size.width-8, self.frame.size.height-4-20)];
        calendar.locale = [NSLocale localeWithLocaleIdentifier:@"zh_cn"];//中文
        calendar.dataSource = self;
        calendar.delegate = self;
        calendar.layer.cornerRadius = 8.0;
        calendar.layer.masksToBounds = YES;
    //    calendar.allowsMultipleSelection = YES;
    //    calendar.swipeToChooseGesture.enabled = YES;
        calendar.backgroundColor = [UIColor whiteColor];
        calendar.appearance.headerMinimumDissolvedAlpha = 0;
        calendar.appearance.caseOptions = FSCalendarCaseOptionsHeaderUsesUpperCase;
        calendar.placeholderType = FSCalendarPlaceholderTypeNone;
        calendar.appearance.titleTodayColor = [UIColor blackColor];
        calendar.appearance.todayColor = [UIColor whiteColor];
        [_bgView addSubview:calendar];
        self.calendar = calendar;
        
//        [calendar mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(_bgView).offset(4.0);
//            make.right.equalTo(_bgView).offset(-4.0);
//            make.top.equalTo(_bgView).offset(2.0);
//            make.bottom.equalTo(_bgView).offset(-2.0);
//        }];
        
        
        UIButton *previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
        previousButton.frame = CGRectMake(0, 5, 95, 34);
        previousButton.titleLabel.font = [UIFont systemFontOfSize:15];
        [previousButton setImage:[UIImage imageNamed:@"calendar_icon_prev"] forState:UIControlStateNormal];
        [previousButton addTarget:self action:@selector(previousClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_bgView addSubview:previousButton];
        self.previousButton = previousButton;
        
        UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
        nextButton.frame = CGRectMake(CGRectGetWidth(self.frame)-95, 5, 95, 34);
        nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
        [nextButton setImage:[UIImage imageNamed:@"calendar_icon_next"] forState:UIControlStateNormal];
        [nextButton addTarget:self action:@selector(nextClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_bgView addSubview:nextButton];
        self.nextButton = nextButton;
    }
    return _calendar;
}

- (void)previousClicked:(id)sender
{
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *previousMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:previousMonth animated:YES];
}

- (void)nextClicked:(id)sender
{
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *nextMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:nextMonth animated:YES];
}

#pragma mark - <FSCalendarDataSource>
- (NSString *)calendar:(FSCalendar *)calendar titleForDate:(NSDate *)date
{
    if ([self.gregorian isDateInToday:date]) {
        return @"今";
    }
    return nil;
}

//当天颜色展示
- (nullable UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance fillDefaultColorForDate:(NSDate *)date{
    NSString *dateString = [self.dateFormatter2 stringFromDate:date];
    for (QWScheduleFindAlllModel *model in self.eventsForDayArr) {
        if([model.scheduledate isEqualToString:dateString]){
            UIColor *color = [self gridColor:model.grid_color];
            if(color)return color;
        }
    }
    return nil;
}
//当天被选中颜色展示
- (UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance fillSelectionColorForDate:(NSDate *)date
{
    NSString *dateString = [self.dateFormatter2 stringFromDate:date];
    for (QWScheduleFindAlllModel *model in self.eventsForDayArr) {
        if([model.scheduledate isEqualToString:dateString]){
            UIColor *color = [self gridColor:model.grid_color];
            if(color)return color;
        }
    }
    return [UIColor whiteColor];
}

- (nullable UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance titleSelectionColorForDate:(NSDate *)date{
    
    NSString *dateString = [self.dateFormatter2 stringFromDate:date];
    for (QWScheduleFindAlllModel *model in self.eventsForDayArr) {
        if([model.scheduledate isEqualToString:dateString]){
            BOOL isWhite = [self checkIsWhiteColor:model.grid_color];
            return isWhite?[UIColor blackColor]:[UIColor whiteColor];
        }
    }
    return [UIColor blackColor];
}


#pragma mark - FSCalendarDelegate

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition{
    if (monthPosition == FSCalendarMonthPositionNext || monthPosition == FSCalendarMonthPositionPrevious) {
        [calendar setCurrentPage:date animated:YES];
    }
    
    NSString *selDateStr = [self.dateFormatter2 stringFromDate:date];
    self.selDate = date;
    QWScheduleFindAlllModel *__block selModel = nil;
    [self.eventsForDayArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        QWScheduleFindAlllModel *model = (QWScheduleFindAlllModel*)obj;
        if([model.scheduledate isEqualToString:selDateStr]){
            selModel = model;
            *stop = YES;
        }
    }];

    if(selModel){
        self.selModel = selModel;
        if(self.calendarSelectedBlock)self.calendarSelectedBlock(selDateStr,selModel);
        [self createRightBgColor:[selModel.timeconfig isEqualToString:@"1"] || [selModel.timeconfig isEqualToString:@"4"]];//约满、被禁用
    }else{
        self.selModel = nil;
        if(self.calendarSelectedBlock)self.calendarSelectedBlock(selDateStr,nil);
        [self createRightBgColor:YES];//约满、被禁用
    }
}


#pragma mark - 数据加载
- (void)setEventsForDayArr:(NSMutableArray *)eventsForDayArr{
    _eventsForDayArr = eventsForDayArr;
    [self.calendar reloadData];
    [self.calendar.delegate calendar:self.calendar didSelectDate:self.selDate atMonthPosition:FSCalendarMonthPositionCurrent];
}


#pragma mark - 颜色生成
- (UIColor *)gridColor:(NSString *)colorStr{
    NSArray *arr = [colorStr componentsSeparatedByString:@","];
    if(arr.count ==3){
        CGFloat r = [NSString stringWithFormat:@"%@",arr[0]].floatValue;
        CGFloat g = [NSString stringWithFormat:@"%@",arr[1]].floatValue;
        CGFloat b = [NSString stringWithFormat:@"%@",arr[2]].floatValue;
        UIColor *color =[UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0];
        return color;
    }
    return nil;
}
#pragma mark - 颜色判断 - 白色
- (BOOL)checkIsWhiteColor:(NSString *)colorStr{
    NSArray *arr = [colorStr componentsSeparatedByString:@","];
    if(arr.count ==3){
        CGFloat r = [NSString stringWithFormat:@"%@",arr[0]].floatValue;
        CGFloat g = [NSString stringWithFormat:@"%@",arr[1]].floatValue;
        CGFloat b = [NSString stringWithFormat:@"%@",arr[2]].floatValue;
        
        if(r == 255 && g == 255 && b ==255){
            return YES;
        }
    }
    return NO;
}



#pragma mark - 颜色提示
- (UIView *)colorsTipsView{
    if(!_colorsTipsView){
        _colorsTipsView = [[UIView alloc]init];
        _colorsTipsView.backgroundColor = [UIColor clearColor];
        [_bgView addSubview:_colorsTipsView];
        
        [_colorsTipsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView).offset(10.0);
            make.right.equalTo(_bgView).offset(-10.0);
            make.bottom.equalTo(_bgView).offset(-6.0);
            make.height.mas_offset(20);
        }];
        
        
        UIView *colorTipOne= [self colorTipItemView:[UIColor whiteColor] withText:@"空闲"];
        [_colorsTipsView addSubview:colorTipOne];
        [colorTipOne mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_colorsTipsView);
            make.top.equalTo(_colorsTipsView);
            make.bottom.equalTo(_colorsTipsView);
        }];
        
        
        UIView *colorTipTwo = [self colorTipItemView:[UIColor colorWithRed:(150)/255.0 green:(255)/255.0 blue:(150)/255.0 alpha:1.0] withText:@"有预约/有会"];
        [_colorsTipsView addSubview:colorTipTwo];
        [colorTipTwo mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(colorTipOne.mas_right).offset(4.0);
            make.top.equalTo(_colorsTipsView);
            make.bottom.equalTo(_colorsTipsView);
        }];
     
        UIView *colorTipThree = [self colorTipItemView:[UIColor colorWithRed:(255)/255.0 green:(180)/255.0 blue:(200)/255.0 alpha:1.0] withText:@"已约满"];
        [_colorsTipsView addSubview:colorTipThree];
        [colorTipThree mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(colorTipTwo.mas_right).offset(4.0);
            make.top.equalTo(_colorsTipsView);
            make.bottom.equalTo(_colorsTipsView);
        }];
        
//        UIView *colorTipThree = [self colorTipItemView:[UIColor colorWithRed:(180)/255.0 green:(180)/255.0 blue:(180)/255.0 alpha:1.0] withText:@"不可预约"];
//        [_colorsTipsView addSubview:colorTipThree];
//        [colorTipThree mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(colorTipTwo.mas_right).offset(4.0);
//            make.top.equalTo(_colorsTipsView);
//            make.bottom.equalTo(_colorsTipsView);
//        }];
        
        
        
        //会议预约
        UIView *buttonView = [self meetingreservationView];
        [_colorsTipsView addSubview:buttonView];
        [buttonView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_colorsTipsView);
            make.bottom.equalTo(_colorsTipsView);
            make.height.mas_offset(30);
        }];
        
        [self createRightBgColor:NO];
        
    }
    return _colorsTipsView;
}

- (UIView *)colorTipItemView:(UIColor *)color withText:(NSString *)text{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = color;
    view.layer.borderWidth= 0.5;
    view.layer.borderColor = [UIColor grayColor].CGColor;
    view.layer.cornerRadius = 2.0;
    view.layer.masksToBounds = YES;
    
    UILabel *label = [[UILabel alloc]init];
    label.text = text;
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:12];
    
//    label.textColor = CGColorEqualToColor(color.CGColor, [UIColor whiteColor].CGColor)?[UIColor blackColor]:[UIColor whiteColor];
    label.textColor = [UIColor blackColor];
    [view addSubview:label];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view).offset(6.0);
        make.right.equalTo(view).offset(-6.0);
        make.top.equalTo(view);
        make.bottom.equalTo(view);
    }];
    return view;
}
#pragma mark - 会议预约 -按钮
- (UIView *)meetingreservationView{
    UIView * view= [[UIView alloc]init];
    view.backgroundColor = [UIColor clearColor];
    UIButton *button =[UIButton buttonWithType:UIButtonTypeCustom];
    self.yuyueButton = button;
    [button setTitle:@"  会议预约  " forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:17];
    [button addTarget:self action:@selector(meetingreservationOnClick) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(view);
    }];
    return view;
}


- (void)meetingreservationOnClick{
    //约满、被禁用 不可点击
    if(self.selModel && ![self.selModel.timeconfig isEqualToString:@"1"] && ![self.selModel.timeconfig isEqualToString:@"4"]){
        if(self.meetingreservationBlock)self.meetingreservationBlock();
    }
}

//- (void)layoutSubviews{
//    [super layoutSubviews];
//
//    [self createRightBgColor:NO];
//}

- (void)createRightBgColor:(BOOL)isCanNot{
//    [self.yuyueButton layoutIfNeeded];
    
    UIColor *color1 =isCanNot?[UIColor lightGrayColor]:[UIColor colorFromHexString:@"#E186BB"];
    UIColor *color2 =isCanNot?[UIColor lightGrayColor]:[UIColor colorFromHexString:@"#D7296B"];
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.colors = @[(__bridge id)color1.CGColor, (__bridge id)color2.CGColor];
    gradientLayer.locations = @[@0.0, @1.0];
    gradientLayer.startPoint = CGPointMake(0, 0.5);
    gradientLayer.endPoint = CGPointMake(1.0, 0.5);
    gradientLayer.frame = CGRectMake(0, 0, self.yuyueButton.frame.size.width, self.yuyueButton.frame.size.height);
    gradientLayer.shadowColor= color2.CGColor;
    gradientLayer.masksToBounds = YES;
    gradientLayer.cornerRadius = 6.0;
    [self.yuyueButton.layer insertSublayer:gradientLayer below:self.yuyueButton.titleLabel.layer];
}

//返回一个view来响应事件
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
    UIView *superView = [super hitTest:point withEvent:event];
    UIView *view = [self.colorsTipsView hitTest:point withEvent:event];
    if (view == nil){
        //转换坐标
        CGPoint tempPoint = [self.yuyueButton convertPoint:point fromView:self];
       //判断点击的点是否在按钮区域内
        if (CGRectContainsPoint(self.yuyueButton.bounds, tempPoint)){
            //返回按钮
            return self.yuyueButton;
        }
    }
    return superView;
}

@end
