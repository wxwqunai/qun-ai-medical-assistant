//
//  QWYuYueTipsView.h
//  assistant
//
//  Created by kevin on 2023/11/2.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWYuYueTipsView : UIView
@property (nonatomic, strong) UILabel *tipLabel;


//- (void)showTipText:(NSString *)textStr;
@end

NS_ASSUME_NONNULL_END
