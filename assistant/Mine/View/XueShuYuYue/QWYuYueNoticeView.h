//
//  QWYuYueNoticeView.h
//  assistant
//
//  Created by qunai on 2023/11/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWYuYueNoticeView : UIView
@property (nonatomic, copy) void (^noticeOnClickBlock)(void);
@end

NS_ASSUME_NONNULL_END
