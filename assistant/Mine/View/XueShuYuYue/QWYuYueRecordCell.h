//
//  QWYuYueRecordCell.h
//  assistant
//
//  Created by qunai on 2023/10/18.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWYuYueRecordCell : QWBaseTableViewCell
@property (nonatomic, strong) UILabel *conferenceLabel;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *tellLabel;
@property (nonatomic, strong) UILabel *localeLabel;
@property (nonatomic, strong) UILabel *wayLabel;
@property (nonatomic, strong) UILabel *startTimeLabel;
@property (nonatomic, strong) UILabel *endTimeLabel;
@property (nonatomic, strong) UILabel *rooNumLabel;
@property (nonatomic, strong) UILabel *representLabel;
@end

NS_ASSUME_NONNULL_END
