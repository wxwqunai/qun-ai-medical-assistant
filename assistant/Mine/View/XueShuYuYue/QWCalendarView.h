//
//  QWCalendarView.h
//  assistant
//
//  Created by qunai on 2023/10/8.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

@interface QWCalendarView : UIView
@property (nonatomic, strong, nullable) QWScheduleFindAlllModel *selModel;
@property (nonatomic, strong) NSMutableArray *eventsForDayArr;

@property (nonatomic,copy) void (^calendarSelectedBlock)(NSString *selDateStr,QWScheduleFindAlllModel * _Nullable selModel); //选中日期
@property (nonatomic,copy) void (^meetingreservationBlock)(void);
@end

NS_ASSUME_NONNULL_END
