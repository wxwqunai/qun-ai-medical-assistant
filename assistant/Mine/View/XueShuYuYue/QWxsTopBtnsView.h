//
//  QWxsTopBtnsView.h
//  assistant
//
//  Created by qunai on 2023/10/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^BtnsClickBlock)(NSString *selStr);

@interface QWxsTopBtnsView : UIView
@property (nonatomic,copy) BtnsClickBlock btnsClickBlock;

@end

NS_ASSUME_NONNULL_END
