//
//  QWYuYueNoticeView.m
//  assistant
//
//  Created by qunai on 2023/11/1.
//

#import "QWYuYueNoticeView.h"
#import "LMJHorizontalScrollText.h"

@interface QWYuYueNoticeView ()
@property (nonatomic,strong) LMJHorizontalScrollText *scrollText;
@property (nonatomic,strong) UIImageView *leftGifImageView;
@property (nonatomic,strong) UIImageView *rightGifImageView;
@end
@implementation QWYuYueNoticeView

- (instancetype)init{
    self = [super init];
    if(self){
        self.backgroundColor = [UIColor colorFromHexString:@"#FFFFF0"];
        [self leftGifImageView];
        [self rightGifImageView];
        [self scrollText];
    }
    return self;
}

- (UIImageView *)leftGifImageView{
    if(!_leftGifImageView){
        _leftGifImageView = [[UIImageView alloc]init];
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *filePath = [[NSBundle bundleWithPath:[[NSBundle mainBundle] bundlePath]] pathForResource:@"announcement" ofType:@"gif"];
            NSData *imageData = [NSData dataWithContentsOfFile:filePath];
            UIImage *image = [UIImage sd_imageWithGIFData:imageData];
            self->_leftGifImageView.image = image;
        });
        [self addSubview:_leftGifImageView];
        
        [_leftGifImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(10.0);
            make.centerY.equalTo(self);
            make.height.mas_equalTo(30.0);
            make.width.mas_equalTo(30.0);
        }];
    }
    return _leftGifImageView;
}

- (UIImageView *)rightGifImageView{
    if(!_rightGifImageView){
       
        _rightGifImageView = [[UIImageView alloc]init];
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *filePath = [[NSBundle bundleWithPath:[[NSBundle mainBundle] bundlePath]] pathForResource:@"announcement" ofType:@"gif"];
            NSData *imageData = [NSData dataWithContentsOfFile:filePath];
            UIImage *image = [UIImage sd_imageWithGIFData:imageData];
            self->_rightGifImageView.image = image;
        });
        _rightGifImageView.transform = CGAffineTransformMakeScale(-1, 1);
        [self addSubview:_rightGifImageView];
        
        [_rightGifImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self).offset(-10.0);
            make.centerY.equalTo(self);
            make.height.mas_equalTo(30.0);
            make.width.mas_equalTo(30.0);
        }];
    }
    return _rightGifImageView;
}

- (LMJHorizontalScrollText *)scrollText{
    if(!_scrollText){
        _scrollText = [[LMJHorizontalScrollText alloc]init];
        _scrollText.textFont=[UIFont fontWithName:@"PingFangSC-Light" size:14];
        _scrollText.textColor = [UIColor colorFromHexString:@"#FF0000"];
        _scrollText.moveMode = LMJTextScrollFromOutside;
        _scrollText.text=@"会议预告";
        [self addSubview:_scrollText];
        
        [_scrollText mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.leftGifImageView.mas_right).offset(4.0);
            make.right.equalTo(self.rightGifImageView.mas_left).offset(-4.0);
            make.top.equalTo(self);
            make.bottom.equalTo(self);
            make.height.mas_equalTo(30.0);
        }];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(buttonClick) forControlEvents:UIControlEventTouchUpInside];
        [_scrollText addSubview:button];
        
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_scrollText);
        }];
        
    }
    return _scrollText;
}
- (void)buttonClick{
    if(self.noticeOnClickBlock)self.noticeOnClickBlock();
}

@end
