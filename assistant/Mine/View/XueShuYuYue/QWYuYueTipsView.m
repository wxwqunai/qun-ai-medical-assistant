//
//  QWYuYueTipsView.m
//  assistant
//
//  Created by kevin on 2023/11/2.
//

#import "QWYuYueTipsView.h"

@implementation QWYuYueTipsView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        [self tipLabel];
    }
    return self;
}

- (UILabel *)tipLabel{
    if(!_tipLabel){
        _tipLabel = [[UILabel alloc]init];
        _tipLabel.font = [UIFont systemFontOfSize:14];
        _tipLabel.textAlignment = NSTextAlignmentCenter;
        _tipLabel.numberOfLines = 0;
        _tipLabel.textColor = [UIColor colorFromHexString:@"#9D9DA0"];
        [self addSubview:_tipLabel];
        
        [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(14.0);
            make.right.equalTo(self).offset(-14.0);
            make.top.equalTo(self).offset(4.0);
            make.bottom.equalTo(self).offset(-4.0);
        }];
    }
    return _tipLabel;
}

//- (void)showTipText:(NSString *)textStr{
//    //timeConfig 0：空闲，1：约满，2:有预约/有会，4禁用
//    
//    _tipLabel.text = @"";
//    if([textStr isEqualToString:@"0"]){
//        _tipLabel.text = [NSString stringWithFormat:@"温馨提示：这天空闲，可预约会议。"];
//    }
//    if([textStr isEqualToString:@"1"]){
//        _tipLabel.text = [NSString stringWithFormat:@"温馨提示：这天会议已约满。"];
//    }
//    if([textStr isEqualToString:@"2"]){
//        _tipLabel.text = [NSString stringWithFormat:@"温馨提示：这天已约x场，还可以约x场（会场数量最多为4个会场）。"];
//    }
//    if([textStr isEqualToString:@"4"]){
//        _tipLabel.text = [NSString stringWithFormat:@"温馨提示：当天不可预约会议。"];
//    }
//  
//}

@end
