//
//  QWYuYueRecordCell.m
//  assistant
//
//  Created by qunai on 2023/10/18.
//

#import "QWYuYueRecordCell.h"
@interface QWYuYueRecordCell()
@property (nonatomic, strong) UIView *bgView;

@end
@implementation QWYuYueRecordCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self bgView];
        
        [self conferenceLabel];
        [self nameLabel];
        [self tellLabel];
        [self localeLabel];
        [self wayLabel];
        [self startTimeLabel];
        [self endTimeLabel];
        [self rooNumLabel];
        [self representLabel];
    }
    return self;
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor colorFromHexString:@"#AFEEEE"];
        _bgView.layer.cornerRadius = 6.0;
        _bgView.layer.masksToBounds = YES;
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(10.0);
            make.right.equalTo(self.contentView).offset(-10.0);
            make.top.equalTo(self.contentView).offset(4.0);
            make.bottom.equalTo(self.contentView).offset(-4.0);
//            make.height.mas_greaterThanOrEqualTo(250.0);
        }];
    }
    return _bgView;
}
#pragma mark - 会议名称
- (UILabel *)conferenceLabel{
    if(!_conferenceLabel){
        _conferenceLabel = [[UILabel alloc]init];
        _conferenceLabel.numberOfLines = 0;
        _conferenceLabel.font = [UIFont boldSystemFontOfSize:18];
        [_bgView addSubview:_conferenceLabel];
        
        [_conferenceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView).offset(4.0);
            make.right.equalTo(_bgView).offset(-4.0);
            make.top.equalTo(_bgView).offset(4.0);
        }];
    }
    return _conferenceLabel;
}
#pragma mark - 预约人
- (UILabel *)nameLabel{
    if(!_nameLabel){
        UILabel *label = [self customLabel];
        label.text = @"姓名/医院：";
        [_bgView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView).offset(4.0);
            make.top.equalTo(_conferenceLabel.mas_bottom).offset(8.0);
            make.width.mas_equalTo(85);
        }];
        
        _nameLabel = [self customLabel];
        [_bgView addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(label.mas_right);
            make.right.equalTo(_bgView).offset(-4.0);
            make.top.equalTo(label.mas_top);
            make.bottom.mas_greaterThanOrEqualTo(label.mas_bottom);
        }];
    }
    return _nameLabel;
}
#pragma mark - 预约人联系方式
- (UILabel *)tellLabel{
    if(!_tellLabel){
        UILabel *label = [self customLabel];
        label.text = @"电话：";
        [_bgView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView).offset(4.0);
            make.top.equalTo(_nameLabel.mas_bottom).offset(8.0);
            make.width.mas_equalTo(45);
        }];
        
        _tellLabel = [self customLabel];
        [_bgView addSubview:_tellLabel];
        [_tellLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(label.mas_right);
            make.right.equalTo(_bgView).offset(-4.0);
            make.top.equalTo(label.mas_top);
            make.bottom.mas_greaterThanOrEqualTo(label.mas_bottom);
        }];
    }
    return _tellLabel;
}
#pragma mark - 举办地点
- (UILabel *)localeLabel{
    if(!_localeLabel){
        UILabel *label = [self customLabel];
        label.text = @"举办地点：";
        [_bgView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView).offset(4.0);
            make.top.equalTo(_tellLabel.mas_bottom).offset(8.0);
            make.width.mas_equalTo(75);
        }];
        
        _localeLabel = [self customLabel];
        [_bgView addSubview:_localeLabel];
        [_localeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(label.mas_right);
            make.right.equalTo(_bgView).offset(-4.0);
            make.top.equalTo(label.mas_top);
            make.bottom.mas_greaterThanOrEqualTo(label.mas_bottom);
        }];
    }
    return _localeLabel;
}

#pragma mark - 参会方式
- (UILabel *)wayLabel{
    if(!_wayLabel){
        UILabel *label = [self customLabel];
        label.text = @"参会方式：";
        [_bgView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView).offset(4.0);
            make.top.equalTo(_localeLabel.mas_bottom).offset(8.0);
            make.width.mas_equalTo(75);
        }];
        
        _wayLabel = [self customLabel];
        [_bgView addSubview:_wayLabel];
        [_wayLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(label.mas_right);
            make.right.equalTo(_bgView).offset(-4.0);
            make.top.equalTo(label.mas_top);
            make.bottom.mas_greaterThanOrEqualTo(label.mas_bottom);
        }];
    }
    return _wayLabel;
}

#pragma mark - 会议开始时间
- (UILabel *)startTimeLabel{
    if(!_startTimeLabel){
        UILabel *label = [self customLabel];
        label.text = @"开始时间：";
        [_bgView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView).offset(4.0);
            make.top.equalTo(_wayLabel.mas_bottom).offset(8.0);
            make.width.mas_equalTo(75);
        }];
        
        _startTimeLabel = [self customLabel];
        [_bgView addSubview:_startTimeLabel];
        [_startTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(label.mas_right);
            make.right.equalTo(_bgView).offset(-4.0);
            make.top.equalTo(label.mas_top);
            make.bottom.mas_greaterThanOrEqualTo(label.mas_bottom);
        }];
    }
    return _startTimeLabel;
}

#pragma mark - 会议结束时间
- (UILabel *)endTimeLabel{
    if(!_endTimeLabel){
        UILabel *label = [self customLabel];
        label.text = @"结束时间：";
        [_bgView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView).offset(4.0);
            make.top.equalTo(_startTimeLabel.mas_bottom).offset(8.0);
            make.width.mas_equalTo(75);
        }];
        
        _endTimeLabel = [self customLabel];
        [_bgView addSubview:_endTimeLabel];
        [_endTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(label.mas_right);
            make.right.equalTo(_bgView).offset(-4.0);
            make.top.equalTo(label.mas_top);
            make.bottom.mas_greaterThanOrEqualTo(label.mas_bottom);
        }];
    }
    return _endTimeLabel;
}

#pragma mark - 会场数量
- (UILabel *)rooNumLabel{
    if(!_rooNumLabel){
        UILabel *label = [self customLabel];
        label.text = @"会场数量：";
        [_bgView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView).offset(4.0);
            make.top.equalTo(_endTimeLabel.mas_bottom).offset(8.0);
            make.width.mas_equalTo(75);
        }];
        
        _rooNumLabel = [self customLabel];
        [_bgView addSubview:_rooNumLabel];
        [_rooNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(label.mas_right);
            make.right.equalTo(_bgView).offset(-4.0);
            make.top.equalTo(label.mas_top);
            make.bottom.mas_greaterThanOrEqualTo(label.mas_bottom);
        }];
    }
    return _rooNumLabel;
}

#pragma mark - 情况说明
- (UILabel *)representLabel{
    if(!_representLabel){
        UILabel *label = [self customLabel];
        label.text = @"情况说明：";
        [_bgView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView).offset(4.0);
            make.top.equalTo(_rooNumLabel.mas_bottom).offset(8.0);
            make.width.mas_equalTo(75);
        }];
        
        _representLabel = [self customLabel];
        [_bgView addSubview:_representLabel];
        [_representLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(label.mas_right);
            make.right.equalTo(_bgView).offset(-4.0);
            make.top.equalTo(label.mas_top);
            make.bottom.mas_greaterThanOrEqualTo(label.mas_bottom);
            make.bottom.equalTo(_bgView.mas_bottom).offset(-4.0);
        }];
    }
    return _representLabel;
}

#pragma mark - 公共
- (UILabel *)customLabel{
    UILabel *label = [[UILabel alloc]init];
    label.numberOfLines = 0;
    label.font = [UIFont systemFontOfSize:15];
    label.textColor = [UIColor colorFromHexString:@"#5C5B5B"];
    return label;
}


@end
