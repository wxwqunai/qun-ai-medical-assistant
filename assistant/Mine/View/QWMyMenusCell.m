//
//  QWMyMenusCell.m
//  assistant
//
//  Created by kevin on 2023/6/30.
//

#import "QWMyMenusCell.h"
@interface QWMyMenusCell ()
@property (nonatomic, strong) NSMutableArray *showArr;

@end
@implementation QWMyMenusCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        [self loadData];
    }
    return self;
}

- (void)loadData{
    if(AppProfile.meMenus == nil) return;
    QWMeMenus *menuData = AppProfile.meMenus;
    NSArray * menus;
    if([self isDoctor]){
        menus = menuData.doctor;
    }else if([self isPatient]){
        menus = menuData.patient;
    }else if([self isAssistant]){
        menus = menuData.assistant;
    }else{
        menus = menuData.admin;
    }
    
    [self.showArr removeAllObjects];
    [self.showArr addObjectsFromArray:menus];
    
    [self.contentView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if(obj.tag >= 100000) [obj removeFromSuperview];
    }];
    
    if(self.showArr.count){
        [self layoutMultiLine];
    }
}

- (BOOL)isAssistant{
    QWqaUserModel *qaUserInfo = AppProfile.qaUserInfo;
    return [qaUserInfo.usertype isEqualToString: @"assistant"];
}
- (BOOL)isDoctor{
    QWqaUserModel *qaUserInfo = AppProfile.qaUserInfo;
    return [qaUserInfo.usertype isEqualToString: @"doctor"];
}
- (BOOL)isPatient{
    QWqaUserModel *qaUserInfo = AppProfile.qaUserInfo;
    return [qaUserInfo.usertype isEqualToString: @"patient"];
}

-(void)layoutMultiLine{
    //多行布局 要考虑换行的问题
    
    CGFloat marginX = 8;  //按钮距离左边和右边的距离
    CGFloat marginY = 15;  //距离上下边缘距离
    CGFloat toTop = 0;  //按钮距离顶部的距离
    CGFloat gapX = 0;    //左右按钮之间的距离
    CGFloat gapY = 15;    //上下按钮之间的距离
    NSInteger col = 4;    //这里只布局5列
    NSInteger count = self.showArr.count;  //这里先设置布局任意个按钮
    
    CGFloat viewWidth = kScreenWidth;  //视图的宽度
    
    CGFloat itemWidth = (viewWidth - marginX *2 - (col - 1)*gapX)/col*1.0f;  //根据列数 和 按钮之间的间距 这些参数基本可以确定要平铺的按钮的宽度
    
    UIView *last = nil;   //上一个按钮
    //准备工作完毕 既可以开始布局了
    for (int i = 0 ; i < count; i++) {
        UIView *item = [self itemViewCreat:i];
        [self.contentView addSubview:item];
        
        //布局
        [item mas_makeConstraints:^(MASConstraintMaker *make) {
            //宽高是固定的，前面已经算好了,高度根据内容自动改变
            make.width.mas_equalTo(itemWidth);
            if((i%col) == 0){
                if(last){
                    //每行第一个
                    make.top.equalTo(last.mas_bottom).mas_offset(gapY);
                    make.left.mas_offset(marginX);
                }else{
                    //第一行第一个
                    CGFloat top = toTop + marginY;//计算距离顶部的距离
                    make.top.mas_offset(top);
                    make.left.mas_offset(marginX);
                }
            }else{
                //每行其他
                make.top.equalTo(last.mas_top);
                make.left.equalTo(last.mas_right).mas_offset(gapX);
            }
            
            if(i==count-1){
                make.bottom.equalTo(self.contentView).mas_offset(-marginY);
            }
            
        }];
        last = item;
        
    }
}
#pragma mark - Private
-(UIView *)itemViewCreat:(NSInteger )num{
    
    UIView *bgView = [[UIView alloc] init];
    bgView.tag = 100000+num;
    bgView.backgroundColor = [UIColor clearColor];
    QWMyMenuItem *menuItem = self.showArr[num];
    NSString *title = menuItem.btnText;
    NSString *pic = menuItem.imgResUrl;
    
    //图片
    UIImageView *imageView = [[UIImageView alloc]init];
//    [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//    imageView.tintColor = Color_Main_Green;
    [imageView sd_setImageWithURL:[NSURL URLWithString:pic]];
    [bgView addSubview:imageView];
    
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(bgView);
        make.top.equalTo(bgView).offset(5);
        make.width.mas_equalTo(30);
        make.height.mas_equalTo(30);
    }];
    
    //文本
    UILabel *label =[[UILabel alloc]init];
    label.text = title;
    label.font = [UIFont systemFontOfSize:15];
    [bgView addSubview:label];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(bgView);
        make.top.equalTo(imageView.mas_bottom).offset(5);
        make.bottom.equalTo(bgView).offset(-5);
    }];
    
    //按钮
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//    button.backgroundColor = [UIColor colorWithRed:arc4random_uniform(256)/255.0 green:arc4random_uniform(256)/255.0 blue:arc4random_uniform(256)/255.0 alpha:1.0f];
    [button addTarget:self action:@selector(buttonClick:) forControlEvents:(UIControlEventTouchUpInside)];
    button.tag = 10000+num;
    [bgView addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(bgView);
    }];
    
    return bgView;
}

- (void)buttonClick:(UIButton *)sender{
    NSInteger num = sender.tag - 10000;
    QWMyMenuItem *menuItem = self.showArr[num];
    if(self.menuItemClickBlock)self.menuItemClickBlock(menuItem);
}

- (NSMutableArray *)showArr{
    if(!_showArr){
        _showArr = [[NSMutableArray alloc] init];
    }
    return _showArr;
}

@end
