//
//  QWMineInfoCell.m
//  assistant
//
//  Created by kevin on 2023/6/12.
//

#import "QWMineInfoCell.h"
@interface QWMineInfoCell()
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIImageView *picImageView;
@property (nonatomic, strong) UIImageView *nextImageView;
@property (nonatomic, strong) UIImageView *lineImageView;
@end
@implementation QWMineInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        [self nameLabel];
        [self nextImageView];
        [self lineImageView];
        
    }
    return self;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font =[UIFont systemFontOfSize:16];
        [self addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(14);
            make.top.equalTo(self);
            make.bottom.equalTo(self);
            make.height.mas_greaterThanOrEqualTo(60.0);
        }];
    }
    return _nameLabel;
}

- (UILabel *)contentLabel{
    if(!_contentLabel){
        _contentLabel = [[UILabel alloc]init];
        _contentLabel.font =[UIFont systemFontOfSize:15];
        _contentLabel.numberOfLines = 0;
        _contentLabel.textAlignment = NSTextAlignmentRight;
        _contentLabel.textColor = [UIColor colorFromHexString:@"#9F9E9E"];
        [self addSubview:_contentLabel];
        
        [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_nextImageView).offset(-20);
            make.top.equalTo(self).offset(8.0);
            make.bottom.equalTo(self).offset(-8.0);
            make.width.equalTo(self).multipliedBy(0.5);
        }];
    }
    return _contentLabel;
}
#pragma mark - 图片- 右边
- (UIImageView *)picImageView{
    if(!_picImageView){
        _picImageView = [[UIImageView alloc]init];
        _picImageView.tintColor = Color_Main_Green;
        _picImageView.layer.masksToBounds = YES;
        _picImageView.layer.cornerRadius = 50.0;
        [self addSubview:_picImageView];
        
        [_picImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_nextImageView).offset(-20);
            make.top.equalTo(self).offset(25);
            make.bottom.equalTo(self).offset(-25);
            make.width.mas_equalTo(100);
            make.height.mas_equalTo(100);
        }];
    }
    return _picImageView;
}

#pragma mark - nextImageView
- (UIImageView *)nextImageView{
    if(!_nextImageView){
        _nextImageView = [[UIImageView alloc]init];
        _nextImageView.image = [UIImage imageNamed:@"next_arrow.png"];
        [self addSubview:_nextImageView];
        
        [_nextImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-14.0);
            make.centerY.equalTo(self);
        }];
    }
    return _nextImageView;
}

#pragma mark - lineImage
- (UIImageView *)lineImageView{
    if(!_lineImageView){
        _lineImageView = [[UIImageView alloc]init];
        _lineImageView.backgroundColor = Color_Line_Gray;
        [self addSubview:_lineImageView];
        
        [_lineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(14.0);
            make.right.mas_equalTo(-14.0);
            make.height.mas_equalTo(1);
            make.bottom.equalTo(self);
        }];
    }
    return _lineImageView;
}


#pragma mark - 处理
- (void)setHeaderUrl:(NSString *)headerUrl{
    _headerUrl = headerUrl;
    if(IsStringEmpty(_headerUrl)) return;
    NSString *url = [QWM_BASE_API_URL stringByAppendingFormat:@"/bokeupload%@",_headerUrl];
    [self.picImageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"ic_header_default.png"]];

}
- (void)setSecondText:(NSString *)secondText{
    _secondText = secondText;
    self.contentLabel.text = _secondText;
}
- (void)setHideRightArrow:(BOOL)hideRightArrow{
    _hideRightArrow = hideRightArrow;
    _nextImageView.hidden = _hideRightArrow;
    [self.contentLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_nextImageView).offset(_hideRightArrow?0:-20);
    }];
}

@end
