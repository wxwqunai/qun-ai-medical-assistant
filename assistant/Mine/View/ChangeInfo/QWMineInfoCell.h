//
//  QWMineInfoCell.h
//  assistant
//
//  Created by kevin on 2023/6/12.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWMineInfoCell : QWBaseTableViewCell

@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, copy)  NSString *secondText;
@property (nonatomic, copy)  NSString *headerUrl;
@property (nonatomic, assign) BOOL hideRightArrow;
@end

NS_ASSUME_NONNULL_END
