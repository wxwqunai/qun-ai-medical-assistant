//
//  QWButtonView.h
//  qwm
//
//  Created by kevin on 2023/3/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^LogoutBlock)(void);
typedef void (^ButtonBlock)(void);

@interface QWButtonView : UIView

@property (nonatomic, strong) UIViewController *superVC;

@property (nonatomic,copy)ButtonBlock buttonBlock;
@property (nonatomic,copy)LogoutBlock logoutBlock;


- (void)changeButtonTitle:(NSString *)title;

#pragma mark - 切换按钮状态-可点击/不可点击
- (void)changeButtonState:(BOOL)isCanClick;
@end

NS_ASSUME_NONNULL_END
