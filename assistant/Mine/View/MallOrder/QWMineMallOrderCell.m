//
//  QWMineMallOrderCell.m
//  assistant
//
//  Created by qunai on 2024/4/8.
//

#import "QWMineMallOrderCell.h"

@interface QWMineMallOrderCell()
@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) OrderItemView *oneView;
@property (nonatomic, strong) OrderItemView *twoView;
@property (nonatomic, strong) OrderItemView *threeView;
@property (nonatomic, strong) OrderItemView *fourView;
@end

@implementation QWMineMallOrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self titleLabel];
        
        [self oneView];
        [self twoView];
        [self threeView];
        [self fourView];
    }
    return self;
}
- (UILabel *)titleLabel{
    if(!_titleLabel){
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"商品订单";
        _titleLabel.font = [UIFont systemFontOfSize:16];
        [self.contentView addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(14.0);
            make.top.equalTo(self.contentView).offset(10);
        }];
    }
    return _titleLabel;
}

- (OrderItemView *)oneView{
    if(!_oneView){
        _oneView = [[OrderItemView alloc]init];
        _oneView.imageView.image = [UIImage imageNamed:@"mall_order_weifu"];
        _oneView.nameLabel.text = @"未付款";
        [_oneView.button addTarget:self action:@selector(itemClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_oneView];
        
        [_oneView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView);
            make.top.equalTo(self.titleLabel.mas_bottom).offset(8);
            make.bottom.equalTo(self.contentView).offset(-10);
            make.width.equalTo(self.contentView).multipliedBy(1.0/4.0);
        }];
    }
    return _oneView;
}

- (OrderItemView *)twoView{
    if(!_twoView){
        _twoView = [[OrderItemView alloc]init];
        _twoView.imageView.image = [UIImage imageNamed:@"mall_order_daifu"];
        _twoView.nameLabel.text = @"待发货";
        [_twoView.button addTarget:self action:@selector(itemClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_twoView];
        
        [_twoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.oneView.mas_right);
            make.top.equalTo(self.oneView);
            make.bottom.equalTo(self.oneView);
            make.width.equalTo(self.oneView);
        }];
    }
    return _twoView;
}

- (OrderItemView *)threeView{
    if(!_threeView){
        _threeView = [[OrderItemView alloc]init];
        _threeView.imageView.image = [UIImage imageNamed:@"mall_order_daishou"];
        _threeView.nameLabel.text = @"待收货";
        [_threeView.button addTarget:self action:@selector(itemClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_threeView];
        
        [_threeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.twoView.mas_right);
            make.top.equalTo(self.oneView);
            make.bottom.equalTo(self.oneView);
            make.width.equalTo(self.oneView);
        }];
    }
    return _threeView;
}

- (OrderItemView *)fourView{
    if(!_fourView){
        _fourView = [[OrderItemView alloc]init];
        _fourView.imageView.image = [UIImage imageNamed:@"mall_order_quanbu"];
        _fourView.nameLabel.text = @"全部订单";
        [_fourView.button addTarget:self action:@selector(itemClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_fourView];
        
        [_fourView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.threeView.mas_right);
            make.top.equalTo(self.oneView);
            make.bottom.equalTo(self.oneView);
            make.width.equalTo(self.oneView);
        }];
    }
    return _fourView;
}

- (void)itemClick:(UIButton *)sender{
    NSInteger index = 0;
    if(sender == self.oneView.button) index = 0;
    if(sender == self.twoView.button) index = 1;
    if(sender == self.threeView.button) index = 2;
    if(sender == self.fourView.button) index = 3;
    if(self.itemClickBlock)self.itemClickBlock(index);
}

@end


@implementation OrderItemView 
- (instancetype)init{
    self = [super init];
    if(self){
        
//        self.backgroundColor = [UIColor redColor];

        [self imageView];
        [self nameLabel];
        [self button];
    }
    return self;
}
- (UIImageView *)imageView{
    if(!_imageView){
        _imageView = [[UIImageView alloc]init];
        [self addSubview:_imageView];
        
        [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset(6.0);
            make.centerX.equalTo(self);
        }];
    }
    return _imageView;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont systemFontOfSize:15];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.top.equalTo(self.imageView.mas_bottom).offset(6);
            make.bottom.equalTo(self).offset(-6);
        }];
    }
    return _nameLabel;
}

- (UIButton *)button{
    if(!_button){
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:_button];
        [_button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return _button;
}

@end
