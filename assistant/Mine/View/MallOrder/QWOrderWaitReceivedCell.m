//
//  QWOrderWaitReceivedCell.m
//  assistant
//
//  Created by qunai on 2024/4/8.
//

#import "QWOrderWaitReceivedCell.h"
@interface QWOrderWaitReceivedCell()
@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UIImageView *storeLogo;
@end
@implementation QWOrderWaitReceivedCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self bgView];
        
        [self numLabel];
        [self storeLogo];
        [self storeNameLabel];
        
        [self picImageView];
        [self productNameLabel];
        [self realPriceLabel];
        [self originalPriceLabel];
    }
    return self;
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 6.0;
        _bgView.layer.masksToBounds = YES;
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView).with.insets(UIEdgeInsetsMake(0, 14, 0, 14));
        }];
    }
    return _bgView;
}

//数量
- (UILabel *)numLabel{
    if(!_numLabel){
        _numLabel = [[UILabel alloc]init];
        _numLabel.textColor = [UIColor grayColor];
        _numLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightLight];
        _numLabel.textAlignment = NSTextAlignmentCenter;
        [self.bgView addSubview:_numLabel];
        [_numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.bgView);
            make.bottom.equalTo(self.bgView);
            make.right.equalTo(self.bgView).offset(-8.0);
            make.width.mas_greaterThanOrEqualTo(14);
        }];
    }
    return _numLabel;
}

- (UIImageView *)storeLogo{
    if (!_storeLogo) {
        _storeLogo = [[UIImageView alloc]init];
        _storeLogo.image = [UIImage imageNamed:@"mall_store"];
        [self.bgView addSubview:_storeLogo];
        [_storeLogo mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView).offset(8.0);
            make.top.equalTo(self.bgView).offset(10.0);
            make.width.mas_equalTo(16);
            make.height.equalTo(_storeLogo.mas_width);
        }];
    }
    return _storeLogo;
}
- (UILabel *)storeNameLabel{
    if(!_storeNameLabel){
        _storeNameLabel = [[UILabel alloc]init];
        _storeNameLabel.textColor = [UIColor blackColor];
        _storeNameLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightSemibold];
        [self.bgView addSubview:_storeNameLabel];
        [_storeNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.storeLogo.mas_right).offset(4.0);
            make.centerY.equalTo(self.storeLogo);
            make.right.equalTo(self.bgView).offset(-8.0);
        }];
    }
    return _storeNameLabel;
}

#pragma mark - 商品图片
- (UIImageView *)picImageView{
    if(!_picImageView){
        _picImageView = [[UIImageView alloc]init];
        _picImageView.layer.cornerRadius = 6.0;
        _picImageView.layer.masksToBounds = YES;
        [self.bgView addSubview:_picImageView];
        [_picImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView).offset(8.0);
            make.top.equalTo(self.storeNameLabel.mas_bottom).offset(10.0);
            make.bottom.equalTo(self.bgView).offset(-10.0);
            make.height.mas_equalTo(80.0);
            make.width.equalTo(_picImageView.mas_height);
        }];
    }
    return _picImageView;
}

#pragma mark - 商品名称
- (UILabel *)productNameLabel{
    if(!_productNameLabel){
        _productNameLabel = [[UILabel alloc]init];
        _productNameLabel.numberOfLines = 2;
        _productNameLabel.font = [UIFont systemFontOfSize:15 weight:(UIFontWeightRegular)];
        [self.bgView addSubview:_productNameLabel];
        [_productNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.picImageView.mas_right).offset(8.0);
            make.top.equalTo(self.picImageView.mas_top);
            make.right.equalTo(self.numLabel.mas_left).offset(-2.0);
        }];
    }
    return _productNameLabel;
}
#pragma mark - 价格
- (UILabel *)realPriceLabel{
    if(!_realPriceLabel){
        _realPriceLabel = [[UILabel alloc]init];
        _realPriceLabel.numberOfLines = 1;
        _realPriceLabel.textColor = [UIColor colorFromHexString:@"#EB5D2A"];
        _realPriceLabel.font = [UIFont systemFontOfSize:15 weight:(UIFontWeightMedium)];
        [self.bgView addSubview:_realPriceLabel];
        [_realPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.picImageView.mas_right).offset(8.0);
            make.bottom.equalTo(self.picImageView);
        }];
    }
    return _realPriceLabel;
}

- (UILabel *)originalPriceLabel{
    if(!_originalPriceLabel){
        _originalPriceLabel = [[UILabel alloc]init];
        _originalPriceLabel.numberOfLines = 1;
        _originalPriceLabel.textColor = [UIColor grayColor];
        _originalPriceLabel.font = [UIFont systemFontOfSize:12 weight:(UIFontWeightLight)];
        [self.bgView  addSubview:_originalPriceLabel];
        [_originalPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.realPriceLabel.mas_right).offset(2.0);
            make.bottom.equalTo(self.realPriceLabel);
        }];
        
        UIImageView *line = [[UIImageView alloc]init];
        line.backgroundColor = [UIColor grayColor];
        [_originalPriceLabel addSubview:line];
        
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_originalPriceLabel);
            make.right.equalTo(_originalPriceLabel);
            make.centerY.equalTo(_originalPriceLabel);
            make.height.mas_equalTo(0.5);
        }];
    }
    return _originalPriceLabel;
}


@end
