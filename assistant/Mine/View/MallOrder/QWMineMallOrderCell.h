//
//  QWMineMallOrderCell.h
//  assistant
//
//  Created by qunai on 2024/4/8.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderItemView : UIView
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UIButton *button;
@end

@interface QWMineMallOrderCell : QWBaseTableViewCell
@property (nonatomic, copy) void (^itemClickBlock)(NSInteger index);
@end

NS_ASSUME_NONNULL_END
