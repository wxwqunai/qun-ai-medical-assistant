//
//  QWMineModel.h
//  assistant
//
//  Created by kevin on 2023/6/30.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWMineModel : NSObject

@end

@interface QWMyMenuItem : NSObject
@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *imgTarget;
@property (nonatomic, copy) NSString *imgResUrl;
@property (nonatomic, copy) NSString *btnText;
@end

@interface QWMeMenus : NSObject
@property (nonatomic, strong) NSArray *doctor;
@property (nonatomic, strong) NSArray *assistant;
@property (nonatomic, strong) NSArray *patient;
@property (nonatomic, strong) NSArray *admin;
@end

@interface QWScheduleFindAlllModel : NSObject
@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *userid;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *gridvalue;
@property (nonatomic, copy) NSString *rowid;
@property (nonatomic, copy) NSString *scheduledate;
@property (nonatomic, copy) NSString *weekday;
@property (nonatomic, copy) NSString *timeconfig;
@property (nonatomic, copy) NSString *extprop;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *flag;
@property (nonatomic, copy) NSString *reserve1;
@property (nonatomic, copy) NSString *reserve2;
@property (nonatomic, copy) NSString *grid_color;
@property (nonatomic, copy) NSString *meet_list;
@property (nonatomic, copy) NSString *meet_num;
@property (nonatomic, copy) NSString *meet_num_left_res;
@end

@interface QWLearningApplyModel : NSObject
@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *useId;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *tell;
@property (nonatomic, copy) NSString *locale;
@property (nonatomic, copy) NSString *way;
@property (nonatomic, copy) NSString *startTime;
@property (nonatomic, copy) NSString *endTime;
@property (nonatomic, copy) NSString *represent;
@property (nonatomic, copy) NSString *result;
@property (nonatomic, copy) NSString *roomNum;
@property (nonatomic, copy) NSString *freeOne;
@property (nonatomic, copy) NSString *freeTwo;
@end
NS_ASSUME_NONNULL_END
