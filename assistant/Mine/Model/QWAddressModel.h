//
//  QWAddressModel.h
//  assistant
//
//  Created by qunai on 2024/4/3.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWAddressModel : NSObject
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, assign) BOOL isDefault;

@end

NS_ASSUME_NONNULL_END
