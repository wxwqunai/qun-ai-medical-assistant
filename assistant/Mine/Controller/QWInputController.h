//
//  QWInputController.h
//  assistant
//
//  Created by kevin on 2023/6/20.
//

#import "QWBaseController.h"

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger, InputType)
{
    InputType_defalut = 20,
    InputType_big = 200,
};

typedef void (^InputInfoBlock)(NSString *value);

@interface QWInputController : QWBaseController
@property (nonatomic ,copy) NSString *inputStr;
@property (nonatomic ,copy) NSString *btnNameStr;
@property (nonatomic,assign) InputType inputType;
@property (nonatomic,copy)InputInfoBlock inputInfoBlock;

@end

NS_ASSUME_NONNULL_END
