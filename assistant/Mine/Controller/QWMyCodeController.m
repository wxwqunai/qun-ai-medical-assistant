//
//  QWMyCodeController.m
//  assistant
//
//  Created by qunai on 2023/10/10.
//

#import "QWMyCodeController.h"
#import "XWQRCodeGenerator.h"
@interface QWMyCodeController ()
@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UIView *userInfoView;
@property (nonatomic, strong) UIImageView *headPicImageView;
@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, strong) UIImageView *codeImageView;
@property (nonatomic, strong) UILabel *tipLabel;

@property (nonatomic, strong) UIView *operationView;
@property (nonatomic, strong) UIButton *scanBtn;
@property (nonatomic, strong) UIButton *savePhotoBtn;
@property (nonatomic, strong) UIButton *shareBtn;
@end

@implementation QWMyCodeController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self customNavigationBarWithBgColor:[UIColor clearColor] withTitleColor:[UIColor clearColor]];
    self.leftBarButtonItem.tintColor = [UIColor blackColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = Color_TableView_Gray;
    
    [self bgView];
    
    [self codeImageView];
    
    [self userInfoView];
    [self headPicImageView];
    [self nameLabel];

    [self tipLabel];
    
    [self operationView];
    [self savePhotoBtn];
    [self scanBtn];
    [self shareBtn];
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.layer.cornerRadius = 10.0;
        _bgView.layer.masksToBounds = YES;
        _bgView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.view);
            make.centerY.equalTo(self.view);
            
            make.width.equalTo(self.view.mas_width).multipliedBy(0.8);
        }];
    }
    return _bgView;
}

#pragma mark - 二维码
- (UIImageView *)codeImageView{
    if(!_codeImageView){
        _codeImageView = [[UIImageView alloc]init];
//        _codeImageView.bounds = CGRectMake(0, 0, kScreenWidth/2.0, kScreenWidth/2.0);
//        _codeImageView.center = CGPointMake(kScreenWidth/2.0, kScreenHeight/2.0);
        
        NSString *url = [URL_DOCTOR_INFO stringByAppendingFormat:@"?id=%@&from=app",AppProfile.qaUserInfo.id];
        UIImage *image = [XWQRCodeGenerator QRCodeWithContentString:url size:200 centerLogo:[UIImage imageNamed:@"logo"]];
        _codeImageView.image = image;
        [_bgView addSubview:_codeImageView];
        
        [_codeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_bgView);
            make.centerX.equalTo(_bgView);
            make.width.equalTo(_bgView.mas_width).multipliedBy(0.8);
            make.height.equalTo(_codeImageView.mas_width);
        }];
    }
    return _codeImageView;
}

#pragma mark - 个人信息
- (UIView *)userInfoView{
    if(!_userInfoView){
        _userInfoView = [[UIView alloc]init];
        _userInfoView.backgroundColor = [UIColor clearColor];
        [_bgView addSubview:_userInfoView];
        
        [_userInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView);
            make.right.equalTo(_bgView);
            make.top.equalTo(_bgView).offset(20.0);
            make.bottom.equalTo(_codeImageView.mas_top).offset(-15.0);
        }];
    }
    return _userInfoView;
}

#pragma mark - 头像
- (UIImageView *)headPicImageView{
    if(!_headPicImageView){
        _headPicImageView = [[UIImageView alloc]init];
        _headPicImageView.backgroundColor = [UIColor lightGrayColor];
        _headPicImageView.layer.cornerRadius = 25;
        _headPicImageView.layer.masksToBounds = YES;
        NSString *headStr = [QWM_BASE_API_URL stringByAppendingFormat:@"/bokeupload%@", AppProfile.qaUserInfo.photo];
        [_headPicImageView sd_setImageWithURL:[NSURL URLWithString:headStr] placeholderImage:[UIImage imageNamed:@"contact_default_avatar"]];
        [_userInfoView addSubview:_headPicImageView];
        
        [_headPicImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_userInfoView);
            make.bottom.equalTo(_userInfoView);
            make.left.equalTo(_userInfoView).offset(14.0);
            make.width.mas_equalTo(50);
            make.height.mas_equalTo(50);
        }];
    }
    return _headPicImageView;
}
#pragma mark - 姓名
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont boldSystemFontOfSize:18];
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.text = AppProfile.qaUserInfo.name;
        [_userInfoView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_headPicImageView.mas_right).offset(10);
            make.right.equalTo(_userInfoView).offset(-14.0);
            make.centerY.equalTo(_userInfoView);
        }];
    }
    return _nameLabel;
}

#pragma mark - 提示
- (UILabel *)tipLabel{
    if(!_tipLabel){
        _tipLabel = [[UILabel alloc]init];
        _tipLabel.text = @"扫一扫与我联系";
        _tipLabel.textAlignment = NSTextAlignmentCenter;
        _tipLabel.textColor = [UIColor colorFromHexString:@"#ACACAC"];
        _tipLabel.font = [UIFont systemFontOfSize:14];
        [_bgView addSubview:_tipLabel];
        
        [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView);
            make.right.equalTo(_bgView);
            make.top.equalTo(_codeImageView.mas_bottom).offset(15.0);
            make.bottom.equalTo(_bgView).offset(-20.0);
        }];
        
    }
    return _tipLabel;
}

#pragma mark - 保存图片/扫一扫/分享
- (UIView *)operationView{
    if(!_operationView){
        _operationView = [[UIView alloc]init];
        _operationView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_operationView];
        
        [_operationView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view.mas_bottom).offset(-[UIDevice tabBarFullHeight]);
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
        }];
    }
    return _operationView;
}

#pragma mark - 扫一扫
-(UIButton *)scanBtn
{
    if (!_scanBtn) {
        _scanBtn =[UIButton buttonWithType:UIButtonTypeCustom];
        [_scanBtn setTitle:@"扫一扫" forState:UIControlStateNormal];
        [_scanBtn setTitleColor: [UIColor colorFromHexString:@"#535B79"] forState:UIControlStateNormal];
        _scanBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [_scanBtn addTarget:self action:@selector(scanBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
        
        [_operationView addSubview:_scanBtn];
        
        [_scanBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_savePhotoBtn.mas_left).offset(-21);
            make.top.equalTo(_operationView);
            make.bottom.equalTo(_operationView);
        }];
    }
    return _scanBtn;
}
-(void)scanBtnOnClick
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - 保存照片
-(UIButton *)savePhotoBtn
{
    if (!_savePhotoBtn) {
        _savePhotoBtn =[UIButton buttonWithType:UIButtonTypeCustom];
        [_savePhotoBtn setTitle:@"保存图片" forState:UIControlStateNormal];
        [_savePhotoBtn setTitleColor: [UIColor colorFromHexString:@"#535B79"] forState:UIControlStateNormal];
        _savePhotoBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [_savePhotoBtn addTarget:self action:@selector(savePhotoBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
        
        [_operationView addSubview:_savePhotoBtn];
        
        UIView *line1View = [[UIView alloc]init];
        line1View.backgroundColor = [UIColor colorFromHexString:@"#ADADAD"];
        [_operationView addSubview:line1View];
        
        UIView *line2View = [[UIView alloc]init];
        line2View.backgroundColor = [UIColor colorFromHexString:@"#ADADAD"];
        [_operationView addSubview:line2View];
        
        [_savePhotoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_operationView);
            make.top.equalTo(_operationView);
            make.bottom.equalTo(_operationView);
        }];
        
        [line1View mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_savePhotoBtn.mas_left).offset(-10);
            make.centerY.equalTo(_operationView);
            make.width.mas_offset(1.0);
            make.height.mas_offset(14);
        }];
        
        [line2View mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_savePhotoBtn.mas_right).offset(10);
            make.centerY.equalTo(_operationView);
            make.width.mas_offset(1.0);
            make.height.mas_offset(14);
        }];
    }
    return _savePhotoBtn;
}
-(void)savePhotoBtnOnClick
{
    UIImage *image = [self createImage];
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), (__bridge void *)self);
}

- (UIImage *)createImage{
    CGSize size = self.bgView.frame.size;
    
    UIImage* image = nil;
    UIGraphicsBeginImageContextWithOptions(size,NO,UIScreen.mainScreen.scale);
    {
        [self.bgView.layer renderInContext:UIGraphicsGetCurrentContext()];
        image= UIGraphicsGetImageFromCurrentImageContext();
    }
    return image;
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if(!error){
        [SWHUDUtil showHudViewTipInSuperView:self.view withMessage:@"保存成功"];
    }else{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"未知错误" message:@"请前往设置排查照片权限是否开启" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"否" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        UIAlertAction *goAction = [UIAlertAction actionWithTitle:@"是" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [[QWCommonMethod Instance] openSystemSetting];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:goAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

#pragma mark - 分享
-(UIButton *)shareBtn
{
    if (!_shareBtn) {
        _shareBtn =[UIButton buttonWithType:UIButtonTypeCustom];
        [_shareBtn setTitle:@"分享" forState:UIControlStateNormal];
        [_shareBtn setTitleColor: [UIColor colorFromHexString:@"#535B79"] forState:UIControlStateNormal];
        _shareBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [_shareBtn addTarget:self action:@selector(shareBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
        
        [_operationView addSubview:_shareBtn];
        
        [_shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_savePhotoBtn.mas_right).offset(21);
            make.top.equalTo(_operationView);
            make.bottom.equalTo(_operationView);
        }];
    }
    return _shareBtn;
}
-(void)shareBtnOnClick
{
    UIImage *image = [self createImage];
    [[QWCommonMethod Instance] sharedFromSystem:@"群爱宝" withUrl: nil withImage:image success:^{
    } failure:^{
    }];
}

@end
