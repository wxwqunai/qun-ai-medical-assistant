//
//  QWAboutUsController.m
//  qwm
//
//  Created by kevin on 2023/3/30.
//

#import "QWAboutUsController.h"

@interface QWAboutUsController ()

@end

@implementation QWAboutUsController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"关于我们";
    
    UILabel *label =[[UILabel alloc]init];
    label.text = @"了解养生，学习养生，尽在群爱";
    label.font = [UIFont systemFontOfSize:20];
    label.textColor = Color_Main_Green;
    [self.view addSubview:label];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.centerY.equalTo(self.view);
    }];
    
    UIView *topView =[[UIView alloc]init];
//    topView.backgroundColor =[UIColor redColor];
    [self.view addSubview:topView];
    
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.top.equalTo(self.view);
        make.bottom.equalTo(label.mas_top);
    }];
    
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.image = [UIImage imageNamed:@"logo.png"];
    [topView addSubview:imageView];
    
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(topView);
        make.centerY.equalTo(topView);
        make.width.equalTo(topView.mas_height).dividedBy(3);
        make.height.equalTo(imageView.mas_width);
    }];
    
    UILabel *vlabel =[[UILabel alloc]init];
    vlabel.text =  [NSString stringWithFormat:@"V%@", [[QWCommonMethod Instance] currentVersion]];
    vlabel.font = [UIFont systemFontOfSize:16];
    vlabel.textColor = Color_Main_Green;
    [self.view addSubview:vlabel];
    
    [vlabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-[UIDevice tabBarHeight]);
    }];
}



@end
