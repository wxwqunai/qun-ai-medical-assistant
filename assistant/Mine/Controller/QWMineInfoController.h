//
//  QWMineInfoController.h
//  assistant
//
//  Created by kevin on 2023/6/8.
//

#import "QWBaseController.h"

NS_ASSUME_NONNULL_BEGIN
/*
 *个人基本信息
 *环信查看个人信息
 */
@interface QWMineInfoController : QWBaseController

@end

NS_ASSUME_NONNULL_END
