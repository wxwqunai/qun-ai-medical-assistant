//
//  QWXueShuYuYueController.m
//  assistant
//
//  Created by qunai on 2023/9/22.
//

#import "QWXueShuYuYueController.h"
#import "QWWebviewController.h"
#import "QWCalendarView.h"
#import "QWMineModel.h"
#import "QWYuYueRecordCell.h"
#import "QWYuYueNoticeView.h"
#import "QWYuYueTipsView.h" //当天日期提示

@interface QWXueShuYuYueController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) QWYuYueNoticeView *noticeView;

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showDataArr;

@property (nonatomic, strong) QWCalendarView *calendarView;
@property (nonatomic, strong) QWScheduleFindAlllModel *selModel; //选中日期

@property (nonatomic, strong) QWYuYueTipsView *footerTipsView;
@end

@implementation QWXueShuYuYueController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"预约记录";
    
    [self noticeView];
    [self tableView];
    [self loadData];
}

#pragma mark - 会议预告、预约
- (QWYuYueNoticeView *)noticeView{
    if(!_noticeView){
        _noticeView = [[QWYuYueNoticeView alloc]init];
        [self.view addSubview:_noticeView];
        
        [_noticeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.mas_offset([UIDevice navigationFullHeight]);
        }];
        
        __weak __typeof(self) weakSelf = self;
        _noticeView.noticeOnClickBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf jumToWebView:URL_learning_meet];
        };
    }
    return _noticeView;
}

#pragma mark - 日历
- (QWCalendarView *)calendarView{
    if(!_calendarView){
        _calendarView =[[QWCalendarView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 320)];
        __weak __typeof(self) weakSelf = self;
        _calendarView.calendarSelectedBlock = ^(NSString * _Nonnull selDateStr, QWScheduleFindAlllModel * _Nonnull selModel) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.selModel = selModel;
            [strongSelf loadfindAppointmentByDay:selDateStr];
            [strongSelf.tableView reloadData];
        };
        
        _calendarView.meetingreservationBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            NSString *url = strongSelf.menuItem.imgTarget;
            if(!IsStringEmpty(url)){
                [strongSelf jumToWebView:url];
            }

        };
    }
    return _calendarView;
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        //        [_tableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
        //        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
        [_tableView registerClass:[QWYuYueRecordCell class] forCellReuseIdentifier:@"QWYuYueRecordCell"];
        
        _tableView.backgroundColor = Color_TableView_Gray;
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        //        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        //        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        //        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        _tableView.tableHeaderView = [self calendarView];
        _tableView.tableFooterView = [self footerTipsView];
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.equalTo(self.noticeView.mas_bottom);
            make.bottom.equalTo(self.view);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0000001;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.showDataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QWYuYueRecordCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWYuYueRecordCell"];
    QWLearningApplyModel *model = self.showDataArr[indexPath.row];
    cell.conferenceLabel.text = model.useId;
    cell.nameLabel.text = model.name;
    cell.tellLabel.text = model.tell;
    cell.localeLabel.text = model.locale;
    cell.wayLabel.text = model.way;
    cell.startTimeLabel.text = model.startTime;
    cell.endTimeLabel.text = model.endTime;
    cell.rooNumLabel.text = model.roomNum;
    cell.representLabel.text = model.represent;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - 当天会议预约状态提示
- (QWYuYueTipsView *)footerTipsView{
    if(!_footerTipsView){
        _footerTipsView =[[QWYuYueTipsView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 80)];
    }
    return _footerTipsView;
}


#pragma mark - 加载数据
- (void)loadData{
    [self loadScheduleFindAlll];
}
#pragma mark - 下拉刷新
- (void)loadPullDownRefreshDataInfo{
    [self loadData];
}

#pragma mark - webview - 跳转
- (void)jumToWebView:(NSString *)url{
    QWWebviewController *webviewVC =[[QWWebviewController alloc]init];
    webviewVC.url = url;
    webviewVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:webviewVC animated:YES];
}
#pragma mark - 所有预约信息
- (void)loadScheduleFindAlll{
    [SWHUDUtil showHudViewInSuperView:self.view withMessage:@"请等待"];
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_learningSchedule_findAll argument:@{}];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        [SWHUDUtil hideHudView];
        [self.tableView endWRefreshing];
        NSString * code = [NSString stringWithFormat:@"%@",result[@"code"]];
        NSString * msg = [NSString stringWithFormat:@"%@",result[@"msg"]];

        if ([code isEqualToString:@"1"] && [msg isEqualToString:@"ok"]) {
            NSArray * data = result[@"data"];
            self.calendarView.eventsForDayArr = [QWScheduleFindAlllModel mj_objectArrayWithKeyValuesArray:data];
            NSLog(@"");
        }
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        [SWHUDUtil hideHudView];
        //错误
        NSLog(@"%@",errorInfo);

    }];
}

#pragma mark - 预约信息 - 加载数据
- (void)loadfindAppointmentByDay:(NSString *)dateStr{
    if(IsStringEmpty(dateStr)) return;
    NSDictionary *dic = @{
        @"day":dateStr,
    };
    [SWHUDUtil showHudViewInSuperView:self.view withMessage:@"请等待"];
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_learning_findByDay argument:dic];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        [SWHUDUtil hideHudView];
        NSArray *data = (NSArray *)result;
        NSArray *allArr = [QWLearningApplyModel mj_objectArrayWithKeyValuesArray:data];
        [self.showDataArr removeAllObjects];
        self.footerTipsView.tipLabel.text = @"";
        if([self.selModel.timeconfig isEqualToString:@"0"]||[self.selModel.timeconfig isEqualToString:@"4"]){//空闲、被禁
            [allArr enumerateObjectsUsingBlock:^(QWLearningApplyModel*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if(!IsStringEmpty(obj.freeTwo)){
                    self.footerTipsView.tipLabel.text = obj.freeTwo;
                    *stop = YES;
                }
            }];
        }else{
            [self.showDataArr addObjectsFromArray:allArr];
            [allArr enumerateObjectsUsingBlock:^(QWLearningApplyModel*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if(!IsStringEmpty(obj.freeTwo)){
                    self.footerTipsView.tipLabel.text = obj.freeTwo;
                    *stop = YES;
                }
            }];
        }
       
        [self.tableView reloadData];
        

    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        [SWHUDUtil hideHudView];
        //错误
        NSLog(@"%@",errorInfo);
        
        [self.showDataArr removeAllObjects];
        self.footerTipsView.tipLabel.text = @"";
        [self.tableView reloadData];
    }];
}

- (NSMutableArray *)showDataArr{
    if(!_showDataArr){
        _showDataArr = [[NSMutableArray alloc]init];
    }
    return _showDataArr;
}

@end
