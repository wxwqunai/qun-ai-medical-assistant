//
//  QWMallOrderHomeController.m
//  assistant
//
//  Created by qunai on 2024/4/8.
//

#import "QWMallOrderHomeController.h"
#import "QWOrderWaitReceivedCell.h"

@interface QWMallOrderHomeController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showDataArr;

@property (nonatomic, strong) UISegmentedControl *segment;
@end

@implementation QWMallOrderHomeController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"商品订单";
    [self segment];
    [self tableView];
    [self loadData];
}

#pragma mark - 顶部切换
- (UISegmentedControl *)segment{
    if(!_segment){
        //初始化一个UISegmentedControl控件test
        
        NSArray *items = @[@"全部",@"未付款",@"待发货",@"待收货",@"已完成"];;
        _segment = [[UISegmentedControl alloc]initWithItems:items];
        _segment.frame = CGRectMake(0, [UIDevice navigationFullHeight]+2, kScreenWidth, 30);

        //设置无边框分段控制器

        //设置test控件的颜色为透明
        _segment.tintColor = [UIColor clearColor];
        //定义选中状态的样式selected，类型为字典
        NSDictionary *selected = @{NSFontAttributeName:[UIFont systemFontOfSize:16],
                                   NSForegroundColorAttributeName:Color_Main_Green};
        //定义未选中状态下的样式normal，类型为字典
        NSDictionary *normal = @{NSFontAttributeName:[UIFont systemFontOfSize:14],
                                     NSForegroundColorAttributeName:[UIColor blackColor]};

        /*
        NSFontAttributeName -> 系统宏定义的特殊键，用来给格式字典中的字体赋值
        NSForegroundColorAttributeName -> 系统宏定义的特殊键，用来给格式字典中的字体颜色赋值
        */

        //通过setTitleTextAttributes: forState: 方法来给test控件设置文字内容的格式
        [_segment setTitleTextAttributes:normal forState:UIControlStateNormal];
        [_segment setTitleTextAttributes:selected forState:UIControlStateSelected];
        
        [_segment addTarget:self action:@selector(click:) forControlEvents:UIControlEventValueChanged];

        //设置test初始状态下的选中下标
        _segment.selectedSegmentIndex = 0;

        //将test添加到某个View上
        [self.view addSubview:_segment];
    }
    return _segment;
}

- (void)click:(UISegmentedControl *)segment{

}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        //        [_tableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
        [_tableView registerClass:[QWOrderWaitReceivedCell class] forCellReuseIdentifier:@"QWOrderWaitReceivedCell"];
        
        _tableView.backgroundColor=Color_TableView_Gray;
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        //        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        //        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        //        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        //        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.equalTo(self.segment.mas_bottom).offset(2);
            make.bottom.equalTo(self.view);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0000001;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QWOrderWaitReceivedCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWOrderWaitReceivedCell"];
    cell.storeNameLabel.text = @"群爱智慧医疗";
    cell.numLabel.text = @"x1";
    cell.picImageView.image =[UIImage imageNamed:@"dev_toothpaste.png"];
    cell.productNameLabel.text = @"敖东大幽小幽抗菌肽口腔抑菌膏牙膏，护理抗菌男女成人清洁口气清新，专利抗菌肽，口腔深护理！";
    cell.realPriceLabel.text = @"¥24.9";
    cell.originalPriceLabel.text = @"¥99.9";
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



#pragma mark - 加载数据
- (void)loadData{
}
@end
