//
//  QWInputController.m
//  assistant
//
//  Created by kevin on 2023/6/20.
//

#import "QWInputController.h"
#import "QWButtonView.h"

@interface QWInputController ()<UITextViewDelegate,UIScrollViewDelegate>
@property (nonatomic,strong) UIScrollView *bgScroolView;
@property (nonatomic,strong) UIView *containerView;

@property (nonatomic,strong) UIView *cnterView;
@property (nonatomic,strong) UITextView *importInfoView;
@property (nonatomic,strong) UILabel *numLabel;

@property (nonatomic,strong) QWButtonView *buttonView;

@end

@implementation QWInputController

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self bgScroolView];
}

-(UIScrollView *)bgScroolView
{
    if (!_bgScroolView) {
        _bgScroolView =[[UIScrollView alloc]init];
        _bgScroolView.alwaysBounceVertical=YES;
        _bgScroolView.backgroundColor =[UIColor whiteColor];
        _bgScroolView.delegate = self;
//        _bgScroolView.contentSize=CGSizeMake(0, kScreenHeight-[UIDevice statusBarHeight]);
        [self.view addSubview:_bgScroolView];
        
        [_bgScroolView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.equalTo(self.view);
            make.bottom.equalTo(self.view);
        }];
        
        _containerView = [[UIView alloc] init];
        _containerView.backgroundColor = [UIColor clearColor];
        [_bgScroolView addSubview:_containerView];
        [_containerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_bgScroolView);
            make.width.equalTo(_bgScroolView); // 需要设置宽度和scrollview宽度一样
        }];

        [self cnterView];
        [self buttonView];
        
        [_containerView mas_makeConstraints:^(MASConstraintMaker *make) {
             make.bottom.equalTo(_buttonView.mas_bottom).offset(20);// 这里放最后一个view的底部
         }];
    }
    return _bgScroolView;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}

#pragma mark - 输入
- (UIView *)cnterView{
    if(!_cnterView){
        _cnterView = [[UIView alloc]init];
        _cnterView.backgroundColor =[UIColor clearColor];
        [_containerView addSubview:_cnterView];
        
        [_cnterView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(18);
            make.right.mas_equalTo(-18);
            make.top.mas_equalTo(_containerView).offset(20.0);
            make.height.mas_equalTo(self.inputType == InputType_defalut?40.0:220);
        }];
        
        [self importInfoView];
        [self numLabel];
    }
    return _cnterView;
}

//输入框
-(UITextView *)importInfoView
{
    if (!_importInfoView) {
        _importInfoView =[[UITextView alloc]init];
        _importInfoView.text = self.inputStr;
        _importInfoView.returnKeyType=UIReturnKeyDone;
        _importInfoView.delegate=self;
        _importInfoView.font=[UIFont fontWithName:@"PingFangSC-Medium" size:15];
        _importInfoView.textColor=[UIColor colorFromHexString:@"#707070"];
        _importInfoView.backgroundColor=[UIColor clearColor];
//        _importInfoView.scrollEnabled = NO;
        
        _importInfoView.layer.borderWidth=1;
        _importInfoView.layer.borderColor=Color_Main_Green.CGColor;
        
        [_cnterView addSubview:_importInfoView];
        
        [_importInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_cnterView);
        }];
    }
    return _importInfoView;
}
#pragma mark- UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if( [ @"\n" isEqualToString: text]){
        //你的响应方法
        [self.view endEditing:YES];
        return NO;
    }
    
    if (range.length + range.location > textView.text.length) {
        return NO;
    }
    
    NSUInteger length = textView.text.length + text.length - range.length;
    
    if (length<=self.inputType) {
        _numLabel.text=[NSString stringWithFormat:@"%ld/%ld",length,self.inputType];
        return YES;
    }
    
    return NO;
}

//字数显示
-(UILabel *)numLabel
{
    if (!_numLabel) {
        _numLabel=[[UILabel alloc]init];
        _numLabel.textAlignment=NSTextAlignmentRight;
        _numLabel.font=[UIFont fontWithName:@"PingFangSC-Medium" size:16];
        _numLabel.text=[NSString stringWithFormat:@"%ld/%ld",_importInfoView.text.length,self.inputType];
        [_cnterView addSubview:_numLabel];
        
        [_numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_importInfoView.mas_bottom).offset(8);
            make.right.equalTo(_importInfoView).offset(-4);
        }];
    }
    return _numLabel;
}

#pragma mark - 提交
- (QWButtonView*)buttonView{
    if(!_buttonView){
        _buttonView= [[QWButtonView alloc]init];
        [_buttonView changeButtonTitle:IsStringEmpty(self.btnNameStr)?@"确定":self.btnNameStr];
        __weak __typeof(self) weakSelf = self;
        _buttonView.buttonBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            
            if(IsStringEmpty(strongSelf.importInfoView.text)){
                [SWHUDUtil hideHudViewWithMessageSuperView:strongSelf.view withMessage:@"请输入内容！"];
                return;
            }
            
            strongSelf.inputInfoBlock(strongSelf.importInfoView.text);
            [strongSelf.navigationController popViewControllerAnimated:YES];
        };
        
        
        [_containerView addSubview:_buttonView];
        
        [_buttonView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.top.mas_equalTo(_numLabel.mas_bottom);
            make.height.mas_equalTo(95.0);
        }];
        
    }
    return _buttonView;
}

@end
