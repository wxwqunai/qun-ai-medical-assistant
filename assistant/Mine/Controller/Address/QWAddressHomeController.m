//
//  QWAddressHomeController.m
//  assistant
//
//  Created by qunai on 2024/4/1.
//

#import "QWAddressHomeController.h"
#import "QWAddressEditedController.h"
#import "QWComButtonView.h"
#import "QWAdsHomeCell.h"
#import "QWAddressModel.h"

@interface QWAddressHomeController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showDataArr;
@property (nonatomic, strong) QWComButtonView *joinButtonView;

@end

@implementation QWAddressHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"收货地址";
    
    [self tableView];
    [self loadData];
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        //        [_tableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
        //        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
        [_tableView registerClass:[QWAdsHomeCell class] forCellReuseIdentifier:@"QWAdsHomeCell"];
        
        _tableView.backgroundColor=Color_TableView_Gray;
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        //        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        //        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        //        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view);
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.joinButtonView.mas_top);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0000001;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.showDataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QWAdsHomeCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWAdsHomeCell"];
    NSDictionary *dic = self.showDataArr[indexPath.row];
    cell.nameLabel.text = dic[@"name"];
    cell.phoneLabel.text = dic[@"phone"];
    cell.addressLabel.text = dic[@"address"];
    cell.defaultLabel.hidden = ![dic[@"isDeault"] boolValue];
    __weak typeof(self) weakSelf = self;
    cell.editedBlock = ^{
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        QWAddressModel *addressModel = [[QWAddressModel alloc]init];
        [strongSelf jumpToAddressEdited:addressModel];
    };
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - 加入会议
- (QWComButtonView *)joinButtonView{
    if(!_joinButtonView){
        _joinButtonView = [[QWComButtonView alloc]init];
        _joinButtonView.btnName = @"添加收货地址";
        [self.view addSubview:_joinButtonView];
        
        __weak typeof(self) weakSelf = self;
        _joinButtonView.makeActionClickBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf jumpToAddressEdited:nil];
        };
        
        [_joinButtonView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.view).offset(-[UIDevice safeDistanceBottom]-20);
            make.height.mas_equalTo(40);
        }];
    }
    return _joinButtonView;
}
- (void)jumpToAddressEdited:(QWAddressModel *)addressModel{
    QWAddressEditedController *editedVC = [[QWAddressEditedController alloc]init];
    editedVC.addressModel = addressModel;
    [self.navigationController pushViewController:editedVC animated:YES];
}

#pragma mark - 加载数据
- (void)loadData{
    NSArray *arr = @[@{@"name":@"A王文",@"phone":@"186****6429",@"address":@"上海市黄浦区南昌路45号",@"isDeault":@YES},
                     @{@"name":@"A李伟",@"phone":@"182****0417",@"address":@"山西省太原市迎泽区迎泽大街新建路口中铁三局",@"isDeault":@NO}];
    self.showDataArr = [NSMutableArray arrayWithArray:arr];
    [self.tableView reloadData];
}

#pragma mark - 下拉刷新
- (void)loadPullDownRefreshDataInfo{
    [_tableView endWRefreshing];
}


@end
