//
//  QWAddressEditedController.h
//  assistant
//
//  Created by qunai on 2024/4/3.
//

#import "QWBaseController.h"
#import "QWAddressModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface QWAddressEditedController : QWBaseController
@property (nonatomic, strong) QWAddressModel *addressModel;
@end

NS_ASSUME_NONNULL_END
