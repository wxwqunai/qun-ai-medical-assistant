//
//  QWAddressEditedController.m
//  assistant
//
//  Created by qunai on 2024/4/3.
//

#import "QWAddressEditedController.h"
#import "QWAdsInputCell.h"
#import "QWAdsTextViewCell.h"
#import "QWAdsSwitchCell.h"
#import "QWComButtonView.h"

@interface QWAddressEditedController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showDataArr;
@property (nonatomic, strong) QWComButtonView *saveButtonView;
@end

@implementation QWAddressEditedController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.addressModel?@"修改收货地址":@"添加收货地址";
    if(self.addressModel) [self.rightBarButtonItem setTitle:@"删除"];
    
    [self saveButtonView];
    [self tableView];
    [self loadData];
}
#pragma mark-导航条右边按钮
-(void)navRightBaseButtonClick
{
    [SWHUDUtil hideHudViewWithMessageSuperView:self.view withMessage:@"删除成功！"];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
    });
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        //        [_tableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
        //        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
        [_tableView registerClass:[QWAdsInputCell class] forCellReuseIdentifier:@"QWAdsInputCell"];
        [_tableView registerClass:[QWAdsTextViewCell class] forCellReuseIdentifier:@"QWAdsTextViewCell"];
        [_tableView registerClass:[QWAdsSwitchCell class] forCellReuseIdentifier:@"QWAdsSwitchCell"];
        
        _tableView.backgroundColor=Color_TableView_Gray;
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        //        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        //        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        //        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        //        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view);
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.saveButtonView.mas_top);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0000001;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rowNum = 0;
    if(section == 0) rowNum = self.showDataArr.count;
    if(section == 1) rowNum = 2;
    return rowNum;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        QWAdsInputCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWAdsInputCell"];
        NSDictionary *dataDic = self.showDataArr[indexPath.row];
        cell.nameLabel.text = dataDic[@"name"];
        cell.inputField.placeholder = [NSString stringWithFormat:@"请输入%@",dataDic[@"name"]];
        return cell;
    }
    
    if(indexPath.section == 1){
        if(indexPath.row == 0){
            QWAdsSwitchCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWAdsSwitchCell"];
            cell.nameLabel.text = @"设置默认地址";
            return cell;
        }
        if(indexPath.row == 1){
            QWAdsTextViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWAdsTextViewCell"];
            cell.nameLabel.text = @"智能识别";
            cell.placeholderLabel.text = @"粘贴收货信息到此处，将自动识别姓名/电话/地址";
            return cell;
        }
    }
    
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"cell"];
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

//- (QWAddressModel *)addressModel{
//    if(!_addressModel){
//        _addressModel = [[QWAddressModel alloc]init];
//    }
//    return _addressModel;
//}

#pragma mark - 保存
- (QWComButtonView *)saveButtonView{
    if(!_saveButtonView){
        _saveButtonView = [[QWComButtonView alloc]init];
        _saveButtonView.btnName = @"保存";
        [self.view addSubview:_saveButtonView];
        
        __weak typeof(self) weakSelf = self;
        _saveButtonView.makeActionClickBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf saveAddress];
        };
        
        [_saveButtonView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.view).offset(-[UIDevice safeDistanceBottom]-20);
            make.height.mas_equalTo(40);
        }];
    }
    return _saveButtonView;
}
- (void)saveAddress{
    [SWHUDUtil hideHudViewWithMessageSuperView:self.view withMessage:@"保存成功！"];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
    });
}

#pragma mark - 加载数据
- (void)loadData{
    self.showDataArr = [NSMutableArray arrayWithArray:@[@{@"name":@"收件人"},@{@"name":@"手机号"},@{@"name":@"省市区"},@{@"name":@"详细地址"}]];
    [self.tableView reloadData];
}



@end
