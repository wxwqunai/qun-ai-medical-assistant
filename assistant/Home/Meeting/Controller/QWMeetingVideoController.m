//
//  QWMeetingVideoController.m
//  assistant
//
//  Created by qunai on 2023/12/12.
//

#import "QWMeetingVideoController.h"
#import <AgoraRtcKit/AgoraRtcKit.h>
#import "KeyCenter.h"
#import "assistant-Swift.h"
#import "QWMeetVideoScrollView.h"
#import "QWMeetingModel.h"
#import "QWMeetOperationsView.h"
#import "QWMeetInfoController.h"
#import "ReplayKit/ReplayKit.h"
#import "RPSystemBroadcastPickerView+FindButton.h"
#import "QWScreenShareOperationsView.h"//屏幕共享-操作

@interface QWMeetingVideoController ()<AgoraRtcEngineDelegate,UIGestureRecognizerDelegate>
@property (nonatomic, strong)AgoraRtcEngineKit *agoraKit;
@property (nonatomic, strong)AgoraRtcChannelMediaOptions *option;

@property (nonatomic, strong) QWMeetVideoScrollView *scrollView;
@property (nonatomic, strong) QWVideoView *localView;
@property (nonatomic, strong) QWVideoView *remoteView;

@property (nonatomic, strong) QWMeetOperationsView *operationsView;

//屏幕分享
@property (nonatomic, strong) AgoraScreenCaptureParameters2 *screenParams;
@property (nonatomic, strong)RPSystemBroadcastPickerView *systemBroadcastPicker;
@property (nonatomic, strong) QWScreenShareOperationsView *screenShareOperationsView;

@end

@implementation QWMeetingVideoController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.agoraKit stopScreenCapture];
    [self.agoraKit disableAudio];
    [self.agoraKit disableVideo];
    [self.agoraKit stopPreview];
    [self.agoraKit leaveChannel:nil];
    [AgoraRtcEngineKit destroy];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorFromHexString:@"#292A2D"];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openMeetOptionsTap:)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];

    [self initUI];
    [self initAgoraVideo];
}

-  (void)initUI{
    [self screenShareOperationsView];
    [self scrollView];
}

#pragma mark - 视频
- (QWMeetVideoScrollView *)scrollView{
    if(!_scrollView){
        _scrollView=[[QWMeetVideoScrollView alloc]init];
        _scrollView.localUserInfoModel = self.localUserInfoModel;
        self.localView = _scrollView.twoChannelView.localView;
        self.remoteView = _scrollView.twoChannelView.remoteView;
        [self.view addSubview:_scrollView];
        [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
    }
    return _scrollView;
}

#pragma mark - Agora 声网
- (void)initAgoraVideo{
    NSString *channelName = self.localUserInfoModel.channelName;
    
    AgoraRtcEngineConfig *config = [[AgoraRtcEngineConfig alloc] init];
    config.appId = KeyCenter.AppId;
    config.channelProfile = AgoraChannelProfileLiveBroadcasting;
    
    self.agoraKit = [AgoraRtcEngineKit sharedEngineWithConfig:config delegate:self];
    self.scrollView.agoraKit = self.agoraKit;
    [self.agoraKit setClientRole:(AgoraClientRoleBroadcaster)]; //用户角色主播（多人视频都是主播）
    [self.agoraKit enableAudio]; //启用音频模块
    [self.agoraKit enableVideo]; //启用视频模块
    
    AgoraVideoEncoderConfiguration *encoderConfig = [[AgoraVideoEncoderConfiguration alloc] initWithSize:CGSizeMake(960, 540)
                                                                                               frameRate:(AgoraVideoFrameRateFps15)
                                                                                                 bitrate:15
                                                                                         orientationMode:(AgoraVideoOutputOrientationModeFixedPortrait)
                                                                                              mirrorMode:(AgoraVideoMirrorModeAuto)];
    [self.agoraKit setVideoEncoderConfiguration:encoderConfig];
    
    // enable volume indicator
    [self.agoraKit enableAudioVolumeIndication:200 smooth:3 reportVad:NO];
    
    [self.scrollView setupLocalVideo]; //设置本地视频
    // you have to call startPreview to see local video
    [self.agoraKit startPreview];
    
    [self.agoraKit setDefaultAudioRouteToSpeakerphone:self.localUserInfoModel.isOpenLoudspeaker]; // 开启或关闭扬声器播放。
    [self.agoraKit muteLocalAudioStream:!self.localUserInfoModel.isOpenMicrophone]; // 开启或关闭麦克风推流。
    [self.agoraKit muteLocalVideoStream:!self.localUserInfoModel.isOpenVideo]; // 开启或关闭视频推流。
    
    // start joining channel
    // 1. Users can only see each other after they join the
    // same channel successfully using the same app id.
    // 2. If app certificate is turned on at dashboard, token is needed
    // when joining channel. The channel name and uid used to calculate
    // the token has to match the ones used for channel join
    AgoraRtcChannelMediaOptions *options = [[AgoraRtcChannelMediaOptions alloc] init];
    self.option = options;
    options.autoSubscribeAudio = YES;
    options.autoSubscribeVideo = YES;
    options.publishCameraTrack = YES;
    options.publishMicrophoneTrack = YES;
    options.clientRoleType = AgoraClientRoleBroadcaster;
    
    [[NetworkManager shared] generateTokenWithChannelName:channelName uid:0 success:^(NSString * _Nullable token) {
                
        //userAccount ：该参数为必填，最大不超过 255 字节，不可填 nil
        NSMutableDictionary *param = [[NSMutableDictionary alloc]init];
        if(!IsStringEmpty(AppProfile.qaUserInfo.id)) [param setValue:AppProfile.qaUserInfo.id forKey:@"id"];
        if(!IsStringEmpty(self.localUserInfoModel.user_header_url)) [param setValue:self.localUserInfoModel.user_header_url forKey:@"userHeaderUrl"];
        if(!IsStringEmpty(self.localUserInfoModel.meet_name)) [param setValue:self.localUserInfoModel.meet_name forKey:@"name"];
        NSString *infoStr = [param mj_JSONString];
        
        int result = [self.agoraKit joinChannelByToken:token channelId:channelName userAccount:infoStr mediaOptions:options joinSuccess:nil];
        if (result != 0) {
            NSLog(@"joinChannel call failed: %d, please check your params", result);
        }
    }];
}
#pragma mark - AgoraRtcEngineDelegate
/// callback when error occured for agora sdk, you are recommended to display the error descriptions on demand
/// to let user know something wrong is happening
/// Error code description can be found at:
/// en: https://api-ref.agora.io/en/voice-sdk/macos/3.x/Constants/AgoraErrorCode.html#content
/// cn: https://docs.agora.io/cn/Voice/API%20Reference/oc/Constants/AgoraErrorCode.html
/// @param errorCode error code of the problem
- (void)rtcEngine:(AgoraRtcEngineKit *)engine didOccurError:(AgoraErrorCode)errorCode {
    [XWLogUtil log:[NSString stringWithFormat:@"Error %ld occur",errorCode] level:(XWLogLevelError)];
}

- (void)rtcEngine:(AgoraRtcEngineKit *)engine didJoinChannel:(NSString *)channel withUid:(NSUInteger)uid elapsed:(NSInteger)elapsed {
    [XWLogUtil log:[NSString stringWithFormat:@"Join %@ with uid %lu elapsed %ldms", channel, uid, elapsed] level:(XWLogLevelDebug)];
    
    AgoraUserInfo *userInfo = [self.agoraKit getUserInfoByUid:uid withError:nil];
    self.localView.userInfoModel.userInfo = userInfo;
}

/// callback when a remote user is joinning the channel, note audience in live broadcast mode will NOT trigger this event
/// @param uid uid of remote joined user
/// @param elapsed time elapse since current sdk instance join the channel in ms
- (void)rtcEngine:(AgoraRtcEngineKit *)engine didJoinedOfUid:(NSUInteger)uid elapsed:(NSInteger)elapsed {
    [XWLogUtil log:[NSString stringWithFormat:@"remote user join: %lu %ldms", uid, elapsed] level:(XWLogLevelDebug)];    
    [self.scrollView addRemoteUid:uid];
}

/// callback when a remote user is leaving the channel, note audience in live broadcast mode will NOT trigger this event
/// @param uid uid of remote joined user
/// @param reason reason why this user left, note this event may be triggered when the remote user
/// become an audience in live broadcasting profile
- (void)rtcEngine:(AgoraRtcEngineKit *)engine didOfflineOfUid:(NSUInteger)uid reason:(AgoraUserOfflineReason)reason {
    [XWLogUtil log:[NSString stringWithFormat:@"remote user left: %lu", uid] level:(XWLogLevelDebug)];
    [self.scrollView deleteRemoteUid:uid];
}

- (void)rtcEngine:(AgoraRtcEngineKit * _Nonnull)engine activeSpeaker:(NSUInteger)speakerUid {
    [self.scrollView activeSpeaker:speakerUid];
}


#pragma mark - 音频处理- 麦克风开关
//本地音频状态发生改变回调
- (void)rtcEngine:(AgoraRtcEngineKit * _Nonnull)engine
localAudioStateChanged:(AgoraAudioLocalState)state error:(AgoraAudioLocalError)error{
    
}
//远端音频流状态发生改变回调
- (void)rtcEngine:(AgoraRtcEngineKit * _Nonnull)engine
remoteAudioStateChangedOfUid:(NSUInteger)uid state:(AgoraAudioRemoteState)state reason:(AgoraAudioRemoteReason)reason elapsed:(NSInteger)elapsed{
    [XWLogUtil log:[NSString stringWithFormat:@"remoteAudioStateChangedOfUid: %lu", uid] level:(XWLogLevelDebug)];
    
    BOOL isOpen = YES;
    if(state == AgoraAudioRemoteStateStopped) isOpen = NO;
    [self.scrollView changeAudioRemoteState:isOpen withUid:uid];
}

#pragma mark - 视频处理- 视频开关
//远端视频状态发生改变回调
- (void)rtcEngine:(AgoraRtcEngineKit * _Nonnull)engine
remoteVideoStateChangedOfUid:(NSUInteger)uid state:(AgoraVideoRemoteState)state reason:(AgoraVideoRemoteReason)reason elapsed:(NSInteger)elapsed{
    [XWLogUtil log:[NSString stringWithFormat:@"remoteVideoStateChangedOfUid: %lu", uid] level:(XWLogLevelDebug)];
    BOOL isOpen = YES;
    if(state == AgoraVideoRemoteStateStopped) isOpen = NO;
    [self.scrollView changeVideoRemoteState:isOpen withUid:uid];
}

#pragma mark -UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isDescendantOfView:self.scrollView.multiChannelView.collectionView]) {
        if ([self.scrollView.multiChannelView.collectionView indexPathForItemAtPoint:[touch locationInView:self.scrollView.multiChannelView.collectionView]]) {
                return NO;
        }
    }
    return YES;
}
#pragma mark - 手势 - 打开/关闭控制页面
- (void)openMeetOptionsTap:(UIGestureRecognizer *)ges{
    if (ges.state == UIGestureRecognizerStateEnded ) {
        self.operationsView.isShowView = !self.operationsView.isShowView;
    }
}
- (QWMeetOperationsView *)operationsView{
    if(!_operationsView){
        _operationsView = [[QWMeetOperationsView alloc]init];
        _operationsView.agoraKit = self.agoraKit;
        _operationsView.localUserInfoModel = self.localUserInfoModel;
        _operationsView.isShowView = NO;
        [self.view addSubview:_operationsView];
        [self.scrollView bringSubviewToFront:self.operationsView];
        [_operationsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
        __weak __typeof(self) weakSelf = self;
        _operationsView.meetOperationsBlock = ^(MeetOperationType type) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if(type == MeetOperationType_end){
                [strongSelf endMeet];
            }
            if(type == MeetOperationType_microphone_open){
                [strongSelf changeLocalAudioState:YES];
            }
            if(type == MeetOperationType_microphone_close){
                [strongSelf changeLocalAudioState:NO];
            }
            if(type == MeetOperationType_video_open){
                [strongSelf changeLocalVideoState:YES];
            }
            if(type == MeetOperationType_video_close){
                [strongSelf changeLocalVideoState:NO];
            }
            if(type == MeetOperationType_openShowMeetingInfo){
                [strongSelf openShowMeetingBaseInfo];
            }
            if(type == MeetOperationType_shareScreen_open){
                [strongSelf startScreenCapture];
            }
            if(type == MeetOperationType_shareScreen_close){
                [strongSelf stopScreenCapture];
            }
        };
    }
    return _operationsView;
}

#pragma mark - 结束会议
- (void)endMeet{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 本地音频变化
- (void)changeLocalAudioState:(BOOL)isOpen{
    self.localView.isOpenAideo = isOpen;
}

#pragma mark - 本地视频变化
- (void)changeLocalVideoState:(BOOL)isOpen{
    self.localView.isOpenVideo = isOpen;
}

#pragma mark - 打开会议基本信息
- (void)openShowMeetingBaseInfo{
    QWMeetInfoController *meetingInfoVC = [[QWMeetInfoController alloc]init];
    meetingInfoVC.localUserInfoModel = self.localUserInfoModel;
    meetingInfoVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:meetingInfoVC animated:NO completion:nil];
}

#pragma mark - 屏幕共享
- (QWScreenShareOperationsView *)screenShareOperationsView{
    if(!_screenShareOperationsView){
        _screenShareOperationsView=[[QWScreenShareOperationsView alloc]init];
        _screenShareOperationsView.hidden = YES;
        [self.view addSubview:_screenShareOperationsView];
        [_screenShareOperationsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
        __weak __typeof(self) weakSelf = self;
        _screenShareOperationsView.audioOptionBlock = ^(BOOL isOpen) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.screenParams.captureAudio = isOpen;
            [strongSelf.agoraKit updateScreenCapture:strongSelf.screenParams];
        };
        
        _screenShareOperationsView.stopShareBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf stopScreenCapture];
        };
    }
    return _screenShareOperationsView;
}
- (AgoraScreenCaptureParameters2 *)screenParams {
    if (_screenParams == nil) {
        _screenParams = [[AgoraScreenCaptureParameters2 alloc] init];
        _screenParams.captureAudio = YES;
        _screenParams.captureVideo = YES;
        AgoraScreenAudioParameters *audioParams = [[AgoraScreenAudioParameters alloc] init];
        audioParams.captureSignalVolume = 50;
        _screenParams.audioParams = audioParams;
        AgoraScreenVideoParameters *videoParams = [[AgoraScreenVideoParameters alloc] init];
        videoParams.dimensions = [self screenShareVideoDimension];
        videoParams.frameRate = AgoraVideoFrameRateFps15;
        videoParams.bitrate = AgoraVideoBitrateStandard;
    }
    return _screenParams;
}

- (void)prepareSystemBroadcaster {
    CGRect frame = CGRectMake(0, 0, 60, 60);
    self.systemBroadcastPicker = [[RPSystemBroadcastPickerView alloc]initWithFrame:frame];
    self.systemBroadcastPicker.showsMicrophoneButton = NO;
    self.systemBroadcastPicker.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin;
    NSString *bundleId = [NSBundle mainBundle].bundleIdentifier;
    self.systemBroadcastPicker.preferredExtension = [NSString stringWithFormat:@"%@.ScreenShareExtension",bundleId];
}

- (CGSize)screenShareVideoDimension {
    CGSize screenSize = UIScreen.mainScreen.bounds.size;
    CGSize boundingSize = CGSizeMake(540, 960);
    CGFloat mW = boundingSize.width / screenSize.width;
    CGFloat mH = boundingSize.height / screenSize.height;
    if (mH < mW) {
        boundingSize.width = boundingSize.height / screenSize.height * screenSize.width;
    } else if (mW < mH) {
        boundingSize.height = boundingSize.width / screenSize.width * screenSize.height;
    }
    return boundingSize;
}
- (void)rtcEngine:(AgoraRtcEngineKit * _Nonnull)engine localVideoStateChangedOfState:(AgoraVideoLocalState)state error:(AgoraLocalVideoStreamError)error sourceType:(AgoraVideoSourceType)sourceType{
    if (sourceType == AgoraVideoSourceTypeScreen) {
        //开始屏幕共享
        if(state == AgoraVideoLocalStateCapturing){
            self.option.publishScreenCaptureAudio = YES;
            self.option.publishScreenCaptureVideo = YES;
            self.option.publishCameraTrack = NO;
            [self.agoraKit updateChannelWithMediaOptions:self.option];
            
            //UI变化
            self.operationsView.isOpenScreenShare = YES;
            self.screenShareOperationsView.hidden = NO;
            [self.view insertSubview:self.screenShareOperationsView belowSubview:self.operationsView];
        }
       //关闭屏幕共享
        if (state == AgoraVideoLocalStateStopped) {
            self.operationsView.isOpenScreenShare = NO;
            self.screenShareOperationsView.hidden = YES;
            [self.view insertSubview:self.screenShareOperationsView belowSubview:self.scrollView];
        }
    }
}

//屏幕共享-打开
- (void)startScreenCapture{
    if (@available(iOS 12.0, *)) {
    }else{
        [SWHUDUtil hideHudViewWithMessageSuperView:self.view withMessage:@"Minimum support iOS version is 12.0"];
        return;
    }

    [self.agoraKit setScreenCaptureScenario:AgoraScreenScenarioVideo];//设置屏幕共享场景，按照实际使用场景
    [self.agoraKit setAudioScenario:AgoraAudioScenarioGameStreaming];
    [self.agoraKit startScreenCapture:self.screenParams];
    [self prepareSystemBroadcaster];
    
    //找到按钮，直接执行
    for (UIView *view in self.systemBroadcastPicker.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            [((UIButton *)view) sendActionsForControlEvents:(UIControlEventAllEvents)];
            break;
        }
    }
}
//屏幕共享-关闭
- (void)stopScreenCapture{
    [self.agoraKit setAudioScenario:AgoraAudioScenarioDefault];
    self.operationsView.isOpenScreenShare = NO;
    self.screenShareOperationsView.hidden = YES;
    [self.view insertSubview:self.screenShareOperationsView belowSubview:self.scrollView];
    
    [self.agoraKit stopScreenCapture];
    self.option.publishScreenCaptureAudio = NO;
    self.option.publishScreenCaptureVideo = NO;
    self.option.publishCameraTrack = YES;
    [self.agoraKit updateChannelWithMediaOptions:self.option];
}

@end
