//
//  QWMeetingHomeController.m
//  assistant
//
//  Created by qunai on 2023/12/11.
//

#import "QWMeetingHomeController.h"
#import "XWQRCodeReader.h"
#import "QWMeetHomeOptionsView.h"
#import "QWMeetEmptybgView.h"
#import "QWMeetingHomeHeaderView.h"
#import "QWMeetingHomeBookingCell.h"

#import "QWMeetJoinInfoController.h"
#import "QWMeetQuickJoinController.h"
#import "QWMeetingBookingController.h"
#import "QWBookingDetailController.h"

@interface QWMeetingHomeController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableDictionary *showDataDic;

@property (nonatomic, strong) QWMeetHomeOptionsView *optionsView;
@property (nonatomic, strong) QWMeetEmptybgView *emptybgView;
@end

@implementation QWMeetingHomeController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self loadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"群爱音视频";
    
    [self tableView];
    [self loadData];
}


#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=100;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        [_tableView registerClass:[QWMeetingHomeHeaderView class] forHeaderFooterViewReuseIdentifier:@"QWMeetingHomeHeaderView"];
        [_tableView registerClass:[QWMeetingHomeBookingCell class] forCellReuseIdentifier:@"QWMeetingHomeBookingCell"];
        
        _tableView.backgroundColor= Color_TableView_Gray;
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        
        //选择操作
        _tableView.tableHeaderView = [self optionsView];
        //无会议空白
        _tableView.backgroundView = [self emptybgView];
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 0.0000001;
//}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    self.emptybgView.hidden = self.showDataDic.count;
    return self.showDataDic.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *startDateStr = self.showDataDic.allKeys[section];
    NSArray *showDataArr = self.showDataDic[startDateStr];
    return showDataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QWMeetingHomeBookingCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWMeetingHomeBookingCell"];
    NSString *startDateStr = self.showDataDic.allKeys[indexPath.section];
    NSArray *showDataArr = self.showDataDic[startDateStr];
    QWAgoraUserInfoModel *bookingModel = showDataArr[indexPath.row];
    cell.nameLabel.text = bookingModel.title;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"HH:mm";
    NSString *start = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:bookingModel.startDate]];
    NSString *end = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:bookingModel.endDate]];

    cell.timeLabel.text = [NSString stringWithFormat:@"%@-%@ · %@",start,end,[bookingModel channelNameBlankSpace]];
    return cell;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    QWMeetingHomeHeaderView *titleView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"QWMeetingHomeHeaderView"];
    NSString *startDateStr = self.showDataDic.allKeys[section];
    titleView.nameLabel.text = startDateStr;
    return titleView;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *startDateStr = self.showDataDic.allKeys[indexPath.section];
    NSArray *showDataArr = self.showDataDic[startDateStr];
    QWAgoraUserInfoModel *bookingModel = showDataArr[indexPath.row];
    [self jumpToMeetingBookingDetail:bookingModel];
}

#pragma mark - 加载数据
- (void)loadData{
    NSMutableArray *storeArr = [AppProfile GetBookingModelArr];
    NSMutableDictionary *mutaDic = [[NSMutableDictionary alloc]init];
    [storeArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        QWAgoraUserInfoModel *bookingModel  = (QWAgoraUserInfoModel *)obj;
        NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:bookingModel.startDate];
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        formatter.dateFormat = @"YYYY年MM月dd日";
        NSString *startDateStr = [formatter stringFromDate:startDate];
        
        NSArray *allKeys = mutaDic.allKeys;
        if([allKeys containsObject:startDateStr]){
            NSMutableArray *arr = [mutaDic objectForKey:startDateStr];
            [arr addObject:bookingModel];
        }else{
            NSMutableArray *arr = [[NSMutableArray alloc]init];
            [arr addObject:bookingModel];
            [mutaDic setObject:arr forKey:startDateStr];
        }
    }];
    
    [self.showDataDic removeAllObjects];
    self.showDataDic = mutaDic;
    
    [self.tableView reloadData];
}
- (NSMutableDictionary *)showDataDic{
    if(!_showDataDic){
        _showDataDic = [[NSMutableDictionary alloc]init];
    }
    return _showDataDic;
}


#pragma mark - 操作
- (QWMeetHomeOptionsView *)optionsView{
    if(!_optionsView){
        _optionsView = [[QWMeetHomeOptionsView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 155)];
        __weak typeof(self) weakSelf = self;
        _optionsView.homeSelectBlock = ^(NSInteger selIndex) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            
            if(![AppProfile isLoginedAlert]) return;
            
            if(selIndex == 0 ){
                [strongSelf jumpToMeetJoinInfo];
            }
            if(selIndex == 1 || selIndex == 4){
                [strongSelf jumpToMeetQuickJoinInfo];
            }
            if(selIndex == 2){
                [strongSelf jumpToMeetingBooking];
            }
            if(selIndex == 5){
                [SWHUDUtil hideHudViewWithMessageSuperView:strongSelf.view withMessage:@"开发中，敬请期待"];
            }
            if(selIndex == 3){
                [strongSelf scanToJoinMeeting];
            }
            
            
            
        };
    }
    return _optionsView;
}

#pragma mark - 空白
- (QWMeetEmptybgView *)emptybgView{
    if(!_emptybgView){
        _emptybgView = [[QWMeetEmptybgView alloc]init];
    }
    return _emptybgView;
}

#pragma mark - 加入会议
- (void)jumpToMeetJoinInfo{
    QWMeetJoinInfoController *joinInfoVC = [[QWMeetJoinInfoController alloc]init];
    [self.navigationController pushViewController:joinInfoVC animated:YES];
}

#pragma mark - 快速会议
- (void)jumpToMeetQuickJoinInfo{
    QWMeetQuickJoinController *quickJoinInfoVC = [[QWMeetQuickJoinController alloc]init];
    [self.navigationController pushViewController:quickJoinInfoVC animated:YES];
}
#pragma mark - 预定会议
- (void)jumpToMeetingBooking{
    QWMeetingBookingController *meetingBookingVC = [[QWMeetingBookingController alloc]init];
    [self.navigationController pushViewController:meetingBookingVC animated:YES];
}
#pragma mark - 会议详情
- (void)jumpToMeetingBookingDetail:(QWAgoraUserInfoModel *)bookingModel{
    QWBookingDetailController *detailVC = [[QWBookingDetailController alloc]init];
    detailVC.bookingModel = bookingModel;
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark - 扫一扫
-(void)scanToJoinMeeting
{
    XWQRCodeReader *readerVC = [[XWQRCodeReader alloc] init];
    readerVC.qrcodeValueBlock = ^(NSString * _Nonnull codeString) {
        if([codeString containsString:QWM_DOMAIN]){
            
            NSDictionary *dic = [QWCommonMethod parameterWithURL:codeString];
            NSString *type = dic[@"type"];
            if([type isEqualToString:@"meetingBooking"]){
                NSDictionary * bookingInfo = [dic[@"bookingInfo"] mj_keyValues];
                QWAgoraUserInfoModel *model = [QWAgoraUserInfoModel mj_objectWithKeyValues:bookingInfo];
                [self jumpToMeetingBookingDetail:model];
            }
        }
    };
    
    UINavigationController* navVc = [[UINavigationController alloc] initWithRootViewController:readerVC];
    navVc.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController: navVc animated:YES completion:nil];
}
@end
