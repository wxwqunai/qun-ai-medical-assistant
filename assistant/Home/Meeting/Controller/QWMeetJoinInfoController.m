//
//  QWMeetJoinInfoController.m
//  assistant
//
//  Created by kevin on 2023/12/12.
//

#import "QWMeetJoinInfoController.h"
#import "QWJoinInputCell.h"
#import "QWMeetingModel.h"
#import "QWComButtonView.h"
#import "QWMeetingVideoController.h"

@interface QWMeetJoinInfoController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) QWComButtonView *joinButtonView;

@property (nonatomic, strong) NSMutableArray *showDataArr;
@property (nonatomic, strong) QWAgoraUserInfoModel *infoModel;
@end

@implementation QWMeetJoinInfoController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"加入会议";
    self.view.backgroundColor = Color_TableView_Gray;
    
    [self joinButtonView];
    
    [self tableView];
    [self loadData];
    [self checkOutInfo:NO];
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        [_tableView registerClass:[QWJoinInputCell class] forCellReuseIdentifier:@"QWJoinInputCell"];
        
        _tableView.backgroundColor=Color_TableView_Gray;
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        //        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        //        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        //        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        //        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view);
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.joinButtonView.mas_top);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 15;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.showDataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *arr = self.showDataArr[section];
    return arr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *arr = self.showDataArr[indexPath.section];
    NSString *nameStr = arr[indexPath.row];
    QWJoinInputCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWJoinInputCell"];
    cell.nameLabel.text = nameStr;
    
    cell.textField.placeholder = [NSString stringWithFormat:@"请输入%@",nameStr];
    cell.textField.delegate = self;
    cell.textField.keyboardType = indexPath.row == 0?UIKeyboardTypeNumberPad:UIKeyboardTypeDefault;
    cell.textField.tag = 1000+10*indexPath.section+indexPath.row;
    
    cell.customSwitch.tag = 2000+10*indexPath.section+indexPath.row;
    [cell.customSwitch addTarget:self action:@selector(switchChange:) forControlEvents:UIControlEventValueChanged];
    cell.isHiddenRight = indexPath.section == 0;
    cell.isHiddenCenter = indexPath.section != 0;
    
    
    if(indexPath.section == 0){
        NSString * text = @"";
        if(indexPath.row == 0) text = [self.infoModel channelNameBlankSpace];
        if(indexPath.row == 1) text = self.infoModel.meet_name;
        cell.textField.text = text;
    }
    
    if(indexPath.section == 1){
        BOOL isOn = NO;
        if(indexPath.row == 0) isOn = self.infoModel.isOpenMicrophone;
        if(indexPath.row == 1) isOn = self.infoModel.isOpenLoudspeaker;
        if(indexPath.row == 2) isOn = self.infoModel.isOpenVideo;
        cell.customSwitch.on = isOn;
    }
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if(textField.tag == 1000){
        self.infoModel.channelName = [textField.text stringByReplacingOccurrencesOfString:@" " withString:@""];;
    }
    if(textField.tag == 1001){
        self.infoModel.meet_name = textField.text;
    }
    
    [self checkOutInfo:NO];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *text = [textField text];
    NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789\b"];
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    if ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound) {
        return NO;
    }
    
    text = [text stringByReplacingCharactersInRange:range withString:string];
    text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString *newString = @"";
    while (text.length > 0) {
        NSString *subString = [text substringToIndex:MIN(text.length, 3)];
        newString = [newString stringByAppendingString:subString];
        if (subString.length == 3) {
            newString = [newString stringByAppendingString:@" "];
        }
        text = [text substringFromIndex:MIN(text.length, 3)];
    }
    
    newString = [newString stringByTrimmingCharactersInSet:[characterSet invertedSet]];
    
    if (newString.length >= 12) {
        return NO;
    }
    
    [textField setText:newString];
    return NO;

}


- (void) switchChange:(UISwitch*)sw {
    if(sw.tag == 2010){
        self.infoModel.isOpenMicrophone = sw.on;
    }
    if(sw.tag == 2011){
        self.infoModel.isOpenLoudspeaker = sw.on;
    }
    if(sw.tag == 2012){
        self.infoModel.isOpenVideo = sw.on;

    }
}

#pragma mark - 加入会议
- (QWComButtonView *)joinButtonView{
    if(!_joinButtonView){
        _joinButtonView = [[QWComButtonView alloc]init];
        _joinButtonView.isEnable = NO;
        _joinButtonView.btnName = @"加入会议";
        [self.view addSubview:_joinButtonView];
        
        __weak typeof(self) weakSelf = self;
        _joinButtonView.makeActionClickBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;

            if([strongSelf checkOutInfo:YES]){
                [strongSelf jumpToMeetingVideo];
            }

        };
        
        [_joinButtonView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.view).offset(-[UIDevice safeDistanceBottom]-20);
            make.height.mas_equalTo(40);
        }];
    }
    return _joinButtonView;
}
- (void)jumpToMeetingVideo{
    QWMeetingVideoController *meetingVideoVC = [[QWMeetingVideoController alloc]init];
    NSDictionary *dic = [self.infoModel mj_keyValues];
    meetingVideoVC.localUserInfoModel = [QWAgoraUserInfoModel mj_objectWithKeyValues:dic];
    [self.navigationController pushViewController:meetingVideoVC animated:YES];
}


- (BOOL)checkOutInfo:(BOOL)isShowTip{
    if(IsStringEmpty(self.infoModel.channelName)){
        _joinButtonView.isEnable = NO;
        if(isShowTip) [SWHUDUtil hideHudViewWithMessageSuperView:self.view withMessage:@"请输入会议号"];
        return NO;
    }
    if(IsStringEmpty(self.infoModel.meet_name)){
        _joinButtonView.isEnable = NO;
        if(isShowTip) [SWHUDUtil hideHudViewWithMessageSuperView:self.view withMessage:@"请输入您的姓名"];
        return NO;
    }
    _joinButtonView.isEnable = YES;
    return YES;
}

#pragma mark - 加载数据
- (void)loadData{
    self.showDataArr = [[NSMutableArray alloc]initWithArray:@[@[@"会议号",@"您的姓名"],@[@"开启麦克风",@"开启扬声器",@"开启视频"]]];
    [self.tableView reloadData];
}

- (QWAgoraUserInfoModel *)infoModel{
    if(!_infoModel){
        _infoModel = [[QWAgoraUserInfoModel alloc]init];
        _infoModel.isLocal = YES;
        _infoModel.channelName = self.channelName;
    }
    return _infoModel;
}


@end
