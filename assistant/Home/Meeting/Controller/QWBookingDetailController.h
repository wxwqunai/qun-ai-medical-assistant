//
//  QWBookingDetailController.h
//  assistant
//
//  Created by qunai on 2024/1/16.
//

#import "QWBaseController.h"
#import "QWMeetingModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWBookingDetailController : QWBaseController
@property (nonatomic, strong) QWAgoraUserInfoModel *bookingModel;
@end

NS_ASSUME_NONNULL_END
