//
//  QWMeetingBookingController.m
//  assistant
//
//  Created by qunai on 2024/1/11.
//

#import "QWMeetingBookingController.h"
#import "QWMeetingInputCell.h"
#import "QWTipsAlertController.h"
#import "QWMeetingDefaultCell.h"
#import "QWMeetingSwitchCell.h"
#import "QWBookingDetailController.h"

@interface QWMeetingBookingController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showDataArr;
@property (nonatomic, strong) QWAgoraUserInfoModel *bookingModel;
@end

@implementation QWMeetingBookingController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"预定会议";
    [self.rightBarButtonItem setTitle:@"完成"];
    
    [self tableView];
    [self loadData];
}
#pragma mark-导航条右边按钮
-(void)navRightBaseButtonClick
{
    [SWHUDUtil showHudViewInSuperView:self.view withMessage:@"请等待"];
    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:self.bookingModel.startDate];
    NSDate *endDate = [startDate dateByAddingTimeInterval:self.bookingModel.duration*60];  //结束时间
    self.bookingModel.endDate = [endDate timeIntervalSince1970];
    if(self.bookingModel_old){
        [AppProfile chageBookingModel:self.bookingModel];
    }else{
        [AppProfile saveVideoMeeting:self.bookingModel];
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [SWHUDUtil hideHudView];
        QWBookingDetailController *detailVC = [[QWBookingDetailController alloc]init];
        detailVC.bookingModel = self.bookingModel;
        [self.navigationController pushViewController:detailVC animated:YES];
    });
}
#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        //        [_tableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
        //        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
        [_tableView registerClass:[QWMeetingInputCell class] forCellReuseIdentifier:@"QWMeetingInputCell"];
        [_tableView registerClass:[QWMeetingDefaultCell class] forCellReuseIdentifier:@"QWMeetingDefaultCell"];
        [_tableView registerClass:[QWMeetingSwitchCell class] forCellReuseIdentifier:@"QWMeetingSwitchCell"];

        _tableView.backgroundColor = Color_TableView_Gray;
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        //        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        //        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        //        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        //        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.0;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.showDataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *arr = self.showDataArr[section];
    return arr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *showCell;
    if(indexPath.section == 0) showCell = [self tableView:tableView cellForSectionOne:indexPath];
    if(indexPath.section == 1)  showCell = [self tableView:tableView cellForSectionTwo:indexPath];
    if(indexPath.section == 2)  showCell = [self tableView:tableView cellForSectionThree:indexPath];
    if(indexPath.section == 3)  showCell = [self tableView:tableView cellForSectionFour:indexPath];
    if(indexPath.section == 4)  showCell = [self tableView:tableView cellForSectionFive:indexPath];
    if(indexPath.section == 5)  showCell = [self tableView:tableView cellForSectionSix:indexPath];
    if(indexPath.section == 6)  showCell = [self tableView:tableView cellForSectionSeven:indexPath];
    
    if(showCell) return showCell;
    
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"cell"];
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.section == 1){
        if(indexPath.row == 0){
            [self jumpToDateChoose:YES];
        }
        if(indexPath.row == 1){
            [self jumpToDateChoose:NO];
        }
    }else{
        [SWHUDUtil hideHudViewWithMessageSuperView:self.view withMessage:@"开发中，敬请期待"];
    }
}

#pragma mark - cell for section one
- (UITableViewCell *)tableView:(UITableView *)tableView cellForSectionOne:(NSIndexPath *)indexPath{
    QWMeetingInputCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWMeetingInputCell"];
    cell.placeholder = @"请输入会议主题";
    cell.textField.text = self.bookingModel.title;
    cell.textFieldBlock = ^(NSString * _Nonnull text) {
        self.bookingModel.title = text;
    };
    return cell;
}

#pragma mark - cell for section two
- (UITableViewCell *)tableView:(UITableView *)tableView cellForSectionTwo:(NSIndexPath *)indexPath{
    NSArray *arr = self.showDataArr[indexPath.section];
    NSString *name = arr[indexPath.row];
    QWMeetingDefaultCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWMeetingDefaultCell"];
    cell.nameLabel.text = name;
    cell.descLabel.text = @"";
    if(indexPath.row == 0) cell.descLabel.text = self.bookingModel.startDateStr;
    if(indexPath.row == 1) cell.descLabel.text =  self.bookingModel.durationStr;
    return cell;
}

#pragma mark - cell for section three
- (UITableViewCell *)tableView:(UITableView *)tableView cellForSectionThree:(NSIndexPath *)indexPath{
    NSArray *arr = self.showDataArr[indexPath.section];
    NSString *name = arr[indexPath.row];
    QWMeetingSwitchCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWMeetingSwitchCell"];
    cell.nameLabel.text = name;
    return cell;
}
#pragma mark - cell for section four
- (UITableViewCell *)tableView:(UITableView *)tableView cellForSectionFour:(NSIndexPath *)indexPath{
    NSArray *arr = self.showDataArr[indexPath.section];
    NSString *name = arr[indexPath.row];
    if(indexPath.row ==0 || indexPath.row == 1 || indexPath.row ==2 || indexPath.row == 3 || indexPath.row == 6){
        QWMeetingSwitchCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWMeetingSwitchCell"];
        cell.nameLabel.text = name;
        return cell;
    }
    if(indexPath.row == 4 || indexPath.row == 5){
        QWMeetingDefaultCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWMeetingDefaultCell"];
        cell.nameLabel.text = name;
        return cell;
    }
    return nil;
}

#pragma mark - cell for section five
- (UITableViewCell *)tableView:(UITableView *)tableView cellForSectionFive:(NSIndexPath *)indexPath{
    NSArray *arr = self.showDataArr[indexPath.section];
    NSString *name = arr[indexPath.row];
    if(indexPath.row == 2){
        QWMeetingSwitchCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWMeetingSwitchCell"];
        cell.nameLabel.text = name;
        return cell;
    }
    if(indexPath.row == 0 || indexPath.row == 1|| indexPath.row == 3){
        QWMeetingDefaultCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWMeetingDefaultCell"];
        cell.nameLabel.text = name;
        return cell;
    }
    return nil;
}

#pragma mark - cell for section six
- (UITableViewCell *)tableView:(UITableView *)tableView cellForSectionSix:(NSIndexPath *)indexPath{
    NSArray *arr = self.showDataArr[indexPath.section];
    NSString *name = arr[indexPath.row];
    QWMeetingDefaultCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWMeetingDefaultCell"];
    cell.nameLabel.text = name;
    return cell;
}

#pragma mark - cell for section seven
- (UITableViewCell *)tableView:(UITableView *)tableView cellForSectionSeven:(NSIndexPath *)indexPath{
    NSArray *arr = self.showDataArr[indexPath.section];
    NSString *name = arr[indexPath.row];
    QWMeetingDefaultCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWMeetingDefaultCell"];
    cell.nameLabel.text = name;
    return cell;
}

#pragma mark - 开始时间\会议时长
- (void)jumpToDateChoose:(BOOL)isStartDate{
    QWTipsAlertController *activityVC =[[QWTipsAlertController alloc]init];
    activityVC.tipType = isStartDate?TipsAlertType_datePicker_date:TipsAlertType_datePicker_countDownTimer;
    activityVC.modalPresentationStyle=UIModalPresentationOverFullScreen;
    [self presentViewController:activityVC animated:NO completion:nil];
    
    __weak __typeof(self) weakSelf = self;
    activityVC.datePickerView.sureBlock = ^(NSDate * _Nonnull date) {
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        if(isStartDate){
            strongSelf.bookingModel.startDate = [date timeIntervalSince1970];
        }else{
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            formatter.dateFormat = @"HH";
            NSInteger hours = [[formatter stringFromDate:date] integerValue];
            
            formatter.dateFormat = @"mm";
            NSInteger minutes = [[formatter stringFromDate:date] integerValue];
            strongSelf.bookingModel.duration = hours*60 +minutes;
        }
        [strongSelf.tableView reloadData];
    };
}

#pragma mark - 加载数据
- (void)loadData{
    if(self.bookingModel_old){
        NSDictionary *dic = [self.bookingModel_old mj_keyValues];
        self.bookingModel = [QWAgoraUserInfoModel mj_objectWithKeyValues:dic];
    }else{
        [self bookingModel];
    }
   
    NSArray *arr = @[
    @[@""],
    @[@"开始时间",@"会议时长",@"时区",@"重复频率"],
    @[@"日历"],
    @[@"开启等候室",@"入会密码",@"开启会议报名",@"允许成员在主持人前入会",@"成员入会时静音",@"会议水印",@"允许成员多端入会"],
    @[@"自动云录制",@"文档",@"允许成员上传文档",@"投票"],
    @[@"应用"],
    @[@"统一虚拟背景"]];
    self.showDataArr = [[NSMutableArray alloc]initWithArray:arr];
    [self.tableView reloadData];
}

- (QWAgoraUserInfoModel *)bookingModel{
    if(!_bookingModel){
        _bookingModel = [[QWAgoraUserInfoModel alloc]init];
        _bookingModel.isLocal = YES;
        _bookingModel.isBooking = YES;
        _bookingModel.title = [NSString stringWithFormat:@"%@预定的会议",AppProfile.qaUserInfo.name];
    }
    return _bookingModel;
}


@end
