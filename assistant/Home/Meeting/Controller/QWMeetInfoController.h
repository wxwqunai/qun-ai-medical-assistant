//
//  QWMeetInfoController.h
//  assistant
//
//  Created by qunai on 2023/12/29.
//

#import "QWBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWMeetInfoController : QWBaseController
@property (nonatomic, strong) QWAgoraUserInfoModel * localUserInfoModel;
@end

NS_ASSUME_NONNULL_END
