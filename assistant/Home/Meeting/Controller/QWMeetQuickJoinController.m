//
//  QWMeetQuickJoinController.m
//  assistant
//
//  Created by qunai on 2023/12/29.
//

#import "QWMeetQuickJoinController.h"
#import "QWQuickJoinSwitchCell.h"
#import "QWComButtonView.h"
#import "QWMeetingModel.h"
#import "QWQuickJoinCell.h"
#import "QWMeetingVideoController.h"

@interface QWMeetQuickJoinController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) QWComButtonView *joinButtonView;
@property (nonatomic, strong) NSMutableArray *showDataArr;

@property (nonatomic, strong) QWAgoraUserInfoModel *infoModel;
@end

@implementation QWMeetQuickJoinController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"快速会议";
    self.view.backgroundColor = Color_TableView_Gray;

    [self joinButtonView];
    
    [self tableView];
    [self loadData];
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        //        [_tableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
        //        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
        [_tableView registerClass:[QWQuickJoinSwitchCell class] forCellReuseIdentifier:@"QWQuickJoinSwitchCell"];
        [_tableView registerClass:[QWQuickJoinCell class] forCellReuseIdentifier:@"QWQuickJoinCell"];
        
        _tableView.backgroundColor=Color_TableView_Gray;
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        //        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        //        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        //        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        //        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view);
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.joinButtonView.mas_top);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.0;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.showDataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *arr = self.showDataArr[section];
    return arr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *arr = self.showDataArr[indexPath.section];
    NSString *nameStr = arr[indexPath.row];
    if(indexPath.row == 0){
        QWQuickJoinSwitchCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWQuickJoinSwitchCell"];
        cell.nameLabel.text = nameStr;
        
        cell.customSwitch.tag = 2000+10*indexPath.section+indexPath.row;
        [cell.customSwitch addTarget:self action:@selector(switchChange:) forControlEvents:UIControlEventValueChanged];
        
        if(indexPath.section == 0)cell.customSwitch.on = self.infoModel.isOpenVideo;
        if(indexPath.section == 1)cell.customSwitch.on = self.infoModel.isUsePersonalChannel;

        return cell;
    }
   
    if(indexPath.section == 1){
        if(indexPath.row == 1){
            QWQuickJoinCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWQuickJoinCell"];
            cell.nameLabel.text = nameStr;
            cell.numLabel.text = [self.infoModel channelNameBlankSpace];
            return cell;
        }
    }
    
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell"];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void) switchChange:(UISwitch*)sw {
    if(sw.tag == 2000){
        self.infoModel.isOpenVideo = sw.on;
    }
    if(sw.tag == 2010){
        self.infoModel.isUsePersonalChannel = sw.on;
        [self.showDataArr removeAllObjects];
        [self.showDataArr addObjectsFromArray:[self changeShowArr:sw.on]];
        [self.tableView reloadData];
    }
}

#pragma mark - 加入会议
- (QWComButtonView *)joinButtonView{
    if(!_joinButtonView){
        _joinButtonView = [[QWComButtonView alloc]init];
        _joinButtonView.btnName = @"进入会议";
        [self.view addSubview:_joinButtonView];
        
        [_joinButtonView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.view).offset(-[UIDevice safeDistanceBottom]-20);
            make.height.mas_equalTo(40);
        }];
        
        __weak typeof(self) weakSelf = self;
        _joinButtonView.makeActionClickBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf jumpToMeetingVideo];
        };
    }
    return _joinButtonView;
}
- (void)jumpToMeetingVideo{
    [self doctorpatientSave];
    QWMeetingVideoController *meetingVideoVC = [[QWMeetingVideoController alloc]init];
    NSDictionary *dic = [self.infoModel mj_keyValues];
    meetingVideoVC.localUserInfoModel = [QWAgoraUserInfoModel mj_objectWithKeyValues:dic];
    [self.navigationController pushViewController:meetingVideoVC animated:YES];
}


#pragma mark - 加载数据
- (void)loadData{
    [self.showDataArr addObjectsFromArray:[self changeShowArr:NO]];
    [self.tableView reloadData];
}

- (NSArray *)changeShowArr:(BOOL)isOpenPerson{
    NSArray * arr;
    if(isOpenPerson){
        arr = @[@[@"开启视频"],@[@"使用个人会议号",@"个人会议号"]];
    }else{
        arr = @[@[@"开启视频"],@[@"使用个人会议号"]];
    }
    return arr;
}
- (NSMutableArray *)showDataArr{
    if(!_showDataArr){
        _showDataArr = [[NSMutableArray alloc]init];
    }
    return _showDataArr;
}

- (QWAgoraUserInfoModel *)infoModel{
    if(!_infoModel){
        _infoModel = [[QWAgoraUserInfoModel alloc]init];
        _infoModel.isLocal = YES;
        _infoModel.isUsePersonalChannel = NO;
    }
    return _infoModel;
}

#pragma mark - 入会记录
- (void)doctorpatientSave{
    if(IsStringEmpty(self.toUserId) || IsStringEmpty(AppProfile.qaUserInfo.id)) return;
    NSDictionary *dic = @{
        @"userId":AppProfile.qaUserInfo.id,
        @"touserId":self.toUserId,
        @"serviceType":@"2"
    };

    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_Doctorpatient_Save argument:dic];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success1) {
        NSLog(@"");
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        //错误
        NSLog(@"%@",errorInfo);
    }];
}

@end
