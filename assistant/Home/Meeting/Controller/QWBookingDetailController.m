//
//  QWBookingDetailController.m
//  assistant
//
//  Created by qunai on 2024/1/16.
//

#import "QWBookingDetailController.h"
#import "QWComButtonView.h"
#import "QWbkDetailInfoCell.h"
#import "QWbkDetailCell.h"
#import "QWMeetingVideoController.h"
#import "QWMeetingDefaultCell.h"
#import "YCXMenu.h"
#import "QWMeetingBookingController.h"
#import "QWShareHomeController.h"
#import "QWMeetingHomeController.h"

@interface QWBookingDetailController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) QWComButtonView *joinButtonView;
@property (nonatomic, strong) NSMutableArray *showDataArr;

@end

@implementation QWBookingDetailController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"会议详情";
    [self.rightBarButtonItem setImage:[UIImage imageNamed:@"ic_web_more.png"]];
    
    [self joinButtonView];
    
    [self tableView];
    [self loadData];
}
#pragma mark-导航条左边按钮
-(void)navLeftBaseButtonClick
{
    [self popToVC:[QWMeetingHomeController class]];
}
#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        [_tableView registerClass:[QWbkDetailInfoCell class] forCellReuseIdentifier:@"QWbkDetailInfoCell"];
        [_tableView registerClass:[QWbkDetailCell class] forCellReuseIdentifier:@"QWbkDetailCell"];
        [_tableView registerClass:[QWMeetingDefaultCell class] forCellReuseIdentifier:@"QWMeetingDefaultCell"];
        
        _tableView.backgroundColor= Color_TableView_Gray;
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        //        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        //        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        //        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        //        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view);
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.joinButtonView.mas_top);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10.0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0000001;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return section == 1?2:1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        QWbkDetailInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWbkDetailInfoCell"];
        cell.bookingModel = self.bookingModel;
        return cell;
    }
    if(indexPath.section == 1){
        QWbkDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWbkDetailCell"];
        cell.nameLabel.text = indexPath.row == 0?@"会议号":@"发起人";
        cell.descLabel.text = indexPath.row == 0?[self.bookingModel channelNameBlankSpace]:self.bookingModel.meet_name;
        cell.numCopyButton.hidden = indexPath.row == 0?NO:YES;
        return cell;
    }
    if(indexPath.section == 2){
        QWMeetingDefaultCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWMeetingDefaultCell"];
        cell.nameLabel.text = @"文档";
        cell.descLabel.text = @"添加";
        return cell;
    }
    
    
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"cell"];
    }
    cell.textLabel.text =[NSString stringWithFormat:@"cell：%ld",indexPath.row];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.section == 2){
        [SWHUDUtil hideHudViewWithMessageSuperView:self.view withMessage:@"开发中，敬请期待"];
    }
}


#pragma mark - 加入会议
- (QWComButtonView *)joinButtonView{
    if(!_joinButtonView){
        _joinButtonView = [[QWComButtonView alloc]init];
        _joinButtonView.btnName = @"进入会议";
        [self.view addSubview:_joinButtonView];
        
        [_joinButtonView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.view).offset(-[UIDevice safeDistanceBottom]-20);
            make.height.mas_equalTo(40);
        }];
        
        __weak typeof(self) weakSelf = self;
        _joinButtonView.makeActionClickBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf jumpToMeetingVideo];
        };
    }
    return _joinButtonView;
}
- (void)jumpToMeetingVideo{
    QWMeetingVideoController *meetingVideoVC = [[QWMeetingVideoController alloc]init];
    NSDictionary *dic = [self.bookingModel mj_keyValues];
    meetingVideoVC.localUserInfoModel = [QWAgoraUserInfoModel mj_objectWithKeyValues:dic];
    [self.navigationController pushViewController:meetingVideoVC animated:YES];
}
#pragma mark - 加载数据
- (void)loadData{
}

#pragma mark - 修改会议信息、分享、取消会议
-(void)navRightBaseButtonClick
{
    if([YCXMenu isShow]){
        [YCXMenu dismissMenu];
        return;
    }
    
    NSArray * menuArray= @[[YCXMenuItem menuItem:@"修改会议信息"
                                                 image:nil
                                                   tag:100
                                              userInfo:nil],
                                 [YCXMenuItem menuItem:@"分享"
                                                 image:nil
                                                   tag:101
                                              userInfo:nil],
                        [YCXMenuItem menuItem:@"取消会议"
                                           image:nil
                                             tag:102
                                        userInfo:nil],
                                ];
    CGRect rect=CGRectMake(kScreenWidth-25, [UIDevice navigationFullHeight], 0, 0);
    [YCXMenu setTintColor:Color_Main_Green];
    [YCXMenu setSelectedColor:Color_Main_Green];
    [YCXMenu setSeparatorColor:[UIColor whiteColor]];
    [YCXMenu showMenuInView:self.view fromRect:CGRectMake(rect.origin.x, rect.origin.y+4, rect.size.width, rect.size.height) menuItems:menuArray selected:^(NSInteger index, YCXMenuItem *item) {
        if(index == 0){
            [self jumpToMeetingBooking];
        }
        if(index ==1){
            [self jumpToShare];
        }
        if(index ==2){
            [self showAlert:@"取消会议" message:@"您是会议的创建者，取消后其他成员将无法入会。" success:^(UIAlertAction * _Nonnull action) {
                [AppProfile deleteBookingModel:self.bookingModel];
                [self navLeftBaseButtonClick];
            } failure:nil];
            
        }
    }];
}

#pragma mark - 预定会议
- (void)jumpToMeetingBooking{
    QWMeetingBookingController *meetingBookingVC = [[QWMeetingBookingController alloc]init];
    meetingBookingVC.bookingModel_old = self.bookingModel;
    [self.navigationController pushViewController:meetingBookingVC animated:YES];
}

#pragma mark - 分享选择
- (void)jumpToShare{
    QWShareHomeController *shareVC = [[QWShareHomeController alloc]init];
    NSString *modelStr  = self.bookingModel.mj_JSONString;
    shareVC.shareUrl = [NSString stringWithFormat:@"%@?type=meetingBooking&bookingInfo=%@",QWM_BASE_API_URL,modelStr];
    shareVC.shareTitle = self.bookingModel.title;
    shareVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:shareVC animated:NO completion:nil];
}

#pragma mark - 返回到指定页面
- (void)popToVC:(id )popVC{
    NSArray * ctrlArray = self.navigationController.viewControllers;
    __block BOOL  isPopSuccess;
    [ctrlArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIViewController * ctrl = (UIViewController *)obj;
        if([ctrl isKindOfClass:[popVC class]]){
            [self.navigationController popToViewController:ctrl animated:YES];
            isPopSuccess = YES;
            *stop = YES;
        }
    }];
    if(!isPopSuccess){
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

@end
