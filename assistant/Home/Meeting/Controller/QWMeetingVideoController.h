//
//  QWMeetingVideoController.h
//  assistant
//
//  Created by qunai on 2023/12/12.
//

#import "QWBaseController.h"
#import "QWMeetingModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWMeetingVideoController : QWBaseController
@property (nonatomic, strong) QWAgoraUserInfoModel * localUserInfoModel;
@end


NS_ASSUME_NONNULL_END
