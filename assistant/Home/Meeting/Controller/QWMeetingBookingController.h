//
//  QWMeetingBookingController.h
//  assistant
//
//  Created by qunai on 2024/1/11.
//

#import "QWBaseController.h"
#import "QWMeetingModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWMeetingBookingController : QWBaseController
@property (nonatomic, strong) QWAgoraUserInfoModel *bookingModel_old;

@end

NS_ASSUME_NONNULL_END
