//
//  QWMeetJoinInfoController.h
//  assistant
//
//  Created by kevin on 2023/12/12.
//

#import "QWBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWMeetJoinInfoController : QWBaseController
@property (nonatomic,copy) NSString * channelName;//会议号
@end

NS_ASSUME_NONNULL_END
