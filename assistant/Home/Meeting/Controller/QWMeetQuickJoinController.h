//
//  QWMeetQuickJoinController.h
//  assistant
//
//  Created by qunai on 2023/12/29.
//

#import "QWBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWMeetQuickJoinController : QWBaseController
@property (nonatomic, copy) NSString *toUserId; //与xx开启会议
@end

NS_ASSUME_NONNULL_END
