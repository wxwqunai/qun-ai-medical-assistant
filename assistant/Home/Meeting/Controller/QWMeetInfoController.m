//
//  QWMeetInfoController.m
//  assistant
//
//  Created by qunai on 2023/12/29.
//

#import "QWMeetInfoController.h"

@interface QWMeetInfoController ()<UIGestureRecognizerDelegate>
@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *qrCodeBtn;
@property (nonatomic, strong) UILabel *encryptionLabel;
@property (nonatomic, strong) UILabel *networkLabel;

@property (nonatomic, strong) UIView *infoView;
@property (nonatomic, strong) UIView *meetNumView;
@property (nonatomic, strong) UILabel *descLabel;
@property (nonatomic, strong) UIView *initiatorItemView;
@property (nonatomic, strong) UIView *myNameItemView;
@property (nonatomic, strong) UIView *timeItemView;

@end

@implementation QWMeetInfoController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openMeetOptionsTap:)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
    
    [self bgView];
    
    [self headerView];
    [self qrCodeBtn];
    [self titleLabel];
    [self encryptionLabel];
    [self networkLabel];
    
    [self infoView];
    [self meetNumView];
    [self initiatorItemView];
    [self myNameItemView];
    [self timeItemView];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self show];
    });
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = Color_TableView_Gray;
        [self.view addSubview:_bgView];
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.view);
            make.height.mas_equalTo(200);
        }];
    }
    return _bgView;
}

#pragma mark - 标题信息
- (UIView *)headerView{
    if(!_headerView){
        _headerView = [[UIView alloc]init];
        _headerView.backgroundColor = [UIColor whiteColor];
        [self.bgView addSubview:_headerView];
        [_headerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView);
            make.right.equalTo(self.bgView);
            make.top.equalTo(self.bgView);
            make.height.mas_greaterThanOrEqualTo(50);
        }];
    }
    return _headerView;
}
- (UILabel *)titleLabel{
    if(!_titleLabel){
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"群爱会议";
        _titleLabel.numberOfLines = 0;
        _titleLabel.font = [UIFont systemFontOfSize:19 weight:UIFontWeightBold];
        [self.headerView addSubview:_titleLabel];
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.headerView).offset(14.0);
            make.right.equalTo(self.qrCodeBtn.mas_left).offset(-10);
            make.top.equalTo(self.headerView).offset(10);
            make.height.mas_greaterThanOrEqualTo(30);
        }];
    }
    return _titleLabel;
}
//二维码
- (UIButton *)qrCodeBtn{
    if(!_qrCodeBtn){
        _qrCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_qrCodeBtn setImage:[UIImage imageNamed:@"share_qrcode.png"] forState:UIControlStateNormal];
        [self.headerView addSubview:_qrCodeBtn];
        
        [_qrCodeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.headerView).offset(-14.0);
            make.top.equalTo(self.headerView).offset(10);
            make.height.mas_equalTo(30);
            make.width.mas_equalTo(30);
        }];
    }
    return _qrCodeBtn;
}
//加密
- (UILabel *)encryptionLabel{
    if(!_encryptionLabel){
        UIImageView *imageView = [[UIImageView alloc]init];
        imageView.image = [UIImage imageNamed:@"mt_security"];
        [self.headerView addSubview:imageView];
        
        _encryptionLabel = [[UILabel alloc]init];
        _encryptionLabel.text = @"会议已加密保护";
        _encryptionLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
        [self.headerView addSubview:_encryptionLabel];
        
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.headerView).offset(14.0);
            make.centerY.equalTo(_encryptionLabel);
        }];
        
        [_encryptionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(imageView.mas_right);
            make.top.equalTo(self.titleLabel.mas_bottom).offset(8);
            make.bottom.equalTo(self.headerView).offset(-10);
        }];
    }
    return _encryptionLabel;
}
//网络
- (UILabel *)networkLabel{
    if(!_networkLabel){
        UIImageView *imageView = [[UIImageView alloc]init];
        imageView.image = [UIImage imageNamed:@"mt_network"];
        [self.headerView addSubview:imageView];
        
        _networkLabel = [[UILabel alloc]init];
        _networkLabel.text = @"网络连接正常";
        _networkLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
        [self.headerView addSubview:_networkLabel];
        
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.encryptionLabel.mas_right).offset(5.0);
            make.centerY.equalTo(_networkLabel);
        }];
        
        [_networkLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(imageView.mas_right);
            make.top.equalTo(self.encryptionLabel.mas_top);
        }];
    }
    return _networkLabel;
}

#pragma mark - 会议信息
- (UIView *)infoView{
    if(!_infoView){
        _infoView = [[UIView alloc]init];
        _infoView.backgroundColor = [UIColor whiteColor];
        [self.bgView addSubview:_infoView];
        
        [_infoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView);
            make.right.equalTo(self.bgView);
            make.top.equalTo(self.headerView.mas_bottom).offset(5.0);
            make.height.mas_greaterThanOrEqualTo(50);
        }];
    }
    return _infoView;
}
//会议号
- (UIView *)meetNumView{
    if(!_meetNumView){
        _meetNumView = [[UIView alloc]init];
        [self.infoView addSubview:_meetNumView];
        
        [_meetNumView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.infoView).offset(14.0);
            make.right.equalTo(self.infoView).offset(-14.0);
            make.top.equalTo(self.infoView);
            make.height.mas_equalTo(30);
        }];
        
        
        UILabel *nameLabel = [[UILabel alloc]init];
        nameLabel.text = @"会议号";
        nameLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightLight];
        [_meetNumView addSubview:nameLabel];
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_meetNumView);
            make.bottom.equalTo(_meetNumView);
            make.left.equalTo(_meetNumView);
            make.width.mas_equalTo(65);
        }];
        
        UILabel *descLabel = [[UILabel alloc]init];
        self.descLabel = descLabel;
        descLabel.textColor = [UIColor coolGrayColor];
        descLabel.font = [UIFont systemFontOfSize:14];
        descLabel.text = [self.localUserInfoModel channelNameBlankSpace];
        [_meetNumView addSubview:descLabel];
        [descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(nameLabel);
            make.left.equalTo(nameLabel.mas_right).offset(15.0);
        }];
        
        UIButton *numCopyButton = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *image = [UIImage imageNamed:@"m_copy"];
        [numCopyButton setImage:image forState:UIControlStateNormal];
        [numCopyButton addTarget:self action:@selector(numCopyButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
        [_meetNumView addSubview:numCopyButton];
        
        [numCopyButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(descLabel.mas_right).offset(8.0);
            make.centerY.equalTo(nameLabel);
            make.width.mas_equalTo(image.size.width);
            make.height.mas_equalTo(image.size.height);
        }];
        
    }
    return _meetNumView;
}
- (void)numCopyButtonOnClick{
    if(self.descLabel.text){
        [SWHUDUtil hideHudViewWithMessageSuperView:self.view withMessage:@"会议号已复制到剪贴板"];
        UIPasteboard *pboard = [UIPasteboard generalPasteboard];
        pboard.string = self.descLabel.text;
    }
}

//发起人
- (UIView *)initiatorItemView{
    if(!_initiatorItemView){
        _initiatorItemView = [[UIView alloc]init];
        [self.infoView addSubview:_initiatorItemView];
        
        [_initiatorItemView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.infoView).offset(14.0);
            make.right.equalTo(self.infoView).offset(-14.0);
            make.top.equalTo(self.meetNumView.mas_bottom);
            make.height.mas_equalTo(30);
        }];
        
        
        UILabel *nameLabel = [[UILabel alloc]init];
        nameLabel.text = @"发起人";
        nameLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightLight];
        [_initiatorItemView addSubview:nameLabel];
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_initiatorItemView);
            make.bottom.equalTo(_initiatorItemView);
            make.left.equalTo(_initiatorItemView);
            make.width.mas_equalTo(65);
        }];
        
        UILabel *descLabel = [[UILabel alloc]init];
        descLabel.textColor = [UIColor coolGrayColor];
        descLabel.font = [UIFont systemFontOfSize:14];
        descLabel.text = self.localUserInfoModel.meet_name;
        [_initiatorItemView addSubview:descLabel];
        [descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(nameLabel);
            make.left.equalTo(nameLabel.mas_right).offset(15.0);
        }];
    }
    return _initiatorItemView;
}
//我的名称
- (UIView *)myNameItemView{
    if(!_myNameItemView){
        _myNameItemView = [[UIView alloc]init];
        [self.infoView addSubview:_myNameItemView];
        
        [_myNameItemView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.infoView).offset(14.0);
            make.right.equalTo(self.infoView).offset(-14.0);
            make.top.equalTo(self.initiatorItemView.mas_bottom);
            make.height.mas_equalTo(30);
        }];
        
        
        UILabel *nameLabel = [[UILabel alloc]init];
        nameLabel.text = @"我的名称";
        nameLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightLight];
        [_myNameItemView addSubview:nameLabel];
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_myNameItemView);
            make.bottom.equalTo(_myNameItemView);
            make.left.equalTo(_myNameItemView);
            make.width.mas_equalTo(65);
        }];
        
        UILabel *descLabel = [[UILabel alloc]init];
        descLabel.textColor = [UIColor coolGrayColor];
        descLabel.font = [UIFont systemFontOfSize:14];
        descLabel.text = self.localUserInfoModel.meet_name;
        [_myNameItemView addSubview:descLabel];
        [descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(nameLabel);
            make.left.equalTo(nameLabel.mas_right).offset(15.0);
        }];
    }
    return _myNameItemView;
}
//参会时长
- (UIView *)timeItemView{
    if(!_timeItemView){
        _timeItemView = [[UIView alloc]init];
        [self.infoView addSubview:_timeItemView];
        
        [_timeItemView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.infoView).offset(14.0);
            make.right.equalTo(self.infoView).offset(-14.0);
            make.top.equalTo(self.myNameItemView.mas_bottom);
            make.height.mas_equalTo(30);
            make.bottom.equalTo(self.infoView).offset(-10.0);
        }];
        
        
        UILabel *nameLabel = [[UILabel alloc]init];
        nameLabel.text = @"参会时长";
        nameLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightLight];
        [_timeItemView addSubview:nameLabel];
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_timeItemView);
            make.bottom.equalTo(_timeItemView);
            make.left.equalTo(_timeItemView);
            make.width.mas_equalTo(65);
        }];
        
        UILabel *descLabel = [[UILabel alloc]init];
        descLabel.textColor = [UIColor coolGrayColor];
        descLabel.font = [UIFont systemFontOfSize:14];
        descLabel.text = @"00:00";
        [_timeItemView addSubview:descLabel];
        [descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(nameLabel);
            make.left.equalTo(nameLabel.mas_right).offset(15.0);
        }];
    }
    return _timeItemView;
}


#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isDescendantOfView:self.bgView]) {
        return NO;
    }
    return YES;
}
#pragma mark - 手势 - 打开/关闭控制页面
- (void)openMeetOptionsTap:(UIGestureRecognizer *)ges{
    if (ges.state == UIGestureRecognizerStateEnded ) {
        [self close];
    }
}
#pragma mark - 动画弹出
- (void)show{
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.bottom.equalTo(self.view.mas_bottom);
        make.height.mas_equalTo(200);
    }];
    
    // 告诉self.view约束需要更新
    [self.view setNeedsUpdateConstraints];
    // 调用此方法告诉self.view检测是否需要更新约束，若需要则更新，下面添加动画效果才起作用
    [self.view updateConstraintsIfNeeded];
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}
#pragma mark - 动画关闭
- (void)close{
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.top.equalTo(self.view.mas_bottom);
        make.height.mas_equalTo(200);
    }];
    
    // 告诉self.view约束需要更新
    [self.view setNeedsUpdateConstraints];
    // 调用此方法告诉self.view检测是否需要更新约束，若需要则更新，下面添加动画效果才起作用
    [self.view updateConstraintsIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:NO completion:nil];
    }];
}

@end
