//
//  QWMeetingModel.h
//  assistant
//
//  Created by qunai on 2023/12/12.
//

#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN

@interface QWMeetingModel : NSObject

@end

@interface QWAgoraUserInfoModel : QWMeetingModel
@property (nonatomic, assign) BOOL isLocal;
@property (nonatomic, assign) BOOL isUsePersonalChannel; //是否使用个人会议号
@property (nonatomic, copy) NSString *channelName; //会议号
@property(assign, nonatomic) NSUInteger uid;
@property (nonatomic, copy) NSString *meet_name; //参会人-名称
@property (nonatomic, assign) BOOL isOpenMicrophone; // 开启麦克风
@property (nonatomic, assign) BOOL isOpenLoudspeaker; //开启扬声器
@property (nonatomic, assign) BOOL isOpenVideo; //开启视频
@property (nonatomic, strong) AgoraUserInfo *userInfo;
@property (nonatomic, copy) NSString *user_id; //参会人-id
@property (nonatomic, copy) NSString *user_header_url; //参会人-头像

@property (nonatomic, assign) BOOL isActiveSpeaker; //当前说话


//预定会议
@property (nonatomic, assign) BOOL isBooking; //是否为预定会议
@property (nonatomic, copy) NSString *title; //会议主题
@property (nonatomic, assign) NSTimeInterval startDate; //会议开始时间
@property (nonatomic, copy, readonly) NSString *startDateStr; //会议开始时间
@property (nonatomic, assign) NSTimeInterval endDate; //会议结束时间
@property (nonatomic, assign) NSInteger duration; //会议时长
@property (nonatomic, copy) NSString *durationStr; //会议时长

- (NSString *)channelNameBlankSpace; //会议号 - 带空格

@end




NS_ASSUME_NONNULL_END
