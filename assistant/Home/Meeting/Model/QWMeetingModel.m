//
//  QWMeetingModel.m
//  assistant
//
//  Created by qunai on 2023/12/12.
//

#import "QWMeetingModel.h"

@implementation QWMeetingModel

#pragma mark - 会议号 - 随机
- (NSString *)getRandomChannelName
{
    NSArray *strArr = [[NSArray alloc]initWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",nil] ;
    NSMutableString *getStr = [[NSMutableString alloc]initWithCapacity:8];
    for(int i = 0; i < 9; i++) //得到六位随机字符,可自己设长度
    {
        int index = arc4random() % ([strArr count]);  //得到数组中随机数的下标
        [getStr appendString:[strArr objectAtIndex:index]];
    }
//    if(getStr.length == 9){
//        [getStr insertString:@" " atIndex:3];
//        [getStr insertString:@" " atIndex:7];
//    }
    return getStr;
}
@end

@implementation QWAgoraUserInfoModel
- (instancetype)init{
    self = [super init];
    if(self){
        self.isOpenMicrophone = YES;
        self.isOpenLoudspeaker = YES;
        self.isOpenVideo = NO;
    }
    return self;
}

- (void)setIsLocal:(BOOL)isLocal{
    _isLocal = isLocal;
    if(IsStringEmpty(self.meet_name)) self.meet_name = AppProfile.qaUserInfo.name;
    NSString *headStr = [QWM_BASE_API_URL stringByAppendingFormat:@"/bokeupload%@", AppProfile.qaUserInfo.photo];
    if(IsStringEmpty(self.user_header_url)) self.user_header_url = headStr;
}


- (void)setIsUsePersonalChannel:(BOOL)isUsePersonalChannel{
    _isUsePersonalChannel = isUsePersonalChannel;
    if(IsStringEmpty(self.channelName)) self.channelName = [self getRandomChannelName];
}

- (void)setUserInfo:(AgoraUserInfo *)userInfo{
    _userInfo = userInfo;
    
    if(_userInfo){
        self.uid = _userInfo.uid;
        
        //解析userAccount
        NSDictionary *userAccountDic = [_userInfo.userAccount mj_JSONObject];
        self.user_id = userAccountDic[@"id"];
        self.meet_name = userAccountDic[@"name"];
        self.user_header_url = userAccountDic[@"userHeaderUrl"];
    }
}

- (void)setIsBooking:(BOOL)isBooking{
    _isBooking = isBooking;
    if(_isBooking){
        if(IsStringEmpty(self.channelName)) self.channelName = [self getRandomChannelName];
        if(!self.duration) self.duration = 30;
        if(!self.startDate) self.startDate = [[NSDate date] timeIntervalSince1970];
    }
}

- (void)setStartDate:(NSTimeInterval)startDate{
    _startDate = startDate;
    if(_startDate){
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        formatter.dateFormat = @"YYYY-MM-dd HH:mm";
        [self setValue:[formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:_startDate]] forKey:@"startDateStr"];
    }else{
        [self setValue:@"" forKey:@"startDateStr"];
    }
}

- (void)setDuration:(NSInteger)duration{
    _duration = duration;
    if(_duration>0){
        self.durationStr = [NSString stringWithFormat:@"%@%ld分钟",_duration/60>0?[NSString stringWithFormat:@"%ld小时",_duration/60]:@"",_duration%60];
    }else{
        self.durationStr = @"";
    }
}

- (NSString *)channelNameBlankSpace{
    NSMutableString *channelName = [[NSMutableString alloc]initWithString:self.channelName?self.channelName:@""];
    if(channelName.length == 9){
        [channelName insertString:@" " atIndex:3];
        [channelName insertString:@" " atIndex:7];
    }
    return channelName;
}

@end


