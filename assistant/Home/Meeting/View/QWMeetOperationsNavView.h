//
//  QWMeetOperationsNavView.h
//  assistant
//
//  Created by qunai on 2023/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWMeetOperationsNavView : UIView
@property (nonatomic, assign) BOOL isOpenLoudspeaker;

@property (nonatomic,copy) void(^endBlock)(void);
@property (nonatomic,copy) void(^loudspeakerBlock)(BOOL isOpen);
@property (nonatomic,copy) void(^openMeetingInfoBlock)(void);
@end

NS_ASSUME_NONNULL_END
