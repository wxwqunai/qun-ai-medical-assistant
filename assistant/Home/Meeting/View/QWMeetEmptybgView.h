//
//  QWMeetEmptybgView.h
//  assistant
//
//  Created by qunai on 2023/12/11.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWMeetEmptybgView : UIView
@property (nonatomic, strong) UIImageView *emptyImageView;

@property (nonatomic, strong) UILabel *contentLabel;
@end

NS_ASSUME_NONNULL_END
