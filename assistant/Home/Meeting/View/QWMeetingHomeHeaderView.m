//
//  QWMeetingHomeHeaderView.m
//  assistant
//
//  Created by qunai on 2024/1/18.
//

#import "QWMeetingHomeHeaderView.h"
@interface QWMeetingHomeHeaderView()
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIImageView *dateIconImageView;

@end
@implementation QWMeetingHomeHeaderView

- (instancetype)initWithReuseIdentifier:(nullable NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if(self){
        self.contentView.backgroundColor = [UIColor clearColor];
        [self bgView];
        [self dateIconImageView];
        [self nameLabel];
    }
    return self;
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.top.equalTo(self.contentView).offset(10.0);
            make.bottom.equalTo(self.contentView);
            make.height.mas_equalTo(25.0);
        }];
    }
    return _bgView;
}

- (UIImageView *)dateIconImageView{
    if(!_dateIconImageView){
        _dateIconImageView = [[UIImageView alloc]init];
        UIImage *image = [UIImage imageNamed:@"m_date_home.png"];
        _dateIconImageView.contentMode = UIViewContentModeScaleAspectFit;
        _dateIconImageView.image = image;
        [self.bgView addSubview:_dateIconImageView];
        
        [_dateIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.bgView);
            make.bottom.equalTo(self.bgView);
            make.left.equalTo(self.bgView).offset(14.0);
        }];
    }
    return _dateIconImageView;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.numberOfLines = 0;
        _nameLabel.textColor = [UIColor colorFromHexString:@"#8a8a8a"];
        _nameLabel.font = [UIFont systemFontOfSize:13 weight:UIFontWeightLight];
        
        [self.bgView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.bgView);
            make.bottom.equalTo(self.bgView);
            make.left.equalTo(self.dateIconImageView.mas_right).offset(4.0);
            make.right.equalTo(self.bgView).offset(-14.0);
        }];
        
    }
    return _nameLabel;
}

@end
