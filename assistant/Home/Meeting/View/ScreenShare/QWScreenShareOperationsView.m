//
//  QWScreenShareOperationsView.m
//  assistant
//
//  Created by qunai on 2024/3/5.
//

#import "QWScreenShareOperationsView.h"

@implementation SSOptionsView
- (instancetype)init{
    self = [super init];
    if(self){
        self.isOpen = YES;
        [self button];
        [self nameLabel];
        
    }
    return self;
}
- (UIButton *)button{
    if(!_button){
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        _button.layer.masksToBounds = YES;
        _button.layer.cornerRadius = 16.0;
        [self addSubview:_button];
        [_button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.equalTo(self);
            make.width.mas_equalTo(70);
            make.height.mas_equalTo(70);
        }];
    }
    return _button;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont systemFontOfSize:16];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        _nameLabel.textColor = [UIColor whiteColor];
        [self addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.top.equalTo(self.button.mas_bottom).offset(15.0);
            make.bottom.equalTo(self);
        }];
    }
    return _nameLabel;
}

- (void)setIconName:(NSString *)iconName{
    _iconName = iconName;
    [_button setImage:[UIImage imageNamed:self.iconName] forState:UIControlStateNormal];
}

- (void)setNameStr:(NSString *)nameStr{
    _nameStr = nameStr;
    _nameLabel.text = _nameStr;
}
@end


@implementation QWScreenShareOperationsView

- (instancetype)init{
    self = [super init];
    if(self){
        self.backgroundColor = [UIColor blackColor];
        [self bgView];
        [self titleLabel];
        [self audioView];
        [self stopShareView];
    }
    return self;
}
- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        [self addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.centerY.equalTo(self);
        }];
    }
    return _bgView;
}

- (UILabel *)titleLabel{
    if(!_titleLabel){
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"您正在共享屏幕";
        _titleLabel.font = [UIFont boldSystemFontOfSize:20];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.textColor = [UIColor whiteColor];
        [self.bgView addSubview:_titleLabel];
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView);
            make.right.equalTo(self.bgView);
            make.top.equalTo(self.bgView);
        }];
    }
    return _titleLabel;
}

#pragma mark - 音频控制
- (SSOptionsView *)audioView{
    if(!_audioView){
        _audioView = [[SSOptionsView alloc]init];
        _audioView.iconName = @"m_sharedScreen_audio";
        _audioView.button.backgroundColor = Color_Main_Green;
        _audioView.nameStr = @"共享屏幕设备音频：开";
        [_audioView.button addTarget:self action:@selector(audioButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [self.bgView addSubview:_audioView];
        
        [_audioView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView);
            make.right.equalTo(self.bgView);
            make.top.equalTo(self.titleLabel.mas_bottom).offset(30.0);
        }];
    }
    return _audioView;
}
- (void)audioButtonClick{
    _audioView.isOpen = !_audioView.isOpen;
    _audioView.button.backgroundColor = _audioView.isOpen?Color_Main_Green:[UIColor grayColor];
    _audioView.nameStr = [NSString stringWithFormat:@"共享屏幕设备音频：%@",_audioView.isOpen?@"开":@"关"];
    if(self.audioOptionBlock)self.audioOptionBlock(_audioView.isOpen);
}

#pragma mark - 关闭屏幕共享
- (SSOptionsView *)stopShareView{
    if(!_stopShareView){
        _stopShareView = [[SSOptionsView alloc]init];
        _stopShareView.iconName = @"m_sharedScreen_close_white";
        _stopShareView.button.backgroundColor = [UIColor colorFromHexString:@"#DF4C4C"];
        _stopShareView.nameStr = @"停止共享";
        [_stopShareView.button addTarget:self action:@selector(stopShareButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [self.bgView addSubview:_stopShareView];
        
        [_stopShareView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView);
            make.right.equalTo(self.bgView);
            make.top.equalTo(self.audioView.mas_bottom).offset(30.0);
            make.bottom.equalTo(self.bgView);
        }];
    }
    return _stopShareView;
}
- (void)stopShareButtonClick{
    if(self.stopShareBlock)self.stopShareBlock();
}

@end


