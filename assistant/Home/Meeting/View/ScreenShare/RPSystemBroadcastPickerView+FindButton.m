//
//  RPSystemBroadcastPickerView+FindButton.m
//  assistant
//
//  Created by qunai on 2024/2/29.
//

#import "RPSystemBroadcastPickerView+FindButton.h"

@implementation RPSystemBroadcastPickerView (FindButton)
- (UIButton *)findButton {
    return [self findButton:self];
}

- (UIButton*)findButton:(UIView*)view {
    if(!view.subviews.count) {
        return nil;
    }
    
    if ([view isKindOfClass:[UIButton class]]) {
        return(UIButton*)view;
    }
    
    UIButton*btn;
    for(UIView*subView in view.subviews) {
        UIView*destinationView = [self findButton:subView];
        if(destinationView) {
            btn = (UIButton*)destinationView;
            break;
        }
    }
    return btn;
}
@end

