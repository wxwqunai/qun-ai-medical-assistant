//
//  QWScreenShareOperationsView.h
//  assistant
//
//  Created by qunai on 2024/3/5.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@interface SSOptionsView : UIView
@property (nonatomic,strong) UIButton *button;
@property (nonatomic,strong) UILabel *nameLabel;

@property (nonatomic,copy) NSString *iconName;
@property (nonatomic,copy) NSString *nameStr;

@property (nonatomic,assign) BOOL isOpen;
@end

@interface QWScreenShareOperationsView : UIView
@property (nonatomic,strong) UIView *bgView;
@property (nonatomic,strong) UILabel *titleLabel;

@property (nonatomic,strong) SSOptionsView *audioView;
@property (nonatomic,strong) SSOptionsView *stopShareView;

@property (nonatomic,copy) void(^audioOptionBlock)(BOOL isOpen);
@property (nonatomic,copy) void(^stopShareBlock)(void);
@end



NS_ASSUME_NONNULL_END
