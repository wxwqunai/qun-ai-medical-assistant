//
//  RPSystemBroadcastPickerView+FindButton.h
//  assistant
//
//  Created by qunai on 2024/2/29.
//

#import <ReplayKit/ReplayKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RPSystemBroadcastPickerView (FindButton)
- (UIButton *)findButton;
@end

NS_ASSUME_NONNULL_END
