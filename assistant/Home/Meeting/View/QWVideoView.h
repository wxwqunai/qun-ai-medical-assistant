//
//  QWVideoView.h
//  assistant
//
//  Created by kevin on 2023/12/13.
//

#import <UIKit/UIKit.h>
#import "QWMeetingModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWVideoView : UIView
@property (nonatomic, strong) QWAgoraUserInfoModel *userInfoModel;
@property (nonatomic, assign) NSUInteger uid;
@property (nonatomic, strong) AgoraRtcVideoCanvas *videoCanvas;

@property (nonatomic, assign) BOOL isOpenVideo;
@property (nonatomic, assign) BOOL isOpenAideo;
@property (nonatomic, copy) NSString *userNameStr;
@property (nonatomic, copy) NSString *userHeaderPicUrl;

@property (nonatomic, assign) BOOL isCurrentSmallScreen; //当前视频两人中小屏
@end

@interface QWVideoShowView : UIView
@property (nonatomic, strong) UIView *videoView;

@property (nonatomic, strong) UIView *baseInfoView;
@property (nonatomic, strong) UIImageView *microphoneImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@end

@interface QWAideoShowView : UIView
@property (nonatomic, strong) UIImageView *userHeaderImageView;
@property (nonatomic, strong) UIImageView *microphoneImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@end


NS_ASSUME_NONNULL_END
