//
//  QWMeetOperationsView.h
//  assistant
//
//  Created by qunai on 2023/12/29.
//

#import <UIKit/UIKit.h>
#import "QWMeetingModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, MeetOperationType) {
    MeetOperationType_default = 0,
    MeetOperationType_end, //结束会议
    MeetOperationType_microphone_open, //麦克风-开
    MeetOperationType_microphone_close, //麦克风-关
    MeetOperationType_loudspeaker_open, //扬声器-开
    MeetOperationType_loudspeaker_close, //扬声器-关
    MeetOperationType_video_open, //视频-开
    MeetOperationType_video_close, //视频-关
    MeetOperationType_openShowMeetingInfo, //打开展示会议基本信息
    MeetOperationType_shareScreen_open, //打开屏幕共享
    MeetOperationType_shareScreen_close, //打开屏幕共享
};

@interface QWMeetOperationsView : UIView
@property (nonatomic,copy) void(^meetOperationsBlock)(MeetOperationType type);

@property (nonatomic, strong) QWAgoraUserInfoModel * localUserInfoModel;
@property (nonatomic, strong)AgoraRtcEngineKit *agoraKit;

@property (nonatomic, assign) BOOL isShowView;
@property (nonatomic, assign) BOOL isOpenScreenShare; //是否打开了屏幕共享
@end

NS_ASSUME_NONNULL_END
