//
//  QWMeetHomeOptionsView.m
//  assistant
//
//  Created by qunai on 2023/12/11.
//

#import "QWMeetHomeOptionsView.h"

@implementation QWMeetHomeOptionsView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor whiteColor];
        NSArray *dataArr = @[@{@"name":@"加入会议",@"pic":@"m_join"},
                             @{@"name":@"快速会议",@"pic":@"m_quickStart"},
                             @{@"name":@"预定会议",@"pic":@"m_date"},
                             @{@"name":@"扫描入会",@"pic":@"m_scan"},
                             @{@"name":@"视频咨询",@"pic":@"m_video_consultation"},
                             @{@"name":@"直播",@"pic":@"m_video_live"}];
        
        
        NSMutableArray *tolAry = [NSMutableArray new];
        for (int i = 0; i< dataArr.count; i ++) {
            NSDictionary *dic = dataArr[i];
            QWMeetOptionItemView *itemView =[[QWMeetOptionItemView alloc]init];
            itemView.imageView.image = [UIImage imageNamed:dic[@"pic"]];
            itemView.nameLabel.text = dic[@"name"];
            itemView.button.tag = 10000+i;
            [itemView.button addTarget:self action:@selector(bottonOnClick:) forControlEvents:UIControlEventTouchUpInside];
            [tolAry addObject:itemView];
        }
        
        UIView *allView = [[QWCommonMethod Instance] mas_HorizontalBisection:tolAry withNum:4 withFixedSpacing:5 leadSpacing:14.0 tailSpacing:14.0];
        [self addSubview:allView];
        
        [allView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return self;
}

- (void)bottonOnClick:(UIButton *)sender{
    if(self.homeSelectBlock)self.homeSelectBlock(sender.tag - 10000);
}


@end

@implementation QWMeetOptionItemView

- (instancetype)init{
    self = [super init];
    if(self){
        [self imageView];
        [self nameLabel];
        [self button];
    }
    return self;
}

- (UIImageView *)imageView{
    if(!_imageView){
        _imageView = [[UIImageView alloc]init];
        _imageView.backgroundColor = Color_Main_Green;
        _imageView.layer.cornerRadius = 6.0;
        _imageView.layer.masksToBounds = YES;
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_imageView];
        [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.equalTo(self).offset(5);
            make.width.mas_equalTo(45);
            make.height.mas_equalTo(45);
        }];
    }
    return _imageView;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightLight];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.top.equalTo(self.imageView.mas_bottom);
            make.height.mas_equalTo(25);
            make.bottom.equalTo(self);
        }];
    }
    return _nameLabel;
}

- (UIButton *)button{
    if(!_button){
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:_button];
        [_button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return _button;
}


@end
