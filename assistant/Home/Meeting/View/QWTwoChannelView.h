//
//  QWTwoChannelView.h
//  assistant
//
//  Created by qunai on 2023/12/14.
//

#import <UIKit/UIKit.h>
#import "QWVideoView.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWTwoChannelView : UIView<UIGestureRecognizerDelegate>
@property (nonatomic, strong) QWVideoView *localView;
@property (nonatomic, strong) QWVideoView *remoteView;

- (void)changeLocalViewFrameToSmall:(BOOL)isBig;
@end

NS_ASSUME_NONNULL_END
