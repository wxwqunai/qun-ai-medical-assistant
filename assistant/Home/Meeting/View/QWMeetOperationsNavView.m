//
//  QWMeetOperationsNavView.m
//  assistant
//
//  Created by qunai on 2023/12/18.
//

#import "QWMeetOperationsNavView.h"

@interface QWMeetOperationsNavView()
@property (nonatomic, strong) UIView *containView;
@property (nonatomic, strong) UIButton *endBtn;

@property (nonatomic, strong) UIView *titleView;
@property (nonatomic, strong) UIButton *titleBtn;

@property (nonatomic, strong) UIButton *loudspeakerBtn;
@end

@implementation QWMeetOperationsNavView

- (instancetype)init{
    self = [super init];
    if(self){
        self.backgroundColor = [[UIColor colorFromHexString:@"#202022"] colorWithAlphaComponent:0.9];
        [self containView];
        
        [self titleView];
        [self titleBtn];
        
        [self endBtn];
        [self loudspeakerBtn];
    }
    return self;
}

- (UIView *)containView{
    if(!_containView){
        _containView = [[UIView alloc]init];
        [self addSubview:_containView];
        [_containView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset([UIDevice safeDistanceTop]);
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.bottom.equalTo(self);
        }];
    }
    return _containView;
}


#pragma mark - 结束
- (UIButton *)endBtn{
    if(!_endBtn){
        _endBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_endBtn setTitle:@"离开" forState:UIControlStateNormal];
        [_endBtn setTitleColor:[UIColor systemRedColor] forState:UIControlStateNormal];
        [_endBtn addTarget:self action:@selector(endBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.containView addSubview:_endBtn];
        
        [_endBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.containView);
            make.bottom.equalTo(self.containView);
            make.right.equalTo(self.containView).offset(-14.0);
        }];
    }
    return _endBtn;
}
- (void)endBtnOnClick{
    if(self.endBlock)self.endBlock();
}

#pragma mark - 标题、时间
- (UIView *)titleView{
    if(!_titleView){
        _titleView = [[UIView alloc]init];
        _titleView.backgroundColor = [UIColor clearColor];
        [self addSubview:_titleView];
        [_titleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.containView);
            make.bottom.equalTo(self.containView);
            make.centerX.equalTo(self.containView);
            make.width.equalTo(self.containView).multipliedBy(0.5);
        }];
    }
    return _titleView;
}

- (UIButton *)titleBtn{
    if(!_titleBtn){
        _titleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        _titleLabel.font = [UIFont systemFontOfSize:17];
//        _titleLabel.text = @"群爱会议";
//        _titleLabel.textAlignment = NSTextAlignmentCenter;
//        _titleLabel.textColor = [UIColor whiteColor];
        [_titleBtn setTitle:@"群爱会议" forState:UIControlStateNormal];
        [_titleBtn setImage:[UIImage imageNamed:@"ic_arrow_down.png"] forState:UIControlStateNormal];
        [_titleBtn setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
        _titleBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
        [_titleBtn addTarget:self action:@selector(titleBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.titleView addSubview:_titleBtn];
        
        [_titleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.titleView);
        }];
    }
    return _titleBtn;
}
- (void)titleBtnClick{
    if(self.openMeetingInfoBlock)self.openMeetingInfoBlock();
}

#pragma mark - 扬声器
- (UIButton *)loudspeakerBtn{
    if(!_loudspeakerBtn){
        _loudspeakerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_loudspeakerBtn setImage:[UIImage imageNamed:self.isOpenLoudspeaker?@"m_loudspeaker":@"m_loudspeaker_close"] forState:UIControlStateNormal];
        [_loudspeakerBtn addTarget:self action:@selector(loudspeakerBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.containView addSubview:_loudspeakerBtn];
        
        [_loudspeakerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.containView);
            make.bottom.equalTo(self.containView);
            make.left.equalTo(self.containView).offset(14.0);
        }];
    }
    return _loudspeakerBtn;
}
- (void)loudspeakerBtnOnClick{
    self.isOpenLoudspeaker = !self.isOpenLoudspeaker;
    if(self.loudspeakerBlock)self.loudspeakerBlock(self.isOpenLoudspeaker);
}

- (void)setIsOpenLoudspeaker:(BOOL)isOpenLoudspeaker{
    _isOpenLoudspeaker = isOpenLoudspeaker;
    [self.loudspeakerBtn setImage:[UIImage imageNamed:self.isOpenLoudspeaker?@"m_loudspeaker":@"m_loudspeaker_close"] forState:UIControlStateNormal];
}

@end
