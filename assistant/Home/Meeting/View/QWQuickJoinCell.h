//
//  QWQuickJoinCell.h
//  assistant
//
//  Created by qunai on 2023/12/29.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWQuickJoinCell : QWBaseTableViewCell
@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, strong) UIView *rightView;
@property (nonatomic, strong) UILabel *numLabel;
@property (nonatomic, strong) UIButton *button;
@end

NS_ASSUME_NONNULL_END
