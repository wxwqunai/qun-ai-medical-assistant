//
//  QWQuickJoinCell.m
//  assistant
//
//  Created by qunai on 2023/12/29.
//

#import "QWQuickJoinCell.h"

@implementation QWQuickJoinCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self nameLabel];
        
        [self rightView];
        [self button];
        [self numLabel];
    }
    return self;
}
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont systemFontOfSize:16];
        
        [self.contentView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            make.left.equalTo(self.contentView).offset(14.0);
            make.width.equalTo(self.contentView).multipliedBy(0.5);
            make.height.mas_equalTo(44);
        }];
        
    }
    return _nameLabel;
}

- (UIView *)rightView{
    if(!_rightView){
        _rightView = [[UIView alloc]init];
        [self.contentView addSubview:_rightView];
        
        [_rightView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-14.0);
            make.top.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
        }];
    }
    return _rightView;
}
- (UILabel *)numLabel{
    if(!_numLabel){
        _numLabel = [[UILabel alloc]init];
        _numLabel.font = [UIFont systemFontOfSize:14];
        
        [self.rightView addSubview:_numLabel];
        
        [_numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.rightView);
            make.right.equalTo(self.button.mas_left).offset(-4);
            make.top.equalTo(self.rightView);
            make.bottom.equalTo(self.rightView);
        }];
    }
    return _numLabel;
}

- (UIButton *)button{
    if(!_button){
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        [_button setImage:[UIImage imageNamed:@"m_copy"] forState:UIControlStateNormal];
        [_button addTarget:self action:@selector(buttonOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.rightView addSubview:_button];
        
        [_button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.rightView);
            make.top.equalTo(self.rightView);
            make.bottom.equalTo(self.rightView);
        }];
    }
    return _button;
}
-(void)buttonOnClick{
    if(_numLabel.text){
        [SWHUDUtil hideHudViewWithMessageSuperView:self.superview withMessage:@"会议号已复制到剪贴板"];
        UIPasteboard *pboard = [UIPasteboard generalPasteboard];
        pboard.string = _numLabel.text;
    }
}

@end
