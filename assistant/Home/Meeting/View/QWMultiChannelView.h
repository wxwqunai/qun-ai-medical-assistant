//
//  QWMultiChannelView.h
//  assistant
//
//  Created by qunai on 2023/12/14.
//

#import <UIKit/UIKit.h>
#import "QWVideoView.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWMultiChannelView : UIView
@property (nonatomic, strong)AgoraRtcEngineKit *agoraKit;
@property (nonatomic, strong) NSMutableArray *channelArr;
@property (strong, nonatomic) UICollectionView *collectionView;

@property (nonatomic,copy) void(^currentRemoteChangeBlock)(QWAgoraUserInfoModel *userInfoModel);
@end


@interface QWMultiChannelCell : UICollectionViewCell
@property (nonatomic, strong) QWVideoView *remoteView;
@property (nonatomic, assign) BOOL isShowBorder;
@property (nonatomic,copy) void(^cellDoubleTapBlock)(void);
@end

NS_ASSUME_NONNULL_END
