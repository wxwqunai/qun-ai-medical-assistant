//
//  QWJoinInputCell.m
//  assistant
//
//  Created by qunai on 2023/12/12.
//

#import "QWJoinInputCell.h"

@implementation QWJoinInputCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self nameLabel];
        
        [self centerView];
        [self textField];
        
        [self rightView];
        [self customSwitch];
    }
    return self;
}
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont systemFontOfSize:16];
        
        [self.contentView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            make.left.equalTo(self.contentView).offset(14.0);
            make.width.mas_equalTo(90);
            make.height.mas_equalTo(44);
        }];
        
    }
    return _nameLabel;
}
#pragma mark - 输入
- (UIView *)centerView{
    if(!_centerView){
        _centerView = [[UIView alloc]init];
        [self.contentView addSubview:_centerView];
        
        [_centerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.nameLabel.mas_right);
            make.top.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            make.right.equalTo(self.rightView.mas_left);
        }];
    }
    return _centerView;
}

- (UITextField *)textField{
    if(!_textField){
        _textField = [[UITextField alloc]init];
//        _textField.placeholder =@"请输入";
//        _textField.textColor =[UIColor colorFromHexString:@"#A2A2A2"];
        _textField.borderStyle = UITextBorderStyleNone;
        _textField.font =[UIFont systemFontOfSize:14];
        
        //边框
//        _textField.layer.borderColor = [UIColor colorFromHexString:@"#A2A2A2"].CGColor;
//        _textField.layer.borderWidth = 1;
        
        //左边距
//        _textField.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, 0)];
//        _textField.leftViewMode = UITextFieldViewModeAlways;
                
        //清除样式
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        [self.centerView addSubview:_textField];
        
        [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_centerView);
            make.height.mas_equalTo(20);
            make.left.equalTo(_centerView);
            make.right.equalTo(_centerView);
        }];
    }
    return _textField;
}

- (void)setIsHiddenCenter:(BOOL)isHiddenCenter{
    _isHiddenCenter = isHiddenCenter;
    _centerView.hidden = _isHiddenCenter;
}


#pragma mark - 开关
- (UIView *)rightView{
    if(!_rightView){
        _rightView = [[UIView alloc]init];
        [self.contentView addSubview:_rightView];
        
        [_rightView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-14.0);
            make.top.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            make.width.mas_equalTo(50);
        }];
    }
    return _rightView;
}

- (UISwitch *)customSwitch{
    if(!_customSwitch){
        _customSwitch = [[UISwitch alloc]init];
        _customSwitch.transform = CGAffineTransformMakeScale(0.7, 0.7);
        _customSwitch.onTintColor = [UIColor blueColor];
        _customSwitch.thumbTintColor = [UIColor whiteColor];
        _customSwitch.on = YES;
        [_rightView addSubview:_customSwitch];
        
        [_customSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_rightView);
            make.centerY.equalTo(_rightView);
        }];
    }
    return _customSwitch;
}

- (void)setIsHiddenRight:(BOOL)isHiddenRight{
    _isHiddenRight = isHiddenRight;
    _rightView.hidden = _isHiddenRight;
    [_rightView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(_isHiddenRight?0:50);
    }];
    
}

@end
