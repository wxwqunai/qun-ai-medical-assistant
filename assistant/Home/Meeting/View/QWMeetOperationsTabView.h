//
//  QWMeetOperationsTabView.h
//  assistant
//
//  Created by qunai on 2023/12/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWMeetOperationsTabView : UIView
@property (nonatomic, assign) BOOL isOpenMicrophone;
@property (nonatomic, assign) BOOL isOpenVideo;
@property (nonatomic, assign) BOOL isOpenScreenShare; //是否打开了屏幕共享

@property (nonatomic,copy) void(^microphoneBlock)(BOOL isOpen);
@property (nonatomic,copy) void(^videoBlock)(BOOL isOpen);
@property (nonatomic,copy) void(^shareScreenOpenBlock)(BOOL isOpen);
@end


@interface QWTabItemsView : UIView
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UIButton *button;


@property (nonatomic, copy) NSString *imageStr;
@property (nonatomic, copy) NSString *nameStr;

@end

NS_ASSUME_NONNULL_END
