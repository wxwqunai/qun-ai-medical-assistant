//
//  QWMeetingDefaultCell.h
//  assistant
//
//  Created by qunai on 2024/1/16.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWMeetingDefaultCell : QWBaseTableViewCell
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UIImageView *rightArrow;
@property (nonatomic, strong) UILabel *descLabel;
@end

NS_ASSUME_NONNULL_END
