//
//  QWMeetingInputCell.m
//  assistant
//
//  Created by qunai on 2024/1/11.
//

#import "QWMeetingInputCell.h"

@implementation QWMeetingInputCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self textField];
    }
    return self;
}

#pragma mark - 输入
- (UITextField *)textField{
    if(!_textField){
        _textField = [[UITextField alloc]init];
//        _textField.placeholder =@"请输入";
//        _textField.textColor =[UIColor colorFromHexString:@"#A2A2A2"];
        _textField.borderStyle = UITextBorderStyleNone;
        _textField.font =[UIFont systemFontOfSize:14];
        
        //边框
//        _textField.layer.borderColor = [UIColor colorFromHexString:@"#A2A2A2"].CGColor;
//        _textField.layer.borderWidth = 1;
        
        //左边距
//        _textField.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, 0)];
//        _textField.leftViewMode = UITextFieldViewModeAlways;
                
        //清除样式
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.delegate = self;
        [self.contentView addSubview:_textField];
        
        [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(14.0);
            make.right.equalTo(self.contentView).offset(-14.0);
            make.top.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            make.height.mas_equalTo(44);
        }];
    }
    return _textField;
}
- (void)setPlaceholder:(NSString *)placeholder{
    _placeholder = placeholder;
    self.textField.placeholder = _placeholder;
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if(self.textFieldBlock)self.textFieldBlock(textField.text);
}
@end
