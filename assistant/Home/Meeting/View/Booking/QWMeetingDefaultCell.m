//
//  QWMeetingDefaultCell.m
//  assistant
//
//  Created by qunai on 2024/1/16.
//

#import "QWMeetingDefaultCell.h"

@implementation QWMeetingDefaultCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self nameLabel];
        [self rightArrow];
        [self descLabel];
    }
    return self;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightLight];
        
        [self.contentView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            make.left.equalTo(self.contentView).offset(14.0);
            make.width.mas_equalTo(120);
            make.height.mas_equalTo(44);
        }];
        
    }
    return _nameLabel;
}

- (UILabel *)descLabel{
    if(!_descLabel){
        _descLabel = [[UILabel alloc]init];
        _descLabel.textAlignment = NSTextAlignmentRight;
        _descLabel.textColor = [UIColor coolGrayColor];
        _descLabel.font = [UIFont systemFontOfSize:14];
        
        [self.contentView addSubview:_descLabel];
        
        [_descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            make.left.equalTo(self.nameLabel.mas_right);
            make.right.equalTo(self.rightArrow.mas_left).offset(-4.0);
        }];
        
    }
    return _descLabel;
}




- (UIImageView *)rightArrow{
    if(!_rightArrow){
        _rightArrow = [[UIImageView alloc]init];
        UIImage *image = [UIImage imageNamed:@"next_arrow.png"];
        _rightArrow.image = image;
        [self.contentView addSubview:_rightArrow];
        
        [_rightArrow mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-14.0);
            make.centerY.equalTo(self);
            make.width.mas_equalTo(image.size.width);
            make.height.mas_equalTo(image.size.height);
        }];
    }
    return _rightArrow;
}

@end
