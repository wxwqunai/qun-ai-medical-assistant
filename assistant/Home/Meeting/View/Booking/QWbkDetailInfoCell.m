//
//  QWbkDetailInfoCell.m
//  assistant
//
//  Created by qunai on 2024/1/16.
//

#import "QWbkDetailInfoCell.h"

@interface QWbkDetailInfoCell()
@property (nonatomic, strong) UIView *timeView;
@property (nonatomic, strong) TimeShowView *startTimeView;
@property (nonatomic, strong) TimeShowView *endTimeView;

@property (nonatomic, strong) DurationView *durationView;
@end
@implementation QWbkDetailInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self nameLabel];
        
        [self timeView];
        [self startTimeView];
        [self endTimeView];
        
        [self durationView];
    }
    return self;
}


- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont boldSystemFontOfSize:17];
        
        [self.contentView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView);
            make.left.equalTo(self.contentView).offset(14.0);
            make.height.mas_equalTo(44);
        }];
        
    }
    return _nameLabel;
}

- (UIView *)timeView{
    if(!_timeView){
        _timeView = [[UIView alloc]init];
        [self.contentView addSubview:_timeView];
        [_timeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(14.0);
            make.right.equalTo(self.contentView).offset(-14.0);
            make.top.equalTo(self.nameLabel.mas_bottom).offset(10.0);
            make.bottom.equalTo(self.contentView).offset(-10.0);
        }];
    }
    return _timeView;
}

#pragma mark- 开始时间
- (TimeShowView *)startTimeView{
    if(!_startTimeView){
        _startTimeView = [[TimeShowView alloc]init];
        [self.timeView addSubview:_startTimeView];
        [_startTimeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.timeView);
            make.top.equalTo(self.timeView);
            make.bottom.equalTo(self.timeView);
        }];
    }
    return _startTimeView;
}

#pragma mark- 结束时间
- (TimeShowView *)endTimeView{
    if(!_endTimeView){
        _endTimeView = [[TimeShowView alloc]init];
        [self.timeView addSubview:_endTimeView];
        [_endTimeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.timeView);
            make.top.equalTo(self.timeView);
            make.bottom.equalTo(self.timeView);
        }];
    }
    return _endTimeView;
}

#pragma mark - 时长
- (DurationView *)durationView{
    if(!_durationView){
        _durationView = [[DurationView alloc]init];
        
        [self.timeView addSubview:_durationView];
        [_durationView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.startTimeView.mas_right);
            make.right.equalTo(self.endTimeView.mas_left);
            make.top.equalTo(self.timeView);
            make.bottom.equalTo(self.timeView);
        }];
    }
    return _durationView;
}

- (void)setBookingModel:(QWAgoraUserInfoModel *)bookingModel{
    _bookingModel = bookingModel;
    self.nameLabel.text = _bookingModel.title;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"HH:mm";
    self.startTimeView.timeOneLabel.text = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:_bookingModel.startDate]]; //开始时间 -时分
    self.endTimeView.timeOneLabel.text = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:_bookingModel.endDate]]; //结束时间 -时分
    
    formatter.dateFormat = @"YYYY年MM月dd日";
    self.startTimeView.timeTwoLabel.text = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:_bookingModel.startDate]];//开始时间 - 年月日
    self.endTimeView.timeTwoLabel.text = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:_bookingModel.endDate]];//结束时间 - 年月日
    
    //时长
    self.durationView.durationLabel.text = _bookingModel.durationStr;
    
    //状态
    self.durationView.stateLabel.text = @"待开始";
}

@end


#pragma mark - 时间view
@implementation TimeShowView
- (instancetype)init{
    self = [super init];
    if(self){
        [self timeOneLabel];
        [self timeTwoLabel];
    }
    return self;
}
- (UILabel *)timeOneLabel{
    if(!_timeOneLabel){
        _timeOneLabel = [[UILabel alloc]init];
        _timeOneLabel.font = [UIFont systemFontOfSize:30 weight:UIFontWeightRegular];
        _timeOneLabel.textAlignment =NSTextAlignmentCenter;
        
        [self addSubview:_timeOneLabel];
        
        [_timeOneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.left.equalTo(self);
            make.right.equalTo(self);
        }];
        
    }
    return _timeOneLabel;
}

- (UILabel *)timeTwoLabel{
    if(!_timeTwoLabel){
        _timeTwoLabel = [[UILabel alloc]init];
        _timeTwoLabel.font = [UIFont systemFontOfSize:13 weight:UIFontWeightLight];
        _timeTwoLabel.textAlignment =NSTextAlignmentCenter;
        [self addSubview:_timeTwoLabel];
        
        [_timeTwoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.timeOneLabel.mas_bottom);
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.bottom.equalTo(self);
        }];
        
    }
    return _timeTwoLabel;
}
@end


#pragma mark - 时长
@implementation DurationView
- (instancetype)init{
    self = [super init];
    if(self){
        [self durationLabel];
        [self stateLabel];
        [self lineLeftImageView];
        [self lineRightImageView];
    }
    return self;
}

#pragma mark - 间隔时间
- (UILabel *)durationLabel{
    if(!_durationLabel){
        UIView *view = [[UIView alloc]init];
        view.backgroundColor = [UIColor colorFromHexString:@"#E9EBEF"];
        view.layer.cornerRadius = 4.0;
        view.layer.masksToBounds = YES;
        [self addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.centerY.equalTo(self);
            make.width.mas_greaterThanOrEqualTo(40);
            make.height.mas_greaterThanOrEqualTo(20);
        }];
        
        
        _durationLabel = [[UILabel alloc]init];
        _durationLabel.textColor = [UIColor colorFromHexString:@"#4D505C"];
        _durationLabel.font = [UIFont systemFontOfSize:10 weight:UIFontWeightLight];
        _durationLabel.textAlignment =NSTextAlignmentCenter;
        [view addSubview:_durationLabel];
        
        [_durationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(view).offset(2.0);
            make.bottom.equalTo(view).offset(-2.0);
            make.left.equalTo(view).offset(2.0);
            make.right.equalTo(view).offset(-2.0);
        }];
        
    }
    return _durationLabel;
}

#pragma mark - 状态
- (UILabel *)stateLabel{
    if(!_stateLabel){
        _stateLabel = [[UILabel alloc]init];
        _stateLabel.font = [UIFont systemFontOfSize:11 weight:UIFontWeightLight];
        _stateLabel.textAlignment =NSTextAlignmentCenter;
        _stateLabel.textColor = [UIColor orangeColor];
        [self addSubview:_stateLabel];
        
        [_stateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.durationLabel.mas_top).offset(-4.0);
            make.centerX.equalTo(self);
        }];
        
    }
    return _stateLabel;
}

#pragma mark - 分割线
- (UIImageView *)lineLeftImageView{
    if(!_lineLeftImageView){
        _lineLeftImageView = [[UIImageView alloc]init];
        _lineLeftImageView.backgroundColor = [UIColor colorFromHexString:@"#CBCDD2"];
        [self addSubview:_lineLeftImageView];
        [_lineLeftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(4.0);
            make.right.equalTo(self.durationLabel.mas_left).offset(-6.0);
            make.centerY.equalTo(self);
            make.height.mas_equalTo(1.5);
        }];
    }
    return _lineLeftImageView;
}

- (UIImageView *)lineRightImageView{
    if(!_lineRightImageView){
        _lineRightImageView = [[UIImageView alloc]init];
        _lineRightImageView.backgroundColor = [UIColor colorFromHexString:@"#CBCDD2"];
        [self addSubview:_lineRightImageView];
        [_lineRightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self).offset(-4.0);
            make.left.equalTo(self.durationLabel.mas_right).offset(6.0);
            make.centerY.equalTo(self);
            make.height.mas_equalTo(1.5);
        }];
    }
    return _lineRightImageView;
}
@end
