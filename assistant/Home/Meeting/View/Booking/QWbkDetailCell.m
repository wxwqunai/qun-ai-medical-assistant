//
//  QWbkDetailCell.m
//  assistant
//
//  Created by qunai on 2024/1/17.
//

#import "QWbkDetailCell.h"

@implementation QWbkDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self nameLabel];
        [self numCopyButton];
        [self descLabel];
    }
    return self;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightLight];
        
        [self.contentView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            make.left.equalTo(self.contentView).offset(14.0);
            make.width.mas_equalTo(50);
            make.height.mas_equalTo(44);
        }];
        
    }
    return _nameLabel;
}

- (UILabel *)descLabel{
    if(!_descLabel){
        _descLabel = [[UILabel alloc]init];
        _descLabel.textColor = [UIColor coolGrayColor];
        _descLabel.font = [UIFont systemFontOfSize:14];
        
        [self.contentView addSubview:_descLabel];
        
        [_descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            make.left.equalTo(self.nameLabel.mas_right).offset(20.0);
            make.right.equalTo(self.numCopyButton.mas_left).offset(-8.0);
        }];
        
    }
    return _descLabel;
}

- (UIButton *)numCopyButton{
    if(!_numCopyButton){
        _numCopyButton = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *image = [UIImage imageNamed:@"m_copy"];
        [_numCopyButton setImage:image forState:UIControlStateNormal];
        [_numCopyButton addTarget:self action:@selector(buttonOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_numCopyButton];
        
        [_numCopyButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.width.mas_equalTo(image.size.width);
            make.height.mas_equalTo(image.size.height);
        }];
    }
    return _numCopyButton;
}
-(void)buttonOnClick{
    if(self.descLabel.text){
        [SWHUDUtil hideHudViewWithMessageSuperView:self.superview withMessage:@"会议号已复制到剪贴板"];
        UIPasteboard *pboard = [UIPasteboard generalPasteboard];
        pboard.string = self.descLabel.text;
    }
}

@end
