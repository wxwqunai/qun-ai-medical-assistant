//
//  QWMeetingSwitchCell.h
//  assistant
//
//  Created by qunai on 2024/1/15.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWMeetingSwitchCell : QWBaseTableViewCell
@property (nonatomic, strong) UILabel *nameLabel;


@property (nonatomic, strong) UIView *rightView;
@property (nonatomic, strong) UISwitch *customSwitch;
@end

NS_ASSUME_NONNULL_END
