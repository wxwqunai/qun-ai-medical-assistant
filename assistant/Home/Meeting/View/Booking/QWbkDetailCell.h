//
//  QWbkDetailCell.h
//  assistant
//
//  Created by qunai on 2024/1/17.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWbkDetailCell : QWBaseTableViewCell
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *descLabel;
@property (nonatomic, strong) UIButton *numCopyButton;

@end

NS_ASSUME_NONNULL_END
