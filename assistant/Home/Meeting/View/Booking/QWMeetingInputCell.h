//
//  QWMeetingInputCell.h
//  assistant
//
//  Created by qunai on 2024/1/11.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWMeetingInputCell : QWBaseTableViewCell<UITextFieldDelegate>

@property (nonatomic, strong) UITextField *textField;

@property (nonatomic, copy) NSString *placeholder;

@property (nonatomic, copy) void(^textFieldBlock)(NSString *text);
@end

NS_ASSUME_NONNULL_END
