//
//  QWbkDetailInfoCell.h
//  assistant
//
//  Created by qunai on 2024/1/16.
//

#import "QWBaseTableViewCell.h"
#import "QWMeetingModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWbkDetailInfoCell : QWBaseTableViewCell
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) QWAgoraUserInfoModel *bookingModel;

@end

@interface TimeShowView : UIView
@property (nonatomic, strong) UILabel *timeOneLabel;
@property (nonatomic, strong) UILabel *timeTwoLabel;
@end

@interface DurationView : UIView
@property (nonatomic, strong) UILabel *stateLabel;
@property (nonatomic, strong) UILabel *durationLabel;
@property (nonatomic, strong) UIImageView *lineLeftImageView;
@property (nonatomic, strong) UIImageView *lineRightImageView;
@end

NS_ASSUME_NONNULL_END
