//
//  QWMeetHomeOptionsView.h
//  assistant
//
//  Created by qunai on 2023/12/11.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWMeetHomeOptionsView : UIView
@property (nonatomic,copy) void(^homeSelectBlock)(NSInteger selIndex);
@end

@interface QWMeetOptionItemView : UIView
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UIButton *button;

@end

NS_ASSUME_NONNULL_END
