//
//  QWJoinInputCell.h
//  assistant
//
//  Created by qunai on 2023/12/12.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWJoinInputCell : QWBaseTableViewCell
@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, strong) UIView *centerView;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, assign) BOOL isHiddenCenter;

@property (nonatomic, strong) UIView *rightView;
@property (nonatomic, strong) UISwitch *customSwitch;
@property (nonatomic, assign) BOOL isHiddenRight;
@end

NS_ASSUME_NONNULL_END
