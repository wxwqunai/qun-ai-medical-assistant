//
//  QWMeetVideoScrollView.m
//  assistant
//
//  Created by qunai on 2023/12/15.
//

#import "QWMeetVideoScrollView.h"
#import "SMPageControl.h"
#import "QWMeetingModel.h"

@interface QWMeetVideoScrollView()<UIScrollViewDelegate>
@property (strong, nonatomic) SMPageControl * pageControl;

@end

@implementation QWMeetVideoScrollView

- (instancetype)init{
    self = [super init];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        
        [self scrollView];
        [self addScrollSubViews];
        [self pageControl];
    }
    return self;
}

- (UIScrollView *)scrollView{
    if(!_scrollView){
        _scrollView = [[UIScrollView alloc]init];
        _scrollView.backgroundColor =[UIColor clearColor];
        _scrollView.delegate =self;
        _scrollView.pagingEnabled =YES;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        [self addSubview:_scrollView];
        [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.top.equalTo(self);
            make.right.equalTo(self);
            make.bottom.equalTo(self);
        }];
    }
    return _scrollView;
}

- (void)addScrollSubViews{
    [self.scrollView addSubview:self.twoChannelView];
    [self.twoChannelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.scrollView);
        make.top.equalTo(self.scrollView);
        make.width.equalTo(self.scrollView.mas_width);
        make.height.mas_equalTo(kScreenHeight);
    }];

    [self.scrollView addSubview:self.multiChannelView];
    [self.multiChannelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.twoChannelView.mas_right);
        make.top.equalTo(self.scrollView);
        make.width.equalTo(self.scrollView.mas_width);
        make.height.mas_equalTo(kScreenHeight);

    }];

    [self.scrollView mas_updateConstraints:^(MASConstraintMaker *make) {
        //让scrollview的contentSize随着内容的增多而变化
        make.right.mas_equalTo(self.multiChannelView.mas_right);
    }];
    self.scrollView.pagingEnabled=YES;
    self.scrollView.scrollEnabled = NO;
    self.scrollView.alwaysBounceHorizontal=YES;
    self.scrollView.contentSize =CGSizeMake(kScreenWidth*2, 0);
}
#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//    if (scrollView.contentOffset.x<0) {
//        [scrollView setContentOffset:CGPointMake(0, scrollView.contentOffset.y) animated:NO];
//    }
//    if (scrollView.contentOffset.x>scrollView.frame.size.width) {
//        [scrollView setContentOffset:CGPointMake(scrollView.frame.size.width, scrollView.contentOffset.y) animated:NO];
//    }

    NSInteger page = scrollView.contentOffset.x/scrollView.frame.size.width;
    _pageControl.currentPage = page;
}

#pragma mark - 两人视频View
- (QWTwoChannelView *)twoChannelView{
    if(!_twoChannelView){
        _twoChannelView = [[QWTwoChannelView alloc]init];
    }
    return _twoChannelView;
}
#pragma mark - 多人视频View
- (QWMultiChannelView *)multiChannelView{
    if(!_multiChannelView){
        _multiChannelView = [[QWMultiChannelView alloc]init];
        
        __weak __typeof(self) weakSelf = self;
        _multiChannelView.currentRemoteChangeBlock = ^(QWAgoraUserInfoModel * _Nonnull userInfoModel) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf replaceCurrentRemote:userInfoModel.uid];
        };
    }
    return _multiChannelView;
}

#pragma mark - 两人视频 - 本地视频
- (void)setupLocalVideo{
    self.twoChannelView.localView.uid = 0;
    [self.agoraKit setupLocalVideo:self.twoChannelView.localView.videoCanvas];
    self.twoChannelView.localView.userInfoModel = self.localUserInfoModel;
    [self.twoChannelView changeLocalViewFrameToSmall:NO];
}

#pragma mark - 多人参会scroll状态改变
- (void)changeScrollState{
    //让scrollview的contentSize随着内容的增多而变化
    if(self.remoteUidArr.count>1){
        self.scrollView.scrollEnabled = YES;
        self.pageControl.hidden = NO;
    }else{
        [self.scrollView setContentOffset:CGPointMake(0, 0)];
        self.scrollView.scrollEnabled = NO;
        self.pageControl.hidden = YES;
    }
}

#pragma mark - 新增参会人
- (void)addRemoteUid:(NSUInteger)uid{
    
    QWAgoraUserInfoModel *userInfoModel = [[QWAgoraUserInfoModel alloc]init];
    AgoraUserInfo *userInfo = [self.agoraKit getUserInfoByUid:uid withError:nil];
    userInfoModel.userInfo = userInfo;
    
    [self.remoteUidArr addObject:userInfoModel];
    
    //第一个参会人/当前聊天参会人
    if(self.remoteUidArr.count > 0){
        QWAgoraUserInfoModel *first_model = (QWAgoraUserInfoModel *)self.remoteUidArr.firstObject;
        if(self.twoChannelView.remoteView.uid != first_model.uid){
            self.twoChannelView.remoteView.uid = first_model.uid;
            [self.agoraKit setupRemoteVideo:self.twoChannelView.remoteView.videoCanvas];
            self.twoChannelView.remoteView.userInfoModel = first_model;
            
            self.twoChannelView.remoteView.hidden = NO;
            [self.twoChannelView changeLocalViewFrameToSmall:YES];
        }
    }
    
    //其他参会人视频通话
    self.multiChannelView.channelArr = self.remoteUidArr;
    
    [self changeScrollState];
}
#pragma mark - 参会人离开
- (void)deleteRemoteUid:(NSUInteger)uid{
    AgoraRtcVideoCanvas *videoCanvas = [[AgoraRtcVideoCanvas alloc]init];
    videoCanvas.uid = uid;
    videoCanvas.view = nil;   // the view to be binded
    [self.agoraKit setupRemoteVideo:videoCanvas];
    
    [self.remoteUidArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        QWAgoraUserInfoModel *userInfoModel = (QWAgoraUserInfoModel *)obj;
        if(userInfoModel.uid == uid){
            [self.remoteUidArr removeObjectAtIndex:idx];
            *stop = YES;
        }
    }];
    
    if(self.remoteUidArr.count >0){
        if(self.twoChannelView.remoteView.uid == uid){
            QWAgoraUserInfoModel *first_model = (QWAgoraUserInfoModel *)self.remoteUidArr.firstObject;
            self.twoChannelView.remoteView.uid = first_model.uid;
            [self.agoraKit setupRemoteVideo:self.twoChannelView.remoteView.videoCanvas];
            self.twoChannelView.remoteView.userInfoModel = first_model;
            
            [self.twoChannelView changeLocalViewFrameToSmall:YES];
        }
    }else{
        self.twoChannelView.remoteView.uid = 0;
        self.twoChannelView.remoteView.hidden = YES;
        [self.twoChannelView changeLocalViewFrameToSmall:NO];
        
    }
    
    //其他参会人视频通话
    self.multiChannelView.channelArr = self.remoteUidArr;
    
    [self changeScrollState];
}

#pragma mark - 更换当前参会人
- (void)replaceCurrentRemote:(NSUInteger)uid{
    [self.remoteUidArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        QWAgoraUserInfoModel *userInfoModel = (QWAgoraUserInfoModel *)obj;
        if(userInfoModel.uid == uid){
            [self.remoteUidArr removeObjectAtIndex:idx];
            [self.remoteUidArr insertObject:userInfoModel atIndex:0];
            *stop = YES;
        }
    }];
    
    if(self.remoteUidArr.count >0){
        QWAgoraUserInfoModel *first_model = (QWAgoraUserInfoModel *)self.remoteUidArr.firstObject;
        if(self.twoChannelView.remoteView.uid != first_model.uid){
            self.twoChannelView.remoteView.uid = first_model.uid;
            [self.agoraKit setupRemoteVideo:self.twoChannelView.remoteView.videoCanvas];
            self.twoChannelView.remoteView.userInfoModel = first_model;
            
            [self.twoChannelView changeLocalViewFrameToSmall:YES];
        }
    }
    
    //其他参会人视频通话
    self.multiChannelView.channelArr = self.remoteUidArr;
    
    [self changeScrollState];
}

#pragma mark - 远端用户当前活跃 - 音量最大者
- (void)activeSpeaker:(NSUInteger)uid{
    [self.remoteUidArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        QWAgoraUserInfoModel *userInfoModel = (QWAgoraUserInfoModel *)obj;
        userInfoModel.isActiveSpeaker = NO;
        if(userInfoModel.uid == uid){
            userInfoModel.isActiveSpeaker = YES;
        }
    }];
    
    //其他参会人视频通话
    self.multiChannelView.channelArr = self.remoteUidArr;
}

- (void)setAgoraKit:(AgoraRtcEngineKit *)agoraKit{
    _agoraKit = agoraKit;
    _multiChannelView.agoraKit = agoraKit;
}

- (NSMutableArray *)remoteUidArr{
    if(!_remoteUidArr){
        _remoteUidArr = [[NSMutableArray alloc]init];
    }
    return _remoteUidArr;
}

#pragma mark - 远程音频状态改变-开启/关闭
- (void)changeAudioRemoteState:(BOOL)isOpne withUid:(NSUInteger)uid{
    [self.remoteUidArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        QWAgoraUserInfoModel *userInfoModel = (QWAgoraUserInfoModel *)obj;
        if(userInfoModel.uid == uid){
            userInfoModel.isOpenMicrophone = isOpne;
            if(idx == 0){
                self.twoChannelView.remoteView.userInfoModel =userInfoModel;
            }else{
                [self.multiChannelView.collectionView reloadData];
            }

            *stop = YES;
        }
    }];
}

#pragma mark - 远程视频状态改变-开启/关闭
- (void)changeVideoRemoteState:(BOOL)isOpne withUid:(NSUInteger)uid{
    [self.remoteUidArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        QWAgoraUserInfoModel *userInfoModel = (QWAgoraUserInfoModel *)obj;
        if(userInfoModel.uid == uid){
            userInfoModel.isOpenVideo = isOpne;
            if(idx == 0){
                self.twoChannelView.remoteView.userInfoModel =userInfoModel;
            }else{
                [self.multiChannelView.collectionView reloadData];
            }
            *stop = YES;
        }
    }];
}

#pragma mark- 小圆圈
-(SMPageControl *)pageControl
{
    if (!_pageControl) {
        _pageControl =[[SMPageControl alloc]init];
        [_pageControl setPageIndicatorImage:[UIImage imageNamed:@"WpageDot"]];
        [_pageControl setCurrentPageIndicatorImage:[UIImage imageNamed:@"WpageCurrentDot"]];
        _pageControl.indicatorMargin = 5.0f;
        _pageControl.indicatorDiameter = 6.0f;
        _pageControl.numberOfPages = 2;
        _pageControl.hidden = YES;
//        _pageControl.currentPageIndicatorTintColor = Color_Main_Green;
//        _pageControl.pageIndicatorTintColor = [UIColor whiteColor];
        [self addSubview:_pageControl];
        
        [_pageControl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.bottom.equalTo(self).offset(-[UIDevice safeDistanceBottom]);
            make.height.equalTo(@20);
        }];
    }
    return _pageControl;
}


@end
