//
//  QWMultiChannelView.m
//  assistant
//
//  Created by qunai on 2023/12/14.
//

#import "QWMultiChannelView.h"
#import "KeyCenter.h"

@interface QWMultiChannelView()<AgoraRtcEngineDelegate,UICollectionViewDelegate, UICollectionViewDataSource>
@property (strong, nonatomic) UICollectionViewFlowLayout *flowLayout;
@property (nonatomic, strong) NSMutableArray *showArr;
@end
@implementation QWMultiChannelView

- (instancetype)init{
    self = [super init];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        [self initUI];
    }
    return self;
}
-(void) initUI{
    [self flowLayout];
    [self collectionView];
}



#pragma mark - 对集合视图进行布局
-(UICollectionViewFlowLayout *)flowLayout
{
    if (!_flowLayout) {
        // 创建集合视图布局对象
        _flowLayout = [[UICollectionViewFlowLayout alloc] init];
        // 设置最小的左右间距
        _flowLayout.minimumInteritemSpacing = 1.0;
        // 设置最小的行间距（上下）
        _flowLayout.minimumLineSpacing = 2.0;
        _flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        // 设置边界范围
        _flowLayout.sectionInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
        _flowLayout.headerReferenceSize =CGSizeMake(0.0, 0.0);
        _flowLayout.footerReferenceSize =CGSizeMake(0.0, 0.0);
    }
    return _flowLayout;
}
-(UICollectionView *)collectionView
{
    if (!_collectionView) {
        CGFloat width = kScreenWidth;
        CGFloat height = kScreenHeight - [UIDevice navigationFullHeight]- [UIDevice tabBarFullHeight];

        _collectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(0, [UIDevice navigationFullHeight], width, height) collectionViewLayout:self.flowLayout];
        _collectionView.showsVerticalScrollIndicator=NO;
        _collectionView.showsHorizontalScrollIndicator=NO;
        _collectionView.backgroundColor = [UIColor clearColor];
        
        [self addSubview:_collectionView];
        
        // 创建集合视图对象
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.scrollEnabled = YES;
        _collectionView.alwaysBounceVertical=YES;
        _collectionView.alwaysBounceHorizontal=NO;
        _collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏

        // 注册
//        [_collectionView registerClass:[QWComplaintHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"QWComplaintHeaderView"];
//        [_collectionView registerClass:[QWComplaintFooterView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"QWComplaintFooterView"];
        [_collectionView registerClass:[QWMultiChannelCell class] forCellWithReuseIdentifier:@"QWMultiChannelCell"];
    }
    return _collectionView;
}
#pragma mark - UICollectionViewDelegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = CGRectGetWidth(collectionView.frame)/3.0 - 1.0*2;
    CGFloat height = CGRectGetHeight(collectionView.frame)/3.0;
    return CGSizeMake(width, height);
}


#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.showArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    QWMultiChannelCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"QWMultiChannelCell" forIndexPath:indexPath];
    QWAgoraUserInfoModel *userInfoModel = self.showArr[indexPath.row];
    cell.remoteView.uid = userInfoModel.uid;
    [self.agoraKit setupRemoteVideo:cell.remoteView.videoCanvas];
    cell.remoteView.userInfoModel = userInfoModel;
    cell.isShowBorder = userInfoModel.isActiveSpeaker;
    cell.cellDoubleTapBlock = ^{
        [self cellDoubleTapOnClick:userInfoModel];
    };
    return cell;
}
//- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//}
#pragma mark - 双击
- (void)cellDoubleTapOnClick:(QWAgoraUserInfoModel *)userInfoModel{
    if(self.currentRemoteChangeBlock)self.currentRemoteChangeBlock(userInfoModel);
}

- (NSMutableArray *)showArr{
    if(!_showArr){
        _showArr = [[NSMutableArray alloc]init];
    }
    return _showArr;
}
- (void)setChannelArr:(NSMutableArray *)channelArr{
    _channelArr = channelArr;

    //其他参会人视频通话
    [self.showArr removeAllObjects];
    if(_channelArr.count>1){
        NSArray *arr = [_channelArr subarrayWithRange:NSMakeRange(1, _channelArr.count-1)];
        [self.showArr addObjectsFromArray:arr];
    }
    
    [self.collectionView reloadData];
}

@end


@implementation QWMultiChannelCell
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor blackColor];
        
        self.layer.borderColor = Color_Main_Green.CGColor;
        self.layer.borderWidth = 0.0;
        
        [self remoteView];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openMeetOptionsTap:)];
        tap.numberOfTapsRequired = 2;
        [self.contentView addGestureRecognizer:tap];
    }
    return self;
}

- (QWVideoView *)remoteView{
    if(!_remoteView){
        _remoteView = [[QWVideoView alloc]init];
        _remoteView.isCurrentSmallScreen = NO;
        [self.contentView addSubview:_remoteView];
        
        [_remoteView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView);
        }];
    }
    return _remoteView;
}
#pragma mark - 当前说话人 - 声音最高
- (void)setIsShowBorder:(BOOL)isShowBorder{
    _isShowBorder = isShowBorder;
    self.layer.borderWidth = _isShowBorder?0.5:0.0;
}

#pragma mark - 手势 - 双击
- (void)openMeetOptionsTap:(UIGestureRecognizer *)ges{
    if(self.cellDoubleTapBlock)self.cellDoubleTapBlock();
}

@end
