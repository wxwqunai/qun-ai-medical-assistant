//
//  QWMeetEmptybgView.m
//  assistant
//
//  Created by qunai on 2023/12/11.
//

#import "QWMeetEmptybgView.h"

@implementation QWMeetEmptybgView

- (instancetype)init{
    self = [super init];
    if(self){
        [self emptyImageView];
        [self contentLabel];
    }
    return self;
}

#pragma mark - 内容
- (UIImageView *)emptyImageView {
    if(!_emptyImageView){
        _emptyImageView  = [[UIImageView alloc]init];
        _emptyImageView.image = [UIImage imageNamed:@"m_empty"];
        [self addSubview:_emptyImageView];
        
        [_emptyImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.centerY.equalTo(self);
        }];
    }
    return _emptyImageView;
}

#pragma mark - 公司名称
- (UILabel *)contentLabel{
    if(!_contentLabel){
        _contentLabel = [[UILabel alloc]init];
        _contentLabel.textColor = [UIColor colorFromHexString:@"#BFBFBF"];
        _contentLabel.font = [UIFont systemFontOfSize:14];
        _contentLabel.textAlignment = NSTextAlignmentCenter;
        _contentLabel.text = @"暂 无 会 议";
        [self addSubview:_contentLabel];
        
        [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.equalTo(self.emptyImageView.mas_bottom).offset(10);
        }];
    }
    return _contentLabel;
}


@end
