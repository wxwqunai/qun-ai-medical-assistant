//
//  QWMeetOperationsView.m
//  assistant
//
//  Created by qunai on 2023/12/29.
//

#import "QWMeetOperationsView.h"
#import "QWMeetOperationsNavView.h"
#import "QWMeetOperationsTabView.h"
@interface QWMeetOperationsView ()
@property (nonatomic, strong) QWMeetOperationsNavView *navView;
@property (nonatomic, strong) QWMeetOperationsTabView *tabView;
@property (nonatomic) NSTimer *timeTimer;
@end

@implementation QWMeetOperationsView
- (instancetype)init{
    self = [super init];
    if(self){
        self.backgroundColor = [UIColor clearColor];

        [self navView];
        [self tabView];
        
        [self timeTimer];
    }
    return self;
}
#pragma mark - 顶部操作View
- (QWMeetOperationsNavView *)navView{
    if(!_navView){
        _navView = [[QWMeetOperationsNavView alloc]init];
        _navView.isOpenLoudspeaker = self.localUserInfoModel.isOpenLoudspeaker;
        [self addSubview:_navView];
        [_navView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.top.equalTo(self);
            make.right.equalTo(self);
            make.height.mas_equalTo([UIDevice navigationFullHeight]);
        }];
        
        __weak __typeof(self) weakSelf = self;
        _navView.endBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf endMeet];
        };
        
        _navView.loudspeakerBlock = ^(BOOL isOpen) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf changeLoudspeaker:isOpen];
        };
        
        _navView.openMeetingInfoBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf openShowMeetingBaseInfo];
        };
    }
    return _navView;
}

#pragma mark - 底部操作View
- (QWMeetOperationsTabView *)tabView{
    if(!_tabView){
        _tabView = [[QWMeetOperationsTabView alloc]init];
        _tabView.isOpenMicrophone = self.localUserInfoModel.isOpenMicrophone;
        _tabView.isOpenVideo = self.localUserInfoModel.isOpenVideo;
        _tabView.isOpenScreenShare = self.isOpenScreenShare;
        [self addSubview:_tabView];
        [_tabView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.bottom.equalTo(self);
        }];
        
        __weak __typeof(self) weakSelf = self;
        _tabView.microphoneBlock = ^(BOOL isOpen) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf changeMicrophone:isOpen];
        };
        
        _tabView.videoBlock = ^(BOOL isOpen) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf changeVideo:isOpen];
        };
        
        _tabView.shareScreenOpenBlock = ^(BOOL isOpen) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if(strongSelf.meetOperationsBlock)strongSelf.meetOperationsBlock(isOpen?MeetOperationType_shareScreen_open:MeetOperationType_shareScreen_close);
        };
    }
    return _tabView;
}

- (void)setLocalUserInfoModel:(QWAgoraUserInfoModel *)localUserInfoModel{
    _localUserInfoModel = localUserInfoModel;
    
    _navView.isOpenLoudspeaker = _localUserInfoModel.isOpenLoudspeaker;

    _tabView.isOpenMicrophone = _localUserInfoModel.isOpenMicrophone;
    _tabView.isOpenVideo = _localUserInfoModel.isOpenVideo;
}
- (void)setIsOpenScreenShare:(BOOL)isOpenScreenShare{
    _isOpenScreenShare = isOpenScreenShare;
    _tabView.isOpenScreenShare = self.isOpenScreenShare;
}

#pragma mark - 结束
- (void)endMeet{
    [[QWCommonMethod Instance] showAlert:@"是否确定离开会议？" message:@"" success:^(UIAlertAction * _Nonnull action) {
        if(self.meetOperationsBlock)self.meetOperationsBlock(MeetOperationType_end);
    } failure:nil];
    
}

#pragma mark - 开关扬声器
- (void)changeLoudspeaker:(BOOL)isOpen{
    self.localUserInfoModel.isOpenLoudspeaker = isOpen;
    [self.agoraKit setEnableSpeakerphone:self.localUserInfoModel.isOpenLoudspeaker]; // 开启或关闭扬声器播放。

    if(self.meetOperationsBlock){
        self.meetOperationsBlock(isOpen?MeetOperationType_loudspeaker_open:MeetOperationType_loudspeaker_close);
    }
}
#pragma mark - 开关麦克风
- (void)changeMicrophone:(BOOL)isOpen{
    self.localUserInfoModel.isOpenMicrophone = isOpen;
    [self.agoraKit muteLocalAudioStream:!self.localUserInfoModel.isOpenMicrophone]; // 开启或关闭麦克风
//    if(isOpen){
//        [self.agoraKit enableAudio];
//    }else{
//        [self.agoraKit disableAudio];
//    }
    
    if(self.meetOperationsBlock){
        self.meetOperationsBlock(isOpen?MeetOperationType_microphone_open:MeetOperationType_microphone_close);
    }
}

#pragma mark - 开关视频
- (void)changeVideo:(BOOL)isOpen{
    self.localUserInfoModel.isOpenVideo = isOpen;
    [self.agoraKit muteLocalVideoStream:!self.localUserInfoModel.isOpenVideo]; // 开启或关闭视频
//    if(isOpen){
//        [self.agoraKit enableVideo];
//    }else{
//        [self.agoraKit disableVideo];
//    }
    if(self.meetOperationsBlock){
        self.meetOperationsBlock(isOpen?MeetOperationType_video_open:MeetOperationType_video_close);
    }
}

#pragma mark - 打开会议基本信息
- (void)openShowMeetingBaseInfo{
    if(self.meetOperationsBlock){
        self.meetOperationsBlock(MeetOperationType_openShowMeetingInfo);
    }
}


//- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
//{
//    CGPoint point = [[touches  anyObject] locationInView:self];
//    CGPoint navView_point = [self.navView.layer convertPoint:point fromLayer:self.layer];
//    CGPoint tabView_point = [self.tabView.layer convertPoint:point fromLayer:self.layer];
//    if(![self.navView.layer containsPoint:navView_point] && ![self.tabView.layer containsPoint:tabView_point]){
//        [self close];
//    }
//}
//- (void)close{
//    if(self.meetOperationsBlock)self.meetOperationsBlock(MeetOperationType_hidden);
//}


#pragma mark - 定时器
- (NSTimer *)timeTimer{
    if(!_timeTimer){
        __block NSInteger num = 0;
        _timeTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
            num = num +1;
            //NSLog(@"timeTimer_num===%ld",num);
            if(num >= 7 && self.isShowView){
                self.isShowView = NO;
            }
        }];
    }
    return _timeTimer;
}
//暂停
- (void)pauseTimer{
    if(_timeTimer){
        [_timeTimer setFireDate:[NSDate distantFuture]];
    }
}

//开始
- (void)resumeTimer{
    if(_timeTimer){
        [_timeTimer setFireDate:[NSDate distantPast]];
    }
}
//删除
- (void)deleteTimer{
    if(_timeTimer){
        [_timeTimer invalidate];
        _timeTimer = nil;
    }
}
- (void)dealloc{
    [self deleteTimer];
}

#pragma mark - 显示/隐藏
- (void)setIsShowView:(BOOL)isShowView{
    _isShowView = isShowView;
    
    self.hidden = !_isShowView;
    if(_isShowView){
        [self timeTimer];
    }else{
        [self deleteTimer];
    }
}
@end
