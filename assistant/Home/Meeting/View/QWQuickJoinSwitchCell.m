//
//  QWQuickJoinSwitchCell.m
//  assistant
//
//  Created by qunai on 2023/12/29.
//

#import "QWQuickJoinSwitchCell.h"

@implementation QWQuickJoinSwitchCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self nameLabel];
        
        [self rightView];
        [self customSwitch];
    }
    return self;
}
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont systemFontOfSize:16];
        
        [self.contentView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            make.left.equalTo(self.contentView).offset(14.0);
            make.width.equalTo(self.contentView).multipliedBy(0.5);
            make.height.mas_equalTo(44);
        }];
        
    }
    return _nameLabel;
}

#pragma mark - 开关
- (UIView *)rightView{
    if(!_rightView){
        _rightView = [[UIView alloc]init];
        [self.contentView addSubview:_rightView];
        
        [_rightView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-14.0);
            make.top.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            make.width.mas_equalTo(50);
        }];
    }
    return _rightView;
}

- (UISwitch *)customSwitch{
    if(!_customSwitch){
        _customSwitch = [[UISwitch alloc]init];
        _customSwitch.transform = CGAffineTransformMakeScale(0.7, 0.7);
        _customSwitch.onTintColor = [UIColor blueColor];
        _customSwitch.thumbTintColor = [UIColor whiteColor];
        _customSwitch.on = NO;
        [_rightView addSubview:_customSwitch];
        
        [_customSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_rightView);
            make.centerY.equalTo(_rightView);
        }];
    }
    return _customSwitch;
}

@end
