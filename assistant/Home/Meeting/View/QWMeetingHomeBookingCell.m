//
//  QWMeetingHomeBookingCell.m
//  assistant
//
//  Created by qunai on 2024/1/18.
//

#import "QWMeetingHomeBookingCell.h"

@implementation QWMeetingHomeBookingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self rightArrow];

        [self nameLabel];
        [self timeLabel];
    }
    return self;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.numberOfLines = 0;
        _nameLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightRegular];
        
        [self.contentView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(4.0);
            make.left.equalTo(self.contentView).offset(14.0);
            make.right.equalTo(self.rightArrow.mas_left).offset(-4.0);
        }];
        
    }
    return _nameLabel;
}

- (UILabel *)timeLabel{
    if(!_timeLabel){
        _timeLabel = [[UILabel alloc]init];
        _timeLabel.textColor = [UIColor coolGrayColor];
        _timeLabel.font = [UIFont systemFontOfSize:12];
        
        [self.contentView addSubview:_timeLabel];
        
        [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.nameLabel.mas_bottom).offset(4.0);
            make.bottom.equalTo(self.contentView).offset(-4.0);
            make.left.equalTo(self.nameLabel.mas_left);
            make.right.equalTo(self.nameLabel.mas_right);
        }];
        
    }
    return _timeLabel;
}

- (UIImageView *)rightArrow{
    if(!_rightArrow){
        UIImageView *lineImageView = [[UIImageView alloc]init];
        lineImageView.backgroundColor = [UIColor colorFromHexString:@"#E9EBEF"];
        [self.contentView addSubview:lineImageView];
        [lineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(14.0);
            make.right.equalTo(self).offset(-14.0);
            make.bottom.equalTo(self);
            make.height.mas_equalTo(0.5);
        }];
        
        _rightArrow = [[UIImageView alloc]init];
        UIImage *image = [UIImage imageNamed:@"next_arrow.png"];
        _rightArrow.image = image;
        [self.contentView addSubview:_rightArrow];
        
        [_rightArrow mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-14.0);
            make.centerY.equalTo(self);
            make.width.mas_equalTo(image.size.width);
            make.height.mas_equalTo(image.size.height);
        }];
    }
    return _rightArrow;
}
@end
