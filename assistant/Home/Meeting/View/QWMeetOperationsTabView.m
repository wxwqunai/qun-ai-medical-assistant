//
//  QWMeetOperationsTabView.m
//  assistant
//
//  Created by qunai on 2023/12/19.
//

#import "QWMeetOperationsTabView.h"

@interface QWMeetOperationsTabView()
@property (nonatomic, strong) UIView *containView;

@property (nonatomic, strong) QWTabItemsView *microphoneView;
@property (nonatomic, strong) QWTabItemsView *videoView;
@property (nonatomic, strong) QWTabItemsView *shareScreenView;
@property (nonatomic, strong) QWTabItemsView *memberManagerView;
@property (nonatomic, strong) QWTabItemsView *moreSetView;

@end

@implementation QWMeetOperationsTabView

- (instancetype)init{
    self = [super init];
    if(self){
        self.backgroundColor = [[UIColor colorFromHexString:@"#202022"] colorWithAlphaComponent:0.9];
        [self containView];
        
        [self microphoneView];
        [self videoView];
        [self shareScreenView];
        [self memberManagerView];
        [self moreSetView];
    }
    return self;
}

- (UIView *)containView{
    if(!_containView){
        _containView = [[UIView alloc]init];
        [self addSubview:_containView];
        [_containView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.top.equalTo(self).offset(5);
            make.bottom.equalTo(self).offset(-[UIDevice safeDistanceBottom]-5);
        }];
    }
    return _containView;
}

#pragma mark - 麦克风
- (QWTabItemsView *)microphoneView{
    if(!_microphoneView){
        _microphoneView = [[QWTabItemsView alloc]init];
        [_microphoneView.button addTarget:self action:@selector(microphoneOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.containView addSubview:_microphoneView];
        
        [_microphoneView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.containView);
            make.bottom.equalTo(self.containView);
            make.left.equalTo(self.containView);
            make.width.equalTo(self.containView.mas_width).multipliedBy(1.0/5.0);
        }];
        
    }
    return _microphoneView;
}
- (void)microphoneOnClick{
    self.isOpenMicrophone = !self.isOpenMicrophone;
    if(self.microphoneBlock)self.microphoneBlock(self.isOpenMicrophone);
}
- (void)setIsOpenMicrophone:(BOOL)isOpenMicrophone{
    _isOpenMicrophone = isOpenMicrophone;
    
    self.microphoneView.nameStr = _isOpenMicrophone?@"静音":@"解除静音";
    self.microphoneView.imageStr = _isOpenMicrophone?@"m_microphone":@"m_microphone_close";
}

#pragma mark - 视频
- (QWTabItemsView *)videoView{
    if(!_videoView){
        _videoView = [[QWTabItemsView alloc]init];
        [_videoView.button addTarget:self action:@selector(videoViewOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.containView addSubview:_videoView];
        
        [_videoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.containView);
            make.bottom.equalTo(self.containView);
            make.left.equalTo(self.microphoneView.mas_right);
            make.width.equalTo(self.containView.mas_width).multipliedBy(1.0/5.0);
        }];
        
    }
    return _videoView;
}

- (void)videoViewOnClick{
    self.isOpenVideo = !self.isOpenVideo;
    if(self.videoBlock)self.videoBlock(self.isOpenVideo);
}
- (void)setIsOpenVideo:(BOOL)isOpenVideo{
    _isOpenVideo = isOpenVideo;
    self.videoView.nameStr = _isOpenVideo?@"停止视频":@"打开视频";
    self.videoView.imageStr = _isOpenVideo?@"m_video":@"m_video_close";
}

#pragma mark - 共享屏幕
- (QWTabItemsView *)shareScreenView{
    if(!_shareScreenView){
        _shareScreenView = [[QWTabItemsView alloc]init];
        _shareScreenView.imageStr = self.isOpenScreenShare?@"m_sharedScreen_close":@"m_sharedScreen";
        _shareScreenView.nameStr = self.isOpenScreenShare?@"停止共享":@"共享屏幕";
        [_shareScreenView.button addTarget:self action:@selector(shareScreenOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.containView addSubview:_shareScreenView];
        
        [_shareScreenView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.containView);
            make.bottom.equalTo(self.containView);
            make.left.equalTo(self.videoView.mas_right);
            make.width.equalTo(self.containView.mas_width).multipliedBy(1.0/5.0);
        }];
        
    }
    return _shareScreenView;
}
- (void)shareScreenOnClick{
    if(self.shareScreenOpenBlock)self.shareScreenOpenBlock(!self.isOpenScreenShare);
}
- (void)setIsOpenScreenShare:(BOOL)isOpenScreenShare{
    _isOpenScreenShare = isOpenScreenShare;
    _shareScreenView.imageStr = _isOpenScreenShare?@"m_sharedScreen_close":@"m_sharedScreen";
    _shareScreenView.nameStr = _isOpenScreenShare?@"停止共享":@"共享屏幕";
}

#pragma mark - 成员管理
- (QWTabItemsView *)memberManagerView{
    if(!_memberManagerView){
        _memberManagerView = [[QWTabItemsView alloc]init];
        _memberManagerView.imageStr = @"m_member_manager";
        _memberManagerView.nameStr = @"成员管理";
        [_memberManagerView.button addTarget:self action:@selector(memberManagerOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.containView addSubview:_memberManagerView];
        
        [_memberManagerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.containView);
            make.bottom.equalTo(self.containView);
            make.left.equalTo(self.shareScreenView.mas_right);
            make.width.equalTo(self.containView.mas_width).multipliedBy(1.0/5.0);
        }];
        
    }
    return _memberManagerView;
}
- (void)memberManagerOnClick{
    [SWHUDUtil hideHudViewWithMessageSuperView:self.superview withMessage:@"开发中，敬请期待"];
}
#pragma mark - 更多设置
- (QWTabItemsView *)moreSetView{
    if(!_moreSetView){
        _moreSetView = [[QWTabItemsView alloc]init];
        _moreSetView.imageStr = @"m_set_more";
        _moreSetView.nameStr = @"更多";
        [_moreSetView.button addTarget:self action:@selector(moreSetOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.containView addSubview:_moreSetView];
        
        [_moreSetView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.containView);
            make.bottom.equalTo(self.containView);
            make.left.equalTo(self.memberManagerView.mas_right);
            make.right.equalTo(self.containView);
        }];
        
    }
    return _moreSetView;
}
- (void)moreSetOnClick{
    [SWHUDUtil hideHudViewWithMessageSuperView:self.superview withMessage:@"开发中，敬请期待"];
}


@end




@implementation QWTabItemsView

- (instancetype)init{
    self = [super init];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        [self imageView];
        [self nameLabel];
        [self button];
        
    }
    return self;
}

- (UIImageView *)imageView{
    if(!_imageView){
        _imageView = [[UIImageView alloc]init];
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_imageView];
        [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.height.mas_equalTo(30);
        }];
    }
    return _imageView;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.textColor = [UIColor whiteColor];
        _nameLabel.font =[UIFont systemFontOfSize:14];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.imageView.mas_bottom).offset(2);
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.bottom.equalTo(self);
        }];
    }
    return _nameLabel;
}
- (UIButton *)button{
    if(!_button){
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:_button];
        [_button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return _button;
}

- (void)setImageStr:(NSString *)imageStr{
    _imageStr = imageStr;
    self.imageView.image = [UIImage imageNamed:imageStr];
}

- (void)setNameStr:(NSString *)nameStr{
    _nameStr = nameStr;
    self.nameLabel.text = nameStr;
}

@end
