//
//  QWMeetVideoScrollView.h
//  assistant
//
//  Created by qunai on 2023/12/15.
//

#import <UIKit/UIKit.h>
#import "QWTwoChannelView.h"
#import "QWMultiChannelView.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWMeetVideoScrollView : UIView
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) QWTwoChannelView *twoChannelView;
@property (nonatomic, strong) QWMultiChannelView *multiChannelView;

@property (nonatomic, strong)AgoraRtcEngineKit *agoraKit;
@property (nonatomic, strong) QWAgoraUserInfoModel * localUserInfoModel;
@property (nonatomic, strong) NSMutableArray *remoteUidArr; //除自己以外，参会人

//设置本地视频
- (void)setupLocalVideo;

//多人参会scroll状态改变
- (void)changeScrollState;

//新增参会人
- (void)addRemoteUid:(NSUInteger)uid;

//参会人离开
- (void)deleteRemoteUid:(NSUInteger)uid;

//远端用户当前活跃 - 音量最大者
- (void)activeSpeaker:(NSUInteger)uid;

//远程音频状态改变-开启/关闭
- (void)changeAudioRemoteState:(BOOL)isOpne withUid:(NSUInteger)uid;

//远程视频状态改变-开启/关闭
- (void)changeVideoRemoteState:(BOOL)isOpne withUid:(NSUInteger)uid;
@end

NS_ASSUME_NONNULL_END
