//
//  QWMeetingHomeHeaderView.h
//  assistant
//
//  Created by qunai on 2024/1/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWMeetingHomeHeaderView : UITableViewHeaderFooterView
@property (nonatomic, strong) UILabel *nameLabel;
@end

NS_ASSUME_NONNULL_END
