//
//  QWTwoChannelView.m
//  assistant
//
//  Created by qunai on 2023/12/14.
//

#import "QWTwoChannelView.h"

@implementation QWTwoChannelView

- (instancetype)init{
    self = [super init];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeCurrentLargeAndSmallScreenTap:)];
        tap.delegate = self;
        [self addGestureRecognizer:tap];
        
        [self remoteView];
        [self localView];
    }
    return self;
}

#pragma mark - 本地视频
- (QWVideoView *)localView{
    if(!_localView){
        _localView = [[QWVideoView alloc]init];
        _localView.backgroundColor = [UIColor clearColor];
        [self addSubview:_localView];
        [_localView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return _localView;
}
- (void)changeLocalViewFrameToSmall:(BOOL)isSmall{
    if(isSmall){
        _localView.isCurrentSmallScreen = YES;
        [_localView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset([UIDevice safeDistanceTop]);
            make.right.equalTo(self).offset(-8.0);
            make.width.equalTo(self).multipliedBy(0.3);
            make.height.equalTo(self).multipliedBy(0.25);
        }];
        
        _remoteView.isCurrentSmallScreen = NO;
        [_remoteView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
        [self sendSubviewToBack:_remoteView];
    }else{
        _localView.isCurrentSmallScreen = NO;
        [_localView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
        _remoteView.isCurrentSmallScreen = YES;
        [_remoteView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset([UIDevice safeDistanceTop]);
            make.right.equalTo(self).offset(-8.0);
            make.width.equalTo(self).multipliedBy(0.3);
            make.height.equalTo(self).multipliedBy(0.25);
        }];
        
        [self sendSubviewToBack:_localView];
    }
}


#pragma mark - 远程视频
- (QWVideoView *)remoteView{
    if(!_remoteView){
        _remoteView = [[QWVideoView alloc]init];
        _remoteView.backgroundColor = [UIColor clearColor];
        [self addSubview:_remoteView];
        [_remoteView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return _remoteView;
}

#pragma mark -UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    //本地视频为小屏幕
    if ([touch.view isDescendantOfView:self.localView] && self.localView.isCurrentSmallScreen && self.localView.userInfoModel) {
        return YES;
    }
    //远端视频为小屏幕
    if ([touch.view isDescendantOfView:self.remoteView] && self.remoteView.isCurrentSmallScreen && self.remoteView.userInfoModel) {
        return YES;
    }
    return NO;
}

#pragma mark - 切换当前聊天人的大小屏
- (void)changeCurrentLargeAndSmallScreenTap:(UIGestureRecognizer *)ges{
    [self changeLocalViewFrameToSmall:!self.localView.isCurrentSmallScreen];
}

@end
