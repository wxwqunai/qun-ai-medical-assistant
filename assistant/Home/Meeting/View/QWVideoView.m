//
//  QWVideoView.m
//  assistant
//
//  Created by kevin on 2023/12/13.
//

#import "QWVideoView.h"
@interface QWVideoView()
@property (nonatomic, strong) QWVideoShowView *videwShowView;
@property (nonatomic, strong) QWAideoShowView *aideoShowView;
@end
@implementation QWVideoView

- (instancetype)init{
    self = [super init];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        [self aideoShowView];
        [self videwShowView];
    }
    return self;
}
- (QWAideoShowView *)aideoShowView{
    if(!_aideoShowView){
        _aideoShowView = [[QWAideoShowView alloc]init];
        _aideoShowView.hidden = YES;
        [self addSubview:_aideoShowView];
        [_aideoShowView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return _aideoShowView;
}

- (QWVideoShowView *)videwShowView{
    if(!_videwShowView){
        _videwShowView = [[QWVideoShowView alloc]init];
        [self addSubview:_videwShowView];
        [_videwShowView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return _videwShowView;
}



#pragma mark - 改变
- (void)setUserInfoModel:(QWAgoraUserInfoModel *)userInfoModel{
    _userInfoModel = userInfoModel;

    self.isOpenVideo = _userInfoModel.isOpenVideo;
    self.isOpenAideo = _userInfoModel.isOpenMicrophone;
    self.userNameStr = _userInfoModel.meet_name;
    if(self.uid != 0) self.uid = _userInfoModel.uid;
    self.userHeaderPicUrl = _userInfoModel.user_header_url;
}

- (void)setIsOpenVideo:(BOOL)isOpenVideo{
    _isOpenVideo = isOpenVideo;
    self.videwShowView.hidden = !_isOpenVideo;
    self.aideoShowView.hidden = _isOpenVideo;
}

- (void)setIsOpenAideo:(BOOL)isOpenAideo{
    _isOpenAideo = isOpenAideo;

    self.videwShowView.microphoneImageView.image = [UIImage imageNamed:_isOpenAideo?@"m_microphone":@"m_microphone_close"];
    self.aideoShowView.microphoneImageView.image = [UIImage imageNamed:_isOpenAideo?@"m_microphone":@"m_microphone_close"];
}

- (void)setUserNameStr:(NSString *)userNameStr{
    _userNameStr = userNameStr;
    
    self.videwShowView.nameLabel.text = _userNameStr;
    self.aideoShowView.nameLabel.text = _userNameStr;
    
    self.videwShowView.baseInfoView.hidden = NO;
}

- (void)setUserHeaderPicUrl:(NSString *)userHeaderPicUrl{
    _userHeaderPicUrl = userHeaderPicUrl;
    [self.aideoShowView.userHeaderImageView sd_setImageWithURL:[NSURL URLWithString:_userHeaderPicUrl] placeholderImage:[UIImage imageNamed:@"m_user_header_default"]];
}

- (void)setUid:(NSUInteger)uid{
    _uid = uid;
    
    AgoraRtcVideoCanvas *videoCanvas = [[AgoraRtcVideoCanvas alloc]init];
    self.videoCanvas = videoCanvas;
    videoCanvas.view = self.videwShowView.videoView;
    videoCanvas.uid = _uid;
    videoCanvas.renderMode = AgoraVideoRenderModeHidden;
}

@end

#pragma mark - 视频展示View
@implementation QWVideoShowView
- (instancetype)init{
    self = [super init];
    if(self){
        [self videoView];
        
        [self baseInfoView];
        [self microphoneImageView];
        [self nameLabel];
    }
    return self;
}

#pragma mark - 视频view
- (UIView *)videoView{
    if(!_videoView){
        _videoView = [[UIView alloc]init];
        [self addSubview:_videoView];
        [_videoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return _videoView;
}
#pragma mark - 用户信息
- (UIView *)baseInfoView{
    if(!_baseInfoView){
        _baseInfoView = [[UIView alloc]init];
        _baseInfoView.hidden = YES;
        _baseInfoView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
        [self addSubview:_baseInfoView];
        [_baseInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.bottom.equalTo(self);
            make.height.mas_equalTo(25);
        }];
    }
    return _baseInfoView;
}

//麦克风
- (UIImageView *)microphoneImageView{
    if(!_microphoneImageView){
        _microphoneImageView = [[UIImageView alloc]init];
        _microphoneImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.baseInfoView addSubview:_microphoneImageView];
        [_microphoneImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.baseInfoView).offset(4.0);
            make.top.equalTo(self.baseInfoView).offset(2.0);
            make.bottom.equalTo(self.baseInfoView).offset(-2.0);
        }];
    }
    return _microphoneImageView;
}
//用户名
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont systemFontOfSize:14];
        _nameLabel.textColor = [UIColor whiteColor];
        [self.baseInfoView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.microphoneImageView.mas_right).offset(2.0);
            make.right.equalTo(self.baseInfoView).offset(-4.0);
            make.top.equalTo(self.baseInfoView);
            make.bottom.equalTo(self.baseInfoView);
        }];
    }
    return _nameLabel;
}

@end


#pragma mark - 音频展示View
@implementation QWAideoShowView
- (instancetype)init{
    self = [super init];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        [self userHeaderImageView];
        [self nameLabel];
        
        [self microphoneImageView];
    }
    return self;
}



-  (void)layoutSubviews{
    [super layoutSubviews];
    _userHeaderImageView.layer.cornerRadius = CGRectGetWidth(_userHeaderImageView.frame)/2.0;
    _userHeaderImageView.layer.masksToBounds = YES;
}

//用户头像
- (UIImageView *)userHeaderImageView{
    if(!_userHeaderImageView){
        _userHeaderImageView = [[UIImageView alloc]init];
        _userHeaderImageView.contentMode = UIViewContentModeScaleAspectFit;
        _userHeaderImageView.image = [UIImage imageNamed:@"m_user_header_default"];
        [self addSubview:_userHeaderImageView];
        [_userHeaderImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.centerY.equalTo(self);
            make.width.equalTo(self).multipliedBy(1.0/2.0);
            make.height.equalTo(_userHeaderImageView.mas_width);
        }];
    }
    return _userHeaderImageView;
}

//麦克风
- (UIImageView *)microphoneImageView{
    if(!_microphoneImageView){
        _microphoneImageView = [[UIImageView alloc]init];
        _microphoneImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_microphoneImageView];
        [_microphoneImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.userHeaderImageView);
            make.right.equalTo(self.userHeaderImageView);
            make.centerY.equalTo(self.userHeaderImageView.mas_bottom);
        }];
    }
    return _microphoneImageView;
}
//用户名
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont systemFontOfSize:14];
        _nameLabel.textColor = [UIColor whiteColor];
        _nameLabel.textAlignment =NSTextAlignmentCenter;
        [_userHeaderImageView addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_userHeaderImageView).offset(4.0);
            make.right.equalTo(_userHeaderImageView).offset(-4.0);
            make.centerY.equalTo(_userHeaderImageView);
        }];
    }
    return _nameLabel;
}

@end
