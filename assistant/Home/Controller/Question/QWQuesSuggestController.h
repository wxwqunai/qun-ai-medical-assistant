//
//  QWQuesSuggestController.h
//  assistant
//
//  Created by qunai on 2023/8/23.
//

#import "QWBaseController.h"
#import "QWQuestModel.h"
NS_ASSUME_NONNULL_BEGIN

typedef void (^MakeCommitBlock)(void);

@interface QWQuesSuggestController : QWBaseController
@property (nonatomic, strong) QWQuesAianswerListModel *answerModel;
@property (nonatomic, copy) MakeCommitBlock makeCommitBlock;
@end

NS_ASSUME_NONNULL_END
