//
//  QWQuesHistoryController.m
//  assistant
//
//  Created by qunai on 2023/7/21.
//

#import "QWQuesHistoryController.h"
#import "QWHistoryCell.h"
#import "QWQuestModel.h"
#import "QWQuesResultController.h"
#import "QWQuesAnswerDetailController.h"
#import "QWQuesSuggestController.h"

@interface QWQuesHistoryController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showDataArr;

@end

@implementation QWQuesHistoryController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self loadData:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"诊断历史";
    
    [self tableView];
    [self loadData:YES];
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
//        [_tableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
//        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
        [_tableView registerClass:[QWHistoryCell class] forCellReuseIdentifier:@"QWHistoryCell"];

        _tableView.backgroundColor=[UIColor whiteColor];
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
//        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
//        _tableView.estimatedSectionHeaderHeight = 100;
//        _tableView.estimatedSectionFooterHeight = 100;
//        _tableView.sectionHeaderHeight = 50;

//        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
//        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0000001;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.showDataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QWHistoryCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWHistoryCell"];
    QWQuesAianswerListModel *model = self.showDataArr[indexPath.row];
    if(self.isHiddenGive) [cell hidGiveSuggestBtn];
    cell.model =model;
    cell.checkSuggestBlock = ^{
        [self jumpToResultPage:model];
    };
    cell.giveSuggestBlock = ^{
        [self jumpToInputPage:model];
    };
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    QWQuesAianswerListModel *model = self.showDataArr[indexPath.row];
    QWQuesAnswerDetailController *detaiVC = [[QWQuesAnswerDetailController alloc]init];
    detaiVC.answer_id = model.id;
    [self.navigationController pushViewController:detaiVC animated:YES];
}
#pragma mark - 结果页面-跳转
- (void)jumpToResultPage:(QWQuesAianswerListModel *)model{
    QWQuesResultController *resultVC = [[QWQuesResultController alloc]init];
    resultVC.aianswerListModel = model;
    resultVC.isHiddenGive = self.isHiddenGive;
    [self.navigationController pushViewController:resultVC animated:YES];
}


#pragma mark - 建议页面-跳转
- (void)jumpToInputPage:(QWQuesAianswerListModel *)model{
    QWQuesSuggestController *vc = [[QWQuesSuggestController alloc]init];
    vc.answerModel = model;
    [self.navigationController pushViewController:vc animated:YES];
    
    vc.makeCommitBlock = ^{
        [self loadData:YES];
    };
}

#pragma mark - 加载数据
- (void)loadData:(BOOL)isLoading{
    if(isLoading) [SWHUDUtil showHudViewInSuperView:self.view withMessage:@"请等待"];
    HXGetRequest * request1 = [[HXGetRequest alloc] initWithRequestUrl:URL_aianswer_LIST argument:@{}];
    [request1 startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        if(isLoading)[SWHUDUtil hideHudView];
        NSArray *resultArr = (NSArray *)result;
        self.showDataArr = [QWQuesAianswerListModel mj_objectArrayWithKeyValuesArray:resultArr];
        [self.tableView reloadData];
        NSLog(@"");
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        if(isLoading)[SWHUDUtil hideHudView];
        //错误
        NSLog(@"%@",errorInfo);
    }];
}

@end
