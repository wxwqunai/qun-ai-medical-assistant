//
//  QWHealthQuestionController.m
//  assistant
//
//  Created by qunai on 2023/7/13.
//

#import "QWHealthQuestionController.h"
#import "QWQuestionCell.h"
#import "QWQuestModel.h"
#import "QWQuestionChangeBtnView.h"
#import "QWQuestHomeController.h"

@interface QWHealthQuestionController ()<UICollectionViewDelegate, UICollectionViewDataSource,QWOptionSelectDelegate>
@property (strong, nonatomic) UICollectionViewFlowLayout *flowLayout;
@property (strong, nonatomic) UICollectionView *collectionView;
@property (nonatomic,strong) NSMutableArray *showInfoArr;
@property (nonatomic, strong) QWQuestionChangeBtnView *bottomChangeView;
@property (nonatomic, strong) NSIndexPath *cur_indexPath;

@property (nonatomic, strong) QWQuesAianswerBeginModel *beginModel;
@end

@implementation QWHealthQuestionController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initUI];
    [self loadData];
}
-(void) initUI{
    self.title = @"智能诊断";
    [self bottomChangeView];
    [self flowLayout];
    [self collectionView];
}
#pragma mark-导航条左边按钮
-(void)navLeftBaseButtonClick
{
    [self diagnosticRulesGetConclusion:NO];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 对集合视图进行布局
-(UICollectionViewFlowLayout *)flowLayout
{
    if (!_flowLayout) {
        // 创建集合视图布局对象
        _flowLayout = [[UICollectionViewFlowLayout alloc] init];
        // 设置最小的左右间距
        _flowLayout.minimumInteritemSpacing = 0.0;
        // 设置最小的行间距（上下）
        _flowLayout.minimumLineSpacing = 0.0;
        _flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        // 设置边界范围
        _flowLayout.sectionInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
        _flowLayout.headerReferenceSize =CGSizeMake(0.0, 0.0);
        _flowLayout.footerReferenceSize =CGSizeMake(0.0, 0.0);
    }
    return _flowLayout;
}
-(UICollectionView *)collectionView
{
    if (!_collectionView) {
        _collectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) collectionViewLayout:self.flowLayout];
        _collectionView.showsVerticalScrollIndicator=NO;
        _collectionView.showsHorizontalScrollIndicator=NO;
        _collectionView.backgroundColor=Color_TableView_Gray;
        
        [self.view addSubview:_collectionView];
        
        // 创建集合视图对象
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.scrollEnabled = YES;
        _collectionView.alwaysBounceVertical=NO;
        _collectionView.alwaysBounceHorizontal=YES;
        [_collectionView setPagingEnabled:YES];
        _collectionView.scrollEnabled = NO;
        
        // 注册
//        [_collectionView registerClass:[QWComplaintHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"QWComplaintHeaderView"];
//        [_collectionView registerClass:[QWComplaintFooterView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"QWComplaintFooterView"];
        [_collectionView registerClass:[QWQuestionCell class] forCellWithReuseIdentifier:@"QWQuestionCell"];
        [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.equalTo(self.view);
            make.bottom.equalTo(self.bottomChangeView.mas_top);
        }];
    }
    return _collectionView;
}
#pragma mark - UICollectionViewDelegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize showSize=CGSizeMake(CGRectGetWidth(collectionView.frame), CGRectGetHeight(collectionView.frame));
    return showSize;
}


#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.showInfoArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    QWQuestionCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"QWQuestionCell" forIndexPath:indexPath];
    cell.indexPath = indexPath;
    cell.delegate =self;
    QWQuesAitimuListModel *model = self.showInfoArr[indexPath.row];
    cell.questModel = model;
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    self.cur_indexPath = indexPath;
    [self.bottomChangeView changeLastStepBtnState:indexPath.row>0];
    [self.bottomChangeView changeConfirmBtnState:NO];
}
-(void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
}

#pragma mark - QWOptionSelectDelegate
- (void)optionSelectBtnDidPress:(QWOptionModel *)optionModel cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    QWQuesAitimuListModel *model = self.showInfoArr[indexPath.row];
    [self aianswerdetailSave:model withSel:optionModel withIndex:indexPath];
}

#pragma mark - 切换题目
- (void)changeQuestion:(NSInteger)row{
    if(row<0 || row>=self.showInfoArr.count) return;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:(UICollectionViewScrollPositionCenteredHorizontally) animated:YES];
//        });
    
    [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:(UICollectionViewScrollPositionCenteredHorizontally) animated:YES];
}

#pragma mark - 上一题、提交
- (QWQuestionChangeBtnView *)bottomChangeView{
    if(!_bottomChangeView){
        _bottomChangeView = [[QWQuestionChangeBtnView alloc]init];
        _bottomChangeView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_bottomChangeView];
        [_bottomChangeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.view);
            make.bottom.equalTo(self.view);
            make.height.mas_offset([UIDevice tabBarFullHeight]);
        }];

        __weak __typeof(self) weakSelf = self;
        _bottomChangeView.changeBtnClickBlock = ^(QWBtnType type) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
        
            //上一题
            if(type == QWBtnType_last){
                [strongSelf changeQuestion:strongSelf.cur_indexPath.row-1];
            }
            
            //完成/提交
            if(type == QWBtnType_confirm){
                QWQuestionCell *cell= (QWQuestionCell *)[strongSelf.collectionView cellForItemAtIndexPath:strongSelf.cur_indexPath];
                if([cell checkOptionModelForSel]){
                    [strongSelf diagnosticRulesGetConclusion:YES];
                }else{
                    [SWHUDUtil hideHudViewWithMessageSuperView:strongSelf.view withMessage:@"请完成作答！"];
                }
            }
        };
    }
    return _bottomChangeView;
}

#pragma mark - 开始答辩
- (void)loadData{
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_aianswer_start argument:@{}];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        NSString *message = result[@"message"];
        if([message isEqualToString:@"OK"]){
            self.beginModel = [QWQuesAianswerBeginModel mj_objectWithKeyValues:result];
            [self loadAitimuList];
        }

    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        //错误
        NSLog(@"%@",errorInfo);
    }];
}

#pragma mark - 所有题目
- (void)loadAitimuList{
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_aitimu_list argument:@{}];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        NSArray *resultArr = (NSArray *)result;
        self.showInfoArr = [QWQuesAitimuListModel mj_objectArrayWithKeyValuesArray:resultArr];
        [self.collectionView reloadData];
        NSLog(@"");
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        //错误
        NSLog(@"%@",errorInfo);
    }];
}

#pragma mark - 提交每一题
- (void)aianswerdetailSave:(QWQuesAitimuListModel*)model withSel:(QWOptionModel *)optionModel withIndex:(NSIndexPath *)indexPath{
    if(self.beginModel == nil) return;
    NSDictionary *dic = @{
        @"answer_id":self.beginModel.answerId,
        @"q_id":model.q_id,
        @"q_title":model.q_title,
        @"q_type":model.q_type,
        @"answer_select":optionModel.num,
        @"answer_select_text":optionModel.content
    };

    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_aianswerdetail_save argument:dic];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        NSString *message = result[@"message"];
        if([message isEqualToString:@"OK"]){
            [self changeQuestion:indexPath.row+1];
            
            if(indexPath.row == self.showInfoArr.count-1){
                [self.bottomChangeView changeConfirmBtnState:YES];
            }
        }
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        //错误
        NSLog(@"%@",errorInfo);
    }];
}

#pragma mark -获取机器诊断和专家专家的结论
- (void)diagnosticRulesGetConclusion:(BOOL)isTips{
    if(self.beginModel == nil) return;
    NSDictionary *dic =@{
        @"id": !IsStringEmpty(self.beginModel.answerId)?self.beginModel.answerId:@"",
    };
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_AiDiagnosticRules_getConclusion argument:dic];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        if(isTips){
            [SWHUDUtil hideHudViewWithMessageSuperView:self.view withMessage:@"提交成功,请前往诊断历史中查看结果!"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                for (UIViewController *controller in self.navigationController.viewControllers) {
                        if ([controller isKindOfClass:[QWQuestHomeController class]]) {
                            QWQuestHomeController *quesHomeVC =(QWQuestHomeController *)controller;
                            [self.navigationController popToViewController:quesHomeVC animated:YES];
                        }
                    }
            });
        }
        NSLog(@"");
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        //错误
        NSLog(@"%@",errorInfo);
    }];
}

@end
