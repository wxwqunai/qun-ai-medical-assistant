//
//  QWQuesHistoryController.h
//  assistant
//
//  Created by qunai on 2023/7/21.
//

#import "QWBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWQuesHistoryController : QWBaseController
@property (nonatomic, assign) BOOL isHiddenGive;
@end

NS_ASSUME_NONNULL_END
