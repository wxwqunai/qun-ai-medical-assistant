//
//  QWQuestHomeController.m
//  assistant
//
//  Created by qunai on 2023/7/18.
//

#import "QWQuestHomeController.h"
#import "QWQuestionHomeCell.h"
#import "QWQuesBaseInfoController.h"
#import "QWQuesHistoryController.h"
#import "QWQuesSettingController.h"
#import "QWQuestModel.h"
#import "QWHealthQuestionController.h"

@interface QWQuestHomeController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showDataArr;
@end

@implementation QWQuestHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title =@"智能诊断";
    
    [self tableView];
    [self loadData];
}
#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
//        [_tableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
//        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
        [_tableView registerClass:[QWQuestionHomeCell class] forCellReuseIdentifier:@"QWQuestionHomeCell"];

        _tableView.backgroundColor=[UIColor whiteColor];
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
//        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
//        _tableView.estimatedSectionHeaderHeight = 100;
//        _tableView.estimatedSectionFooterHeight = 100;
//        _tableView.sectionHeaderHeight = 50;

//        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
//        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.showDataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *arr =self.showDataArr[section];
    return arr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QWQuestionHomeCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWQuestionHomeCell"];
    NSArray *dataArr =self.showDataArr[indexPath.section];
    NSDictionary *dic = dataArr[indexPath.row];
    NSString *name = dic[@"name"];
    NSString *color = dic[@"color"];
    cell.nameLabel.text = name;
    cell.bgView.backgroundColor = [UIColor colorFromHexString:color];
    return cell;
//    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell"];
//    if(!cell){
//        cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"cell"];
//    }
//    cell.textLabel.text =[NSString stringWithFormat:@"cell：%ld",indexPath.row];
//    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.section == 0 ){
        if(indexPath.row == 0){
            QWQuesBaseInfoController *vc = [[QWQuesBaseInfoController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }
        
        if(indexPath.row == 1){
            QWQuesHistoryController *historyVC = [[QWQuesHistoryController alloc]init];
            historyVC.isHiddenGive = YES;
            [self.navigationController pushViewController:historyVC animated:YES];
        }
    }else{
        if(indexPath.row == 0){
            QWQuesHistoryController *historyVC = [[QWQuesHistoryController alloc]init];
            historyVC.isHiddenGive = NO;
            [self.navigationController pushViewController:historyVC animated:YES];
        }
        
        if(indexPath.row == 1){
            QWQuesSettingController *settingVC = [[QWQuesSettingController alloc]init];
            [self.navigationController pushViewController:settingVC animated:YES];
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    CGRect rect = [tableView rectForHeaderInSection:section];
    
    UIView *view = [[UIView alloc]initWithFrame:rect];
    view.backgroundColor = [UIColor clearColor];
    
    UILabel *label = [[UILabel alloc]init];
    label.backgroundColor = Color_Main_Green;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.center = CGPointMake(rect.size.width/2.0, rect.size.height/2.0);
    label.bounds = CGRectMake(0, 0, 80, 30);
    label.layer.cornerRadius = 6.0;
    label.layer.masksToBounds = YES;
    label.text = section ==0?@"患者":@"医生";
    [view addSubview:label];
    return view;
}
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
//
//}

#pragma mark - 加载数据
- (void)loadData{
    self.showDataArr = [NSMutableArray arrayWithArray:@[
        @[@{@"name":@"开始诊断",@"color":@"#99CCFF"},
          @{@"name":@"诊断历史",@"color":@"#FF9966"}],
        @[@{@"name":@"诊断列表",@"color":@"#87CEEB"},
          @{@"name":@"系统设置",@"color":@"#7B68EE"}]
    ]];
    [self.tableView reloadData];
}

@end
