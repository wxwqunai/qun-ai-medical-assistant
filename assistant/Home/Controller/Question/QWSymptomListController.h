//
//  QWSymptomListController.h
//  assistant
//
//  Created by qunai on 2023/7/19.
//

#import "QWBaseController.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, QWSymptomType) {
    QWSymptomType_doctor = 0,
    QWSymptomType_patient,
};

@interface QWSymptomListController : QWBaseController
@property (nonatomic, assign) QWSymptomType symptomType;
@end

NS_ASSUME_NONNULL_END
