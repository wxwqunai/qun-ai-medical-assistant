//
//  QWQuesSuggestController.m
//  assistant
//
//  Created by qunai on 2023/8/23.
//

#import "QWQuesSuggestController.h"
#import "QWButtonView.h"
#import "QWQuesSuggestView.h"

@interface QWQuesSuggestController ()<UITextViewDelegate,UIScrollViewDelegate>
@property (nonatomic,strong) UIScrollView *bgScroolView;
@property (nonatomic,strong) UIView *containerView;

@property (nonatomic,strong) UIView *cnterView;
@property (nonatomic,strong) UITextView *suggestOneView;
@property (nonatomic,strong) UILabel *placeholderOneLabel;

@property (nonatomic,strong) UITextView *suggestTwoView;
@property (nonatomic,strong) UILabel *placeholderTwoLabel;

@property (nonatomic,strong) QWQuesSuggestView *suggestsView;

@property (nonatomic,strong) QWButtonView *buttonView;

@end

@implementation QWQuesSuggestController

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"专家建议";
    
    [self bgScroolView];
    [self loadData];
}

-(UIScrollView *)bgScroolView
{
    if (!_bgScroolView) {
        _bgScroolView =[[UIScrollView alloc]init];
        _bgScroolView.alwaysBounceVertical=YES;
        _bgScroolView.backgroundColor =[UIColor whiteColor];
        _bgScroolView.delegate = self;
//        _bgScroolView.contentSize=CGSizeMake(0, kScreenHeight-[UIDevice statusBarHeight]);
        [self.view addSubview:_bgScroolView];
        
        [_bgScroolView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.equalTo(self.view);
            make.bottom.equalTo(self.view);
        }];
        
        _containerView = [[UIView alloc] init];
        _containerView.backgroundColor = [UIColor clearColor];
        [_bgScroolView addSubview:_containerView];
        [_containerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_bgScroolView);
            make.width.equalTo(_bgScroolView); // 需要设置宽度和scrollview宽度一样
        }];

        [self cnterView];
        [self buttonView];
        
        [_containerView mas_makeConstraints:^(MASConstraintMaker *make) {
             make.bottom.equalTo(_buttonView.mas_bottom).offset(20);// 这里放最后一个view的底部
         }];
    }
    return _bgScroolView;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    [self.view endEditing:YES];
}

#pragma mark - 输入
- (UIView *)cnterView{
    if(!_cnterView){
        _cnterView = [[UIView alloc]init];
        _cnterView.backgroundColor =[UIColor clearColor];
        [_containerView addSubview:_cnterView];
        
        [_cnterView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(18);
            make.right.mas_equalTo(-18);
            make.top.mas_equalTo(_containerView).offset(20.0);
            make.height.mas_greaterThanOrEqualTo(120.0);
        }];
        
        [self suggestOneView];
        [self suggestTwoView];
        [self suggestsView];
    }
    return _cnterView;
}

#pragma mark - 建议1
-(UITextView *)suggestOneView
{
    if (!_suggestOneView) {
        _suggestOneView =[[UITextView alloc]init];
        _suggestOneView.text = self.answerModel.rengong1;
        _suggestOneView.returnKeyType=UIReturnKeyDone;
        _suggestOneView.delegate=self;
        _suggestOneView.font=[UIFont fontWithName:@"PingFangSC-Medium" size:15];
        _suggestOneView.textColor=[UIColor colorFromHexString:@"#707070"];
        _suggestOneView.backgroundColor=[UIColor clearColor];
//        _importInfoView.scrollEnabled = NO;
        
        _suggestOneView.layer.borderWidth=1;
        _suggestOneView.layer.borderColor=Color_Main_Green.CGColor;
        
        [_cnterView addSubview:_suggestOneView];
        
        [_suggestOneView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_cnterView);
            make.right.equalTo(_cnterView);
            make.top.equalTo(_cnterView);
            make.height.mas_offset(120);
        }];
        
        _placeholderOneLabel =[[UILabel alloc]init];
        _placeholderOneLabel.text = @"请输入建议1";
        _placeholderOneLabel.hidden = !IsStringEmpty(self.answerModel.rengong1);
        _placeholderOneLabel.font = [UIFont systemFontOfSize:15];
        _placeholderOneLabel.textColor = [UIColor colorFromHexString:@"#CBCBCB"];
        [_suggestOneView addSubview:_placeholderOneLabel];
        
        [_placeholderOneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(4);
            make.top.mas_offset(8);
        }];
    }
    return _suggestOneView;
}

#pragma mark - 建议2
-(UITextView *)suggestTwoView
{
    if (!_suggestTwoView) {
        _suggestTwoView =[[UITextView alloc]init];
        _suggestTwoView.text = self.answerModel.rengong2;
        _suggestTwoView.returnKeyType=UIReturnKeyDone;
        _suggestTwoView.delegate=self;
        _suggestTwoView.font=[UIFont fontWithName:@"PingFangSC-Medium" size:15];
        _suggestTwoView.textColor=[UIColor colorFromHexString:@"#707070"];
        _suggestTwoView.backgroundColor=[UIColor clearColor];
//        _suggestTwoView.scrollEnabled = NO;
        
        _suggestTwoView.layer.borderWidth=1;
        _suggestTwoView.layer.borderColor=Color_Main_Green.CGColor;
        
        [_cnterView addSubview:_suggestTwoView];
        
        [_suggestTwoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_cnterView);
            make.right.equalTo(_cnterView);
            make.top.equalTo(_suggestOneView.mas_bottom).offset(10);
            make.height.mas_offset(120);
        }];
        
        _placeholderTwoLabel =[[UILabel alloc]init];
        _placeholderTwoLabel.text = @"请输入建议2";
        _placeholderTwoLabel.hidden = !IsStringEmpty(self.answerModel.rengong2);
        _placeholderTwoLabel.font = [UIFont systemFontOfSize:15];
        _placeholderTwoLabel.textColor = [UIColor colorFromHexString:@"#CBCBCB"];
        [_suggestTwoView addSubview:_placeholderTwoLabel];
        
        [_placeholderTwoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(4);
            make.top.mas_offset(8);
        }];
    }
    return _suggestTwoView;
}
#pragma mark- UITextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if(textView == _suggestOneView) _placeholderOneLabel.hidden = YES;
    if(textView == _suggestTwoView) _placeholderTwoLabel.hidden = YES;
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    if(textView == _suggestOneView) _placeholderOneLabel.hidden = !IsStringEmpty(textView.text);
    if(textView == _suggestTwoView) _placeholderTwoLabel.hidden = !IsStringEmpty(textView.text);
    return YES;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if( [ @"\n" isEqualToString: text]){
        //你的响应方法
        [self.view endEditing:YES];
        return NO;
    }
    
    if (range.length + range.location > textView.text.length) {
        return NO;
    }
    
    return YES;
}


#pragma mark - 建议-快捷
- (QWQuesSuggestView *)suggestsView{
    if(!_suggestsView){
        _suggestsView = [[QWQuesSuggestView alloc]init];
        [_containerView addSubview:_suggestsView];
        
        [_suggestsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_cnterView);
            make.right.equalTo(_cnterView);
            make.top.equalTo(_suggestTwoView.mas_bottom).offset(10);
            make.bottom.equalTo(_cnterView);
//            make.height.mas_offset(120);
        }];
    }
    return _suggestsView;
}



#pragma mark - 提交
- (QWButtonView*)buttonView{
    if(!_buttonView){
        _buttonView= [[QWButtonView alloc]init];
        [_buttonView changeButtonTitle:@"提交"];
        __weak __typeof(self) weakSelf = self;
        _buttonView.buttonBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            
            if(IsStringEmpty(strongSelf.suggestOneView.text)&& IsStringEmpty(strongSelf.suggestTwoView.text)){
                [SWHUDUtil hideHudViewWithMessageSuperView:strongSelf.view withMessage:@"请输入建议"];
                return;
            }
            
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            [dic setValue:strongSelf.answerModel.id forKey:@"id"];
            [dic setValue:strongSelf.suggestOneView.text forKey:@"rengong1"];
            [dic setValue:strongSelf.suggestTwoView.text forKey:@"rengong2"];

            HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_aianswer_start argument:dic];
            [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
                NSString *message = result[@"message"];
                if([message isEqualToString:@"OK"]){
                    if(strongSelf.makeCommitBlock)strongSelf.makeCommitBlock();
                    [strongSelf.navigationController popViewControllerAnimated:YES];
                }

            } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
                //错误
                NSLog(@"%@",errorInfo);
            }];
        };
        
        
        [_containerView addSubview:_buttonView];
        
        [_buttonView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.top.mas_equalTo(_cnterView.mas_bottom);
            make.height.mas_equalTo(95.0);
        }];
        
    }
    return _buttonView;
}


#pragma mark - 加载数据
- (void)loadData{
    NSString * filePath = [[NSBundle mainBundle] pathForResource:@"97种心脏病结论" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];//获取指定路径的data文件
    NSArray *suggestsArr = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    _suggestsView.dataArr = suggestsArr;
    NSLog(@"");
    
}



@end
