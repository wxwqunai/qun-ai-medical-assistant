//
//  QWQuesAnswerDetailController.h
//  assistant
//
//  Created by qunai on 2023/8/23.
//

#import "QWBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWQuesAnswerDetailController : QWBaseController
@property (nonatomic, copy) NSString *answer_id;
@end

NS_ASSUME_NONNULL_END
