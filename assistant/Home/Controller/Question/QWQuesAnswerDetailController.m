//
//  QWQuesAnswerDetailController.m
//  assistant
//
//  Created by qunai on 2023/8/23.
//

#import "QWQuesAnswerDetailController.h"
#import "QWQuestModel.h"
#import "QWOptionTitleView.h"
#import "QWOptionConfirmView.h"
#import "QWSingleOptionCell.h"
#import "QWInputOptionCell.h"
#import "QWQuestionAddController.h"
@interface QWQuesAnswerDetailController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showDataArr;//所有题目与选项
@property (nonatomic, strong) NSMutableArray *aianswerdArr;//用户作答选项
@end

@implementation QWQuesAnswerDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"答卷详情";

    [self tableView];
    [self loadData];
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        [_tableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
        [_tableView registerClass:[QWSingleOptionCell class] forCellReuseIdentifier:@"QWSingleOptionCell"];
        [_tableView registerClass:[QWInputOptionCell class] forCellReuseIdentifier:@"QWInputOptionCell"];
        
        _tableView.backgroundColor=[UIColor whiteColor];
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        //        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        //        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        //        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 0.0000001;
//}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.showDataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    QWQuesAitimuListModel *model = self.showDataArr[section];
    return model.options.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QWQuesAitimuListModel *model = self.showDataArr[indexPath.section];
    QWOptionModel *optionModel = model.options[indexPath.row];
    if([model.q_type isEqualToString:@"input"]){
        QWInputOptionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWInputOptionCell"];
        cell.optionModel = optionModel;
        return cell;
    }else{
        QWSingleOptionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWSingleOptionCell"];
        cell.nameStr = [NSString stringWithFormat:@"%@：%@",optionModel.num,optionModel.content];
        cell.isSeleted = optionModel.isSel;
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    QWOptionTitleView *titleView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"QWOptionTitleView"];
    QWQuesAitimuListModel *model = self.showDataArr[section];
    NSString * ptionTypeStr = [self reOptionType:model.q_type];
    NSString * contentStr = [NSString stringWithFormat:@"%ld.%@%@",section+1,model.q_title,ptionTypeStr];
    titleView.nameStr = contentStr;
    return titleView;
}

#pragma mark - 选项类型
- (NSString *)reOptionType:(NSString *)optionType{
    NSString *result = @"";
    if([optionType isEqualToString:@"select"]){
        result = @"(单选)";
    }else if([optionType isEqualToString:@"multiple"]){
        result = @"(多选)";
    }else if([optionType isEqualToString:@"single_input"]){
        result = @"(单选/填空)";
    }
    else if([optionType isEqualToString:@"input"]){
        result = @"(填空)";
    }
    return result;
}

#pragma mark - 加载数据
- (void)loadData{
    dispatch_group_t  group = dispatch_group_create();
    [self loadAitimuList:group];
    [self aianswerdetail:group];
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        
        
        //计算用户选择项
        [self.showDataArr enumerateObjectsUsingBlock:^(QWQuesAitimuListModel*  _Nonnull timuModel, NSUInteger idx, BOOL * _Nonnull stop) {
            [self.aianswerdArr enumerateObjectsUsingBlock:^(QWQuesAianswerdSavaModel*  _Nonnull answerdModel, NSUInteger idx, BOOL * _Nonnull stop) {
                if([timuModel.q_id isEqualToString:answerdModel.q_id]){
                    [timuModel.options enumerateObjectsUsingBlock:^(QWOptionModel * _Nonnull optionModel, NSUInteger idx, BOOL * _Nonnull stop) {
                        if([optionModel.content isEqualToString:answerdModel.answer_select_text]&&[optionModel.num isEqualToString:answerdModel.answer_select]){
                            optionModel.isSel = YES;
                        }
                    }];
                }
            }];
        }];
        
        [self.tableView reloadData];
     });
}
#pragma mark - 一次测试的所有答卷详情
- (void)aianswerdetail:(dispatch_group_t)group{
    if(IsStringEmpty(self.answer_id)) return;
    dispatch_group_enter(group);
    NSDictionary *dic = @{@"answer_id":self.answer_id};
    HXGetRequest * request1 = [[HXGetRequest alloc] initWithRequestUrl:URL_aianswerdetail_getOneUserPaper argument:dic];
    [request1 startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
       
        NSArray *resultArr = (NSArray *)result;
        self.aianswerdArr = [QWQuesAianswerdSavaModel mj_objectArrayWithKeyValuesArray:resultArr];
        dispatch_group_leave(group);
        NSLog(@"");
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        //错误
        dispatch_group_leave(group);
        NSLog(@"%@",errorInfo);
    }];
}

#pragma mark - 所有题目
- (void)loadAitimuList:(dispatch_group_t)group{
    dispatch_group_enter(group);
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_aitimu_list argument:@{}];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        NSArray *resultArr = (NSArray *)result;
        self.showDataArr = [QWQuesAitimuListModel mj_objectArrayWithKeyValuesArray:resultArr];
        
        dispatch_group_leave(group);
        NSLog(@"");
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        //错误
        dispatch_group_leave(group);
        NSLog(@"%@",errorInfo);
    }];
}




@end
