//
//  QWQuesResultController.m
//  assistant
//
//  Created by qunai on 2023/7/24.
//

#import "QWQuesResultController.h"
#import "QWResultBaseInfoCell.h"
#import "QWQuestResultCell.h"
#import "QWInputController.h"
#import "QWSuggestCell.h"

@interface QWQuesResultController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
//@property (nonatomic, strong) NSMutableArray *showDataArr;

@end

@implementation QWQuesResultController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"诊断建议";
    
    [self tableView];
    [self loadData];
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
//        [_tableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
//        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
        [_tableView registerClass:[QWResultBaseInfoCell class] forCellReuseIdentifier:@"QWResultBaseInfoCell"];
        [_tableView registerClass:[QWQuestResultCell class] forCellReuseIdentifier:@"QWQuestResultCell"];
        [_tableView registerClass:[QWSuggestCell class] forCellReuseIdentifier:@"QWSuggestCell"];
        
        _tableView.backgroundColor=[UIColor whiteColor];
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
//        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
//        _tableView.estimatedSectionHeaderHeight = 100;
//        _tableView.estimatedSectionFooterHeight = 100;
//        _tableView.sectionHeaderHeight = 50;

//        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
//        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger secNum = 1;
//    if(section == 0) secNum = 1;
//    if(section == 1) secNum = self.historyModel.result.count;
//    if(section == 2) secNum = self.historyModel.suggests.count;
    return secNum;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        QWResultBaseInfoCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWResultBaseInfoCell"];
        cell.model =self.aianswerListModel;
        return cell;
    }else if(indexPath.section == 1){
        QWQuestResultCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWQuestResultCell"];
        cell.nameLabel.text = [self customStr:self.aianswerListModel.ai_suggest1];
        return cell;
    }else{
        QWSuggestCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWSuggestCell"];
        cell.suggestLabel.text = [self customRengong];
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    CGRect rect = [tableView rectForHeaderInSection:section];
    UIView *view =[[UIView alloc]initWithFrame:rect];
    view.backgroundColor = [UIColor colorFromHexString:@"#AFEEEE"];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(14, 0, rect.size.width, rect.size.height)];
    label.textColor = [UIColor colorFromHexString:@"#4169E1"];
    NSString *name = @"";
    if(section == 0) name = @"基本信息";
    if(section == 1) name = @"可能患的疾病";
    if(section == 2) name = @"专家建议";
    label.text = name;
    label.font = [UIFont boldSystemFontOfSize:20];
    [view addSubview:label];
    return view;
}

- (NSString *)customStr:(NSString *)str{
    
    NSString *changeStr = @"";
    NSArray *array = [str componentsSeparatedByString:@";"];
    for (int i = 0; i<array.count; i++) {
        NSString *itemStr = array[i];
        if(!IsStringEmpty(itemStr)){
            changeStr = [changeStr stringByAppendingFormat:@"%@%@;",i==0?@"":@"\n",itemStr];
        }
    }
    
    if(IsStringEmpty(changeStr)){
        changeStr = @"尚无明确建议";
    }
    return changeStr;
}

- (NSString*)customRengong{
    NSString *rengongStr = @"";
    NSInteger num = 0;
    if(!IsStringEmpty(self.aianswerListModel.rengong1)){
        num = num+1;
        rengongStr = [rengongStr stringByAppendingFormat:@"%ld.%@",num,self.aianswerListModel.rengong1];
    }
    if(!IsStringEmpty(self.aianswerListModel.rengong2)){
        num = num+1;
        rengongStr = [rengongStr stringByAppendingFormat:@"\n%ld.%@",num,self.aianswerListModel.rengong2];
    }
    
    
    if(IsStringEmpty(rengongStr)){
        rengongStr = @"等待专家人工审查";
    }
    
    return rengongStr;
}
#pragma mark - 加载数据
- (void)loadData{
//    NSMutableString *aa =[NSMutableString stringWithString:self.aianswerListModel.ai_suggest1];
//    NSString *bb = [aa stringByReplacingOccurrencesOfString:@";" withString:@";\n"];
//NSLog(@"")
        
//    NSDictionary *dic =@{
//        @"id": !IsStringEmpty(self.aianswerListModel.id)?self.aianswerListModel.id:@"",
//    };
//    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_AiDiagnosticRules_getConclusion argument:dic];
//    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
//
//        NSLog(@"");
//    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
//        //错误
//        NSLog(@"%@",errorInfo);
//    }];
}
@end
