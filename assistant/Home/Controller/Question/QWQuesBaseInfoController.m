//
//  QWQuesBaseInfoController.m
//  assistant
//
//  Created by qunai on 2023/7/18.
//

#import "QWQuesBaseInfoController.h"
#import "QWBaseInfoHeaderView.h"
#import "QWQuesSexCell.h"
#import "QWQuesAgeCell.h"
#import "QWQuesRulerCell.h"
#import "QWQuesBtnBottomView.h"
#import "QWSymptomListController.h"

@interface QWQuesBaseInfoController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *titleArr;
@property (nonatomic, strong) QWQuesBtnBottomView *bottomView;

@end

@implementation QWQuesBaseInfoController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"信息完善";
    
    [self bottomView];
    [self tableView];
    [self loadData];
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        [_tableView registerClass:[QWBaseInfoHeaderView class] forHeaderFooterViewReuseIdentifier:@"QWBaseInfoHeaderView"];
//        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
        [_tableView registerClass:[QWQuesSexCell class] forCellReuseIdentifier:@"QWQuesSexCell"];
        [_tableView registerClass:[QWQuesAgeCell class] forCellReuseIdentifier:@"QWQuesAgeCell"];
        [_tableView registerClass:[QWQuesRulerCell class] forCellReuseIdentifier:@"QWQuesRulerCell"];

        _tableView.backgroundColor=[UIColor whiteColor];
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
//        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.equalTo(self.view);
            make.bottom.equalTo(self.bottomView.mas_top);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.titleArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        QWQuesSexCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWQuesSexCell"];
        return cell;
    }
    
    if(indexPath.section == 1){
        QWQuesAgeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWQuesAgeCell"];
        return cell;
    }
    
    if(indexPath.section == 2){
        QWQuesRulerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWQuesRulerCell"];
        [cell addRulerView:QWRulerType_height];
        return cell;
    }
    
    if(indexPath.section == 3){
        QWQuesRulerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWQuesRulerCell"];
        [cell addRulerView:QWRulerType_weight];
        return cell;
    }
    
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"cell"];
    }
    cell.textLabel.text =[NSString stringWithFormat:@"cell：%ld",indexPath.row];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    QWBaseInfoHeaderView *titleView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"QWBaseInfoHeaderView"];
    titleView.nameLabel.text = self.titleArr[section];
    return titleView;
}
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
//
//}

#pragma mark - 下一步
- (QWQuesBtnBottomView *)bottomView{
    if(!_bottomView){
        _bottomView = [[QWQuesBtnBottomView alloc]init];
        _bottomView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_bottomView];
        [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.view);
            make.bottom.equalTo(self.view);
            make.height.mas_offset([UIDevice tabBarFullHeight]+10);
        }];
        
        __weak __typeof(self) weakSelf = self;
        _bottomView.confirmClickBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            QWSymptomListController *symptomListVC = [[QWSymptomListController alloc]init];
            symptomListVC.symptomType = QWSymptomType_patient;
            [strongSelf.navigationController pushViewController:symptomListVC animated:YES];
        };
    }
    return _bottomView;
}

#pragma mark - 加载数据
- (void)loadData{
    self.titleArr = @[
    @"性别",@"出生日期",@"身高(cm)",@"体重(kg)"
    ];
    [self.tableView reloadData];
}


@end
