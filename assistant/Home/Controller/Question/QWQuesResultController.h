//
//  QWQuesResultController.h
//  assistant
//
//  Created by qunai on 2023/7/24.
//

#import "QWBaseController.h"
#import "QWQuestModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWQuesResultController : QWBaseController
@property (nonatomic, strong) QWQuesAianswerListModel * aianswerListModel;
@property (nonatomic, assign) BOOL isHiddenGive;
@end

NS_ASSUME_NONNULL_END
