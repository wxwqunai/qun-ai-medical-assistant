//
//  QWSymptomListController.m
//  assistant
//
//  Created by qunai on 2023/7/19.
//

#import "QWSymptomListController.h"
#import "QWHealthQuestionController.h"
#import "QWQuestionAddController.h"
#import "QWQuesBtnBottomView.h"
#import "QWQuestModel.h"
#import "QWSymptomCell.h"

@interface QWSymptomListController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic, strong) UIView *tablesBgView;
@property (nonatomic, strong) UITableView *leftTableView;
@property (nonatomic, strong) NSMutableArray *showDataArr;

@property (nonatomic, strong) UITableView *rightTableView;
@property (nonatomic, assign) NSInteger leftSelect_row_num;

@property (nonatomic, strong) QWQuesBtnBottomView *bottomView;
@end

@implementation QWSymptomListController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"症状列表";
    [self bottomView];
    [self tablesBgView];
    [self leftTableView];
    [self rightTableView];
    [self loadData];
}

- (UIView *)tablesBgView{
    if(!_tablesBgView){
        _tablesBgView = [[UIView alloc]init];
        _tablesBgView.backgroundColor = Color_TableView_Gray;
        [self.view addSubview:_tablesBgView];
        
        [_tablesBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.equalTo(self.view);
            make.bottom.equalTo(self.bottomView.mas_top);
        }];
    }
    
    return _tablesBgView;
}
#pragma mark - tabbleView
- (UITableView *)leftTableView{
    if(!_leftTableView){
        _leftTableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _leftTableView.estimatedRowHeight = 100;
        _leftTableView.rowHeight = UITableViewAutomaticDimension;
        _leftTableView.estimatedSectionFooterHeight=0;
        _leftTableView.estimatedSectionHeaderHeight=0;
        _leftTableView.showsVerticalScrollIndicator=NO;
        _leftTableView.showsHorizontalScrollIndicator=NO;
        
        _leftTableView.delegate =self;
        _leftTableView.dataSource =self;
        
//        [_leftTableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
//        [_leftTableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
        [_leftTableView registerClass:[QWSymptomCell class] forCellReuseIdentifier:@"QWSymptomCell_left"];

        _leftTableView.backgroundColor=[UIColor clearColor];
        _leftTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
//        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        
        [self.tablesBgView addSubview:_leftTableView];
        [_leftTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.tablesBgView).offset(14);
            make.right.equalTo(self.tablesBgView.mas_centerX).offset(-4);
            make.top.equalTo(self.tablesBgView).offset(10);
            make.bottom.equalTo(self.tablesBgView);
        }];
        
    }
    return _leftTableView;
}
- (UITableView *)rightTableView{
    if(!_rightTableView){
        _rightTableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _rightTableView.estimatedRowHeight = 100;
        _rightTableView.rowHeight = UITableViewAutomaticDimension;
        _rightTableView.estimatedSectionFooterHeight=0;
        _rightTableView.estimatedSectionHeaderHeight=0;
        _rightTableView.showsVerticalScrollIndicator=NO;
        _rightTableView.showsHorizontalScrollIndicator=NO;
        
        _rightTableView.delegate =self;
        _rightTableView.dataSource =self;
        
//        [_rightTableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
//        [_rightTableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
        [_rightTableView registerClass:[QWSymptomCell class] forCellReuseIdentifier:@"QWSymptomCell_right"];

        _rightTableView.backgroundColor=[UIColor clearColor];
        _rightTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
//        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        
        [self.tablesBgView addSubview:_rightTableView];
        [_rightTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.tablesBgView).offset(-14);
            make.left.equalTo(self.tablesBgView.mas_centerX).offset(4);
            make.top.equalTo(self.tablesBgView).offset(10);
            make.bottom.equalTo(self.tablesBgView);
        }];
        
    }
    return _rightTableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0000001;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == self.leftTableView){
        return self.showDataArr.count;
    }else{
        QWSymptomBodyModel * bodyModel = self.showDataArr[_leftSelect_row_num];
        return bodyModel.symptomList.count;
    }

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView == self.leftTableView){
        QWSymptomBodyModel * bodyModel = self.showDataArr[indexPath.row];
        QWSymptomCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWSymptomCell_left"];
        cell.nameLabel.text = bodyModel.content;
        [cell changeUIForSelState:_leftSelect_row_num == indexPath.row];
        return cell;
    }else{
        QWSymptomBodyModel * bodyModel = self.showDataArr[_leftSelect_row_num];
        QWSymptomModel *symptomModel = bodyModel.symptomList[indexPath.row];
        QWSymptomCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWSymptomCell_right"];
        cell.nameLabel.text = symptomModel.content;
        [cell changeUIForSelState:symptomModel.isSel];
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(tableView == self.leftTableView){
        _leftSelect_row_num = indexPath.row;
        QWSymptomBodyModel * bodyModel = self.showDataArr[indexPath.row];
        bodyModel.isSel = !bodyModel.isSel;
    }else{
        QWSymptomBodyModel * bodyModel = self.showDataArr[_leftSelect_row_num];
        QWSymptomModel *symptomModel = bodyModel.symptomList[indexPath.row];
        symptomModel.isSel = !symptomModel.isSel;
    }
    [self.leftTableView reloadData];
    [self.rightTableView reloadData];
}

#pragma mark - 下一步
- (QWQuesBtnBottomView *)bottomView{
    if(!_bottomView){
        _bottomView = [[QWQuesBtnBottomView alloc]init];
        _bottomView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_bottomView];
        [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.view);
            make.bottom.equalTo(self.view);
            make.height.mas_offset([UIDevice tabBarFullHeight]+10);
        }];
        
        __weak __typeof(self) weakSelf = self;
        _bottomView.confirmClickBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if(strongSelf.symptomType == QWSymptomType_patient){
                QWHealthQuestionController *questionVC = [[QWHealthQuestionController alloc]init];
                [strongSelf.navigationController pushViewController:questionVC animated:YES];
            }else if(strongSelf.symptomType == QWSymptomType_doctor){
                NSMutableArray *selArr = [strongSelf OutputsAllSelectedSymptoms];
                QWQuestionAddController *addVC = [[QWQuestionAddController alloc]init];
                addVC.selSymptomArr = selArr;
                [strongSelf.navigationController pushViewController:addVC animated:YES];
            }
        };
    }
    return _bottomView;
}


#pragma mark - 加载数据
- (void)loadData{
    
    _leftSelect_row_num = 0;

    NSString * pathStr = [[NSBundle mainBundle] pathForResource:@"QWQuesSymptom" ofType:@"plist"];
    
    //总的字典
    NSDictionary * riskBeanDict = [[NSDictionary alloc]initWithContentsOfFile:pathStr][@"RiskBean"];
    
    NSString * resultCode = [NSString stringWithFormat:@"%@",riskBeanDict[@"resultCode"]];
    
    if ([resultCode isEqualToString:@"200"]) {
        //题目列表
        NSArray * questArr = riskBeanDict[@"bodyList"];
        self.showDataArr = [QWSymptomBodyModel mj_objectArrayWithKeyValuesArray:questArr];
        [self.leftTableView reloadData];
    }
}


#pragma mark - 身体列表 - 选中状态恢复
- (void)reStoreSymptomBodyForSel{
    
}

#pragma mark - 症状列表 - 选中状态恢复

#pragma mark - 输出所有选中症状
- (NSMutableArray *)OutputsAllSelectedSymptoms{
    NSMutableArray *selectSymptomsArr = [[NSMutableArray alloc]init];
    [self.showDataArr enumerateObjectsUsingBlock:^(QWSymptomBodyModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj.symptomList enumerateObjectsUsingBlock:^(QWSymptomModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if(obj.isSel == YES){
                [selectSymptomsArr addObject:obj];
            }
        }];
    }];
    
    return selectSymptomsArr;
}

@end
