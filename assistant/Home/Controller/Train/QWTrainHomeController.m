//
//  QWTrainHomeController.m
//  assistant
//
//  Created by qunai on 2023/7/18.
//

#import "QWTrainHomeController.h"
#import "QWWebviewController.h"
#import "QWTipsAlertController.h"
#import "QWTrainModel.h"
#import "QWTrainHomeCell.h"
#import "QWDevelopmentView.h"

@interface QWTrainHomeController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) QWDevelopmentView *developView;
@property (nonatomic, strong)UISegmentedControl *segment;

@property(nonatomic, strong) UIView *tablesBgView;
@property (nonatomic, strong) UITableView *leftTableView;
@property (nonatomic, strong) UITableView *rightTableView;

@property (nonatomic, assign) NSInteger leftSelect_row_num;
@property (nonatomic, strong) NSMutableArray *showDataArr;


@property (nonatomic, strong) NSMutableArray *medicineArr;
@property (nonatomic, strong) NSMutableArray *addressArr;

@end

@implementation QWTrainHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"内部培训";
    [self developView];
    
    [self segment];
    [self tablesBgView];
    [self leftTableView];
    [self rightTableView];
    [self loadData];
    
    [self changeShowUI:NO];
}

#pragma mark - 开发中
- (QWDevelopmentView *)developView{
    if(!_developView){
        _developView = [[QWDevelopmentView alloc]init];
        _developView.hidden = YES;
        [self.view addSubview:_developView];
        
        [_developView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.view);
            make.centerY.equalTo(self.view);
        }];
        
        __weak __typeof(self) weakSelf = self;
        _developView.longPressBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf showCheckCodeTipsAlert];
        };
    }
    return _developView;
}

#pragma mark - 顶部切换
- (UISegmentedControl *)segment{
    if(!_segment){
        //初始化一个UISegmentedControl控件test
        _segment = [[UISegmentedControl alloc]initWithItems:@[@"区域",@"药品"]];
        _segment.hidden = YES;
        _segment.frame = CGRectMake(0, [UIDevice navigationFullHeight]+2, kScreenWidth, 44);

        //设置无边框分段控制器

        //设置test控件的颜色为透明
        _segment.tintColor = [UIColor clearColor];
        //定义选中状态的样式selected，类型为字典
        NSDictionary *selected = @{NSFontAttributeName:[UIFont systemFontOfSize:20],
                                   NSForegroundColorAttributeName:Color_Main_Green};
        //定义未选中状态下的样式normal，类型为字典
        NSDictionary *normal = @{NSFontAttributeName:[UIFont systemFontOfSize:14],
                                     NSForegroundColorAttributeName:[UIColor blackColor]};

        /*
        NSFontAttributeName -> 系统宏定义的特殊键，用来给格式字典中的字体赋值
        NSForegroundColorAttributeName -> 系统宏定义的特殊键，用来给格式字典中的字体颜色赋值
        */

        //通过setTitleTextAttributes: forState: 方法来给test控件设置文字内容的格式
        [_segment setTitleTextAttributes:normal forState:UIControlStateNormal];
        [_segment setTitleTextAttributes:selected forState:UIControlStateSelected];
        
        [_segment addTarget:self action:@selector(click:) forControlEvents:UIControlEventValueChanged];

        //设置test初始状态下的选中下标
        _segment.selectedSegmentIndex = 0;

        //将test添加到某个View上
        [self.view addSubview:_segment];
    }
    return _segment;
}

- (void)click:(UISegmentedControl *)segment{
    _leftSelect_row_num = 0;
    self.showDataArr = segment.selectedSegmentIndex == 0?self.addressArr:self.medicineArr;
    [self changeSelForShorData];
    [self.leftTableView reloadData];
    [self.rightTableView reloadData];
}

- (UIView *)tablesBgView{
    if(!_tablesBgView){
        _tablesBgView = [[UIView alloc]init];
        _tablesBgView.hidden = YES;
        _tablesBgView.backgroundColor = Color_TableView_Gray;
        [self.view addSubview:_tablesBgView];
        
        [_tablesBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.equalTo(self.segment.mas_bottom).offset(2);
            make.bottom.equalTo(self.view);
        }];
    }
    
    return _tablesBgView;
}
#pragma mark - tabbleView
- (UITableView *)leftTableView{
    if(!_leftTableView){
        _leftTableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _leftTableView.estimatedRowHeight = 100;
        _leftTableView.rowHeight = UITableViewAutomaticDimension;
        _leftTableView.estimatedSectionFooterHeight=0;
        _leftTableView.estimatedSectionHeaderHeight=0;
        _leftTableView.showsVerticalScrollIndicator=NO;
        _leftTableView.showsHorizontalScrollIndicator=NO;
        
        _leftTableView.delegate =self;
        _leftTableView.dataSource =self;
        
//        [_leftTableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
//        [_leftTableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
        [_leftTableView registerClass:[QWTrainHomeCell class] forCellReuseIdentifier:@"QWTrainHomeCell_left"];

        _leftTableView.backgroundColor=[UIColor clearColor];
        _leftTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
//        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        
        [self.tablesBgView addSubview:_leftTableView];
        [_leftTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.tablesBgView).offset(14);
            make.width.equalTo(self.tablesBgView.mas_width).multipliedBy(0.3);
            make.top.equalTo(self.tablesBgView);
            make.bottom.equalTo(self.tablesBgView);
        }];
        
    }
    return _leftTableView;
}
- (UITableView *)rightTableView{
    if(!_rightTableView){
        _rightTableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _rightTableView.estimatedRowHeight = 100;
        _rightTableView.rowHeight = UITableViewAutomaticDimension;
        _rightTableView.estimatedSectionFooterHeight=0;
        _rightTableView.estimatedSectionHeaderHeight=0;
        _rightTableView.showsVerticalScrollIndicator=NO;
        _rightTableView.showsHorizontalScrollIndicator=NO;
        
        _rightTableView.delegate =self;
        _rightTableView.dataSource =self;
        
//        [_rightTableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
//        [_rightTableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
        [_rightTableView registerClass:[QWTrainHomeCell class] forCellReuseIdentifier:@"QWTrainHomeCell_right"];

        _rightTableView.backgroundColor=[UIColor clearColor];
        _rightTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
//        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        
        [self.tablesBgView addSubview:_rightTableView];
        [_rightTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.tablesBgView).offset(-14);
            make.left.equalTo(self.leftTableView.mas_right).offset(8);
            make.top.equalTo(self.tablesBgView);
            make.bottom.equalTo(self.tablesBgView);
        }];
        
    }
    return _rightTableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0000001;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == self.leftTableView){
        return self.showDataArr.count;
    }else{
        QWTrainListModel * listModel = self.showDataArr[_leftSelect_row_num];
        return listModel.list.count;
    }

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.leftTableView){
        QWTrainListModel * listModel = self.showDataArr[indexPath.row];
        QWTrainHomeCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWTrainHomeCell_left"];
        cell.progressView.hidden = YES;
        cell.nameLabel.text = listModel.content;
        [cell changeUIForSelState:listModel.isSel];
        return cell;
    }else{
        QWTrainListModel * QWTrainListModel = self.showDataArr[_leftSelect_row_num];
        QWTrainItemModel *model = QWTrainListModel.list[indexPath.row];
        QWTrainHomeCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWTrainHomeCell_right"];
        cell.nameLabel.text = model.content;
        [cell changeUIForSelState:model.isSel];
        
        cell.progressView.hidden = NO;
        [cell changeSateBtn:!IsStringEmpty(model.file)];
        
        __weak __typeof(cell)weakCell = cell;
        cell.stateBtnClcikBlock = ^{
            __strong __typeof(weakCell)strongCell = weakCell;
            [self downLoadFile:model forCell:strongCell];
        };
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(tableView == self.leftTableView){
        [self clearSelForLeftTable];
        _leftSelect_row_num = indexPath.row;
        QWTrainListModel * listModel = self.showDataArr[_leftSelect_row_num];
        listModel.isSel = !listModel.isSel;
    }else{
        [self clearSelForRightTable];
        QWTrainListModel * listModel = self.showDataArr[_leftSelect_row_num];
        QWTrainItemModel *model = listModel.list[indexPath.row];
        model.isSel = !model.isSel;
        
        [self jumToWebView:model.url];
    }
    [self.leftTableView reloadData];
    [self.rightTableView reloadData];
    
    
}

#pragma mark - 加载数据
- (void)loadData{
    
    _leftSelect_row_num = 0;

    NSString * pathStr = [[NSBundle mainBundle] pathForResource:@"QWTrain" ofType:@"plist"];
    
    //总的字典
    NSDictionary * riskBeanDict = [[NSDictionary alloc]initWithContentsOfFile:pathStr][@"RiskBean"];
    
    NSString * resultCode = [NSString stringWithFormat:@"%@",riskBeanDict[@"resultCode"]];
    
    if ([resultCode isEqualToString:@"200"]) {
        NSArray * adressArr = riskBeanDict[@"addressList"];
        NSArray * medicineArr = riskBeanDict[@"medicineList"];
        
        self.addressArr = [QWTrainListModel mj_objectArrayWithKeyValuesArray:adressArr];
        self.medicineArr = [QWTrainListModel mj_objectArrayWithKeyValuesArray:medicineArr];

        self.showDataArr = self.addressArr;
        [self changeSelForShorData];
        [self.leftTableView reloadData];
        [self.rightTableView reloadData];
    }
}

#pragma mark - 弹框- 验证码
- (void)showCheckCodeTipsAlert{
    QWTipsAlertController *activityVC =[[QWTipsAlertController alloc]init];
    activityVC.tipType = TipsAlertType_checkCode;
    activityVC.modalPresentationStyle=UIModalPresentationOverFullScreen;
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:activityVC animated:NO completion:nil];
    
    __weak __typeof(activityVC)weakActivityVC = activityVC;
    activityVC.checkCodeView.sureBlock = ^(NSString * _Nonnull code) {
        __strong __typeof(weakActivityVC)strongActivityVC = weakActivityVC;
        [strongActivityVC.checkCodeView checkInput:[code isEqualToString:@"888"]];
        if([code isEqualToString:@"888"]){
            [self changeShowUI:YES];
        }
    };
}

#pragma mark - 隐藏/展示主要内容
- (void)changeShowUI:(BOOL)isShowMain{
    if(isShowMain){
        self.developView.hidden = YES;
        self.tablesBgView.hidden = NO;
        self.segment.hidden = NO;
    }else{
        self.developView.hidden = NO;
        self.tablesBgView.hidden = YES;
        self.segment.hidden = YES;
    }
}

#pragma mark - webview - 跳转
- (void)jumToWebView:(NSString *)url{
    if (IsStringEmpty(url)) return;
    QWWebviewController *webviewVC =[[QWWebviewController alloc]init];
    webviewVC.url = url;
    webviewVC.title = @"内部培训";
    webviewVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:webviewVC animated:YES];
}


#pragma mark - 清空选项
- (void)clearSelForLeftTable{
    [self.showDataArr enumerateObjectsUsingBlock:^(QWTrainListModel *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.isSel = NO;
        [obj.list enumerateObjectsUsingBlock:^(QWTrainItemModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj.isSel = NO;
        }];
    }];
}
- (void)clearSelForRightTable{
    QWTrainListModel * listModel = self.showDataArr[_leftSelect_row_num];
    [listModel.list enumerateObjectsUsingBlock:^(QWTrainItemModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.isSel = NO;
    }];
}

- (void)changeSelForShorData{
    [self.showDataArr enumerateObjectsUsingBlock:^(QWTrainListModel *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.isSel = idx==0?YES:NO;
        [obj.list enumerateObjectsUsingBlock:^(QWTrainItemModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj.isSel = NO;
        }];
    }];
}

#pragma mark - 资源下载
- (void)downLoadFile:(QWTrainItemModel *)model forCell:(QWTrainHomeCell *) cell{
    if(IsStringEmpty(model.file)) return;
    NSString *url = model.file;
    [HXGetRequest downloadPach:url withLocalPath:nil withPress:^(NSProgress * _Nonnull progress) {
        //需要在主线程中更新下载进度
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.progressNum = progress.fractionCompleted;
        });
    } withSuccess:^(__kindof HXRequest * _Nonnull request, NSDictionary * _Nonnull result, BOOL success) {
        NSURL *cachePath = (NSURL *)request.responseObject;
        [[QWCommonMethod Instance] sharedFromSystem:model.fileName withUrl:cachePath withImage:nil success:^{
            [self saveFileSuccessAlert];
        } failure:^{
            
        }];
    } failure:^(__kindof HXRequest * _Nonnull request, NSString * _Nonnull errorInfo) {
        
    }];
}

//系统文件存储成功
- (void)saveFileSuccessAlert{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"存储成功" message:@"请前往手机软件“文件”查看,或是否打开文件所在地?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *sureAction = [UIAlertAction actionWithTitle:@"是" style: UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[QWCommonMethod Instance] openSystemAppFile];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"否" style: UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alertController addAction:sureAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
