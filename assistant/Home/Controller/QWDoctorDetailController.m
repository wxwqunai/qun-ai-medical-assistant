//
//  QWDoctorDetailController.m
//  assistant
//
//  Created by kevin on 2023/5/18.
//

#import "QWDoctorDetailController.h"
#import "QWDoctorDetailInfoCell.h"
#import "EMChatViewController.h"

@interface QWDoctorDetailController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) QADoctorDetailModel *detailModel;
@end

@implementation QWDoctorDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"医生详情";
    [self.rightBarButtonItem setImage:[UIImage imageNamed:@"ic_collectNot.png"]];
    
    [self tableView];
    
    [self loadPullDownRefreshDataInfo];
}

- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        [_tableView registerClass:[QWDoctorDetailInfoCell class] forCellReuseIdentifier:@"QWDoctorDetailInfoCell"];
        
        _tableView.backgroundColor=Color_TableView_Gray;
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;

//        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
//        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0000001;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.detailModel != nil ?1:0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QWDoctorDetailInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWDoctorDetailInfoCell"];
    cell.detailModel = self.detailModel;
    cell.chatBlock = ^{
        [self jumpToChat];
    };
    return cell;
}

#pragma mark - 聊天 - 跳转
- (void)jumpToChat{
    if([AppProfile isLoginedAlert]){
        // ConversationId 接收消息方的环信ID:@"user2"
        // type 聊天类型:EMConversationTypeChat    单聊类型
        // createIfNotExist 如果会话不存在是否创建会话：YES
         EMChatViewController *chatViewController = [[EMChatViewController alloc] initWithConversationId:self.detailModel.id conversationType:EMConversationTypeChat];
        [self.navigationController pushViewController:chatViewController animated:YES];
    }
}

#pragma mark-导航条右边按钮 - 关注 - 请求
-(void)navRightBaseButtonClick
{
    if(![AppProfile isLoginedAlert]) return;
    
    NSDictionary *dic = @{
        @"userid":AppProfile.qaUserInfo.id,
        @"beUserid":self.detailModel.id
    };
    [SWHUDUtil showHudViewInSuperView:self.view withMessage:@"请等待"];
    HXPostRequest * request = [[HXPostRequest alloc] initWithRequestUrl:URL_my_addAttention argument:dic];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        long code = [result[@"code"] longValue];
        NSString *msg = result[@"msg"];
        if(code == 1){
            [SWHUDUtil hideHudViewWithSuccessMessage:msg];
        }else{
            [SWHUDUtil hideHudViewWithFailureMessage:msg];
        }
      
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        [SWHUDUtil hideHudView];
    }];
}


#pragma mark- 数据加载
-(void)loadPullDownRefreshDataInfo
{
    NSString *urlStr = [URL_GetUser_INFO stringByAppendingFormat:@"/%@",self.doctorModel.id];
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:urlStr argument:[NSDictionary new]];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        long code = [result[@"code"] longValue];
        if(code == 1){
            [SWHUDUtil hideHudView];
            NSDictionary *data = result[@"data"];
            self.detailModel = [QADoctorDetailModel mj_objectWithKeyValues:data];
            [self.tableView reloadData];
        }else{
            NSString *msg = result[@"msg"];
            [SWHUDUtil hideHudViewWithFailureMessage:msg];
        }
      
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        [SWHUDUtil hideHudView];
    }];
}




@end
