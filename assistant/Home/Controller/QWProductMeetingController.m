//
//  QWProductMeetingController.m
//  assistant
//
//  Created by qunai on 2024/1/8.
//

#import "QWProductMeetingController.h"
#import "QWMenuScreeningView.h"
#import "QWProductMeetingCell.h"
#import "QWWebviewController.h"

@interface QWProductMeetingController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) QWMenuScreeningView *menuScreeningView;  //条件选择器

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *pilstArr;
@property (nonatomic, strong) NSMutableArray *showDataArr;

@end

@implementation QWProductMeetingController
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [self.menuScreeningView menuScreeningViewDismiss];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"产品学术会议汇总";
    
    [self menuScreeningView];

    [self tableView];
    [self loadData];
}
#pragma mark - 日期、地址筛选
- (QWMenuScreeningView *)menuScreeningView{
    if(!_menuScreeningView){
        _menuScreeningView = [[QWMenuScreeningView alloc] initWithFrame:CGRectMake(0, [UIDevice navigationFullHeight], kScreenWidth, 36)];
        [self.view addSubview:self.menuScreeningView];
        _menuScreeningView.backgroundColor = [UIColor whiteColor];
        __weak typeof(self) weakSelf = self;
        _menuScreeningView.menuSelectedOnBlock = ^(NSString * _Nonnull dateStr, NSString * _Nonnull city) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf filterDate:dateStr withCity:city];
        };
    }
    return _menuScreeningView;
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        //        [_tableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
        //        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
                [_tableView registerClass:[QWProductMeetingCell class] forCellReuseIdentifier:@"QWProductMeetingCell"];
        
        _tableView.backgroundColor=Color_TableView_Gray;
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        //        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        //        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        //        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        //        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.menuScreeningView.mas_bottom);
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.view);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0000001;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.showDataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QWProductMeetingCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWProductMeetingCell"];
    NSDictionary *dic = self.showDataArr[indexPath.row];
    cell.nameLabel.text = dic[@"name"];
    NSString *startDate = dic[@"startDate"];
    NSString *endDate = dic[@"endDate"];
    NSString *date = [startDate isEqualToString:endDate]?startDate:[startDate stringByAppendingFormat:@"--%@",endDate];
    NSString *address = dic[@"address"];
    NSString *schedule_url = dic[@"schedule_url"];
    cell.desLabel.text = [NSString stringWithFormat:@"%@•%@",date,address];
    cell.newsOnBlock = ^{
        [self jumToWebView:schedule_url];
    };
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *dic = self.showDataArr[indexPath.row];
    NSString *url = dic[@"url"];
    [self jumToWebView:url];
}

#pragma mark - webview - 跳转
- (void)jumToWebView:(NSString *)url{
    QWWebviewController *webviewVC =[[QWWebviewController alloc]init];
    webviewVC.url = url;
    webviewVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:webviewVC animated:YES];
}

#pragma mark - 加载数据
- (void)loadData{
    self.pilstArr = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"projuctMeeting.plist" ofType:nil]];
    [self.showDataArr addObjectsFromArray:self.pilstArr];
    [self.tableView reloadData];
}

- (void)filterDate:(NSString *)dateStr withCity:(NSString *)cityStr{
    
    [self.showDataArr removeAllObjects];
    [self.pilstArr enumerateObjectsUsingBlock:^(NSDictionary *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *startDateStr = obj[@"startDate"];
        NSString *address = obj[@"address"];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        formatter.dateFormat = @"YYYY.MM.dd";
        NSDate *satrtDate = [formatter dateFromString:startDateStr];
        formatter.dateFormat = @"YYYY年M月";
        NSString *changeStartDateStr = [formatter stringFromDate:satrtDate];
        
        //只有时间
        BOOL isOnlyDate = !IsStringEmpty(dateStr) && ([changeStartDateStr isEqualToString:dateStr]||[dateStr isEqualToString:@"全部"]) && IsStringEmpty(cityStr);
        //只有地址
        BOOL isOnlyAddress = !IsStringEmpty(cityStr)&& ([cityStr containsString:address]||[cityStr isEqualToString:@"全部"]) && IsStringEmpty(dateStr);
        //有时间和地址
        BOOL isAll = !IsStringEmpty(dateStr) &&  ([changeStartDateStr isEqualToString:dateStr]||[dateStr isEqualToString:@"全部"]) && !IsStringEmpty(cityStr)&& ([cityStr containsString:address]||[cityStr isEqualToString:@"全部"]);
        
        if(isOnlyDate || isOnlyAddress|| isAll ){
            [self.showDataArr addObject:obj];
        }
        
    }];
                
    [self.tableView reloadData];
}

-(NSMutableArray *)showDataArr{
    if (!_showDataArr) {
        _showDataArr = [[NSMutableArray alloc]init];
    }
    return _showDataArr;
}

@end
