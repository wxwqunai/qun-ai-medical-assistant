//
//  XWQRCodeGenerator.h
//  assistant
//
//  Created by qunai on 2023/10/9.
//  生成二维码图片
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XWQRCodeGenerator : NSObject
+ (UIImage *)QRCodeWithContentString:(NSString *)contentString size:(CGFloat)size;
+ (UIImage *)QRCodeWithContentString:(NSString *)contentString size:(CGFloat)size centerLogo:(UIImage *)centerLogo;
@end

NS_ASSUME_NONNULL_END
