//
//  XWQRCodeReader.h
//  assistant
//
//  Created by qunai on 2023/10/9.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XWQRCodeReader : UIViewController
@property (nonatomic, copy) void (^qrcodeValueBlock)(NSString *codeString);
@end

NS_ASSUME_NONNULL_END
