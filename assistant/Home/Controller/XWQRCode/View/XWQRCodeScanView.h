//
//  XWQRCodeScanView.h
//  assistant
//
//  Created by qunai on 2023/10/9.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XWQRCodeScanView : UIView
// 是否隐藏开启闪光灯按钮
@property (nonatomic, assign) float brightnessValue;
@property (nonatomic, copy) void (^offFlashBlock)(BOOL flag);
- (instancetype)initWithScanRect:(CGRect)scanRect;
@end

NS_ASSUME_NONNULL_END
