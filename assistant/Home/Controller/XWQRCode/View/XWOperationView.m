//
//  XWOperationView.m
//  assistant
//
//  Created by qunai on 2023/10/10.
//

#import "XWOperationView.h"
@interface XWOperationView()
@property (nonatomic, strong) UIView *myCodeView;
@property (nonatomic, strong) UIView *photoView;
@end
@implementation XWOperationView

- (instancetype)init{
    self = [super init];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        
        [self myCodeView];
        
        [self photoView];
    }
    return self;
}

- (UIView *)myCodeView{
    if(!_myCodeView){
        _myCodeView = [self itemView:@"XWCode.png" withName:@"我的二维码" action:@selector(myCodeOnClick)];
        [self addSubview:_myCodeView];
        
        [_myCodeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.bottom.equalTo(self);
            make.left.equalTo(self).offset(20.0);
        }];
    }
    return _myCodeView;
}

- (void)myCodeOnClick{
    if(self.operationClickBlock)self.operationClickBlock(@"myCode");
}

- (UIView *)photoView{
    if(!_photoView){
        _photoView = [self itemView:@"XWPhoto.png" withName:@"相册" action:@selector(photoOnClick)];
        [self addSubview:_photoView];
        
        [_photoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.bottom.equalTo(self);
            make.right.equalTo(self).offset(-20.0);
        }];
    }
    return _photoView;
}
- (void)photoOnClick{
    if(self.operationClickBlock)self.operationClickBlock(@"photo");
}

- (UIView *)itemView:(NSString *)pic withName:(NSString *)name action:(SEL)action{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor clearColor];
    //图标
    UIView *imgBgView = [[UIView alloc]init];
    imgBgView.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.5];
    imgBgView.layer.cornerRadius = 20.0;
    imgBgView.layer.masksToBounds = YES;
    [view addSubview:imgBgView];
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.image =[UIImage imageNamed:pic];
//    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [imgBgView addSubview:imageView];
    
    //文本
    UILabel *label = [[UILabel alloc]init];
    label.text = name;
    label.font = [UIFont systemFontOfSize:14];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    [view addSubview:label];
    
    //点击
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];
    
    [imgBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(view);
        make.top.equalTo(view);
        make.height.mas_offset(40.0);
        make.width.mas_offset(40.0);
    }];
    
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(imgBgView);
        make.centerY.equalTo(imgBgView);
        make.height.mas_offset(20.0);
        make.width.mas_offset(20.0);
    }];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imgBgView.mas_bottom).offset(8.0);
        make.bottom.equalTo(view);
        make.left.equalTo(view);
        make.right.equalTo(view);
        make.width.mas_greaterThanOrEqualTo(imgBgView.mas_width);
    }];
    
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(view);
    }];
    return view;
}

@end
