//
//  XWOperationView.h
//  assistant
//
//  Created by qunai on 2023/10/10.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XWOperationView : UIView
@property (nonatomic, copy) void (^operationClickBlock)(NSString *operate);

@end

NS_ASSUME_NONNULL_END
