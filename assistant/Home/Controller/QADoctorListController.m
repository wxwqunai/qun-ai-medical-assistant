//
//  QADoctorListController.m
//  assistant
//
//  Created by kevin on 2023/4/27.
//

#import "QADoctorListController.h"
#import "QWHomeModel.h"
#import "QWWebviewController.h"
#import "QADoctorItemCell.h"
#import "QASearchView.h"
#import "QWDoctorDetailController.h"

@interface QADoctorListController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) NSMutableArray *showDataArr;

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *doctorDataArr;

@property (nonatomic, strong) QASearchView *searchView;
@end

@implementation QADoctorListController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"医疗专家";
        
    [self searchView];
    [self tableView];
    [self loadAllData];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //解决机型不同不能全屏问题
    self.navigationController.navigationBar.translucent = YES;
    [self setExtendedLayoutIncludesOpaqueBars:NO];
}

#pragma mark - 搜索
- (QASearchView *)searchView{
    if(!_searchView){
        _searchView = [[QASearchView alloc] init];
        __weak __typeof(self) weakSelf = self;
        _searchView.searchCompleteBlock = ^(NSString * _Nonnull searchStr) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            
            if(IsStringEmpty(searchStr)){
                [strongSelf.showDataArr removeAllObjects];
                [strongSelf.showDataArr addObjectsFromArray:strongSelf.doctorDataArr];
                [strongSelf.tableView reloadData];
                return;
            }
            
            [strongSelf.showDataArr removeAllObjects];
            for (QADoctorModel *model in strongSelf.doctorDataArr) {
                if([model.name containsString:searchStr] ||
                   [model.phone containsString:searchStr] ||
                   [model.companyId containsString:searchStr] ||
                   [model.officeId containsString:searchStr] ||
                   [model.level containsString:searchStr] ){
                    [strongSelf.showDataArr addObject:model];
                }
            }
            [strongSelf.tableView reloadData];
        };
        [self.view addSubview:_searchView];
        
        [_searchView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.equalTo(self.view).offset([UIDevice navigationFullHeight]);
            make.height.mas_equalTo(60);
        }];
        
        //解决机型不同不能全屏问题
        self.navigationController.navigationBar.translucent = NO;
        [self setExtendedLayoutIncludesOpaqueBars:YES];
    }
    return _searchView;
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        [_tableView registerClass:[QADoctorItemCell class] forCellReuseIdentifier:@"QADoctorItemCell"];


        _tableView.backgroundColor=Color_TableView_Gray;
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;

//        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
//        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.equalTo(_searchView.mas_bottom);
            make.bottom.equalTo(self.view);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0000001;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.showDataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QADoctorItemCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QADoctorItemCell"];
    QADoctorModel*model = self.showDataArr[indexPath.row];
    cell.model = model;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    QADoctorModel*model = self.showDataArr[indexPath.row];
    NSString *url = [URL_DOCTOR_INFO stringByAppendingFormat:@"?id=%@&from=app",model.id];
    [[UserInfoStore sharedInstance] fetchUserInfosFromServer:@[[model.id lowercaseString]]];
    [self jumToWebView:url];
    
//    QADoctorModel*model = self.showDataArr[indexPath.row];
//    QWDoctorDetailController *detailVC = [[QWDoctorDetailController alloc]init];
//    detailVC.doctorModel = model;
//    detailVC.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark - 所有数据-请求
- (void) loadAllData{
    [self loadDoctorListData];//医生列表数据
}

#pragma mark - 医生列表-请求
- (void)loadDoctorListData{
    [SWHUDUtil showHudViewInSuperView:self.view withMessage:@"请等待"];
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_DOCTOR_LIST argument:[NSDictionary dictionary]];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        [SWHUDUtil hideHudView];
        NSArray *resArr =(NSArray *)result;
        NSMutableArray *arr =[QADoctorModel mj_objectArrayWithKeyValuesArray:resArr];
        [self.doctorDataArr removeAllObjects];
        [self.doctorDataArr addObjectsFromArray:arr];
        
        [self.showDataArr removeAllObjects];
        [self.showDataArr addObjectsFromArray:arr];
        [self.tableView reloadData];
        NSLog(@"");
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        //错误
        [SWHUDUtil hideHudView];
        NSLog(@"%@",errorInfo);
    }];
}

- (NSMutableArray *)showDataArr{
    if(!_showDataArr){
        _showDataArr = [[NSMutableArray alloc]init];
    }
    return _showDataArr;
}
- (NSMutableArray *)doctorDataArr{
    if(!_doctorDataArr){
        _doctorDataArr = [[NSMutableArray alloc]init];
    }
    return _doctorDataArr;
}

#pragma mark - webview - 跳转
- (void)jumToWebView:(NSString *)url{
    QWWebviewController *webviewVC =[[QWWebviewController alloc]init];
    webviewVC.url = url;
    webviewVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:webviewVC animated:YES];
}

@end
