//
//  QWPatientDetailController.m
//  assistant
//
//  Created by qunai on 2023/8/16.
//

#import "QWPatientDetailController.h"
#import "QWDevelopmentView.h"

@interface QWPatientDetailController ()

@end

@implementation QWPatientDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"患者详情";
    
    QWDevelopmentView *view = [[QWDevelopmentView alloc]init];
    [self.view addSubview:view];
    
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.centerY.equalTo(self.view);
    }];
}



@end
