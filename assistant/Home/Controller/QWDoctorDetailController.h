//
//  QWDoctorDetailController.h
//  assistant
//
//  Created by kevin on 2023/5/18.
//

#import "QWBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWDoctorDetailController : QWBaseController
@property (nonatomic, strong) QADoctorModel* doctorModel;
@end

NS_ASSUME_NONNULL_END
