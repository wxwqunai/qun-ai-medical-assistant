//
//  QWServiceTeamHomeController.m
//  assistant
//
//  Created by kevin on 2023/9/5.
//

#import "QWServiceTeamHomeController.h"
#import "QWServiceTeamHomeCell.h"

@interface QWServiceTeamHomeController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showDataArr;

@end

@implementation QWServiceTeamHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"客服团队";
    
    [self tableView];
    [self loadData];
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        //        [_tableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
        //        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
                [_tableView registerClass:[QWServiceTeamHomeCell class] forCellReuseIdentifier:@"QWServiceTeamHomeCell"];
        
        _tableView.backgroundColor=[UIColor whiteColor];
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        //        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        //        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        //        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        //        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0000001;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.showDataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QWServiceTeamHomeCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWServiceTeamHomeCell"];
    NSDictionary *dic = self.showDataArr[indexPath.row];
    cell.modelDic = dic;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



#pragma mark - 加载数据
- (void)loadData{
    self.showDataArr = [[NSMutableArray alloc]initWithArray:@[
//                        @{@"name":@"项锡山",@"level":@"金牌医助"},
//                        @{@"name":@"南智瀚",@"level":@"金牌医助"},
//                        @{@"name":@"张扬",@"level":@"金牌医助"},
//                        @{@"name":@"李金朋",@"level":@"金牌医助"},
//                        @{@"name":@"闫东勇",@"level":@"金牌医助"},
//                        @{@"name":@"原红星",@"level":@"金牌医助"},
//                        @{@"name":@"卫超",@"level":@"金牌医助"},
//                        @{@"name":@"白云凤",@"level":@"金牌医助"},
//                        @{@"name":@"张国强",@"level":@"金牌医助"},
//                        @{@"name":@"刘敏",@"level":@"金牌医助"},
//                        @{@"name":@"申壮",@"level":@"金牌医助"},
//                        @{@"name":@"刘伦江",@"level":@"金牌医助"},
//                        @{@"name":@"张营",@"level":@"金牌医助"},
//                        @{@"name":@"米飞龙",@"level":@"金牌医助"},
//                        @{@"name":@"石杰鹏",@"level":@"金牌医助"},
//                        @{@"name":@"刘宏",@"level":@"金牌医助"},
//                        @{@"name":@"段佳慧",@"level":@"金牌医助"},
//                        @{@"name":@"刘晋军",@"level":@"金牌医助"},
//                        @{@"name":@"杨凯",@"level":@"金牌医助"},
//                        @{@"name":@"卫超俊",@"level":@"金牌医助"},
//                        @{@"name":@"姚青梅",@"level":@"金牌医助"},
//                        @{@"name":@"李永春",@"level":@"金牌医助"},
//                        @{@"name":@"石智强",@"level":@"金牌医助"},
//                        @{@"name":@"郭昶宏",@"level":@"金牌医助"},
//                        @{@"name":@"张伟伟",@"level":@"金牌医助"},
//                        @{@"name":@"聂彦兵",@"level":@"金牌医助"},
//                        @{@"name":@"李颖洁",@"level":@"金牌医助"},
//                        @{@"name":@"程鹏",@"level":@"金牌医助"},
//                        @{@"name":@"袁理",@"level":@"金牌医助"},
//                        @{@"name":@"武鉴桥",@"level":@"金牌医助"}
        @{@"name":@"项**",@"level":@"金牌医助"},
        @{@"name":@"南**",@"level":@"金牌医助"},
        @{@"name":@"张**",@"level":@"金牌医助"},
        @{@"name":@"李**",@"level":@"金牌医助"},
        @{@"name":@"闫**",@"level":@"金牌医助"},
        @{@"name":@"原**",@"level":@"金牌医助"},
        @{@"name":@"卫**",@"level":@"金牌医助"},
        @{@"name":@"白**",@"level":@"金牌医助"},
        @{@"name":@"张**",@"level":@"金牌医助"},
        @{@"name":@"刘**",@"level":@"金牌医助"},
        @{@"name":@"申**",@"level":@"金牌医助"},
        @{@"name":@"刘**",@"level":@"金牌医助"},
        @{@"name":@"张**",@"level":@"金牌医助"},
        @{@"name":@"米**",@"level":@"金牌医助"},
        @{@"name":@"石**",@"level":@"金牌医助"},
        @{@"name":@"刘**",@"level":@"金牌医助"},
        @{@"name":@"段**",@"level":@"金牌医助"},
        @{@"name":@"刘**",@"level":@"金牌医助"},
        @{@"name":@"杨**",@"level":@"金牌医助"},
        @{@"name":@"卫**",@"level":@"金牌医助"},
        @{@"name":@"姚**",@"level":@"金牌医助"},
        @{@"name":@"李**",@"level":@"金牌医助"},
        @{@"name":@"石**",@"level":@"金牌医助"},
        @{@"name":@"郭**",@"level":@"金牌医助"},
        @{@"name":@"张**",@"level":@"金牌医助"},
        @{@"name":@"聂**",@"level":@"金牌医助"},
        @{@"name":@"李**",@"level":@"金牌医助"},
        @{@"name":@"程**",@"level":@"金牌医助"},
        @{@"name":@"袁**",@"level":@"金牌医助"},
        @{@"name":@"武**",@"level":@"金牌医助"}
    ]];
}

@end
