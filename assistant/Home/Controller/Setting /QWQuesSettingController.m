//
//  QWQuesSettingController.m
//  assistant
//
//  Created by qunai on 2023/8/16.
//

#import "QWQuesSettingController.h"
#import "QWQuesSettingHeaderView.h"
#import "QWQuesSettingCell.h"
#import "QWQuesWebViewController.h"
#import "QWQuesAllController.h"
#import "QWRobotController.h"
#import "QWAIController.h"

@interface QWQuesSettingController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showDataArr;
@end

@implementation QWQuesSettingController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"系统设置";
    
    [self tableView];
    [self loadData];
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        [_tableView registerClass:[QWQuesSettingHeaderView class] forHeaderFooterViewReuseIdentifier:@"QWQuesSettingHeaderView"];
        //        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
        [_tableView registerClass:[QWQuesSettingCell class] forCellReuseIdentifier:@"QWQuesSettingCell"];
        
        _tableView.backgroundColor=[UIColor whiteColor];
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        //        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        //        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        //        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        //        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 60.0;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.showDataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *arr = self.showDataArr[section];
    return arr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QWQuesSettingCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWQuesSettingCell"];
    NSArray *arr = self.showDataArr[indexPath.section];
    cell.nameLabel.text =arr[indexPath.row];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.section == 0){
        NSArray *arr = self.showDataArr[indexPath.section];
        NSString *title =arr[indexPath.row];
        if(indexPath.row == 0){
            [self jumToWebView:@"https://mp.weixin.qq.com/s/PZgoAyk1X7jrz8W4tkk-gw" withTitle:title];
        }else if(indexPath.row == 1){
            [self jumToFileWebView:@"冠状动脉粥样硬化性心脏病诊断标准.ppt" withTitle:title];
        }else if(indexPath.row == 2){
            [self jumToFileWebView:@"心内评分量表.docx" withTitle:title];
        }
    }
    if(indexPath.section == 1){
        if(indexPath.row == 0){
            QWQuesAllController *quesAllVC =[[QWQuesAllController alloc]init];
            [self.navigationController pushViewController:quesAllVC animated:YES];
        }else if(indexPath.row == 1){
            QWRobotController *robotVC =[[QWRobotController alloc]init];
            [self.navigationController pushViewController:robotVC animated:YES];
        }else if(indexPath.row == 2){
            QWAIController *aiVC =[[QWAIController alloc]init];
            [self.navigationController pushViewController:aiVC animated:YES];
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    QWQuesSettingHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"QWQuesSettingHeaderView"];
    headerView.nameLabel.text = section == 0?@"医学资料":@"系统设置";
    headerView.iconImageView.image = [UIImage imageNamed:section == 0?@"ques_profile.png":@"ques_setting.png"];
    headerView.moreClickBlock = ^{
       
    };
    return headerView;
}

#pragma mark - 加载数据
- (void)loadData{
    self.showDataArr = [[NSMutableArray alloc]initWithArray:@[
        @[@"心衰指南文档",@"心脏病诊断资料",@"心脏健康评估量表"],
        @[@"问答题库设置",@"机器人自动诊断",@"人工智能"]
    ]];
    [self.tableView reloadData];
}

#pragma mark - webview - 跳转
- (void)jumToWebView:(NSString *)url withTitle:(NSString *)title{
    QWQuesWebViewController *webviewVC =[[QWQuesWebViewController alloc]init];
    webviewVC.title = title;
    webviewVC.url = url;
    webviewVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:webviewVC animated:YES];
}
- (void)jumToFileWebView:(NSString *)fileName withTitle:(NSString *)title{
    QWQuesWebViewController *webviewVC =[[QWQuesWebViewController alloc]init];
    webviewVC.title = title;
    webviewVC.mainFileNameStr = fileName;
    webviewVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:webviewVC animated:YES];
}
@end
