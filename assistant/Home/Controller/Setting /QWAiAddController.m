//
//  QWAiAddController.m
//  assistant
//
//  Created by qunai on 2023/8/24.
//

#import "QWAiAddController.h"
#import "QWDevelopmentView.h"
#import "QWQuestionHomeCell.h"
#import "QWAiAddCaseController.h"
#import "QWAiAddSubitemController.h"

@interface QWAiAddController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showDataArr;

@end

@implementation QWAiAddController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"录入";
    
    [self tableView];
    [self loadData];
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        //        [_tableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
        //        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
                [_tableView registerClass:[QWQuestionHomeCell class] forCellReuseIdentifier:@"QWQuestionHomeCell"];
        
        _tableView.backgroundColor=[UIColor whiteColor];
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        //        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        //        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        //        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        //        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0000001;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.showDataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QWQuestionHomeCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWQuestionHomeCell"];
    NSDictionary *dic =self.showDataArr[indexPath.row];
    NSString *name = dic[@"name"];
    NSString *color = dic[@"color"];
    cell.nameLabel.text = name;
    cell.bgView.backgroundColor = [UIColor colorFromHexString:color];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.row == 0){//病例
        QWAiAddCaseController *addCaseVC = [[QWAiAddCaseController alloc]init];
        [self.navigationController pushViewController:addCaseVC animated:YES];
    }
    if(indexPath.row == 1){//分项
        QWAiAddSubitemController *addSubitemVC = [[QWAiAddSubitemController alloc]init];
        [self.navigationController pushViewController:addSubitemVC animated:YES];
    }
}



#pragma mark - 加载数据
- (void)loadData{
    self.showDataArr = [NSMutableArray arrayWithArray:@[
        @{@"name":@"病例录入",@"color":@"#99CCFF"},
        @{@"name":@"分项录入",@"color":@"#FF9966"}
    ]];
    [self.tableView reloadData];
}
@end
