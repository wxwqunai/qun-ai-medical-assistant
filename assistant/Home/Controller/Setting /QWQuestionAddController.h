//
//  QWQuestionAddController.h
//  assistant
//
//  Created by qunai on 2023/7/26.
//

#import "QWBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWQuestionAddController : QWBaseController
@property (nonatomic,strong) NSArray *selSymptomArr;
@end

NS_ASSUME_NONNULL_END
