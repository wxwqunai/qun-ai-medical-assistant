//
//  QWRobotController.m
//  assistant
//
//  Created by qunai on 2023/8/17.
//

#import "QWRobotController.h"
#import "QWQuestModel.h"
#import "QWRobotHeaderView.h"
#import "QWRobotCell.h"
#import "QWRobotAddController.h"

@interface QWRobotController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showDataArr;
@property (nonatomic, strong) QWRobotHeaderView *headerView;
@end

@implementation QWRobotController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"诊断知识库";
    
    [self.rightBarButtonItem setTitle:@"添加"];
    
    [self tableView];
    [self loadData];
}

#pragma mark-导航条右边按钮
-(void)navRightBaseButtonClick
{
    QWRobotAddController *addVC = [[QWRobotAddController alloc]init];
    [self.navigationController pushViewController:addVC animated:YES];
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.tableHeaderView = [self headerView];
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        //        [_tableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
        //        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
                [_tableView registerClass:[QWRobotCell class] forCellReuseIdentifier:@"QWRobotCell"];
        
        _tableView.backgroundColor=[UIColor whiteColor];
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        //        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        //        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0000001;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.showDataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QWRobotCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWRobotCell"];
    QWQuesAiDiagnosticRulesModel *model = self.showDataArr[indexPath.section];
    cell.superVC = self;
    cell.model = model;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - 标题
- (QWRobotHeaderView *)headerView{
    if(!_headerView){
        _headerView =[[QWRobotHeaderView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 40)];
    }
    return _headerView;
}


#pragma mark - 加载数据
- (void)loadData{
    [SWHUDUtil showHudViewInSuperView:self.view withMessage:@"请等待"];
        HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_AiDiagnosticRules_LIST argument:@{}];
        [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
            [SWHUDUtil hideHudView];
            long code = [result[@"code"] longValue];
            if(code == 1){
                NSArray *resArr = result[@"data"];
                self.showDataArr =[QWQuesAiDiagnosticRulesModel mj_objectArrayWithKeyValuesArray:resArr];
                [self.tableView reloadData];
            }
            NSLog(@"");
        } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
            [SWHUDUtil hideHudView];
            //错误
            NSLog(@"%@",errorInfo);
        }];
}
@end
