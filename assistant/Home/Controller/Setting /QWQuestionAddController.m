//
//  QWQuestionAddController.m
//  assistant
//
//  Created by qunai on 2023/7/26.
//

#import "QWQuestionAddController.h"
#import "QWQuesAddTipsView.h"
#import "QWQuestModel.h"
#import "QWQuestTitleView.h"
#import "QWQuesAddBtnView.h"
#import "QWOptionInputCell.h"
#import "QWQuestHomeController.h"

@interface QWQuestionAddController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showDataArr;
@property (nonatomic, strong) QWQuesAddTipsView *tipsView;
@property (nonatomic, strong) QWQuesAddBtnView *addBtnView;
@end

@implementation QWQuestionAddController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"编辑";
    [self.rightBarButtonItem setTitle:@"提交"];

    self.view.backgroundColor = Color_TableView_Gray;
    
    [self tipsView];
    [self tableView];
    
    [self loadData];
}
#pragma mark-导航条右边按钮
-(void)navRightBaseButtonClick
{
    [SWHUDUtil hideHudViewWithMessageSuperView:self.view withMessage:@"提交成功，后台正在审核！"];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        for (UIViewController *controller in self.navigationController.viewControllers) {
                if ([controller isKindOfClass:[QWQuestHomeController class]]) {
                    QWQuestHomeController *quesHomeVC =(QWQuestHomeController *)controller;
                    [self.navigationController popToViewController:quesHomeVC animated:YES];
                }
            }
    });
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        [_tableView registerClass:[QWQuestTitleView class] forHeaderFooterViewReuseIdentifier:@"QWQuestTitleView"];
        [_tableView registerClass:[QWOptionInputCell class] forCellReuseIdentifier:@"QWOptionInputCell"];

        _tableView.backgroundColor=[UIColor clearColor];
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
//        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        _tableView.estimatedSectionHeaderHeight = 150;
//        _tableView.estimatedSectionFooterHeight = 100;
//        _tableView.sectionHeaderHeight = 50;

//        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
//        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        _tableView.tableFooterView = [self addBtnView];
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.equalTo(self.tipsView.mas_bottom);
            make.bottom.equalTo(self.view);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10.0;
}
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 0.0000001;
//}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.showDataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    QWAddQuestionModel *questModel = self.showDataArr[section];
    return questModel.options.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QWAddQuestionModel *questModel = self.showDataArr[indexPath.section];
    if([questModel.type isEqualToString:@"单选"] ||[questModel.type isEqualToString:@"多选"] ){
        QWOptionInputCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWOptionInputCell"];
        QWAddOptionModel *addOptionModel = questModel.options[indexPath.row];
        cell.optionModel = addOptionModel;
        cell.nameLabel.text = [NSString stringWithFormat:@"选项%ld：",indexPath.row+1];
        cell.optionControl = ^(QWOptionControl control) {
            [self optionControlForQuesttion:control withIndexPath:indexPath];
        };
        return cell;
    }
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"cell"];
    }
    cell.textLabel.text =[NSString stringWithFormat:@"cell：%ld",indexPath.row];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    QWQuestTitleView *titleView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"QWQuestTitleView"];
    QWAddQuestionModel *questModel = self.showDataArr[section];
    titleView.questModel= questModel;
    titleView.numLabel.text = [NSString stringWithFormat:@"%ld.",section+1];
    titleView.superVC = self;
    titleView.deleteQuestBlock = ^{
        [self deleteQuestion:section];
    };
    titleView.changeQuestTypeBlock = ^{
        //填空题不需要选项
        if([questModel.type isEqualToString:@"填空"]){
            [questModel.options removeAllObjects];
        }else{
            if(questModel.options.count<=0){
                [self optionControlForQuesttion:QWOptionControl_add withIndexPath:[NSIndexPath indexPathForRow:-1 inSection:section]];
                [self optionControlForQuesttion:QWOptionControl_add withIndexPath:[NSIndexPath indexPathForRow:0 inSection:section]];
            }
        }
        
        [tableView reloadData];
    };
    return titleView;
}

#pragma mark - 顶部提示
- (QWQuesAddTipsView *)tipsView{
    if(!_tipsView){
        _tipsView = [[QWQuesAddTipsView alloc]init];
        _tipsView.symptomsStr = [self selectSymptoms];
        [self.view addSubview:_tipsView];
        [_tipsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.view);
            make.top.equalTo(self.view).offset([UIDevice navigationFullHeight]);
        }];
    }
    return _tipsView;
}

#pragma mark - 添加问题-按钮
- (QWQuesAddBtnView *)addBtnView{
    if(!_addBtnView){
        _addBtnView = [[QWQuesAddBtnView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 44)];
        [self.view addSubview:_addBtnView];
        
        __weak __typeof(self) weakSelf = self;
        _addBtnView.addQuestBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf addQuestion];
        };
    }
    return _addBtnView;
}
#pragma mark - 添加问题
- (void)addQuestion{
    [self.showDataArr addObject:[self question]];
    [self.tableView reloadData];
}
#pragma mark - 删除问题
- (void)deleteQuestion:(NSInteger)num{
    [self.showDataArr removeObjectAtIndex:num];
    [self.tableView reloadData];
}

#pragma mark - 选项-添加/删除
- (void)optionControlForQuesttion:(QWOptionControl)control withIndexPath:(NSIndexPath *)indexPath{
    QWAddQuestionModel *questModel = self.showDataArr[indexPath.section];
    if(control == QWOptionControl_add && questModel.options.count<9){//加
        [questModel.options insertObject:[self option] atIndex:indexPath.row+1];
    }
    if(control == QWOptionControl_sub && questModel.options.count>1){//减
        [questModel.options removeObjectAtIndex:indexPath.row];
    }
    [self.tableView reloadData];
}

#pragma mark - 加载数据
- (void)loadData{
    self.showDataArr = [NSMutableArray arrayWithObjects:[self question],[self question], nil];
}

#pragma mark - 添加问题模版
- (QWAddQuestionModel *)question{
    
    NSDictionary *dic = @{
        @"title":@"",
        @"type":@"单选",
        @"options":@[@{@"content":@""},@{@"content":@""}],
    };
    QWAddQuestionModel *model = [QWAddQuestionModel mj_objectWithKeyValues:dic];
    
    return model;
}

#pragma mark - 添加问题模版
- (QWAddOptionModel *)option{
    NSDictionary *dic = @{@"content":@""};
    QWAddOptionModel *model = [QWAddOptionModel mj_objectWithKeyValues:dic];
    return model;
}

#pragma mark - 已选症状
- (NSString *)selectSymptoms{
    NSString * __block seletStr = @"";
    [self.selSymptomArr enumerateObjectsUsingBlock:^(QWSymptomModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        seletStr = [seletStr stringByAppendingFormat:idx == 0?@"%@":@"、%@",obj.content];
    }];
    return seletStr;
}

@end
