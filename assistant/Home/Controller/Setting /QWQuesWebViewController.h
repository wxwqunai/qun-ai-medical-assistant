//
//  QWQuesWebViewController.h
//  assistant
//
//  Created by qunai on 2023/8/17.
//

#import "QWBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWQuesWebViewController : QWBaseController
@property (nonatomic,copy) NSString *url;

@property (nonatomic,copy) NSString *mainFileNameStr;

@property (nonatomic,strong) NSURL *fileUrl;
@end

NS_ASSUME_NONNULL_END
