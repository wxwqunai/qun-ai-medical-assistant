//
//  QWRobotPickerController.h
//  assistant
//
//  Created by qunai on 2023/8/29.
//

#import <UIKit/UIKit.h>
#import "QWQuestModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^MakeSureBlock)(QWQuesAitimuListModel *timuModel);

@interface QWRobotPickerController : UIViewController
@property (nonatomic, strong) NSMutableArray *aitimuArr;
@property (nonatomic, copy)MakeSureBlock makeSureBlock;
@end

NS_ASSUME_NONNULL_END
