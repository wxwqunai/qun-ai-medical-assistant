//
//  QWQuesWebViewController.m
//  assistant
//
//  Created by qunai on 2023/8/17.
//

#import "QWQuesWebViewController.h"
#import <WebKit/WebKit.h>
@interface QWQuesWebViewController ()<WKNavigationDelegate>
@property (nonatomic,strong) WKWebView *webView;
@property (nonatomic,strong) UIProgressView *progressView;//进度条

@end

@implementation QWQuesWebViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = YES;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.rightBarButtonItem setImage:[UIImage imageNamed:@"ic_web_home.png"]];
    [self webView];
    [self progressView];
}
#pragma mark-导航条左边按钮
-(void)navLeftBaseButtonClick
{
    if([self.webView canGoBack]){
        [self.webView goBack];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}
#pragma mark-导航条右边按钮
-(void)navRightBaseButtonClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -网页
-(WKWebView *)webView
{
    if (!_webView) {
        _webView =[[WKWebView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
        _webView.navigationDelegate=self;
        [_webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];//监听进度条
        _webView.scrollView.showsVerticalScrollIndicator=NO;
        _webView.scrollView.showsHorizontalScrollIndicator=YES;
        if(!IsStringEmpty(self.url)){
            NSURLRequest *request =[[NSURLRequest alloc]initWithURL:[NSURL URLWithString:[self wrapUrl]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:5.0];
            [_webView loadRequest:request];
        }
        
        if(!IsStringEmpty(self.mainFileNameStr)){
            // 创建URL
            NSURL *url = [[NSBundle mainBundle] URLForResource:self.mainFileNameStr withExtension:nil];
            // 创建请求
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            // 通过url加载文件
            [_webView loadRequest:request];
        }
        
        if(self.fileUrl){
            NSURLRequest *request =[[NSURLRequest alloc]initWithURL:self.fileUrl cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:5.0];
            [_webView loadRequest:request];
        }
        
        
        [self.view addSubview:_webView];
        
        if ([_webView.scrollView respondsToSelector:@selector(setContentInsetAdjustmentBehavior:)]) {
            _webView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        
        [_webView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
        }];
    }
    return _webView;
}
// 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{
//    [SWHUDUtil showHudViewInSuperView:self.view withMessage:@""];
}
// 当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation
{
}
// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    if(IsStringEmpty(self.title) && !IsStringEmpty(webView.title)){
        self.title = webView.title;
    }
//    [SWHUDUtil hideHudView];
}
// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation
{
//    [SWHUDUtil hideHudView];
}

- (void)webView:(WKWebView*)webView decidePolicyForNavigationAction:(WKNavigationAction*)navigationAction
decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    
    decisionHandler(WKNavigationActionPolicyAllow);
}

#pragma mark - url
- (NSString *)wrapUrl{
    NSString *url = self.url;
    if(!IsStringEmpty(url) && [url containsString:QWM_DOMAIN]){
        url = [url stringByAppendingFormat:@"%@%@",[url containsString:@"?"]?@"&":@"?",@"from=app&operatingSystem=ios"];
        if(AppProfile.isLogined){
            url = [url stringByAppendingFormat:@"&autoTest=autoLogin&userName=%@&password=%@",AppProfile.qaUserInfo.phone,AppProfile.qaUserInfo.password];
        }
    }
    return url;
}


#pragma mark -进度条
-(UIProgressView *)progressView
{
    if (!_progressView) {
        _progressView=[[UIProgressView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 1)];
        _progressView.progressTintColor=[UIColor colorFromHexString:@"#97e070"];
        _progressView.trackTintColor=[UIColor whiteColor];
        [_webView addSubview:_progressView];
    }
    return _progressView;
}
// 计算wkWebView进度条
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (object == self.webView && [keyPath isEqualToString:@"estimatedProgress"]) {
        CGFloat newprogress = [[change objectForKey:NSKeyValueChangeNewKey] doubleValue];
        if (newprogress == 1) {
            [self.progressView setProgress:1 animated:NO];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.progressView.hidden = YES;
            });
        }else {
            [self.progressView setProgress:newprogress animated:YES];
            self.progressView.hidden = NO;
        }
    }
}


@end
