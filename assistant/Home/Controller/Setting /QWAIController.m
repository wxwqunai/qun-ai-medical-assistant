//
//  QWAIController.m
//  assistant
//
//  Created by qunai on 2023/8/17.
//

#import "QWAIController.h"
#import "QWQuestModel.h"
#import "QWAiHeaderView.h"
#import "QWAiCell.h"
#import "QWAiAddController.h"

@interface QWAIController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showDataArr;
@property (nonatomic, strong) QWAiHeaderView *headerView;

@end

@implementation QWAIController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"病例与结论";
    
    [self.rightBarButtonItem setTitle:@"录入"];
    
    [self tableView];
    [self loadData];
}

#pragma mark-导航条右边按钮
-(void)navRightBaseButtonClick
{
    QWAiAddController *addVC = [[QWAiAddController alloc]init];
    [self.navigationController pushViewController:addVC animated:YES];
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.tableHeaderView = [self headerView];
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        //        [_tableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
        //        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
                [_tableView registerClass:[QWAiCell class] forCellReuseIdentifier:@"QWAiCell"];
        
        _tableView.backgroundColor=[UIColor whiteColor];
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        //        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        //        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0000001;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.showDataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QWAiCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWAiCell"];
    cell.numLabel.text = [NSString stringWithFormat:@"%ld",indexPath.row+1];
    cell.superVC =self;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - 标题
- (QWAiHeaderView *)headerView{
    if(!_headerView){
        _headerView =[[QWAiHeaderView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 40)];
    }
    return _headerView;
}


#pragma mark - 加载数据
- (void)loadData{
    [SWHUDUtil showHudViewInSuperView:self.view withMessage:@"请等待"];
    [self loadPullDownRefreshDataInfo];
}

#pragma mark - 下拉刷新
- (void)loadPullDownRefreshDataInfo{
    self.showDataArr = [[NSMutableArray alloc]initWithArray:@[@{},@{},@{},@{},@{},@{},@{},@{},@{},@{},@{},@{},@{},@{},@{},@{},@{},@{},@{},@{}]];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [SWHUDUtil hideHudView];
        [self.tableView reloadData];
        [self.tableView judgeFooterState:self.showDataArr.count>20];
    });
}
#pragma mark - 下拉加载
- (void)loadPullUpRefreshDataInfo{
    
    [self.showDataArr addObjectsFromArray:@[@{},@{},@{},@{},@{}]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        [self.tableView judgeFooterState:self.showDataArr.count>50];
    });
}

@end
