//
//  QWAiAddSubitemController.m
//  assistant
//
//  Created by qunai on 2023/9/1.
//

#import "QWAiAddSubitemController.h"
#import "QWAiCaseInputCell.h"
#import "QWRobotAddConclusionCell.h"
#import "QWAiAddSubitemCell.h"

@interface QWAiAddSubitemController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showDataArr;

@property (nonatomic, strong) NSMutableArray *conditionArr; //条件
@property (nonatomic, strong) NSMutableArray *conclusionArr; //结论

@end

@implementation QWAiAddSubitemController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"分项录入";
    
    [self tableView];
    [self loadData];
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        //        [_tableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
        //        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
        [_tableView registerClass:[QWAiCaseInputCell class] forCellReuseIdentifier:@"QWAiCaseInputCell"];
        [_tableView registerClass:[QWRobotAddConclusionCell class] forCellReuseIdentifier:@"QWRobotAddConclusionCell"];
        [_tableView registerClass:[QWAiAddSubitemCell class] forCellReuseIdentifier:@"QWAiAddSubitemCell"];

        
        _tableView.backgroundColor=[UIColor whiteColor];
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        //        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        //        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        //        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        //        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return section == 0?0.0000001:44;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger row_num = 0;
    if(section ==0) row_num = 1;
    if(section == 1) row_num = self.conditionArr.count;
    if(section == 2) row_num = self.conclusionArr.count;
    return row_num;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        QWAiCaseInputCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWAiCaseInputCell"];
        return cell;
    }
    
    if(indexPath.section == 1){
        QWAiAddSubitemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWAiAddSubitemCell"];
        QWQuesAiAddSubitemModel *model = self.conditionArr[indexPath.row];
        cell.model =model;
        //加减
        cell.addOrSubControlBlock = ^(NSString * _Nonnull control) {
            if([control isEqualToString:@"+"]){
                [self addCondition];
            }
            if([control isEqualToString:@"-"]){
                [self subCondition:indexPath.row];
            }
        };
        return cell;
    }
    
    if(indexPath.section == 2){
        QWRobotAddConclusionCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWRobotAddConclusionCell"];
        QWAiConclusionModel *model = self.conclusionArr[indexPath.row];
        cell.model = model;
        cell.numLabel.text = [NSString stringWithFormat:@"结论%ld",indexPath.row+1];
        cell.isHiddenTips = indexPath.row != 0;
        //加减
        cell.addOrSubControlBlock = ^(NSString * _Nonnull control) {
            if([control isEqualToString:@"+"]){
                [self addConclusion];
            }
            if([control isEqualToString:@"-"]){
                [self subConclusion:indexPath.row];
            }
        };
        return cell;
    }
    
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"cell"];
    }
    cell.textLabel.text =[NSString stringWithFormat:@"cell：%ld",indexPath.row];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    CGRect rect = [tableView rectForHeaderInSection:section];
    UIView *view =[[UIView alloc]initWithFrame:rect];
    view.backgroundColor = [UIColor colorFromHexString:@"#AFEEEE"];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(14, 0, rect.size.width, rect.size.height)];
    label.textColor = [UIColor colorFromHexString:@"#4169E1"];
    NSString *name = @"";
    if(section == 1) name = @"判断项";
    if(section == 2) name = @"结论";
    label.text = name;
    label.font = [UIFont boldSystemFontOfSize:20];
    [view addSubview:label];
    return view;
}

#pragma mark - 条件-增
- (void)addCondition{
//    if(self.conditionArr.count >= 3)return;
    [self.conditionArr addObject:[QWQuesAiAddSubitemModel new]];
    [self.tableView reloadData];
}
#pragma mark - 条件-删
- (void)subCondition:(NSInteger)index_row{
    if(self.conditionArr.count == 1)return;
    [self.conditionArr removeObjectAtIndex:index_row];
    [self.tableView reloadData];
}
#pragma mark - 条件-改
- (void)changeCondition:(NSInteger)index_row{
    [self.conditionArr replaceObjectAtIndex:index_row withObject:[QWQuesAiAddSubitemModel new]];
    [self.tableView reloadData];
}

#pragma mark - 结论-增
- (void)addConclusion{
//    if(self.conclusionArr.count >= 3)return;
    [self.conclusionArr addObject:[QWAiConclusionModel new]];
    [self.tableView reloadData];
}
#pragma mark - 结论-删
- (void)subConclusion:(NSInteger)index_row{
    if(self.conclusionArr.count == 1)return;
    [self.conclusionArr removeObjectAtIndex:index_row];
    [self.tableView reloadData];
}
#pragma mark - 条件-改
- (void)changeConclusion:(NSInteger)index_row{
    [self.conditionArr replaceObjectAtIndex:index_row withObject:[QWAiConclusionModel new]];
    [self.tableView reloadData];
}

#pragma mark - 加载数据
- (void)loadData{
    [self.conditionArr addObject:[QWQuesAiAddSubitemModel new]];
    [self.conditionArr addObject:[QWQuesAiAddSubitemModel new]];
    
    [self.conclusionArr addObject:[QWAiConclusionModel new]];
    [self.conclusionArr addObject:[QWAiConclusionModel new]];
    
    [self.tableView reloadData];
}
- (NSMutableArray *)conditionArr{
    if(!_conditionArr){
        _conditionArr = [[NSMutableArray alloc]init];
    }
    return _conditionArr;
}
- (NSMutableArray *)conclusionArr{
    if(!_conclusionArr){
        _conclusionArr = [[NSMutableArray alloc]init];
    }
    return _conclusionArr;
}

@end
