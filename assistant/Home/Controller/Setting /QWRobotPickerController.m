//
//  QWRobotPickerController.m
//  assistant
//
//  Created by qunai on 2023/8/29.
//

#import "QWRobotPickerController.h"

@interface QWRobotPickerController ()<UIPickerViewDelegate,UIPickerViewDataSource>
@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UIView *btnsView;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIButton *sureButton;

@property (nonatomic, strong) UIPickerView *pickerView;
@end

@implementation QWRobotPickerController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self addCloseGesture]; //手势关闭
    [self bgView];
    
    [self btnsView];
    [self cancelButton];
    [self sureButton];
    
    [self pickerView];
    
    [self loadData];
    
}
#pragma mark - bgView
- (UIView *)bgView{
    if(!_bgView){
        _bgView =[[UIView alloc]init];
        _bgView.backgroundColor = Color_Main_Green;
        [self.view addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.view);
            make.height.equalTo(self.view).multipliedBy(0.35);
        }];
    }
    return _bgView;
}

#pragma mark - 取消/关闭
- (UIView *)btnsView{
    if(!_btnsView){
        _btnsView =[[UIView alloc]init];
        _btnsView.backgroundColor = [UIColor clearColor];
        [self.bgView addSubview:_btnsView];
        
        [_btnsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView);
            make.right.equalTo(_bgView);
            make.top.equalTo(_bgView);
            make.height.mas_offset(52.0);
        }];
        
        
        UIImageView *lineH = [[UIImageView alloc]init];
        lineH.backgroundColor = Color_Line_Gray;
        [_btnsView addSubview:lineH];
        [lineH mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_btnsView);
            make.right.equalTo(_btnsView);
            make.bottom.equalTo(_btnsView);
            make.height.mas_offset(0.8);
        }];
        
        UIImageView *lineV = [[UIImageView alloc]init];
        lineV.backgroundColor = Color_Line_Gray;
        [_btnsView addSubview:lineV];
        [lineV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_btnsView);
            make.bottom.equalTo(_btnsView);
            make.centerX.equalTo(_btnsView);
            make.width.mas_offset(0.8);
        }];
        
    }
    return _btnsView;
}

#pragma mark - 取消
- (UIButton *)cancelButton{
    if(!_cancelButton){
        _cancelButton= [self customButton];
        [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        [_cancelButton addTarget:self action:@selector(cancelButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [_btnsView addSubview:_cancelButton];
        
        [_cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_btnsView.mas_right).multipliedBy(0.1);
            make.top.equalTo(_btnsView).offset(6.0);
            make.bottom.equalTo(_btnsView).offset(-6.0);
            make.width.equalTo(_btnsView).multipliedBy(0.3);
        }];
    }
    return _cancelButton;
}
- (void)cancelButtonClick{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - 确定
- (UIButton *)sureButton{
    if(!_sureButton){
        _sureButton= [self customButton];
        [_sureButton setTitle:@"确定" forState:UIControlStateNormal];
        [_sureButton addTarget:self action:@selector(sureButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [_btnsView addSubview:_sureButton];
        
        [_sureButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_btnsView.mas_right).multipliedBy(0.9);
            make.top.equalTo(_btnsView).offset(6.0);
            make.bottom.equalTo(_btnsView).offset(-6.0);
            make.width.equalTo(_btnsView).multipliedBy(0.3);
        }];
    }
    return _sureButton;
}
- (void)sureButtonClick{
    [self dismissViewControllerAnimated:YES completion:nil];
    NSInteger row = [_pickerView selectedRowInComponent:0];
    QWQuesAitimuListModel *model = self.aitimuArr[row];
    if(self.makeSureBlock) self.makeSureBlock(model);
}
#pragma mark - pickerView
-(UIPickerView *)pickerView{
    if(!_pickerView){
        _pickerView = [[UIPickerView alloc]init];
        _pickerView.delegate = self;
        _pickerView.dataSource = self;
        [_bgView addSubview:_pickerView];
        
        [_pickerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView);
            make.right.equalTo(_bgView);
            make.top.equalTo(_btnsView.mas_bottom);
            make.bottom.equalTo(_bgView);
        }];
    }
    return _pickerView;
}

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.aitimuArr.count;
}

#pragma mark - UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    QWQuesAitimuListModel *model = self.aitimuArr[row];
    NSString *showStr = [NSString stringWithFormat:@"%ld.%@",[model.q_id integerValue]+1,model.q_title];
    return showStr;
}



#pragma mark - 关闭
- (void)addCloseGesture{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer:tap];
}
- (void)tapAction:(UITapGestureRecognizer *)tap{
    CGPoint point = [tap locationInView:self.view];
    if(!CGRectContainsPoint(self.bgView.frame, point)){
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


#pragma mark - 公共
- (UIButton *)customButton{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:17];
    button.layer.cornerRadius = 4.0;
    button.layer.masksToBounds = YES;
    button.backgroundColor = [UIColor colorFromHexString:@"#4682B4"];
    return button;
}


#pragma mark - 数据加载
- (void)loadData{
   if(!self.aitimuArr.count) [self loadAitimuList];
}

#pragma mark - 所有题目
- (void)loadAitimuList{
    [SWHUDUtil showHudViewInSuperView:self.view withMessage:@"请等待"];
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_aitimu_list argument:@{}];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        [SWHUDUtil hideHudView];
        NSArray *resultArr = (NSArray *)result;
        self.aitimuArr = [QWQuesAitimuListModel mj_objectArrayWithKeyValuesArray:resultArr];
        [self.pickerView reloadAllComponents];
        NSLog(@"");
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        [SWHUDUtil hideHudView];
        //错误
        NSLog(@"%@",errorInfo);
    }];
}
@end
