//
//  QWAiAddCaseController.m
//  assistant
//
//  Created by qunai on 2023/8/31.
//

#import "QWAiAddCaseController.h"
#import "QWAiCaseInputCell.h"
#import "QWAiCaseInputViewCell.h"
#import "QWAiFileHeaderView.h"
#import "QWAiFileCell.h"
#import "QWQuesWebViewController.h"

@interface QWAiAddCaseController ()<UITableViewDelegate,UITableViewDataSource,UIDocumentPickerDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showDataArr;

@property (nonatomic, strong) NSMutableArray *fileArr;
@property (nonatomic, assign) NSInteger select_file_row_num;
@end

@implementation QWAiAddCaseController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"病例录入";
    [self.rightBarButtonItem setTitle:@"提交"];
    
    [self tableView];
    [self loadData];
}

#pragma mark-导航条右边按钮
-(void)navRightBaseButtonClick
{
    [SWHUDUtil hideHudViewWithMessageSuperView:self.view withMessage:@"维护中！"];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
    });
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        //        [_tableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
        //        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
        [_tableView registerClass:[QWAiCaseInputCell class] forCellReuseIdentifier:@"QWAiCaseInputCell"];
        [_tableView registerClass:[QWAiCaseInputViewCell class] forCellReuseIdentifier:@"QWAiCaseInputViewCell"];
        [_tableView registerClass:[QWAiFileCell class] forCellReuseIdentifier:@"QWAiFileCell"];
        
        _tableView.backgroundColor=[UIColor whiteColor];
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        //        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        //        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        //        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        //        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return section == 1? 40:0.0000001;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger row_num = 0;
    if(section ==0) row_num = 4;
    if(section ==1) row_num = self.fileArr.count;
    if(section ==2) row_num = 1;
    return row_num;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        if(indexPath.row == 0){
            QWAiCaseInputCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWAiCaseInputCell"];
            return cell;
        }
        if(indexPath.row == 1){
            QWAiCaseInputViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWAiCaseInputViewCell"];
            cell.nameStr = @"主诉";
            return cell;
        }
        
        if(indexPath.row == 2){
            QWAiCaseInputViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWAiCaseInputViewCell"];
            cell.nameStr = @"现病史";
            return cell;
        }
        if(indexPath.row == 3){
            QWAiCaseInputViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWAiCaseInputViewCell"];
            cell.nameStr = @"既病史";
            return cell;
        }
    }
    
    if(indexPath.section == 1){
        QWAiFileCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWAiFileCell"];
        NSDictionary *dic = self.fileArr[indexPath.row];
        cell.dataDic = dic;
        cell.superVC = self;
        cell.fileChooseOpenBlock = ^{
            [self openFileChoose:indexPath.row];
        };
        cell.openFileBlock = ^{
            [self openFile:indexPath.row];
        };
        cell.fileDeleteBlock = ^{
            [self fileDelete:indexPath.row];
        };
        return cell;
    }
    
    if(indexPath.section == 2){
        if(indexPath.row == 0){
            QWAiCaseInputViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWAiCaseInputViewCell"];
            cell.nameStr = @"结论建议";
            return cell;
        }
    }
    
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"cell"];
    }
    cell.textLabel.text =[NSString stringWithFormat:@"cell：%ld",indexPath.row];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(section ==1){
        CGRect rect = [tableView rectForHeaderInSection:section];
        QWAiFileHeaderView *view =[[QWAiFileHeaderView alloc]initWithFrame:rect];
        return view;
    }
    
    return [UIView new];
}


#pragma mark - 文件选择-添加
- (void)openFileChoose:(NSInteger)row_index{
    _select_file_row_num = row_index;
    
    NSArray *documentTypes = @[@"public.text",
                               @"public.content",
                               @"public.source-code",
                               @"public.image",
                               @"public.movie",
                               @"public.audio",
                               @"public.audiovisual-content",
                               @"com.adobe.pdf",
                               @"com.apple.keynote.key",
                               @"com.microsoft.word.doc",
                               @"com.microsoft.word.docx",
                               @"com.microsoft.excel.xls",
                               @"com.microsoft.excel.xlsx",
                               @"com.microsoft.powerpoint.ppt"];
    
    UIDocumentPickerViewController *documentPickerViewController = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:documentTypes inMode:UIDocumentPickerModeImport];
    documentPickerViewController.delegate = self;
    [self presentViewController:documentPickerViewController animated:YES completion:nil];
}
- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentsAtURLs:(nonnull NSArray<NSURL *> *)urls{
    // 获取授权
//    BOOL fileUrlAuthozied = [urls.firstObject startAccessingSecurityScopedResource];
//    if (!fileUrlAuthozied) {
//        return;
//    }
    // 通过文件协调工具来得到新的文件地址，以此得到文件保护功能
    NSFileCoordinator *fileCoordinator = [[NSFileCoordinator alloc] init];
    NSError *error;
    
    [fileCoordinator coordinateReadingItemAtURL:urls.firstObject options:0 error:&error byAccessor:^(NSURL *newURL) {
        // 读取文件
        NSString *fileName = [newURL lastPathComponent];
        NSError *error = nil;
        NSData *fileData = [NSData dataWithContentsOfURL:newURL options:NSDataReadingMappedIfSafe error:&error];
        if (error) {
            // 读取出错
        } else {
            
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithDictionary:self.fileArr[_select_file_row_num]];
            [dic setValue:fileName forKey:@"fileName"];
            [dic setValue:fileData forKey:@"file"];
            [dic setValue:urls.firstObject forKey:@"fileUrl"];
            
            [self.fileArr replaceObjectAtIndex:_select_file_row_num withObject:dic];
            [self.tableView reloadData];
            
            // 上传
            NSLog(@"fileName : %@", fileName);
            // [self uploadingWithFileData:fileData fileName:fileName fileURL:newURL];
        }
//                [self.superVC dismissViewControllerAnimated:YES completion:NULL];
    }];
//    [urls.firstObject stopAccessingSecurityScopedResource];
}

#pragma mark - 文件选择-删除
- (void)fileDelete:(NSInteger)row_num{
    if(self.fileArr.count>row_num){
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithDictionary:self.fileArr[row_num]];
        [dic setValue:@"" forKey:@"fileName"];
        [dic setValue:@"" forKey:@"file"];
        [dic setValue:@"" forKey:@"fileUrl"];
        
        [self.fileArr replaceObjectAtIndex:row_num withObject:dic];
        [self.tableView reloadData];
    }
}

#pragma mark - 打开文件
- (void)openFile:(NSInteger)row_num{
    NSDictionary *dic = self.fileArr[row_num];
    NSURL *fileUrl = dic[@"fileUrl"];
    NSString *fileName = dic[@"fileName"];
    QWQuesWebViewController *webviewVC =[[QWQuesWebViewController alloc]init];
    webviewVC.title = fileName;
    webviewVC.fileUrl = fileUrl;
    webviewVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:webviewVC animated:YES];
}

#pragma mark - 加载数据
- (void)loadData{
    
    self.fileArr = [[NSMutableArray alloc]initWithArray:@[@{@"fileTypeName":@"病例报告",@"fileName":@""},
                                                          @{@"fileTypeName":@"心电图",@"fileName":@""},
                                                          @{@"fileTypeName":@"心脏超声",@"fileName":@""},
                                                          @{@"fileTypeName":@"化验单",@"fileName":@""},
                                                          @{@"fileTypeName":@"视频文件",@"fileName":@""},
                                                          @{@"fileTypeName":@"其他文件",@"fileName":@""}]];
    [self.tableView reloadData];
}


@end
