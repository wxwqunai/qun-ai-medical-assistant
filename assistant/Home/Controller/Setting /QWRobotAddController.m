//
//  QWRobotAddController.m
//  assistant
//
//  Created by qunai on 2023/8/24.
//

#import "QWRobotAddController.h"
#import "QWQuesBtnBottomView.h"
#import "QWRobotAddConclusionCell.h"
#import "QWRobotAddConditionCell.h"
#import "QWDevelopmentView.h"
#import "QWRobotPickerController.h"
#import "QWQuestModel.h"

@interface QWRobotAddController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *conditionArr; //条件
@property (nonatomic, strong) NSMutableArray *conclusionArr; //结论
@property (nonatomic, strong) NSMutableArray *aitimuArr;
@property (nonatomic, strong) QWQuesBtnBottomView *bottomView;
@end

@implementation QWRobotAddController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"添加知识";
    QWDevelopmentView *view = [[QWDevelopmentView alloc]init];
    [self.view addSubview:view];
    
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.centerY.equalTo(self.view);
    }];
    
    [self bottomView];
    [self tableView];
    [self loadData];
    
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        //        [_tableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
        //        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
        [_tableView registerClass:[QWRobotAddConditionCell class] forCellReuseIdentifier:@"QWRobotAddConditionCell"];
        [_tableView registerClass:[QWRobotAddConclusionCell class] forCellReuseIdentifier:@"QWRobotAddConclusionCell"];

        
        _tableView.backgroundColor=[UIColor whiteColor];
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        //        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        //        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        //        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        //        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.equalTo(self.view);
            make.bottom.equalTo(self.bottomView.mas_top);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger num = 0;
    if(section == 0) num = self.conditionArr.count;
    if(section == 1) num = self.conclusionArr.count;
    return num;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        QWRobotAddConditionCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWRobotAddConditionCell"];
        QWQuesAitimuListModel *model = self.conditionArr[indexPath.row];
        cell.model = model;
        cell.numLabel.text = [NSString stringWithFormat:@"条件%ld",indexPath.row+1];
        //题目选择
        cell.topicNumChooseBlock = ^{
            [self changeCondition:indexPath.row];
        };
        //加减
        cell.addOrSubControlBlock = ^(NSString * _Nonnull control) {
            if([control isEqualToString:@"+"]){
                [self addCondition];
            }
            if([control isEqualToString:@"-"]){
                [self subCondition:indexPath.row];
            }
        };
        return cell;
    }else{
        QWRobotAddConclusionCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QWRobotAddConclusionCell"];
        QWAiConclusionModel *model = self.conclusionArr[indexPath.row];
        cell.model = model;
        cell.numLabel.text = [NSString stringWithFormat:@"结论%ld",indexPath.row+1];
        cell.isHiddenTips = indexPath.row != 0;
        //加减
        cell.addOrSubControlBlock = ^(NSString * _Nonnull control) {
            if([control isEqualToString:@"+"]){
                [self addConclusion];
            }
            if([control isEqualToString:@"-"]){
                [self subConclusion:indexPath.row];
            }
        };
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    CGRect rect = [tableView rectForHeaderInSection:section];
    UIView *view =[[UIView alloc]initWithFrame:rect];
    view.backgroundColor = [UIColor colorFromHexString:@"#AFEEEE"];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(14, 0, rect.size.width, rect.size.height)];
    label.textColor = [UIColor colorFromHexString:@"#4169E1"];
    NSString *name = @"";
    if(section == 0) name = @"添加条件(最多3个)";
    if(section == 1) name = @"添加结论或建议(最多3个)";
    label.text = name;
    label.font = [UIFont boldSystemFontOfSize:20];
    [view addSubview:label];
    return view;
}

#pragma mark - 条件-增
- (void)addCondition{
    if(self.conditionArr.count >= 3)return;
    QWRobotPickerController *pickerVC = [self showTopicNumChooseView];
    pickerVC.makeSureBlock = ^(QWQuesAitimuListModel * _Nonnull timuModel) {
        [self.conditionArr addObject:timuModel];
        [self.tableView reloadData];
    };
}
#pragma mark - 条件-删
- (void)subCondition:(NSInteger)index_row{
    if(self.conditionArr.count == 1)return;
    [self.conditionArr removeObjectAtIndex:index_row];
    [self.tableView reloadData];
}
#pragma mark - 条件-改
- (void)changeCondition:(NSInteger)index_row{
    QWRobotPickerController *pickerVC = [self showTopicNumChooseView];
    pickerVC.makeSureBlock = ^(QWQuesAitimuListModel * _Nonnull timuModel) {
        [self.conditionArr replaceObjectAtIndex:index_row withObject:timuModel];
        [self.tableView reloadData];
    };
}
#pragma mark - 条件 - 题目选择
- (QWRobotPickerController *)showTopicNumChooseView{
    QWRobotPickerController *pickerVC =[[QWRobotPickerController alloc]init];
//    pickerVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    pickerVC.aitimuArr = [self deleteChoosedTimu];
    pickerVC.modalPresentationStyle=UIModalPresentationOverFullScreen;
    [self presentViewController:pickerVC animated:YES completion:nil];
    return pickerVC;
}
//删除已选题目
- (NSMutableArray *)deleteChoosedTimu{
    
    NSMutableArray *arr = [NSMutableArray arrayWithArray:self.aitimuArr];
    [self.conditionArr enumerateObjectsUsingBlock:^(QWQuesAitimuListModel * _Nonnull conditionModel, NSUInteger idx, BOOL * _Nonnull stop) {
        [arr enumerateObjectsUsingBlock:^(QWQuesAitimuListModel *  _Nonnull timuModel, NSUInteger idx, BOOL * _Nonnull stop) {
            if([conditionModel.id isEqualToString:timuModel.id]){
                [arr removeObject:timuModel];
            }
        }];
    }];
    
    return arr;
}

#pragma mark - 结论-增
- (void)addConclusion{
    if(self.conclusionArr.count >= 3)return;
    [self.conclusionArr addObject:[QWAiConclusionModel new]];
    [self.tableView reloadData];
}
#pragma mark - 结论-删
- (void)subConclusion:(NSInteger)index_row{
    if(self.conclusionArr.count == 1)return;
    [self.conclusionArr removeObjectAtIndex:index_row];
    [self.tableView reloadData];
}
#pragma mark - 条件-改
- (void)changeConclusion:(NSInteger)index_row{
    [self.conditionArr replaceObjectAtIndex:index_row withObject:[QWAiConclusionModel new]];
    [self.tableView reloadData];
}



#pragma mark - 提交
- (QWQuesBtnBottomView *)bottomView{
    if(!_bottomView){
        _bottomView = [[QWQuesBtnBottomView alloc]init];
        _bottomView.btnName = @"提交";
        _bottomView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_bottomView];
        [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.view);
            make.bottom.equalTo(self.view);
            make.height.mas_offset([UIDevice tabBarFullHeight]+10);
        }];
        
        __weak __typeof(self) weakSelf = self;
        _bottomView.confirmClickBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [SWHUDUtil hideHudViewWithMessageSuperView:strongSelf.view withMessage:@"提交成功，等待审核！"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [strongSelf.navigationController popViewControllerAnimated:YES];
            });
        };
    }
    return _bottomView;
}

#pragma mark - 加载数据
- (void)loadData{
    [self loadAitimuList];
}

#pragma mark - 所有题目
- (void)loadAitimuList{
    [SWHUDUtil showHudViewInSuperView:self.view withMessage:@"请等待"];
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_aitimu_list argument:@{}];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        [SWHUDUtil hideHudView];
        NSArray *resultArr = (NSArray *)result;
        self.aitimuArr = [QWQuesAitimuListModel mj_objectArrayWithKeyValuesArray:resultArr];
        
        //默认数据展示
        if(self.aitimuArr.count>0)[self.conditionArr addObject:self.aitimuArr[0]];
        if(self.aitimuArr.count>1)[self.conditionArr addObject:self.aitimuArr[1]];
        
        if(self.aitimuArr.count>0)[self.conclusionArr addObject:[QWAiConclusionModel new]];
        if(self.aitimuArr.count>1)[self.conclusionArr addObject:[QWAiConclusionModel new]];
        
        [self.tableView reloadData];
        NSLog(@"");
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        [SWHUDUtil hideHudView];
        //错误
        NSLog(@"%@",errorInfo);
    }];
}

- (NSMutableArray *)conditionArr{
    if(!_conditionArr){
        _conditionArr = [[NSMutableArray alloc]init];
    }
    return _conditionArr;
}
- (NSMutableArray *)conclusionArr{
    if(!_conclusionArr){
        _conclusionArr = [[NSMutableArray alloc]init];
    }
    return _conclusionArr;
}
@end
