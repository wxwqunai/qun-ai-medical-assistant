//
//  QWManageController.m
//  assistant
//
//  Created by qunai on 2023/8/11.
//

#import "QWManageController.h"
#import "QWPatiMedicalRecordCell.h" //就诊记录cell
#import "QWPatiMyDoctorCell.h" //我的医生cell
#import "QWDocPatientsRecordCell.h" //接诊记录cell
#import "QWDocMyPatientCell.h" //我的患者cell
#import "QWPatientDetailController.h" //患者详情
#import "QWQuesBtnBottomView.h"
#import "QWDoctorsController.h"
#import "QWManageModel.h"


@interface QWManageController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, copy) NSString *usertype;
@property (nonatomic, strong) UISegmentedControl *segment;

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *showDataArr;
@property (nonatomic, strong) NSArray *resultArr;

@property (nonatomic, strong) QWQuesBtnBottomView *bottomView;

@end

@implementation QWManageController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"医患管理";
    self.usertype = AppProfile.qaUserInfo.usertype;
    
    if([self.usertype isEqualToString:@"doctor"] || [self.usertype isEqualToString:@"patient"]){
        [self segment];
        [self bottomView];
    }

    [self tableView];
    [self loadData];
    
}

#pragma mark - 顶部切换
- (UISegmentedControl *)segment{
    if(!_segment){
        //初始化一个UISegmentedControl控件test
        
        NSArray *items;
        if([self.usertype isEqualToString:@"doctor"]){
            items = @[@"接诊记录",@"我的患者"];
        }
        if([self.usertype isEqualToString:@"patient"]){
            items = @[@"就诊记录",@"我的医生"];
        }
        _segment = [[UISegmentedControl alloc]initWithItems:items];
        _segment.frame = CGRectMake(0, [UIDevice navigationFullHeight]+2, kScreenWidth, 44);

        //设置无边框分段控制器

        //设置test控件的颜色为透明
        _segment.tintColor = [UIColor clearColor];
        //定义选中状态的样式selected，类型为字典
        NSDictionary *selected = @{NSFontAttributeName:[UIFont systemFontOfSize:20],
                                   NSForegroundColorAttributeName:Color_Main_Green};
        //定义未选中状态下的样式normal，类型为字典
        NSDictionary *normal = @{NSFontAttributeName:[UIFont systemFontOfSize:14],
                                     NSForegroundColorAttributeName:[UIColor blackColor]};

        /*
        NSFontAttributeName -> 系统宏定义的特殊键，用来给格式字典中的字体赋值
        NSForegroundColorAttributeName -> 系统宏定义的特殊键，用来给格式字典中的字体颜色赋值
        */

        //通过setTitleTextAttributes: forState: 方法来给test控件设置文字内容的格式
        [_segment setTitleTextAttributes:normal forState:UIControlStateNormal];
        [_segment setTitleTextAttributes:selected forState:UIControlStateSelected];
        
        [_segment addTarget:self action:@selector(click:) forControlEvents:UIControlEventValueChanged];

        //设置test初始状态下的选中下标
        _segment.selectedSegmentIndex = 0;

        //将test添加到某个View上
        [self.view addSubview:_segment];
    }
    return _segment;
}

- (void)click:(UISegmentedControl *)segment{
//    if([self.usertype isEqualToString:@"patient"]){
//        self.type = segment.selectedSegmentIndex == 0?QWManageType_patient_medicalRecord:QWManageType_patient_myDoctor;
//    }else{
//        self.type = segment.selectedSegmentIndex == 0?QWManageType_doctor_patientsRecord:QWManageType_doctor_myPatient;
//    }
    [self changeChooseToShowData];
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        //        [_tableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
        //        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
        [_tableView registerClass:[QWPatiMedicalRecordCell class] forCellReuseIdentifier:@"QWPatiMedicalRecordCell"];
        [_tableView registerClass:[QWPatiMyDoctorCell class] forCellReuseIdentifier:@"QWPatiMyDoctorCell"];
        [_tableView registerClass:[QWDocPatientsRecordCell class] forCellReuseIdentifier:@"QWDocPatientsRecordCell"];
        [_tableView registerClass:[QWDocMyPatientCell class] forCellReuseIdentifier:@"QWDocMyPatientCell"];
        
        _tableView.backgroundColor=Color_TableView_Gray;
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        //        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        //        _tableView.estimatedSectionHeaderHeight = 100;
        //        _tableView.estimatedSectionFooterHeight = 100;
        //        _tableView.sectionHeaderHeight = 50;
        
        //        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
        //        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        if([self.usertype isEqualToString:@"doctor"] || [self.usertype isEqualToString:@"patient"]){
            [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.view);
                make.right.equalTo(self.view);
                make.top.equalTo(self.segment.mas_bottom).offset(2);
                make.bottom.equalTo(self.bottomView.mas_top);
            }];
        }else{
            [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(self.view);
            }];
        }
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0000001;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.showDataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.usertype isEqualToString:@"patient"]){
        if(self.segment.selectedSegmentIndex == 0){
            QWPatiMedicalRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWPatiMedicalRecordCell"];
            NSDictionary *dic = self.showDataArr[indexPath.row];
            cell.nameLabel.text = dic[@"name"];
            cell.companyLabel.text = dic[@"companyId"];
            cell.officeLabel.text = dic[@"officeId"];
            cell.timeLabel.text = dic[@"time"];
            return cell;
        }else{
            QWPatiMyDoctorCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWPatiMyDoctorCell"];
            cell.superVC = self;
            QWManageModel *model = self.showDataArr[indexPath.row];
            cell.model = model;
            return cell;
        }
    }else if([self.usertype isEqualToString:@"doctor"]){
        if(self.segment.selectedSegmentIndex == 0){
            QWDocPatientsRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWDocPatientsRecordCell"];
            NSDictionary *dic = self.showDataArr[indexPath.row];
            cell.nameLabel.text = [NSString stringWithFormat:@"%@, %@, %@岁",dic[@"name"],dic[@"sex"],dic[@"age"]];
            cell.symptomLabel.text = dic[@"symptoms"];
            cell.timeLabel.text = dic[@"createDate"];
            return cell;
        }else{
            QWDocMyPatientCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWDocMyPatientCell"];
            QWManageModel *model = self.showDataArr[indexPath.row];
            cell.model = model;
            cell.superVC =self;
            return cell;
        }
    }else{
        QWPatiMyDoctorCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWPatiMyDoctorCell"];
        cell.superVC = self;
        QWManageModel *model = self.showDataArr[indexPath.row];
        cell.model = model;
        return cell;
    }

//    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell"];
//    if(!cell){
//        cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"cell"];
//    }
//    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if([self.usertype isEqualToString:@"doctor"]){
        QWPatientDetailController *patientDetailVC = [[QWPatientDetailController alloc]init];
        [self.navigationController pushViewController:patientDetailVC animated:YES];
    }

}

#pragma mark - 下一步
- (QWQuesBtnBottomView *)bottomView{
    if(!_bottomView){
        _bottomView = [[QWQuesBtnBottomView alloc]init];
        _bottomView.btnName = [self.usertype isEqualToString:@"patient"]?@"添加就诊记录":@"添加接诊记录";
        _bottomView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_bottomView];
        [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.view);
            make.bottom.equalTo(self.view);
            make.height.mas_offset([UIDevice tabBarFullHeight]+10);
        }];
        
        __weak __typeof(self) weakSelf = self;
        _bottomView.confirmClickBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if([strongSelf.usertype isEqualToString:@"patient"]){
                QWDoctorsController *docsVC = [[QWDoctorsController alloc]init];
                [strongSelf.navigationController pushViewController:docsVC animated:YES];
            }else{
                [SWHUDUtil hideHudViewWithMessageSuperView:strongSelf.view withMessage:@"维护中！"];
            }
            
            
        };
    }
    return _bottomView;
}

#pragma mark - 加载数据
- (void)loadData{
    if(IsStringEmpty(AppProfile.qaUserInfo.id)) return;
    NSDictionary *dic = @{
        @"userId":AppProfile.qaUserInfo.id,
        @"usertype":@""
    };
    [SWHUDUtil showHudViewInSuperView:self.view withMessage:@"请等待"];
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_Doctorpatient argument:dic];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success1) {
        [SWHUDUtil hideHudView];
        NSArray *resArr = (NSArray *)result;
        self.resultArr = [QWManageModel mj_objectArrayWithKeyValuesArray:resArr];
        [self changeChooseToShowData];
        NSLog(@"");
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        [SWHUDUtil hideHudView];
        //错误
        NSLog(@"%@",errorInfo);
    }];
    
    [self changeChooseToShowData];
}
- (void)changeChooseToShowData{
    
    NSArray *patientRecord = @[@{
        @"name":@"赵医生",
        @"companyId":@"西安市和平医院",
        @"officeId":@"心内科",
        @"time":@"2023-08-11",
    },@{
        @"name":@"钱医生",
        @"companyId":@"太原市和平医院",
        @"officeId":@"神经科",
        @"time":@"2023-08-10",
    },@{
        @"name":@"孙医生",
        @"companyId":@"西安市和平医院",
        @"officeId":@"血压科",
        @"time":@"2023-08-09",
    },@{
        @"name":@"李医生",
        @"companyId":@"西安市和平医院",
        @"officeId":@"血液科",
        @"time":@"2023-08-08",
    }];
    

    
    NSArray *patientsRecord = @[@{
        @"name":@"张三",
        @"sex":@"男",
        @"age":@"55",
        @"symptoms":@"头痛、头晕、头胀",
        @"createDate":@"2023-07-24 13:22",
    },@{
        @"name":@"李四",
        @"sex":@"男",
        @"age":@"60",
        @"symptoms":@"腹痛、腹泻、腹胀",
        @"createDate":@"2023-07-21 15:20",
    },@{
        @"name":@"王五",
        @"sex":@"男",
        @"age":@"49",
        @"symptoms":@"腰痛、腰部肿块",
        @"createDate":@"2023-07-20 13:32",
    },@{
        @"name":@"李*",
        @"sex":@"女",
        @"age":@"30",
        @"symptoms":@"喷嚏、鼻塞、鼻痒、流涕",
        @"createDate":@"2023-07-19 14:20",
    }];
    

    
    [self.showDataArr removeAllObjects];
    
    if([self.usertype isEqualToString:@"patient"]){
        if(self.segment.selectedSegmentIndex == 0){
            [self.showDataArr addObjectsFromArray:patientRecord];
        }else{
            [self.showDataArr addObjectsFromArray:self.resultArr];
        }
    }else if([self.usertype isEqualToString:@"doctor"]){
        if(self.segment.selectedSegmentIndex == 0){
            [self.showDataArr addObjectsFromArray:patientsRecord];
        }else{
            [self.showDataArr addObjectsFromArray:self.resultArr];
        }
    }else{
        [self.showDataArr addObjectsFromArray:self.resultArr];
    }

    [self.tableView reloadData];
}

- (NSMutableArray *)showDataArr{
    if(!_showDataArr){
        _showDataArr = [[NSMutableArray alloc]init];
    }
    return _showDataArr;
}

@end
