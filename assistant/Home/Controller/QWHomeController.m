//
//  QWHomeController.m
//  qwm
//
//  Created by kevin on 2023/3/23.
//

#import "QWHomeController.h"
#import "QABannerView.h"
#import "QWHomeModel.h"
#import "QWWebviewController.h"
#import "QADoctorItemCell.h"
#import "QAMenusCell.h"
#import "QADocHeaderView.h"
#import "QADoctorListController.h"
#import "QWDoctorDetailController.h"
#import "QWCopyrightInfoView.h"
#import "QWComButtonView.h"
#import "QWScrollTextCell.h"
#import "QWFuncMenusCell.h"
#import "XWQRCodeReader.h" //扫描
#import "QWMyCodeController.h" //我的二维码
#import "QWMeetingHomeController.h"
#import "QWMeetJoinInfoController.h" //视频通话- 加入会议
#import "QWProductMeetingController.h" //学会会议汇总
#import "QWBookingDetailController.h"
#import "QWHomeNavigationBarView.h"
#import "QWNewsHomeController.h" //消息


@interface QWHomeController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) NSMutableArray *bannerDataArr;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *doctorDataArr;
@property (nonatomic, strong) NSMutableArray *noticeArr;
@property (nonatomic,strong) QWHomeNavigationBarView * homeNavView;
@end

@implementation QWHomeController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
//    [self hiddenLeftBarButtonItem];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"首页";
    // 监测网络情况
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name: kReachabilityChangedNotification
                                               object: nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTableView) name:QWHomeVC_TableView_reload object:nil];
    
    [self tableView];
    [self homeNavView];
    [self loadAllData];
}

#pragma mark - 首页自定义导航栏
- (QWHomeNavigationBarView *)homeNavView{
    if(!_homeNavView){
        _homeNavView = [[QWHomeNavigationBarView alloc]init];
        [self.view addSubview:_homeNavView];
        
        [_homeNavView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view);
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.height.mas_equalTo([UIDevice navigationFullHeight]);
        }];
        
        __weak __typeof(self) weakSelf = self;
        _homeNavView.newsOpenBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf jumpToNews];
        };
    }
    return _homeNavView;
}


#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        [_tableView registerClass:[QADocHeaderView class] forHeaderFooterViewReuseIdentifier:@"QADocHeaderView"];
        [_tableView registerClass:[QADoctorItemCell class] forCellReuseIdentifier:@"QADoctorItemCell"];
        [_tableView registerClass:[QAMenusCell class] forCellReuseIdentifier:@"QAMenusCell"];
        [_tableView registerClass:[QWScrollTextCell class] forCellReuseIdentifier:@"QWScrollTextCell"];
        [_tableView registerClass:[QWFuncMenusCell class] forCellReuseIdentifier:@"QWFuncMenusCell"];
        

        _tableView.backgroundColor=Color_TableView_Gray;
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏

//        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
//        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view);
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.view).offset(-[UIDevice tabBarFullHeight]);
        }];
        
        //公司信息
        QWCopyrightInfoView *view = [[QWCopyrightInfoView alloc]init];
        _tableView.backgroundView = view;
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    CGFloat height =0.0000001;
    if(section == 2) height = 40.0;
    return height;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat height =0.0000001;
    if(section == 0) height = kScreenWidth/2.0;
    if(section == 2) height = 40.0;
    return height;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger num = 0;
    if(section == 0) num = 2;
    if(section == 1) num = 1;
    if(section == 2) num = _doctorDataArr.count;
    return num;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        if(indexPath.row == 0){
            QWScrollTextCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWScrollTextCell"];
            cell.noticeArr = self.noticeArr;
            __weak __typeof(self) weakSelf = self;
            cell.handlerTitleClickCallBack = ^(NSInteger index) {
                __strong __typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf handleNews:index];
            };
            return cell;
        }
        if(indexPath.row == 1){
            QWFuncMenusCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWFuncMenusCell"];
            cell.unreadIMCount = self.unreadIMCount;
            __weak __typeof(self) weakSelf = self;
            cell.btnsClickBlock = ^(NSString * _Nonnull selStr) {
                __strong __typeof(weakSelf)strongSelf = weakSelf;
                if([selStr isEqualToString:@"left"]){//扫一扫
                    [strongSelf jumpToCodeReader];
                }
                if([selStr isEqualToString:@"right"]){//咨询
                    [strongSelf jumpToTabNews];
                }
                if([selStr isEqualToString:@"center"]){//视频会议
                    [strongSelf jumpToVideoMeeting];
                }
            };
            return cell;
        }
    }
    if(indexPath.section == 1){
        //首页菜单
        QAMenusCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QAMenusCell"];
        cell.menuItemClickBlock = ^(NSDictionary * _Nonnull data) {
            [self menuButtonClick:data];
        };
        return cell;
    }
    if(indexPath.section == 2){
        QADoctorItemCell *cell =[tableView dequeueReusableCellWithIdentifier:@"QADoctorItemCell"];
        QADoctorModel*model = _doctorDataArr[indexPath.row];
        cell.model = model;
        return cell;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"cell"];
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.section == 2){
        QADoctorModel*model = _doctorDataArr[indexPath.row];
        [[UserInfoStore sharedInstance] fetchUserInfosFromServer:@[[model.id lowercaseString]]];
        NSString *url = [URL_DOCTOR_INFO stringByAppendingFormat:@"?id=%@&from=app",model.id];
        [self jumToWebView:url];
        
//        QADoctorModel*model = _doctorDataArr[indexPath.row];
//        QWDoctorDetailController *detailVC = [[QWDoctorDetailController alloc]init];
//        detailVC.doctorModel = model;
//        detailVC.hidesBottomBarWhenPushed = YES;
//        [self.navigationController pushViewController:detailVC animated:YES];
    }
  
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(section == 0){
        QABannerView *bannerView =[[QABannerView alloc]init];
        bannerView.showArr = self.bannerDataArr;
        __weak __typeof(self) weakSelf = self;
        bannerView.bannerClickBlock = ^(QABannerModel * _Nonnull bannerModel) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf jumToWebView:bannerModel.imgTarget];
        };
        return bannerView;
    }
    if(section == 2){
        QADocHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"QADocHeaderView"];
        headerView.moreClickBlock = ^{
            [self jumpToDoctorList];
        };
        return headerView;
    }
    return [UIView new];
}

-  (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if(section == 2 && self.doctorDataArr.count>0){
        QWComButtonView *bottomView = [[QWComButtonView alloc]init];
        bottomView.btnName = @"检索更多专家";
        bottomView.makeActionClickBlock = ^{
            [self jumpToDoctorList];
        };
        return bottomView;
    }
    return [UIView new];
}

#pragma mark - 菜单按钮-跳转
- (void)menuButtonClick:(NSDictionary *)data{
    NSInteger state = [data[@"state"] integerValue];
    BOOL isCheckLogin = [data[@"isCheckLogin"] boolValue];
    if(isCheckLogin && ![AppProfile isLoginedAlert]){
        return;
    }
    
    if(state == 1){
        NSString *class = data[@"class"];
        UIViewController *viewController = [[NSClassFromString(class) alloc] init];
        viewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:viewController animated:YES];
    }else{
        NSString *url = data[@"url"];
        if(!IsStringEmpty(url)){
            [self jumToWebView:url];
        }
    }
}

#pragma mark - 所有数据-请求
- (void) loadAllData{
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[QWCommonMethod Instance] fetchDataGuideWay];
    });
 
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [self loadBannerData]; //banner数据
        [self loadDoctorListData];//医生列表数据
        [self loadTipsData];//跑马灯
    });
}
#pragma mark - banner-请求
- (void)loadBannerData{
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_BANNER argument:[NSDictionary dictionary]];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        NSArray *resArr =(NSArray *)result;
        NSMutableArray *arr =[QABannerModel mj_objectArrayWithKeyValuesArray:resArr];
        [self.bannerDataArr removeAllObjects];
        self.bannerDataArr = arr;
        [self.tableView reloadData];
        [self loadDoctorListDefaltData];
        NSLog(@"");
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        //错误
        NSLog(@"%@",errorInfo);
    }];
}

#pragma mark - 医生列表-请求
- (void)loadDoctorListData{
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_DOCTOR_LIST argument:[NSDictionary dictionary]];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        NSArray *resArr =(NSArray *)result;
        NSArray *subResArr =[resArr subarrayWithRange:NSMakeRange(0, resArr.count>15?15:resArr.count)];//15人
        [self.doctorDataArr removeAllObjects];
        if(subResArr && subResArr.count>10000){
            [[NSUserDefaults standardUserDefaults] setObject:subResArr forKey:NSUserDefaults_doctorlist_keep_15];
            self.doctorDataArr =[QADoctorModel mj_objectArrayWithKeyValuesArray:subResArr];
        }else{
            NSArray * default_arr = [self loadDoctorListDefaltData];
            self.doctorDataArr =[QADoctorModel mj_objectArrayWithKeyValuesArray:default_arr];
        }
        
        [self.tableView reloadData];
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        //错误
        NSLog(@"%@",errorInfo);
        
        //请求失败加载默认数据
        [self.doctorDataArr removeAllObjects];
        NSArray * default_arr = [self loadDoctorListDefaltData];
        self.doctorDataArr =[QADoctorModel mj_objectArrayWithKeyValuesArray:default_arr];
        [self.tableView reloadData];
    }];
}

#pragma mark - 跑马灯-请求
- (void)loadTipsData{
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_Home_Notice argument:@{}];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success1) {
        [self.noticeArr removeAllObjects];
        NSArray *arr =(NSArray *)result;
        self.noticeArr = [QWHomeNoticeModel mj_objectArrayWithKeyValuesArray:arr];
        [self.tableView reloadData];
        NSLog(@"");
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        //错误
        NSLog(@"%@",errorInfo);
    }];
}
- (NSMutableArray *)noticeArr{
    if(!_noticeArr){
        _noticeArr = [[NSMutableArray alloc]init];
    }
    return _noticeArr;
}

#pragma mark - webview - 跳转
- (void)jumToWebView:(NSString *)url{
    QWWebviewController *webviewVC =[[QWWebviewController alloc]init];
    webviewVC.url = url;
    webviewVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:webviewVC animated:YES];
}

#pragma mark - 医生列表 - 跳转
- (void)jumpToDoctorList{
    QADoctorListController *docListVC =[[QADoctorListController alloc]init];
    docListVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:docListVC animated:YES];
}

#pragma mark - tab消息 - 跳转
- (void)jumpToTabNews{
    [self turnToTabVCAtIndex:2];
}
#pragma mark - 扫一扫 - 跳转
- (void)jumpToCodeReader{
    XWQRCodeReader *readerVC = [[XWQRCodeReader alloc] init];
    readerVC.qrcodeValueBlock = ^(NSString * _Nonnull codeString) {
        if([codeString containsString:QWM_DOMAIN]){
            NSDictionary *dic = [QWCommonMethod parameterWithURL:codeString];
            NSString *type = dic[@"type"];
            if([type isEqualToString:@"meetingBooking"]){
                NSDictionary * bookingInfo = [dic[@"bookingInfo"] mj_keyValues];
                QWAgoraUserInfoModel *model = [QWAgoraUserInfoModel mj_objectWithKeyValues:bookingInfo];
                [self jumpToMeetingBookingDetail:model];
                return;
            }
            codeString = [codeString stringByAppendingString:@"&open_from=mobile&enter=ios_scan"];
            [self jumToWebView:codeString];
        }
    };
    
    UINavigationController* navVc = [[UINavigationController alloc] initWithRootViewController:readerVC];
    navVc.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController: navVc animated:YES completion:nil];
}

#pragma mark - 视频会议 - 跳转
- (void)jumpToVideoMeeting{
    QWMeetingHomeController *meetingVC = [[QWMeetingHomeController alloc]init];
    meetingVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:meetingVC animated:YES];
}
#pragma mark - 会议详情
- (void)jumpToMeetingBookingDetail:(QWAgoraUserInfoModel *)bookingModel{
    QWBookingDetailController *detailVC = [[QWBookingDetailController alloc]init];
    detailVC.bookingModel = bookingModel;
    detailVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark - 消息 - 跳转
- (void)jumpToNews{
    QWNewsHomeController *newsVC = [[QWNewsHomeController alloc]init];
    newsVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:newsVC animated:YES];
}

#pragma mark -网络监听
- (void)reachabilityChanged:(NSNotification *)note {
    // 连接改变
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    //处理连接改变后的情况
    NetworkStatus status = [curReach currentReachabilityStatus];
    
    if (status != NotReachable) {
       if(self.bannerDataArr == nil || self.doctorDataArr == nil) [self loadAllData];
    }
}

#pragma mark - 刷新页面
- (void)refreshTableView{
    [self.tableView reloadData];
}

#pragma mark - 医生列表数据-本地默认数据

- (NSArray *)loadDoctorListDefaltData{
    //缓存
    NSArray *userDefalut_arr = [[NSUserDefaults standardUserDefaults] objectForKey:NSUserDefaults_doctorlist_keep_15];
    if(userDefalut_arr.count>0) return userDefalut_arr;
    
    //本地
    NSString * filePath = [[NSBundle mainBundle] pathForResource:@"QWDoctorListDefault" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];//获取指定路径的data文件
    NSArray *local_arr = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    return local_arr;
}

#pragma mark - 消息点击
-  (void)handleNews:(NSInteger)index{
    QWHomeNoticeModel *model = self.noticeArr[index];
    NSString *target = model.target;
    if(IsStringEmpty(target)) return;
    
    
    if([target isEqualToString:@"page_1V1_video_meet"]){
        NSString *extprop = model.extprop;
        if(IsStringEmpty(extprop))return;
        [self jumpToMeetJoinInfo:extprop];
    }
    if([target isEqualToString:@"page_product_meet_all"]){
        [self jumpToProductMeeting];
    }
}

#pragma mark - 视频1V1通话 - 跳转
- (void)jumpToMeetJoinInfo:(NSString *)channel{
    QWMeetJoinInfoController *joinInfoVC = [[QWMeetJoinInfoController alloc]init];
    joinInfoVC.channelName = channel;
    joinInfoVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:joinInfoVC animated:YES];
}
#pragma mark - 产品学术会议汇总 - 跳转
- (void)jumpToProductMeeting{
    QWProductMeetingController *productMeetingVC = [[QWProductMeetingController alloc]init];
    productMeetingVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:productMeetingVC animated:YES];
}



@end
