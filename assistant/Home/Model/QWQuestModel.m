//
//  QWQuestModel.m
//  assistant
//
//  Created by qunai on 2023/7/18.
//

#import "QWQuestModel.h"

@implementation QWOptionModel
- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
}

- (id)valueForUndefinedKey:(NSString *)key{
    return nil;
}
@end

@implementation QWQuestModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{
        @"optionList":[QWOptionModel class]
    };
}
@end

@implementation QWSymptomModel
@end

@implementation QWSymptomBodyModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{
        @"symptomList":[QWSymptomModel class]
    };
}
@end

@implementation QWHistoryResutModel
@end

@implementation QWSuggestModel
@end

@implementation QWHistoryModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{
        @"result":[QWHistoryResutModel class],
        @"suggests":[QWSuggestModel class]
    };
}
@end


//诊断问题选项
@implementation QWAddOptionModel
@end

//添加诊断问题
@implementation QWAddQuestionModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{
        @"options":[QWAddOptionModel class]
    };
}
@end


//问题-题目
@implementation QWQuestionTopicModel
@end

//问题-选项
@implementation QWQuestionOptionModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{
        @"options":[QWOptionModel class]
    };
}
@end


@implementation QWAiConclusionModel
@end

@implementation QWQuestionAllModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{
        @"ai_conclusion":[QWAiConclusionModel class]
    };
}
@end

@implementation QWQuesAianswerListModel
@end

@implementation QWQuesAitimuListModel
- (instancetype)init{
    self = [super init];
    if(self){
        self.options = [[NSMutableArray alloc]init];
    }
    return self;
}

- (void)setXuanxiangA:(NSString *)xuanxiangA{
    _xuanxiangA = xuanxiangA;
    [self addOption:_xuanxiangA];
}
- (void)setXuanxiangB:(NSString *)xuanxiangB{
    _xuanxiangB = xuanxiangB;
    [self addOption:_xuanxiangB];
}
- (void)setXuanxiangC:(NSString *)xuanxiangC{
    _xuanxiangC = xuanxiangC;
    [self addOption:_xuanxiangC];
}
- (void)setXuanxiangD:(NSString *)xuanxiangD{
    _xuanxiangD = xuanxiangD;
    [self addOption:_xuanxiangD];
}
- (void)setXuanxiangE:(NSString *)xuanxiangE{
    _xuanxiangE = xuanxiangE;
    [self addOption:_xuanxiangE];
}
- (void)setXuanxiangF:(NSString *)xuanxiangF{
    _xuanxiangF = xuanxiangF;
    [self addOption:_xuanxiangF];
}

- (void)addOption:(NSString *)content{
    if(IsStringEmpty(content)) return;
    QWOptionModel *model = [[QWOptionModel alloc]init];
    model.content =content;
    model.num = [self changeIntToLetter:self.options.count];
    [self.options addObject:model];
}
- (NSString *)changeIntToLetter:(NSInteger)num{
    NSString *letter = @"";
    if(num == 0) letter = @"A";
    if(num == 1) letter = @"B";
    if(num == 2) letter = @"C";
    if(num == 3) letter = @"D";
    if(num == 4) letter = @"E";
    if(num == 5) letter = @"F";
    if(num == 6) letter = @"G";
    if(num == 7) letter = @"H";
    if(num == 8) letter = @"I";
    if(num == 9) letter = @"J";
    return letter;
}
@end

@implementation QWQuesAianswerBeginModel

@end

@implementation QWQuesAianswerdSavaModel
@end


@implementation QWQuesARconditionModel
@end
@implementation QWQuesAiDiagnosticRulesModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{
        @"ai_conclusion":[QWAiConclusionModel class],
        @"ai_condition":[QWQuesARconditionModel class]
    };
}
@end

@implementation QWQuesAiAddSubitemModel
@end

