//
//  QWManageModel.m
//  assistant
//
//  Created by qunai on 2024/1/31.
//

#import "QWManageModel.h"

@implementation QWManageModel
- (instancetype)init{
    self = [super init];
    if(self){
        self.age = [self getRandomNumber:25 to:65];
    }
    return self;
}
-(NSInteger)getRandomNumber:(NSInteger)from to:(NSInteger)to
{
    return (int)(from + (arc4random() % (to - from + 1)));
}
@end
