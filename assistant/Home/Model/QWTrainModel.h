//
//  QWTrainModel.h
//  assistant
//
//  Created by qunai on 2023/7/31.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWTrainModel : NSObject

@end

@interface QWTrainItemModel : NSObject
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *code;
@property (nonatomic, assign)  BOOL   isSel;
@property (nonatomic, copy) NSString *file;
@property (nonatomic, copy) NSString *fileName;
@end

@interface QWTrainListModel : NSObject
@property (nonatomic, copy) NSString *content;
@property (nonatomic, strong)  NSArray<QWTrainItemModel*>  *list;
@property (nonatomic, assign)  BOOL   isSel;
@property (nonatomic, assign)  BOOL   isCheckCode;
@end

NS_ASSUME_NONNULL_END
