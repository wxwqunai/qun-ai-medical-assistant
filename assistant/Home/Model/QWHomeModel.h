//
//  QWHomeModel.h
//  qwm
//
//  Created by kevin on 2023/4/7.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWHomeModel : NSObject

@end

@interface QABannerModel : NSObject
@property (nonatomic, assign) NSInteger id;
@property (nonatomic, copy) NSString *imgResUrl;
@property (nonatomic, copy) NSString *imgTarget;
@end

@interface QADoctorModel : NSObject
@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *companyId;
@property (nonatomic, copy) NSString *officeId;
@property (nonatomic, copy) NSString *level;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *photo;
@end

@interface QADoctorDetailModel : NSObject
@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *usertype;
@property (nonatomic, copy) NSString *companyId;
@property (nonatomic, copy) NSString *officeId;
@property (nonatomic, copy) NSString *level;
@property (nonatomic, copy) NSString *major;
@property (nonatomic, copy) NSString *resume;
@property (nonatomic, copy) NSString *loginName;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSString *no;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *wechat;
@property (nonatomic, copy) NSString *photo;
@property (nonatomic, copy) NSString *loginIp;
@property (nonatomic, copy) NSString *delFlag;
@property (nonatomic, copy) NSString *isAttestation;
@property (nonatomic, copy) NSString *makeAppointment;
@property (nonatomic, copy) NSString *byAppointment;
@property (nonatomic, copy) NSString *isshow;
@end


@interface QWHomeNoticeModel : NSObject
@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *msgfromid;
@property (nonatomic, copy) NSString *msgfromname;
@property (nonatomic, copy) NSString *msgtoid;
@property (nonatomic, copy) NSString *msgtoname;
@property (nonatomic, copy) NSString *msgtype;
@property (nonatomic, copy) NSString *chattime;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *remark;
@property (nonatomic, copy) NSString *extprop;
@property (nonatomic, copy) NSString *target;
@end

NS_ASSUME_NONNULL_END
