//
//  QWManageModel.h
//  assistant
//
//  Created by qunai on 2024/1/31.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWManageModel : NSObject
@property (nonatomic, copy) NSString *companyId;
@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *officeId;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *photo;
@property (nonatomic, assign) NSInteger age;
@property (nonatomic, copy) NSString *serviceType;
@property (nonatomic, copy) NSString *touserId;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *usertype;
@end

NS_ASSUME_NONNULL_END
