//
//  QWQuestModel.h
//  assistant
//
//  Created by qunai on 2023/7/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWOptionModel : NSObject
///** 选项 */
//@property (nonatomic, copy)  NSString  *  optionContent;
///** 选项的序号 */
//@property (nonatomic, copy)  NSString  *  optionNum;
/** 选项是否被选中 */
@property(nonatomic, assign)  BOOL   isSel;
/** 选项 */
@property (nonatomic, copy)  NSString  *  content;
/**单选、多选、输入框*/
@property (nonatomic, copy)  NSString  *  type;
/**下一题id*/
@property (nonatomic, copy)  NSString  *  next_topic_id;

@property (nonatomic, copy) NSString * num;

@end

@interface QWQuestModel : NSObject
/** 题目id */
@property (nonatomic, copy)  NSString  *  id;
/** 选项数组 */
@property (nonatomic, strong)  NSArray<QWOptionModel*>  *  optionList;
/** 选项类型 */
@property (nonatomic, copy)  NSString * optionType;
/** 题目 */
@property (nonatomic, copy)  NSString  *  questionContent;
/** 问题组 */
@property (nonatomic, copy)  NSString  *  questionGroup;
/** 问题的第几个个数 */
@property (nonatomic, copy)  NSString  *  questionNum;

/** 答案 */
@property (nonatomic, copy)  NSString  *  answer;
/** 是否被选中 */
@property(nonatomic, assign)  BOOL   isSelected;
@end




@interface QWSymptomModel : NSObject
@property (nonatomic, copy)  NSString  *  id;
@property (nonatomic, copy)  NSString  * body_id;
@property (nonatomic, copy)  NSString  *  content;
@property (nonatomic, assign)  BOOL   isSel;
@end

@interface QWSymptomBodyModel : NSObject
@property (nonatomic, copy)  NSString  *  id;
@property (nonatomic, copy)  NSString  *  content;
@property (nonatomic, assign)  BOOL   isSel;
@property (nonatomic, strong)  NSArray<QWSymptomModel*>  *  symptomList;
@end

@interface QWHistoryResutModel : NSObject
@property (nonatomic, copy)  NSString  *  simpleDes;
@property (nonatomic, copy)  NSString  *  percent;
@property (nonatomic, copy)  NSString  *  diseaseName;
@end


@interface QWSuggestModel : NSObject
@property (nonatomic, copy)  NSString  *  doctorCompany;
@property (nonatomic, copy)  NSString  *  doctorName;
@property (nonatomic, copy)  NSString  *  content;

@end

@interface QWHistoryModel : NSObject
@property (nonatomic, copy)  NSString  *  id;
@property (nonatomic, copy)  NSString  *  state;
@property (nonatomic, copy)  NSString  *  createDate;
@property (nonatomic, copy)  NSString  *  symptoms;
@property (nonatomic, copy)  NSString  *  weight;
@property (nonatomic, copy)  NSString  *  height;
@property (nonatomic, copy)  NSString  *  age;
@property (nonatomic, copy)  NSString  *  sex;

@property (nonatomic, strong)  NSArray<QWHistoryResutModel*>  *result;
@property (nonatomic, strong)  NSArray<QWSuggestModel*>  *suggests;
@end



//诊断问题选项
@interface QWAddOptionModel : NSObject
@property (nonatomic, copy)  NSString  *  content;
@end

//添加诊断问题
@interface QWAddQuestionModel : NSObject
@property (nonatomic, copy)  NSString  *  title;
@property (nonatomic, copy)  NSString  *  type;
@property (nonatomic, strong)  NSMutableArray<QWAddOptionModel*>  *options;
@end


@interface QWQuestionOptionModel : NSObject
/** 所属题目id */
@property (nonatomic, copy)  NSString  * belong_topic_id;
/** 选项 */
@property (nonatomic, strong)  NSArray<QWOptionModel*>  *options;
@end


@interface QWQuestionTopicModel : NSObject
/** 题目id */
@property (nonatomic, copy)  NSString  * topic_id;
/** 选项类型 */
@property (nonatomic, copy)  NSString * optionType;
/** 题目内容*/
@property (nonatomic, copy)  NSString  *  content;

@property (nonatomic, strong) QWQuestionOptionModel *optionsModel;
@end


@interface QWAiConclusionModel : NSObject
@property (nonatomic, copy)  NSString  *suggest;
@property (nonatomic, copy)  NSString  *w;
@end

@interface QWQuestionAllModel : NSObject
@property (nonatomic, strong)  NSMutableArray<QWAiConclusionModel*>  *ai_conclusion;
@property (nonatomic, copy)  NSMutableArray  *ai_condition;
@property (nonatomic, copy)  NSString  *ext_prop;
@property (nonatomic, copy)  NSString  *flag;
@property (nonatomic, copy)  NSString  *id;
@property (nonatomic, copy)  NSString  *state;
@end

@interface QWQuesAianswerListModel : NSObject
@property (nonatomic, copy)  NSString  *ai_suggest1;
@property (nonatomic, copy)  NSString  *ai_suggest3;
@property (nonatomic, copy)  NSString  *answer_ip;
@property (nonatomic, copy)  NSString  *answer_time;
@property (nonatomic, copy)  NSString  *id;
@property (nonatomic, copy)  NSString  *rengong1;
@property (nonatomic, copy)  NSString  *rengong2;
@property (nonatomic, copy)  NSString  *user_id;
@property (nonatomic, copy)  NSString  *user_name;
@property (nonatomic, copy)  NSString  *w1;
@property (nonatomic, copy)  NSString  *w2;
@property (nonatomic, copy)  NSString  *w3;
@property (nonatomic, copy)  NSString  *w4;
@property (nonatomic, copy)  NSString  *w5;
@end

@interface QWQuesAitimuListModel : NSObject
@property (nonatomic, copy)  NSString  *ext_prop;
@property (nonatomic, copy)  NSString  *flag;
@property (nonatomic, copy)  NSString  *id;
@property (nonatomic, copy)  NSString  *input;
@property (nonatomic, copy)  NSString  *level;
@property (nonatomic, copy)  NSString  *next_id;
@property (nonatomic, copy)  NSString  *parent_id;
@property (nonatomic, copy)  NSString  *q_id;
@property (nonatomic, copy)  NSString  *q_title;
@property (nonatomic, copy)  NSString  *q_type;
@property (nonatomic, copy)  NSString  *show;
@property (nonatomic, copy)  NSString  *state;
@property (nonatomic, copy)  NSString  *xuanxiangA;
@property (nonatomic, copy)  NSString  *xuanxiangB;
@property (nonatomic, copy)  NSString  *xuanxiangC;
@property (nonatomic, copy)  NSString  *xuanxiangD;
@property (nonatomic, copy)  NSString  *xuanxiangE;
@property (nonatomic, copy)  NSString  *xuanxiangF;
@property (nonatomic, strong)  NSMutableArray<QWOptionModel*> *options;
@end

@interface QWQuesAianswerBeginModel : NSObject
@property (nonatomic, copy)  NSString  *ZD_user_ID;
@property (nonatomic, copy)  NSString  *ZD_user_name;
@property (nonatomic, copy)  NSString  *answerId;
@property (nonatomic, copy)  NSString  *answertime;
@property (nonatomic, copy)  NSString  *current_q_id;
@property (nonatomic, copy)  NSString  *message;
@end


@interface QWQuesAianswerdSavaModel : NSObject
@property (nonatomic, copy)  NSString  *answer_id;
@property (nonatomic, copy)  NSString  *answer_select;
@property (nonatomic, copy)  NSString  *answer_select_text;
@property (nonatomic, copy)  NSString  *id;
@property (nonatomic, copy)  NSString  *q_id;
@property (nonatomic, copy)  NSString  *q_type;
@end


@interface QWQuesARconditionModel : NSObject
@property (nonatomic, copy)  NSString  *q_id;
@property (nonatomic, copy)  NSString  *q_title;
@property (nonatomic, copy)  NSString  *answer_select;
@property (nonatomic, copy)  NSString  *answer_select_text;
@end

@interface QWQuesAiDiagnosticRulesModel : NSObject
@property (nonatomic, copy)  NSString  *ext_prop;
@property (nonatomic, copy)  NSString  *flag;
@property (nonatomic, copy)  NSString  *state;
@property (nonatomic, copy)  NSString  *id;
@property (nonatomic, strong)  NSMutableArray<QWAiConclusionModel*> *ai_conclusion;
@property (nonatomic, strong)  NSMutableArray<QWQuesARconditionModel*> *ai_condition;
@end

@interface QWQuesAiAddSubitemModel : NSObject
@property (nonatomic, copy)  NSString  *judgment_text;
@property (nonatomic, copy)  NSString  *result_text;
@end

NS_ASSUME_NONNULL_END
