//
//  QWScrollTextCell.m
//  assistant
//
//  Created by qunai on 2023/10/9.
//

#import "QWScrollTextCell.h"

@implementation QWScrollTextCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self bgView];
        [self newsImageView];
        [self verScrollText];
    }
    return self;
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor colorFromHexString:@"#FFFFF0"];
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.top.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            make.height.mas_equalTo(35.0);
        }];
    }
    return _bgView;
}

- (UIImageView *)newsImageView{
    if(!_newsImageView){
        _newsImageView = [[UIImageView alloc]init];
        _newsImageView.image = [UIImage imageNamed:@"home_news"];
        _newsImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.bgView addSubview:_newsImageView];
        [_newsImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView).offset(14);
            make.top.equalTo(self.bgView);
            make.bottom.equalTo(self.bgView);
        }];
    }
    return _newsImageView;
}


- (XWVerticalScrollText *)verScrollText{
    if(!_verScrollText){
        [self.newsImageView layoutIfNeeded];
        CGFloat x = 27;
        CGFloat width = kScreenWidth - x -14;
        _verScrollText = [[XWVerticalScrollText alloc]initWithFrame:CGRectMake(x, 0, width, 35)];
        _verScrollText.titleColor = [UIColor colorFromHexString:@"#FF0000"];
        _verScrollText.titleFont = [UIFont systemFontOfSize:15];
        _verScrollText.backgroundColor = [UIColor clearColor];
        __weak __typeof(self) weakSelf = self;
        _verScrollText.handlerTitleClickCallBack = ^(NSInteger index) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if(strongSelf.handlerTitleClickCallBack)strongSelf.handlerTitleClickCallBack(index-1);
        };
        [_bgView addSubview:_verScrollText];
    }
    return _verScrollText;
}

- (void)setNoticeArr:(NSArray *)noticeArr{
    _noticeArr = noticeArr;
    
    NSMutableArray *__block strArr = [[NSMutableArray alloc]initWithArray:@[]];
    NSString * phone = AppProfile.qaUserInfo.phone;
    [_noticeArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        QWHomeNoticeModel *model = (QWHomeNoticeModel *)obj;
        if([model.msgtoid isEqualToString:@"all"]){
            [strArr addObject:model.content];
        }else if(![model.msgtoid isEqualToString:@"all"] && [model.msgtoid isEqualToString:phone]){
            [strArr addObject:model.content];
        }
    }];
    self.verScrollText.titleArr=strArr;

}

@end
