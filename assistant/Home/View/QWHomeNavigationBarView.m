//
//  QWHomeNavigationBarView.m
//  assistant
//
//  Created by qunai on 2024/3/14.
//

#import "QWHomeNavigationBarView.h"

@implementation QWHomeNavigationBarView

- (instancetype)init{
    self = [super init];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        [self leftView];
//        [self logoImageView];
//        [self titleLabel];
        
        [self rightView];
        [self newsBtn];
//        [self scansBtn];
    }
    return self;
}

#pragma mark - 左边操作View
- (UIView *)leftView{
    if(!_leftView){
        _leftView = [[UIView alloc]init];
        [self addSubview:_leftView];
        
        [_leftView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo([UIDevice safeDistanceTop]);
            make.left.equalTo(self);
            make.bottom.equalTo(self);
        }];
    }
    return _leftView;
}

//- (UIImageView *)logoImageView{
//    if(!_logoImageView){
//        _logoImageView = [[UIImageView alloc]init];
//    }
//    return _logoImageView;
//}


#pragma mark - 右边操作View
- (UIView *)rightView{
    if(!_rightView){
        _rightView = [[UIView alloc]init];
        [self addSubview:_rightView];
        
        [_rightView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo([UIDevice safeDistanceTop]);
            make.right.equalTo(self);
            make.bottom.equalTo(self);
        }];
    }
    return _rightView;
}

#pragma mark - 消息
- (UIButton *)newsBtn{
    if(!_newsBtn){
        _newsBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _newsBtn.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
        [_newsBtn setImage:[UIImage imageNamed:@"home_new.png"] forState:UIControlStateNormal];
        _newsBtn.layer.masksToBounds = YES;
        _newsBtn.layer.cornerRadius = 30/2.0;
        [_newsBtn addTarget:self action:@selector(newsBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.rightView addSubview:_newsBtn];
        
        [_newsBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.rightView).offset(-14.0);
            make.left.equalTo(self.rightView);
            make.top.equalTo(self.rightView);
            make.width.mas_equalTo(30);
            make.height.mas_equalTo(30);
        }];
    }
    return _newsBtn;
}

-(void)newsBtnClick{
    if(self.newsOpenBlock)self.newsOpenBlock();
}

//#pragma mark - 扫一扫
//- (UIButton *)scansBtn{
//    if(!_scansBtn){
//        _scansBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [_scansBtn setImage:[UIImage imageNamed:@"nav_scans.png"] forState:UIControlStateNormal];
//        [self.rightView addSubview:_scansBtn];
//        
//        [_scansBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.right.equalTo(self.newsBtn.mas_left);
//            make.top.equalTo(self.rightView);
//            make.bottom.equalTo(self.rightView);
//            make.width.mas_equalTo(40.0);
//        }];
//    }
//    return _scansBtn;
//}


@end

