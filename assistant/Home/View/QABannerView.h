//
//  QABannerView.h
//  assistant
//
//  Created by kevin on 2023/4/13.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^BannerClickBlock)(QABannerModel *bannerModel);

@interface QABannerView : UIView
@property (nonatomic,strong) NSArray *showArr;

@property (nonatomic,copy)BannerClickBlock bannerClickBlock;
@end

NS_ASSUME_NONNULL_END
