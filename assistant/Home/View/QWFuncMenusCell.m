//
//  QWFuncMenusCell.m
//  assistant
//
//  Created by qunai on 2023/10/9.
//

#import "QWFuncMenusCell.h"
@interface QWFuncMenusCell ()
@property (nonatomic,strong) UIView *bgView;

@property (nonatomic,strong) UIView *leftView;
@property (nonatomic,strong) UIImageView *leftImageView;
@property (nonatomic,strong) UIButton *leftBtn;

@property (nonatomic,strong) UIView *rightView;
@property (nonatomic,strong) UIImageView *rightImageView;
@property (nonatomic,strong) UIButton *rightBtn;
@property (nonatomic,strong) UILabel *rightUnReadLabel; // 未读数

@property (nonatomic,strong) UIView *centerView;
@property (nonatomic,strong) UIButton *centerBtn;

@end
@implementation QWFuncMenusCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self bgView];
        
        [self centerView];
        
        [self leftView];
        [self rightView];
        
        [self centerBtn];
        [self leftBtn];
        [self rightBtn];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    [self createLeftBgColor];
    
    [self createRightBgColor];
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(14.0);
            make.right.equalTo(self.contentView).offset(-14.0);
            make.top.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            make.height.mas_equalTo(50.0);
        }];
    }
    return _bgView;
}

#pragma mark- 扫一扫
- (UIView *)leftView{
    if(!_leftView){
        _leftView = [[UIView alloc]init];
        _leftView.backgroundColor = [UIColor clearColor];
        [_bgView addSubview:_leftView];
        
        [_leftView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
            make.width.equalTo(_bgView).multipliedBy(0.31);
        }];
        
        UIImageView *bgImageView = [[UIImageView alloc]init];
        self.leftImageView = bgImageView;
        [_leftView addSubview:bgImageView];
        [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_leftView);
        }];
        
        UILabel *nameLabel = [[UILabel alloc]init];
        nameLabel.text = @"扫一扫";
        nameLabel.font = [UIFont systemFontOfSize:20];
        nameLabel.textColor = [UIColor whiteColor];
        nameLabel.textAlignment = NSTextAlignmentCenter;
        [_leftView addSubview:nameLabel];
        
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_leftView);
        }];
        
//        UIImageView *imageView = [[UIImageView alloc]init];
//        imageView.image = [UIImage imageNamed:@"yy_meetingNotice.png"];
//        [_leftView addSubview:imageView];
        
//        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(nameLabel.mas_bottom).offset(-8.0);
//            make.right.equalTo(_leftView).offset(-4.0);
//            make.bottom.equalTo(_leftView).offset(-4.0);
//            make.height.mas_equalTo(40);
//            make.width.mas_equalTo(40);
//        }];
    }
    return _leftView;
}

#pragma mark - 会议创建
- (UIView *)centerView{
    if(!_centerView){
        _centerView = [[UIView alloc]init];
        _centerView.layer.masksToBounds = YES;
        _centerView.layer.cornerRadius = 6.0;
        _centerView.backgroundColor = [UIColor colorFromHexString:@"#39e7ba"];
        [_bgView addSubview:_centerView];
        
        [_centerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_bgView);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
            make.width.equalTo(_bgView).multipliedBy(0.31);
        }];
        
        UILabel *nameLabel = [[UILabel alloc]init];
        nameLabel.text = @"视频会议";
        nameLabel.font = [UIFont systemFontOfSize:20];
        nameLabel.textColor = [UIColor whiteColor];
        nameLabel.textAlignment = NSTextAlignmentCenter;
        [_centerView addSubview:nameLabel];
        
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_centerView);
        }];

    }
    return _rightView;
}
#pragma mark - 咨询
- (UIView *)rightView{
    if(!_rightView){
        _rightView = [[UIView alloc]init];
        _rightView.backgroundColor = [UIColor clearColor];
        [_bgView addSubview:_rightView];
        
        [_rightView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_bgView);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
            make.width.equalTo(_bgView).multipliedBy(0.31);
        }];
        
        UIImageView *bgImageView = [[UIImageView alloc]init];
        self.rightImageView = bgImageView;
        [_rightView addSubview:bgImageView];
        [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_rightView);
        }];
        
        UILabel *nameLabel = [[UILabel alloc]init];
        nameLabel.text = @"咨询";
        nameLabel.font = [UIFont systemFontOfSize:20];
        nameLabel.textColor = [UIColor whiteColor];
        nameLabel.textAlignment = NSTextAlignmentCenter;
        [_rightView addSubview:nameLabel];
        
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_rightView);
        }];
        
        UILabel *unReadLabl = [[UILabel alloc]init];
        unReadLabl.hidden = YES;
        unReadLabl.font = [UIFont systemFontOfSize:13];
        unReadLabl.textColor = [UIColor whiteColor];
            unReadLabl.textAlignment = NSTextAlignmentCenter;
        unReadLabl.backgroundColor = [UIColor colorFromHexString:@"#DE3830"];
        unReadLabl.layer.cornerRadius = 10;
        unReadLabl.layer.masksToBounds = YES;
        self.rightUnReadLabel = unReadLabl;
        [_rightView addSubview:unReadLabl];
        
        [unReadLabl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_rightView.mas_right);
            make.top.equalTo(_rightView.mas_top);
            make.height.mas_equalTo(20);
            make.width.mas_greaterThanOrEqualTo(20);
        }];
    }
    return _rightView;
}

-(UIButton *)leftBtn
{
    if (!_leftBtn) {
        _leftBtn =[UIButton buttonWithType:UIButtonTypeCustom];
        [_leftBtn addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [_leftView addSubview:_leftBtn];
        [_leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_leftView);
        }];
    }
    return _leftBtn;
}

-(UIButton *)centerBtn
{
    if (!_centerBtn) {
        _centerBtn =[UIButton buttonWithType:UIButtonTypeCustom];
        [_centerBtn addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [_centerView addSubview:_centerBtn];
        [_centerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_centerView);
        }];
    }
    return _centerBtn;
}

-(UIButton *)rightBtn
{
    if (!_rightBtn) {
        _rightBtn =[UIButton buttonWithType:UIButtonTypeCustom];
        [_rightBtn addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [_bgView addSubview:_rightBtn];
        _bgView.layer.masksToBounds = NO;
        [_rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_rightView);
        }];
    }
    return _rightBtn;
}

- (void)buttonClick:(UIButton *)sender{
    NSString *tagStr = @"";
    if(sender == self.leftBtn) tagStr = @"left";
    if(sender == self.centerBtn) tagStr = @"center";
    if(sender == self.rightBtn) tagStr = @"right";
    if(self.btnsClickBlock)self.btnsClickBlock(tagStr);
}


#pragma mark - 绘制渐变色背景
- (void)createLeftBgColor{
    [self.leftImageView layoutIfNeeded];
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.colors = @[(__bridge id)[UIColor colorFromHexString:@"#93DBFD"].CGColor, (__bridge id)[UIColor colorFromHexString:@"#39e7ba"].CGColor];
    gradientLayer.locations = @[@0.0, @1.0];
    gradientLayer.startPoint = CGPointMake(0, 0.5);
    gradientLayer.endPoint = CGPointMake(1.0, 0.5);
    gradientLayer.frame = CGRectMake(0, 0, self.leftImageView.frame.size.width, self.leftImageView.frame.size.height);
    gradientLayer.shadowColor= [UIColor colorFromHexString:@"#8C96FD"].CGColor;
    gradientLayer.masksToBounds = YES;
    gradientLayer.cornerRadius = 6.0;
    [self.leftImageView.layer addSublayer:gradientLayer];
}

- (void)createRightBgColor{
    [self.rightImageView layoutIfNeeded];
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.colors = @[(__bridge id)[UIColor colorFromHexString:@"#39e7ba"].CGColor, (__bridge id)[UIColor colorFromHexString:@"#00CED1"].CGColor];
    gradientLayer.locations = @[@0.0, @1.0];
    gradientLayer.startPoint = CGPointMake(0, 0.5);
    gradientLayer.endPoint = CGPointMake(1.0, 0.5);
    gradientLayer.frame = CGRectMake(0, 0, self.rightImageView.frame.size.width, self.rightImageView.frame.size.height);
    gradientLayer.shadowColor= [UIColor colorFromHexString:@"#39e7ba"].CGColor;
    gradientLayer.masksToBounds = YES;
    gradientLayer.cornerRadius = 6.0;
    [self.rightImageView.layer addSublayer:gradientLayer];
}

#pragma mark - 消息-聊天未读数
- (void)setUnreadIMCount:(NSString *)unreadIMCount{
    _unreadIMCount = unreadIMCount;
    if(!IsStringEmpty(_unreadIMCount)){
        self.rightUnReadLabel.text =_unreadIMCount;
        self.rightUnReadLabel.hidden = NO;
    }else{
        self.rightUnReadLabel.text =@"";
        self.rightUnReadLabel.hidden = YES;
    }
}

@end
