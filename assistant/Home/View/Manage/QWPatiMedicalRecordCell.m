//
//  QWPatiMedicalRecordCell.m
//  assistant
//
//  Created by qunai on 2023/8/14.
//

#import "QWPatiMedicalRecordCell.h"
@interface QWPatiMedicalRecordCell ()
@property (nonatomic, strong) UIView *baseInfoView;

@end
@implementation QWPatiMedicalRecordCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        [self baseInfoView];
    }
    return self;
}

#pragma mark - view
- (UIView *)baseInfoView{
    if(!_baseInfoView){
        _baseInfoView = [[UIView alloc]init];
        _baseInfoView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_baseInfoView];
        
        [_baseInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(14.0);
            make.top.equalTo(self.contentView).offset(2.0);
            make.right.equalTo(self.contentView).offset(-14.0);
            make.bottom.equalTo(self.contentView).offset(-2.0);
        }];
    
        [self nameLabel];
        
        [self companyLabel];
        
        [self officeLabel];
        [self timeLabel];
    }
    return _baseInfoView;
}

#pragma mark - 姓名
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont boldSystemFontOfSize:18];
        _nameLabel.textColor = [UIColor blackColor];
        [_baseInfoView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_baseInfoView);
            make.top.equalTo(_baseInfoView).offset(5.0);
        }];
    }
    return _nameLabel;
}

#pragma mark - 医院
- (UILabel *)companyLabel{
    if(!_companyLabel){
        _companyLabel = [[UILabel alloc]init];
        _companyLabel.numberOfLines = 0;
        _companyLabel.font = [UIFont systemFontOfSize:17];
        [_baseInfoView addSubview:_companyLabel];
        
        [_companyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_baseInfoView);
            make.right.equalTo(_baseInfoView);
            make.top.equalTo(_nameLabel.mas_bottom).offset(10.0);
        }];
    }
    return _companyLabel;
}

#pragma mark - 科室
- (UILabel *)officeLabel{
    if(!_officeLabel){
        _officeLabel = [[UILabel alloc]init];
        _officeLabel.font = [UIFont systemFontOfSize:14];
        _officeLabel.textColor = [UIColor colorFromHexString:@"#7A7A7A"];
        [_baseInfoView addSubview:_officeLabel];
        
        [_officeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_baseInfoView);
            make.top.equalTo(_companyLabel.mas_bottom).offset(10.0);
            make.bottom.equalTo(_baseInfoView).offset(-5.0);
        }];
    }
    return _officeLabel;
}

#pragma mark - 级别
- (UILabel *)timeLabel{
    if(!_timeLabel){
        _timeLabel = [[UILabel alloc]init];
        _timeLabel.font = [UIFont systemFontOfSize:14 ];
        _timeLabel.textColor = [UIColor colorFromHexString:@"#7A7A7A"];
        [_baseInfoView addSubview:_timeLabel];
        
        [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_officeLabel.mas_right).offset(20);
            make.right.equalTo(_baseInfoView);
            make.centerY.equalTo(_officeLabel);
        }];
    }
    return _timeLabel;
}


#pragma mark - 数据
//- (void)setModel:(QADoctorModel *)model{
//    _model = model;
//
//    //姓名
//    _nameLabel.text = _model.name;
//
//    //电话
//    if(!IsStringEmpty(_model.phone) && _model.phone.length>7){
//        NSString *numberString = [_model.phone stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
//        _phoneLabel.text = numberString;
//    }
//
//    //医院
//    _companyLabel.text = _model.companyId;
//
//    //科室
//    _officeLabel.text = _model.officeId;
//
//    //级别
//    _levelLabel.text = _model.level;
//}

@end

