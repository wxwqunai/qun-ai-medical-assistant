//
//  QWPatiMyDoctorCell.h
//  assistant
//
//  Created by qunai on 2023/8/14.
//

#import "QWBaseTableViewCell.h"
#import "QWManageModel.h"

NS_ASSUME_NONNULL_BEGIN
@interface QWPatiMyDoctorCell : QWBaseTableViewCell
@property (nonatomic, strong) UIView *baseInfoView;
@property (nonatomic, strong) UIButton *nameBtn;
@property (nonatomic, strong) UILabel *companyLabel;
@property (nonatomic, strong) UILabel *officeLabel;

@property (nonatomic, strong) UIViewController *superVC;
@property (nonatomic, strong) QWManageModel *model;

@end

NS_ASSUME_NONNULL_END
