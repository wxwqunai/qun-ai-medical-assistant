//
//  QWDoctorCell.h
//  assistant
//
//  Created by qunai on 2023/8/16.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWDoctorCell : QWBaseTableViewCell
@property (nonatomic,strong) QADoctorModel* model;
@end

NS_ASSUME_NONNULL_END
