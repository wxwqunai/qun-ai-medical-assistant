//
//  QWDocPatientsRecordCell.m
//  assistant
//
//  Created by qunai on 2023/8/15.
//

#import "QWDocPatientsRecordCell.h"
@interface QWDocPatientsRecordCell ()
@property (nonatomic, strong) UIView *baseInfoView;
@property (nonatomic, strong) UIImageView *nextImageView;
@end
@implementation QWDocPatientsRecordCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        [self baseInfoView];
    }
    return self;
}

#pragma mark - view
- (UIView *)baseInfoView{
    if(!_baseInfoView){
        _baseInfoView = [[UIView alloc]init];
        _baseInfoView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_baseInfoView];
        
        [_baseInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(14.0);
            make.top.equalTo(self.contentView).offset(2.0);
            make.right.equalTo(self.contentView).offset(-14.0);
            make.bottom.equalTo(self.contentView).offset(-2.0);
        }];
    
        [self nameLabel];
        [self symptomLabel];
        [self timeLabel];
        [self nextImageView];
    }
    return _baseInfoView;
}

#pragma mark - 姓名
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont systemFontOfSize:18];
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.numberOfLines = 0;
        [_baseInfoView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_baseInfoView);
            make.right.equalTo(_baseInfoView);
            make.top.equalTo(_baseInfoView).offset(5.0);
        }];
    }
    return _nameLabel;
}
#pragma mark - 所选症状
- (UILabel *)symptomLabel{
    if(!_symptomLabel){
        _symptomLabel = [[UILabel alloc]init];
        _symptomLabel.font = [UIFont systemFontOfSize:17];
        _symptomLabel.textColor = [UIColor blackColor];
        _symptomLabel.numberOfLines = 0;
        [_baseInfoView addSubview:_symptomLabel];
        
        [_symptomLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_baseInfoView);
            make.right.equalTo(_baseInfoView);
            make.top.equalTo(_nameLabel.mas_bottom).offset(8.0);
        }];
    }
    return _symptomLabel;
}
#pragma mark - 时间
- (UILabel *)timeLabel{
    if(!_timeLabel){
        _timeLabel = [[UILabel alloc]init];
        _timeLabel.font = [UIFont systemFontOfSize:16];
        _timeLabel.textColor = [UIColor colorFromHexString:@"#7A7A7A"];
        [_baseInfoView addSubview:_timeLabel];
        
        [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_baseInfoView);
            make.right.equalTo(_baseInfoView);
            make.top.equalTo(_symptomLabel.mas_bottom).offset(8.0);
            make.bottom.equalTo(_baseInfoView).offset(-5.0);
        }];
    }
    return _timeLabel;
}

#pragma mark - nextImageView
- (UIImageView *)nextImageView{
    if(!_nextImageView){
        _nextImageView = [[UIImageView alloc]init];
        UIImage *image = [UIImage imageNamed:@"next_arrow.png"];
        _nextImageView.image = image;
        [_baseInfoView addSubview:_nextImageView];
        
        [_nextImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_baseInfoView);
            make.centerY.equalTo(self);
            make.width.mas_equalTo(image.size.width);
            make.height.mas_equalTo(image.size.height);
        }];
    }
    return _nextImageView;
}
#pragma mark - 数据


@end
