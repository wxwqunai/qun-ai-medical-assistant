//
//  QWDoctorCell.m
//  assistant
//
//  Created by qunai on 2023/8/16.
//

#import "QWDoctorCell.h"
@interface QWDoctorCell ()
@property (nonatomic, strong) UIImageView *headPicImageView;

@property (nonatomic, strong) UIView *baseInfoView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *phoneLabel;
@property (nonatomic, strong) UILabel *companyLabel;
@property (nonatomic, strong) UILabel *officeLabel;

@property (nonatomic, strong) UIButton *consultBtn;
@end
@implementation QWDoctorCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self headPicImageView];
        [self consultBtn];
        [self baseInfoView];
    }
    return self;
}

#pragma mark - 头像
- (UIImageView *)headPicImageView{
    if(!_headPicImageView){
        _headPicImageView = [[UIImageView alloc]init];
        _headPicImageView.backgroundColor = [UIColor lightGrayColor];
        _headPicImageView.layer.cornerRadius = 40;
        _headPicImageView.layer.masksToBounds = YES;
        [self.contentView addSubview:_headPicImageView];
        
        [_headPicImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView);
            make.left.equalTo(self.contentView).offset(15);
            make.width.mas_equalTo(80);
            make.height.mas_equalTo(80);
        }];
    }
    return _headPicImageView;
}

#pragma mark - 咨询
- (UIButton *)consultBtn{
    if(!_consultBtn){
        _consultBtn =[UIButton buttonWithType:UIButtonTypeCustom];
        [_consultBtn setTitle:@"诊断" forState:UIControlStateNormal];
        
        [_consultBtn setTitleColor:Color_Main_Green forState:UIControlStateNormal];
        _consultBtn.titleLabel.font =[UIFont systemFontOfSize:15];
        _consultBtn.layer.borderWidth = 1.0;
        _consultBtn.layer.borderColor = Color_Main_Green.CGColor;
        _consultBtn.layer.cornerRadius = 4.0;
        _consultBtn.layer.masksToBounds = YES;
        
        [_consultBtn addTarget:self action:@selector(consultButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_consultBtn];
        
        [_consultBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-10.0);
            make.centerY.equalTo(self.contentView);
            make.width.mas_equalTo(60);
        }];
    }
    return _consultBtn;
}
-(void)consultButtonClick:(UIButton *)sender{
    [SWHUDUtil hideHudViewWithMessageSuperView:self withMessage:@"维护中！"];
}

#pragma mark - view
- (UIView *)baseInfoView{
    if(!_baseInfoView){
        _baseInfoView = [[UIView alloc]init];
        _baseInfoView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_baseInfoView];
        
        [_baseInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_headPicImageView.mas_right).offset(20.0);
            make.top.equalTo(self.contentView);
            make.right.equalTo(self.consultBtn.mas_left).offset(-4.0);
            make.bottom.equalTo(self.contentView);
            make.height.mas_greaterThanOrEqualTo(110);
        }];
    
        [self nameLabel];
        [self phoneLabel];
        
        [self companyLabel];
        
        [self officeLabel];
    }
    return _baseInfoView;
}

#pragma mark - 姓名
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont boldSystemFontOfSize:18];
        _nameLabel.textColor = [UIColor blackColor];
        [_baseInfoView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_baseInfoView);
            make.top.equalTo(_baseInfoView).offset(5.0);
        }];
    }
    return _nameLabel;
}
#pragma mark - 电话
- (UILabel *)phoneLabel{
    if(!_phoneLabel){
        _phoneLabel = [[UILabel alloc]init];
        _phoneLabel.font = [UIFont systemFontOfSize:16];
        [_baseInfoView addSubview:_phoneLabel];
        
        [_phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameLabel.mas_right).offset(20);
            make.right.equalTo(_baseInfoView);
            make.centerY.equalTo(_nameLabel);
            make.width.mas_greaterThanOrEqualTo(100);
        }];
    }
    return _phoneLabel;
}

#pragma mark - 医院
- (UILabel *)companyLabel{
    if(!_companyLabel){
        _companyLabel = [[UILabel alloc]init];
        _companyLabel.numberOfLines = 2;
        _companyLabel.font = [UIFont systemFontOfSize:17];
        [_baseInfoView addSubview:_companyLabel];
        
        [_companyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_baseInfoView);
            make.right.equalTo(_baseInfoView);
            make.top.equalTo(_nameLabel.mas_bottom).offset(10.0);
        }];
    }
    return _companyLabel;
}

#pragma mark - 科室
- (UILabel *)officeLabel{
    if(!_officeLabel){
        _officeLabel = [[UILabel alloc]init];
        _officeLabel.numberOfLines = 2;
        _officeLabel.font = [UIFont systemFontOfSize:14];
        _officeLabel.textColor = [UIColor colorFromHexString:@"#7A7A7A"];
        [_baseInfoView addSubview:_officeLabel];
        
        [_officeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_baseInfoView);
            make.right.equalTo(_baseInfoView);
            make.top.equalTo(_companyLabel.mas_bottom).offset(10.0);
        }];
    }
    return _officeLabel;
}

#pragma mark - 数据
- (void)setModel:(QADoctorModel *)model{
    _model = model;
    
    //头像
    NSString *headStr = [QWM_BASE_API_URL stringByAppendingFormat:@"/bokeupload%@", _model.photo];
    [_headPicImageView sd_setImageWithURL:[NSURL URLWithString:headStr] placeholderImage:[UIImage imageNamed:@"contact_default_avatar"]];
    
    //姓名
    _nameLabel.text = _model.name;
    
    //电话
    if(!IsStringEmpty(_model.phone) && _model.phone.length>7){
        NSString *numberString = [_model.phone stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
        _phoneLabel.text = numberString;
    }
    
    //医院
    _companyLabel.text = _model.companyId;
    
    //科室、级别
    _officeLabel.text = [NSString stringWithFormat:@"%@  %@",_model.officeId,IsStringEmpty(_model.level)?@"":_model.level];
}


@end
