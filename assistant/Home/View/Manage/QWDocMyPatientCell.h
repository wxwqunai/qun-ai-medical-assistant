//
//  QWDocMyPatientCell.h
//  assistant
//
//  Created by qunai on 2023/8/15.
//

#import "QWBaseTableViewCell.h"
#import "QWManageModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWDocMyPatientCell : QWBaseTableViewCell
@property (nonatomic, strong) UIView *baseInfoView;
@property (nonatomic, strong) UIButton *nameBtn;
@property (nonatomic, strong) UILabel *sexLabel;
@property (nonatomic, strong) UILabel *ageLabel;

@property (nonatomic, strong) UIViewController *superVC;
@property (nonatomic, strong) QWManageModel *model;
@end

NS_ASSUME_NONNULL_END
