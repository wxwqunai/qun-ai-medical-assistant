//
//  QWDocMyPatientCell.m
//  assistant
//
//  Created by qunai on 2023/8/15.
//

#import "QWDocMyPatientCell.h"
#import "EMChatViewController.h"
#import "QWPatientDetailController.h" //患者详情
#import "QWMeetQuickJoinController.h"

@interface QWDocMyPatientCell ()
@property (nonatomic,strong) UIView *rightView;
@property (nonatomic, strong) UIButton *consultBtn;
@property (nonatomic, strong) UIButton *videoBtn;
@end
@implementation QWDocMyPatientCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        [self baseInfoView];
        [self nameBtn];
        [self sexLabel];
        [self ageLabel];
        
        [self rightView];
        [self consultBtn];
        [self videoBtn];
    }
    return self;
}

#pragma mark - view
- (UIView *)baseInfoView{
    if(!_baseInfoView){
        _baseInfoView = [[UIView alloc]init];
        _baseInfoView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_baseInfoView];
        
        [_baseInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(14.0);
            make.top.equalTo(self.contentView).offset(2.0);
            make.right.equalTo(self.contentView).offset(-14.0);
            make.bottom.equalTo(self.contentView).offset(-2.0);
        }];
    }
    return _baseInfoView;
}

#pragma mark -  姓名
- (UIButton *)nameBtn{
    if(!_nameBtn){
        _nameBtn =[UIButton buttonWithType:UIButtonTypeCustom];
        [_nameBtn setTitle:@"" forState:UIControlStateNormal];
        [_nameBtn setImage:[UIImage imageNamed:@"next_arrow.png"] forState:UIControlStateNormal];
        
        [_nameBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _nameBtn.titleLabel.font =[UIFont boldSystemFontOfSize:18];
        
        [_nameBtn setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
        _nameBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 8, 0, 0);

        
        [_nameBtn addTarget:self action:@selector(nameButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [_baseInfoView addSubview:_nameBtn];
        
        [_nameBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_baseInfoView);
            make.top.equalTo(_baseInfoView).offset(5.0);
        }];
    }
    return _nameBtn;
}
-(void)nameButtonClick:(UIButton *)sender{
    QWPatientDetailController *detailVC = [[QWPatientDetailController alloc]init];
    detailVC.hidesBottomBarWhenPushed = YES;
    [self.superVC.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark - 性别
- (UILabel *)sexLabel{
    if(!_sexLabel){
        _sexLabel = [[UILabel alloc]init];
        _sexLabel.font = [UIFont systemFontOfSize:15];
        _sexLabel.textColor = [UIColor colorFromHexString:@"#7A7A7A"];
        [_baseInfoView addSubview:_sexLabel];
        
        [_sexLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_baseInfoView);
            make.right.equalTo(_baseInfoView);
            make.top.equalTo(_nameBtn.mas_bottom).offset(10.0);
        }];
    }
    return _sexLabel;
}

#pragma mark - 年龄
- (UILabel *)ageLabel{
    if(!_ageLabel){
        _ageLabel = [[UILabel alloc]init];
        _ageLabel.font = [UIFont systemFontOfSize:15];
        _ageLabel.textColor = [UIColor colorFromHexString:@"#7A7A7A"];
        [_baseInfoView addSubview:_ageLabel];
        
        [_ageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_baseInfoView);
            make.top.equalTo(_sexLabel.mas_bottom).offset(10.0);
            make.bottom.equalTo(_baseInfoView).offset(-5.0);
        }];
    }
    return _ageLabel;
}


#pragma mark - view
- (UIView *)rightView{
    if(!_rightView){
        _rightView = [[UIView alloc]init];
        [_baseInfoView addSubview:_rightView];
        
        [_rightView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_baseInfoView).offset(-14.0);
            make.top.equalTo(_baseInfoView);
            make.bottom.equalTo(_baseInfoView);
        }];
    }
    return _rightView;
}

#pragma mark - 咨询
- (UIButton *)consultBtn{
    if(!_consultBtn){
        _consultBtn =[UIButton buttonWithType:UIButtonTypeCustom];
        [_consultBtn setTitle:@"咨询" forState:UIControlStateNormal];
        
        [_consultBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        _consultBtn.titleLabel.font =[UIFont systemFontOfSize:15];
        _consultBtn.layer.borderWidth = 1.0;
        _consultBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _consultBtn.layer.cornerRadius = 4.0;
        _consultBtn.layer.masksToBounds = YES;
        
        [_consultBtn addTarget:self action:@selector(consultButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.rightView addSubview:_consultBtn];
        
        [_consultBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.rightView);
            make.left.equalTo(self.rightView);
            make.bottom.equalTo(self.rightView.mas_centerY).offset(-2);
            make.width.mas_equalTo(60);
        }];
    }
    return _consultBtn;
}
-(void)consultButtonClick:(UIButton *)sender{
    [self jumpToChat];
}

#pragma mark - 视频通话
- (UIButton *)videoBtn{
    if(!_videoBtn){
        _videoBtn =[UIButton buttonWithType:UIButtonTypeCustom];
        [_videoBtn setTitle:@"视频" forState:UIControlStateNormal];
        
        [_videoBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        _videoBtn.titleLabel.font =[UIFont systemFontOfSize:15];
        _videoBtn.layer.borderWidth = 1.0;
        _videoBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _videoBtn.layer.cornerRadius = 4.0;
        _videoBtn.layer.masksToBounds = YES;
        
        [_videoBtn addTarget:self action:@selector(videoBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.rightView addSubview:_videoBtn];
        
        [_videoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.rightView.mas_centerY).offset(2);
            make.centerX.equalTo(self.rightView);
            make.width.mas_equalTo(60);
        }];
    }
    return _videoBtn;
}
- (void)videoBtnClick{
    [self jumpToMeetQuickJoin];
}

#pragma mark - 聊天 - 跳转
- (void)jumpToChat{
    if([AppProfile isLoginedAlert] && !IsStringEmpty(_model.touserId)){
        [[UserInfoStore sharedInstance] fetchUserInfosFromServer:@[[_model.touserId lowercaseString]]];
         EMChatViewController *chatViewController = [[EMChatViewController alloc] initWithConversationId:_model.touserId conversationType:EMConversationTypeChat];
        [self.superVC.navigationController pushViewController:chatViewController animated:YES];
    }
}

#pragma mark - 聊天 - 跳转
- (void)jumpToMeetQuickJoin{
    if([AppProfile isLoginedAlert] && !IsStringEmpty(_model.touserId)){
        QWMeetQuickJoinController *quickJoinInfoVC = [[QWMeetQuickJoinController alloc]init];
        quickJoinInfoVC.toUserId = _model.touserId;
        quickJoinInfoVC.hidesBottomBarWhenPushed = YES;
        [[[QWCommonMethod Instance] getCurrentVC].navigationController pushViewController:quickJoinInfoVC animated:YES];
    }
}

#pragma mark - 数据
- (void)setModel:(QWManageModel *)model{
    _model = model;
    
    [_nameBtn setTitle:_model.userName forState:UIControlStateNormal];
    _sexLabel.text = @"性别：男";
    _ageLabel.text = [NSString stringWithFormat:@"年龄：%ld",_model.age];
    
    
    
    BOOL isHadConsult = [_model.serviceType containsString:@"1"];
    [_consultBtn setTitleColor:isHadConsult?Color_Main_Green:[UIColor lightGrayColor] forState:UIControlStateNormal];
    _consultBtn.layer.borderColor = isHadConsult?Color_Main_Green.CGColor:[UIColor lightGrayColor].CGColor;
    
    BOOL isHadVideo = [_model.serviceType containsString:@"2"];
    [_videoBtn setTitleColor:isHadVideo?Color_Main_Green:[UIColor lightGrayColor] forState:UIControlStateNormal];
    _videoBtn.layer.borderColor = isHadVideo?Color_Main_Green.CGColor:[UIColor lightGrayColor].CGColor;

    
    
}


@end
