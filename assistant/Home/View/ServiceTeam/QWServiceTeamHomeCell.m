//
//  QWServiceTeamHomeCell.m
//  assistant
//
//  Created by kevin on 2023/9/5.
//

#import "QWServiceTeamHomeCell.h"

@interface QWServiceTeamHomeCell ()
@property (nonatomic, strong) UIImageView *lineImageView;

@property (nonatomic, strong) UIImageView *headPicImageView;

@property (nonatomic, strong) UIView *baseInfoView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *levelLabel;

@property (nonatomic, strong) UIButton *contactBtn;
@end
@implementation QWServiceTeamHomeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self lineImageView];
        
        [self headPicImageView];
        [self baseInfoView];
        
        [self contactBtn];
    }
    return self;
}

- (UIImageView *)lineImageView{
    if(!_lineImageView){
        _lineImageView = [[UIImageView alloc]init];
        _lineImageView.backgroundColor = Color_Line_Gray;
        [self.contentView addSubview:_lineImageView];
        
        [_lineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.left.equalTo(self.contentView);
            make.height.mas_offset(0.5);
        }];
    }
    return _lineImageView;
}

#pragma mark - 头像
- (UIImageView *)headPicImageView{
    if(!_headPicImageView){
        _headPicImageView = [[UIImageView alloc]init];
        _headPicImageView.backgroundColor = [UIColor lightGrayColor];
        _headPicImageView.layer.cornerRadius = 30;
        _headPicImageView.layer.masksToBounds = YES;
        [self.contentView addSubview:_headPicImageView];
        
        [_headPicImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView);
            make.left.equalTo(self.contentView).offset(14.0);
            make.width.mas_equalTo(60);
            make.height.mas_equalTo(60);
        }];
    }
    return _headPicImageView;
}

#pragma mark - view
- (UIView *)baseInfoView{
    if(!_baseInfoView){
        _baseInfoView = [[UIView alloc]init];
        _baseInfoView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_baseInfoView];
        
        [_baseInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_headPicImageView.mas_right).offset(18.0);
            make.top.equalTo(self.contentView);
            make.right.equalTo(self.contentView).offset(-14.0);
            make.bottom.equalTo(self.contentView);
            make.height.mas_greaterThanOrEqualTo(70);
        }];
    
        [self nameLabel];
        
        [self levelLabel];
        
    }
    return _baseInfoView;
}

#pragma mark - 姓名
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont boldSystemFontOfSize:18];
        _nameLabel.textColor = [UIColor blackColor];
        [_baseInfoView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_baseInfoView);
            make.bottom.equalTo(_baseInfoView.mas_centerY).offset(-5.0);
        }];
    }
    return _nameLabel;
}

#pragma mark - 级别
- (UILabel *)levelLabel{
    if(!_levelLabel){
        _levelLabel = [[UILabel alloc]init];
        _levelLabel.font = [UIFont systemFontOfSize:16];
        _levelLabel.textColor = [UIColor colorFromHexString:@"#FF4500"];
        [_baseInfoView addSubview:_levelLabel];
        
        [_levelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_baseInfoView);
            make.top.equalTo(_baseInfoView.mas_centerY).offset(5.0);
        }];
    }
    return _levelLabel;
}

#pragma mark - 获取联系方式
- (UIButton *)contactBtn{
    if(!_contactBtn){
        
        UIView *view = [[UIView alloc]init];
        view.backgroundColor = [UIColor clearColor];
        [_baseInfoView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_baseInfoView);
            make.centerY.equalTo(_baseInfoView);
        }];
       
        
        _contactBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_contactBtn setTitle:@"获取联系方式" forState:UIControlStateNormal];
        _contactBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        _contactBtn.titleLabel.numberOfLines = 0;
        [_contactBtn addTarget:self action:@selector(contactBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:_contactBtn];
        
        [_contactBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(view).offset(14.0);
            make.right.equalTo(view).offset(-14.0);
            make.top.equalTo(view).offset(4.0);
            make.bottom.equalTo(view).offset(-4.0);
        }];
        
        [view layoutIfNeeded];
        CAGradientLayer *gradientLayer = [CAGradientLayer layer];
        gradientLayer.colors = @[(__bridge id)[UIColor colorFromHexString:@"#437AE4"].CGColor, (__bridge id)[UIColor colorFromHexString:@"#BF4AC2"].CGColor];
        gradientLayer.locations = @[@0.0, @1.0];
        gradientLayer.startPoint = CGPointMake(0, 0.5);
        gradientLayer.endPoint = CGPointMake(1.0, 0.5);
        gradientLayer.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
        gradientLayer.shadowColor= [UIColor colorFromHexString:@"#437AE4"].CGColor;
        gradientLayer.masksToBounds = YES;
        gradientLayer.cornerRadius = view.frame.size.height/2.0;
        [view.layer addSublayer:gradientLayer];
        
        [view bringSubviewToFront:_contactBtn];
    }
    return _contactBtn;
}
- (void)contactBtnClick{
    [SWHUDUtil hideHudViewWithMessageSuperView:self withMessage:@"未公开"];
}

#pragma mark - 数据
- (void)setModelDic:(NSDictionary *)modelDic{
    _modelDic = modelDic;
    
    //头像
    NSString *headStr = [QWM_BASE_API_URL stringByAppendingFormat:@"/bokeupload%@", _modelDic[@"photo"]];
    [_headPicImageView sd_setImageWithURL:[NSURL URLWithString:headStr] placeholderImage:[UIImage imageNamed:@"contact_default_avatar"]];

    //姓名
    _nameLabel.text = _modelDic[@"name"];


    //级别
    _levelLabel.text = _modelDic[@"level"];

   
        
}

@end
