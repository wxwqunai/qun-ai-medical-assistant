//
//  QWServiceTeamHomeCell.h
//  assistant
//
//  Created by kevin on 2023/9/5.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWServiceTeamHomeCell : QWBaseTableViewCell
@property (nonatomic, strong) NSDictionary *modelDic;
@end

NS_ASSUME_NONNULL_END
