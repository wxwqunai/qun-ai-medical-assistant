//
//  QWTrainHomeCell.m
//  assistant
//
//  Created by qunai on 2023/8/8.
//

#import "QWTrainHomeCell.h"

@implementation QWTrainHomeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];
        [self bgView];
        [self progressView];
        [self statebtn];
        [self nameLabel];
    }
    return self;
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.top.equalTo(self.contentView).offset(1);
            make.bottom.equalTo(self.contentView).offset(-1);
            make.height.mas_offset(56.0);
        }];
    }
    return _bgView;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font =[UIFont systemFontOfSize:17];
        [self.bgView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView).offset(8);
            make.top.equalTo(self.bgView);
            make.bottom.equalTo(self.bgView);
            
        }];
    }
    return _nameLabel;
}


#pragma mark - 资料下载
- (UIView *)progressView{
    if(!_progressView){
        _progressView = [[UIView alloc]init];
        _progressView.backgroundColor = [UIColor clearColor];
        [self.bgView addSubview:_progressView];
        
        [_progressView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.bgView).offset(-4);
            make.centerY.equalTo(self.bgView);
        }];
        
        
        _progressImageView = [[UIImageView alloc]init];
        _progressImageView.backgroundColor = [UIColor colorFromHexString:@"#1E90FF"];
        [_progressView addSubview:_progressImageView];
        
        [_progressImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_progressView);
            make.top.equalTo(_progressView);
            make.bottom.equalTo(_progressView);
            make.width.equalTo(_progressView.mas_width).multipliedBy(0.0);
        }];
    }
    return _progressView;
}


- (UIButton *)statebtn{
    if(!_statebtn){
        _statebtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _statebtn.enabled = NO;
        _statebtn.layer.borderWidth = 0.0;
        _statebtn.layer.borderColor = [UIColor redColor].CGColor;
        [_statebtn setTitle:@"待配置" forState:UIControlStateNormal];
        [_statebtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        _statebtn.titleLabel.font =[UIFont systemFontOfSize:13];
        [_statebtn addTarget:self action:@selector(statebtnClcik) forControlEvents:UIControlEventTouchUpInside];
        [_progressView addSubview:_statebtn];
        
        [_statebtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_progressView);
            make.width.mas_offset(60);
        }];
    }
    return _statebtn;
}

- (void)statebtnClcik{
    if(self.stateBtnClcikBlock)self.stateBtnClcikBlock();
}

- (void)changeSateBtn:(BOOL)isCanDowload{
    if(isCanDowload){
        _statebtn.enabled = YES;
        _statebtn.layer.borderWidth = 1.0;
        _statebtn.layer.borderColor = Color_Main_Green.CGColor;
        [_statebtn setTitle:@"资料下载" forState:UIControlStateNormal];
        [_statebtn setTitleColor:Color_Main_Green forState:UIControlStateNormal];
        
        [_statebtn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_offset(80);
        }];
        
        
        _progressImageView.hidden = NO;
    }else{
        _statebtn.enabled = NO;
        _statebtn.layer.borderWidth = 0.0;
        _statebtn.layer.borderColor = [UIColor redColor].CGColor;
        [_statebtn setTitle:@"待配置" forState:UIControlStateNormal];
        [_statebtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_statebtn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_offset(60);
        }];
        
        _progressImageView.hidden = YES;
    }
}

#pragma mark - 选中
- (void)changeUIForSelState:(BOOL)isel{
    self.bgView.backgroundColor = isel?Color_Main_Green:[UIColor whiteColor];
    self.nameLabel.textColor = isel?[UIColor whiteColor]:[UIColor blackColor];
}

#pragma mark- 下载进度
- (void)setProgressNum:(double)progressNum{
    _progressNum = progressNum;
    NSLog(@"_progressNum====%f",_progressNum);
    [_progressImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_progressView);
        make.top.equalTo(_progressView);
        make.bottom.equalTo(_progressView);
        make.width.equalTo(_progressView.mas_width).multipliedBy(_progressNum);
    }];
    
    
    
    
    if(_progressNum <= 0){
        [_statebtn setTitle:@"资料下载" forState:UIControlStateNormal];
        [_statebtn setTitleColor:Color_Main_Green forState:UIControlStateNormal];
    }else if(_progressNum >= 1){
        [_statebtn setTitle:@"重新下载" forState:UIControlStateNormal];
        [_statebtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }else{
        [_statebtn setTitle:@"下载中" forState:UIControlStateNormal];
        [_statebtn setTitleColor:_progressNum>0.7?[UIColor whiteColor]:Color_Main_Green forState:UIControlStateNormal];
    }
    
}


@end
