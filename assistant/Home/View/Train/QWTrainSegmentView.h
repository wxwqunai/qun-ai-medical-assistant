//
//  QWTrainSegmentView.h
//  assistant
//
//  Created by qunai on 2023/7/31.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
//定义block，用来传递点击的第几个按钮
typedef void (^PassValueBlock)(NSInteger index);

@interface QWTrainSegmentView : UIView
//定义一下block
@property (nonatomic, strong) PassValueBlock returnBlock;
@property (nonatomic, strong) UIImageView *selectImage;//这个就是短红线

- (instancetype) initWithTitleArray:(NSArray *)array;
@end

NS_ASSUME_NONNULL_END



