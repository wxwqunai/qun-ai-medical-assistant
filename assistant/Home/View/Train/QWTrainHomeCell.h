//
//  QWTrainHomeCell.h
//  assistant
//
//  Created by qunai on 2023/8/8.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN
typedef void (^StateBtnClcikBlock)(void);

@interface QWTrainHomeCell : QWBaseTableViewCell
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, strong) UIView *progressView;
@property (nonatomic, strong) UIImageView *progressImageView;
@property (nonatomic, strong) UIButton *statebtn;

@property (nonatomic, assign) double progressNum;


@property (nonatomic, copy) StateBtnClcikBlock stateBtnClcikBlock;

- (void)changeUIForSelState:(BOOL)isel;
- (void)changeSateBtn:(BOOL)isCanDowload;
@end

NS_ASSUME_NONNULL_END
