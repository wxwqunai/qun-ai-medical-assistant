//
//  QWOptionConfirmView.h
//  assistant
//
//  Created by qunai on 2023/7/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^ConfirmClickBlock)(void);

@interface QWOptionConfirmView : UITableViewHeaderFooterView

@property (nonatomic,copy)ConfirmClickBlock confirmClickBlock;

@end

NS_ASSUME_NONNULL_END
