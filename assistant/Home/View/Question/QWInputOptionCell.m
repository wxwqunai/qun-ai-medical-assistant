//
//  QWInputOptionCell.m
//  assistant
//
//  Created by qunai on 2023/7/20.
//

#import "QWInputOptionCell.h"

@implementation QWInputOptionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self importInfoView];
    }
    return self;
}

//输入框
-(UITextView *)importInfoView
{
    if (!_importInfoView) {
        _importInfoView =[[UITextView alloc]init];
        _importInfoView.text = @"";
        _importInfoView.returnKeyType=UIReturnKeyDone;
        _importInfoView.delegate=self;
        _importInfoView.font=[UIFont fontWithName:@"PingFangSC-Medium" size:15];
        _importInfoView.textColor=[UIColor colorFromHexString:@"#707070"];
        _importInfoView.backgroundColor=[UIColor clearColor];
//        _importInfoView.scrollEnabled = NO;
        
        _importInfoView.layer.borderWidth=1;
        _importInfoView.layer.borderColor=Color_Main_Green.CGColor;
        
        [self.contentView addSubview:_importInfoView];
        
        [_importInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(20);
            make.right.equalTo(self.contentView).offset(-20);
            make.top.equalTo(self.contentView).offset(5);
            make.bottom.equalTo(self.contentView).offset(-5);
            make.height.mas_equalTo(80);
        }];
        
        _placeholderLabel =[[UILabel alloc]init];
        _placeholderLabel.text = @"请输入";
        _placeholderLabel.font = [UIFont systemFontOfSize:15];
        _placeholderLabel.textColor = [UIColor colorFromHexString:@"#CBCBCB"];
        [_importInfoView addSubview:_placeholderLabel];
        
        [_placeholderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(4);
            make.top.mas_offset(8);
        }];
    }
    return _importInfoView;
}

#pragma mark- UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
//    self.optionModel.content = textView.text;
    return YES;
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    _placeholderLabel.hidden = YES;
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    self.optionModel.content = textView.text;
    _placeholderLabel.hidden = !IsStringEmpty(textView.text);
    return YES;
}

- (void)setOptionModel:(QWOptionModel *)optionModel{
    _optionModel = optionModel;
    _importInfoView.text = _optionModel.content;
}

@end
