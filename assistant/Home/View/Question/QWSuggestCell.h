//
//  QWSuggestCell.h
//  assistant
//
//  Created by qunai on 2023/7/25.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWSuggestCell : QWBaseTableViewCell
@property (nonatomic, strong) UIView *baseInfoView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *suggestLabel;
@end

NS_ASSUME_NONNULL_END
