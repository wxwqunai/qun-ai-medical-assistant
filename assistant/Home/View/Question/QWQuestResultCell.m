//
//  QWQuestResultCell.m
//  assistant
//
//  Created by qunai on 2023/7/24.
//

#import "QWQuestResultCell.h"

@implementation QWQuestResultCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
   
        [self nameLabel];
//        [self desLabel];
    }
    return self;
}


- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont systemFontOfSize:17];
        _nameLabel.numberOfLines = 0;
        [self.contentView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(15.0);
            make.top.equalTo(self.contentView).offset(8.0);
            make.right.equalTo(self.contentView).offset(-15.0);
            make.bottom.equalTo(self.contentView).offset(-8.0);
        }];
    }
    return _nameLabel;
}

//- (UILabel *)desLabel{
//    if(!_desLabel){
//        _desLabel = [[UILabel alloc]init];
//        _desLabel.font = [UIFont systemFontOfSize:15];
//        _desLabel.textColor = [UIColor colorFromHexString:@"#778899"];
//        _desLabel.numberOfLines = 0;
//        [self.contentView addSubview:_desLabel];
//
//        [_desLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(self.contentView).offset(15.0);
//            make.top.equalTo(self.nameLabel.mas_bottom).offset(8.0);
//            make.right.equalTo(self.contentView).offset(-15.0);
//            make.bottom.equalTo(self.contentView).offset(-8.0);
//        }];
//    }
//    return _desLabel;
//}

@end
