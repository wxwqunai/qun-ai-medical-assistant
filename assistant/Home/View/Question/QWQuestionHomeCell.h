//
//  QWQuestionHomeCell.h
//  assistant
//
//  Created by qunai on 2023/7/18.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWQuestionHomeCell : QWBaseTableViewCell
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UILabel *nameLabel;
@end

NS_ASSUME_NONNULL_END
