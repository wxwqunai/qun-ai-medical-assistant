//
//  QWBaseInfoHeaderView.h
//  assistant
//
//  Created by qunai on 2023/7/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWBaseInfoHeaderView : UITableViewHeaderFooterView
@property (nonatomic, strong) UILabel *nameLabel;
@end

NS_ASSUME_NONNULL_END
