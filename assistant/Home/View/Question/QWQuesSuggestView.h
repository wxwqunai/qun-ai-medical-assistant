//
//  QWQuesSuggestView.h
//  assistant
//
//  Created by qunai on 2023/8/30.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWQuesSuggestView : UIView
@property (nonatomic, strong) NSArray *dataArr;
@end

NS_ASSUME_NONNULL_END
