//
//  QWSingleOptionCell.m
//  assistant
//
//  Created by qunai on 2023/7/14.
//

#import "QWSingleOptionCell.h"
@interface QWSingleOptionCell()
@property (nonatomic, strong) UILabel *nameLabel;

@end

@implementation QWSingleOptionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self circleButton];
        [self nameLabel];
    }
    return self;
}
#pragma mark - 图标 - 圆
- (UIButton *)circleButton{
    if(!_circleButton){
        _circleButton = [[UIButton alloc]init];
        _circleButton.layer.cornerRadius = 10.0;
        _circleButton.layer.masksToBounds = YES;
        _circleButton.backgroundColor = [UIColor grayColor];
        _circleButton.backgroundColor = self.isSeleted?Color_Main_Green:[UIColor grayColor];
        _circleButton.userInteractionEnabled = NO;
        [self addSubview:_circleButton];
        
        [_circleButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(20);
            make.centerY.equalTo(self);
            make.height.mas_offset(20);
            make.width.mas_offset(20);
        }];
    }
    return _circleButton;
}

#pragma mark - name
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont systemFontOfSize:14];
        _nameLabel.numberOfLines = 0;
        [self addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_circleButton.mas_right).offset(5);
            make.right.equalTo(self).offset(-20);
            make.top.mas_offset(5.0);
            make.bottom.mas_offset(-5.0);
            make.height.mas_greaterThanOrEqualTo(44);
        }];
    }
    return _nameLabel;
}

- (void)setNameStr:(NSString *)nameStr{
    _nameStr = nameStr;
    _nameLabel.text = _nameStr;
}
- (void)setIsSeleted:(BOOL)isSeleted{
    _isSeleted = isSeleted;
    _circleButton.backgroundColor = self.isSeleted?Color_Main_Green:[UIColor grayColor];
}



@end
