//
//  QWOptionTitleView.m
//  assistant
//
//  Created by qunai on 2023/7/14.
//

#import "QWOptionTitleView.h"
@interface QWOptionTitleView()
@property (nonatomic, strong) UILabel *nameLabel;
@end

@implementation QWOptionTitleView


- (instancetype)initWithReuseIdentifier:(nullable NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if(self){
//        self.backgroundColor = [UIColor whiteColor];
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self nameLabel];
    }
    return self;
}

#pragma mark - name
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont boldSystemFontOfSize:17];
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.numberOfLines = 0;
        [self addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(14);
            make.top.equalTo(self).offset(8);
            make.bottom.equalTo(self).offset(-8);
            make.right.equalTo(self).offset(-14);
        }];
    }
    return _nameLabel;
}

- (void)setNameStr:(NSString *)nameStr{
    _nameStr = nameStr;
    _nameLabel.text = _nameStr;
}

@end
