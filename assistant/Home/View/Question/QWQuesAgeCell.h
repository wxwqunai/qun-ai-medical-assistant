//
//  QWQuesAgeCell.h
//  assistant
//
//  Created by qunai on 2023/7/19.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWQuesAgeCell : QWBaseTableViewCell
@property (nonatomic, strong) UIDatePicker *datePicker;
@end

NS_ASSUME_NONNULL_END
