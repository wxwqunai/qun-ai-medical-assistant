//
//  QWQuestionChangeBtnView.h
//  assistant
//
//  Created by qunai on 2023/7/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, QWBtnType) {
    QWBtnType_default = 0,
    QWBtnType_last,
    QWBtnType_next,
    QWBtnType_confirm,
};

typedef void (^ChangeBtnClickBlock)(QWBtnType type);
@interface QWQuestionChangeBtnView : UIView
@property (nonatomic,copy) ChangeBtnClickBlock changeBtnClickBlock;

//- (void)changeBtnState:(NSInteger)cur_num withAll:(NSInteger)all_num;

- (void)changeLastStepBtnState:(BOOL)isEnable;

- (void)changeConfirmBtnState:(BOOL)isEnable;
@end

NS_ASSUME_NONNULL_END
