//
//  QWOptionTitleView.h
//  assistant
//
//  Created by qunai on 2023/7/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWOptionTitleView : UITableViewHeaderFooterView
@property (nonatomic,copy) NSString *nameStr;
@end

NS_ASSUME_NONNULL_END
