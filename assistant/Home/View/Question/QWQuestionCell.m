//
//  QWQuestionCell.m
//  assistant
//
//  Created by qunai on 2023/7/14.
//

#import "QWQuestionCell.h"
#import "QWOptionTitleView.h"
#import "QWOptionConfirmView.h"
#import "QWSingleOptionCell.h"
#import "QWQuestModel.h"
#import "QWInputOptionCell.h"

@implementation QWQuestionCell
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorFromHexString:@"#F4F4F4"];
        
        [self tableView];
    }
    return self;
}

#pragma mark - tabbleView
- (UITableView *)tableView{
    if(!_tableView){
        _tableView =[[UITableView alloc]initWithFrame:self.bounds style:UITableViewStyleGrouped];
        _tableView.estimatedRowHeight = 400;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedSectionFooterHeight=0;
        _tableView.estimatedSectionHeaderHeight=0;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.showsHorizontalScrollIndicator=NO;
        
        _tableView.delegate =self;
        _tableView.dataSource =self;
        
        [_tableView registerClass:[QWOptionTitleView class] forHeaderFooterViewReuseIdentifier:@"QWOptionTitleView"];
        [_tableView registerClass:[QWOptionConfirmView class] forHeaderFooterViewReuseIdentifier:@"QWOptionConfirmView"];
//        [_tableView registerClass:[QADoctorItemCell class] forCellReuseIdentifier:@"QADoctorItemCell"];
        [_tableView registerClass:[QWSingleOptionCell class] forCellReuseIdentifier:@"QWSingleOptionCell"];
        [_tableView registerClass:[QWInputOptionCell class] forCellReuseIdentifier:@"QWInputOptionCell"];
        

        _tableView.backgroundColor=[UIColor whiteColor];
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
//        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//铺满全屏
        _tableView.estimatedSectionHeaderHeight = 100;
        _tableView.estimatedSectionFooterHeight = 100;
//        _tableView.sectionHeaderHeight = 50;

//        [_tableView addRefreshHeaderWithTarget:self action:@selector(loadPullDownRefreshDataInfo)];//下拉刷新
//        [_tableView addLoadMoreFooterWithTarget:self action:@selector(loadPullUpRefreshDataInfo)];//上拉加载
        
        [self addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
    }
    return _tableView;
}
#pragma mark- UITableViewDelegate,UITableViewDataSource
//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    return 0.0000001;
//}
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 0.0000001;
//}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.questModel.options.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QWOptionModel *model = self.questModel.options[indexPath.row];
    if([model.type isEqualToString:@"input"]){
        QWInputOptionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWInputOptionCell"];
        cell.optionModel = model;
        return cell;
    }else{
        QWSingleOptionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QWSingleOptionCell"];
        cell.nameStr = [NSString stringWithFormat:@"%@%@",model.num,model.content];
        cell.isSeleted = model.isSel;
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    QWOptionModel *model = self.questModel.options[indexPath.row];
    
    //单选
    if([self.questModel.q_type isEqualToString:@"select"]){
        [self reStoreOptionModelForSel];
        model.isSel = YES;

        if (self.delegate&&[self.delegate respondsToSelector:@selector(optionSelectBtnDidPress:cellForItemAtIndexPath:)]) {
            [self.delegate optionSelectBtnDidPress:model cellForItemAtIndexPath:self.indexPath];
        }
    }
    
    //多选
    if([self.questModel.q_type isEqualToString:@"multiple"]){
        model.isSel = !model.isSel;
    }
    
    //单选_输入框
    if([self.questModel.q_type isEqualToString:@"single_input"] ){
        if([model.type isEqualToString:@"single"]){
            [self reStoreOptionModelForSel];
            model.isSel = YES;

            if (self.delegate&&[self.delegate respondsToSelector:@selector(optionSelectBtnDidPress:cellForItemAtIndexPath:)]) {
                [self.delegate optionSelectBtnDidPress:model cellForItemAtIndexPath:self.indexPath];
            }
        }
    }

    [self.tableView reloadData];

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    QWOptionTitleView *titleView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"QWOptionTitleView"];
    NSString * ptionTypeStr = [self reOptionType];
    NSString * contentStr = [NSString stringWithFormat:@"%ld.%@%@",self.indexPath.row+1,self.questModel.q_title,ptionTypeStr];
    titleView.nameStr = contentStr;
    return titleView;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    //多选
    if([self.questModel.q_type isEqualToString:@"multiple"]){
        QWOptionConfirmView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"QWOptionConfirmView"];
        view.confirmClickBlock = ^{
            
            QWOptionModel *sel_model;
            for (QWOptionModel *model in self.questModel.options) {
                if(model.isSel)sel_model = model;
            }
            
            if(sel_model == nil) {
                [SWHUDUtil hideHudViewWithMessageSuperView:self withMessage:@"请完成作答！"];
                return;
            }
            
            if (self.delegate&&[self.delegate respondsToSelector:@selector(optionSelectBtnDidPress:cellForItemAtIndexPath:)]) {
                [self.delegate optionSelectBtnDidPress:sel_model cellForItemAtIndexPath:self.indexPath];
            }
        };
        return view;
    }
    return nil;
}

- (void)setQuestModel:(QWQuesAitimuListModel *)questModel{
    _questModel = questModel;
    [self.tableView reloadData];
}

#pragma mark - 选项类型
- (NSString *)reOptionType{
    NSString *result = @"";
    if([self.questModel.q_type isEqualToString:@"select"]){
        result = @"(单选)";
    }else if([self.questModel.q_type isEqualToString:@"multiple"]){
        result = @"(多选)";
    }else if([self.questModel.q_type isEqualToString:@"single_input"]){
        result = @"(单选/填空)";
    }
    else if([self.questModel.q_type isEqualToString:@"input"]){
        result = @"(填空)";
    }
    return result;
}

#pragma mark - 清空选中状态
- (void)reStoreOptionModelForSel{
    for (QWOptionModel *model in self.questModel.options) {
        model.isSel = NO;
    }
}

#pragma mark - 检查是否已填选完成
- (BOOL)checkOptionModelForSel{
    BOOL result = NO;
    for (QWOptionModel *model in self.questModel.options) {
        if(model.isSel){
            result = YES;
            break;
        }
        
        if([model.type isEqualToString:@"input"]){
            if(!IsStringEmpty(model.content)){
                result = YES;
                break;
            }
        }
    }
    return result;
}

@end
