//
//  QWQuesBtnBottomView.h
//  assistant
//
//  Created by qunai on 2023/7/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^ConfirmClickBlock)(void);
@interface QWQuesBtnBottomView : UIView
@property (nonatomic,copy)ConfirmClickBlock confirmClickBlock;

@property (nonatomic, copy) NSString *btnName;

@end

NS_ASSUME_NONNULL_END
