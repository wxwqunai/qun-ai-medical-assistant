//
//  QWSymptomCell.h
//  assistant
//
//  Created by qunai on 2023/7/20.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWSymptomCell : QWBaseTableViewCell
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UILabel *nameLabel;

- (void)changeUIForSelState:(BOOL)isel;
@end

NS_ASSUME_NONNULL_END
