//
//  QWQuesRulerCell.m
//  assistant
//
//  Created by qunai on 2023/7/19.
//

#import "QWQuesRulerCell.h"

@implementation QWQuesRulerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self nameLabel];
        [self rulerView];
    }
    return self;
}


- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font =[UIFont systemFontOfSize:16];
        [self.contentView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.top.equalTo(self.contentView).offset(15.0);
            make.height.mas_offset(20);
        }];
    }
    return _nameLabel;
}

- (void)addRulerView:(QWRulerType)type{
    
    NSInteger mixNuber = 0;
    NSInteger maxNuber = 0;
    NSInteger defaultVaule = 0;
    if(type == QWRulerType_height){
        mixNuber = 65;
        maxNuber = 235;
        defaultVaule = 165;
    }
    
    if(type == QWRulerType_weight){
        mixNuber = 15;
        maxNuber = 195;
        defaultVaule = 60;
    }
    
    
    if(!_rulerView){
        _rulerView = [[ZHRulerView alloc]initWithMixNuber:mixNuber maxNuber:maxNuber showType:(rulerViewshowHorizontalType) rulerMultiple:10];
        _rulerView.defaultVaule = defaultVaule;
        _rulerView.delegate = self;
        [self.contentView addSubview:_rulerView];
        
        [_rulerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.top.equalTo(self.nameLabel.mas_bottom).offset(18);
            make.bottom.equalTo(self.contentView);
            make.width.mas_equalTo(kScreenWidth/4.0*3.0);
            make.height.mas_equalTo(60);
        }];
        
        
        _nameLabel.text = [NSString stringWithFormat:@"%ld",defaultVaule];
    }
    
    
}

#pragma mark - ZHRulerViewDelegate
- (void)getRulerValue:(CGFloat)rulerValue withScrollRulerView:(ZHRulerView *)rulerView{
    _nameLabel.text = [NSString stringWithFormat:@"%.1f",rulerValue];
}

@end
