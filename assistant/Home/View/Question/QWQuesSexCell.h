//
//  QWQuesSexCell.h
//  assistant
//
//  Created by qunai on 2023/7/18.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWQuesSexCell : QWBaseTableViewCell
@property (nonatomic, strong) UIButton *maleBtn;
@property (nonatomic, strong) UIButton *femaleBtn;
@end

NS_ASSUME_NONNULL_END
