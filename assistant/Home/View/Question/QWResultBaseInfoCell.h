//
//  QWResultBaseInfoCell.h
//  assistant
//
//  Created by qunai on 2023/8/23.
//

#import "QWBaseTableViewCell.h"
#import "QWQuestModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWResultBaseInfoCell : QWBaseTableViewCell
@property (nonatomic,copy) QWQuesAianswerListModel *model;

@end

NS_ASSUME_NONNULL_END
