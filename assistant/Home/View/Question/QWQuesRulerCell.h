//
//  QWQuesRulerCell.h
//  assistant
//
//  Created by qunai on 2023/7/19.
//

#import "QWBaseTableViewCell.h"
#import "ZHRulerView.h"


typedef NS_ENUM(NSInteger, QWRulerType) {
    QWRulerType_default = 0,
    QWRulerType_height,
    QWRulerType_weight,
};

NS_ASSUME_NONNULL_BEGIN

@interface QWQuesRulerCell : QWBaseTableViewCell<ZHRulerViewDelegate>
@property (nonatomic, strong) ZHRulerView *rulerView;
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UILabel *nameLabel;

- (void)addRulerView:(QWRulerType)type;
@end

NS_ASSUME_NONNULL_END
