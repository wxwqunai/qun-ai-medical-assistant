//
//  QWQuestionCell.h
//  assistant
//
//  Created by qunai on 2023/7/14.
//

#import <UIKit/UIKit.h>
#import "QWQuestModel.h"
NS_ASSUME_NONNULL_BEGIN

@class QWQuestionCell;
@protocol QWOptionSelectDelegate <NSObject>

@optional;
- (void)optionSelectBtnDidPress:(QWOptionModel* _Nullable)optionModel
         cellForItemAtIndexPath:(NSIndexPath *)indexPath;

@end

@interface QWQuestionCell : UICollectionViewCell<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) QWQuesAitimuListModel *questModel;
/** 代理 */
@property(nonatomic, weak)  id<QWOptionSelectDelegate>  delegate;

#pragma mark - 检查是否已有选中状态
- (BOOL)checkOptionModelForSel;
@end

NS_ASSUME_NONNULL_END
