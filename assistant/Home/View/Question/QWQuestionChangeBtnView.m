//
//  QWQuestionChangeBtnView.m
//  assistant
//
//  Created by qunai on 2023/7/14.
//

#import "QWQuestionChangeBtnView.h"
@interface QWQuestionChangeBtnView()
@property (nonatomic, strong) UIView *buttonView;
@property (nonatomic, strong) UIButton *lastStepButton;
//@property (nonatomic, strong) UIButton *nextStepButton;
@property (nonatomic, strong) UIButton *confirmButton;
@end
@implementation QWQuestionChangeBtnView

-(instancetype)init
{
    self =[super init];
    if (self) {
//        self.backgroundColor = [UIColor whiteColor];
        [self buttonView];
        [self confirmButton];
        [self lastStepButton];
//        [self nextStepButton];
    }
    return self;
}

- (UIView *)buttonView{
    if(!_buttonView){
        _buttonView = [[UIView alloc]init];
        _buttonView.backgroundColor =[UIColor clearColor];
        [self addSubview:_buttonView];
        
        [_buttonView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.top.equalTo(self);
            make.bottom.equalTo(self).offset(-[UIDevice safeDistanceBottom]-10);
        }];
    }
    return _buttonView;
}

#pragma mark - 上一步
-(UIButton *)lastStepButton
{
    if (!_lastStepButton) {
        _lastStepButton =[UIButton buttonWithType:UIButtonTypeCustom];
        [_lastStepButton setTitle:@"上一题" forState:UIControlStateNormal];
        [_lastStepButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _lastStepButton.titleLabel.font = [UIFont systemFontOfSize:14];
        _lastStepButton.backgroundColor = [UIColor lightGrayColor];
        _lastStepButton.enabled = NO;
        [_lastStepButton addTarget:self action:@selector(lastStepButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        _lastStepButton.layer.cornerRadius = 6;
        _lastStepButton.layer.masksToBounds = YES;
        
        [self.buttonView addSubview:_lastStepButton];
        
        [_lastStepButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.buttonView);
            make.bottom.equalTo(self.buttonView);
            make.right.equalTo(self.confirmButton.mas_left).offset(-20);
            make.left.equalTo(self.buttonView).offset(40);
        }];
    }
    return _lastStepButton;
}
-(void)lastStepButtonClick:(UIButton *)sender
{
    if(self.changeBtnClickBlock)self.changeBtnClickBlock(QWBtnType_last);
}

//#pragma mark - 下一步
//-(UIButton *)nextStepButton
//{
//    if (!_nextStepButton) {
//        _nextStepButton =[UIButton buttonWithType:UIButtonTypeCustom];
//        [_nextStepButton setTitle:@"下一题" forState:UIControlStateNormal];
//        [_nextStepButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        _nextStepButton.titleLabel.font = [UIFont systemFontOfSize:14];
//        _nextStepButton.backgroundColor = Color_Main_Green;
//        [_nextStepButton addTarget:self action:@selector(nextStepButtonClick:) forControlEvents:UIControlEventTouchUpInside];
//
//        _nextStepButton.layer.cornerRadius = 6;
//        _nextStepButton.layer.masksToBounds = YES;
//
//        [self.buttonView addSubview:_nextStepButton];
//
//        [_nextStepButton mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(self.confirmButton.mas_right).offset(20);
//            make.width.equalTo(self.buttonView.mas_width).multipliedBy(0.25);
//            make.height.equalTo(self.buttonView);
//        }];
//    }
//    return _nextStepButton;
//}
//-(void)nextStepButtonClick:(UIButton *)sender
//{
//    if(self.changeBtnClickBlock)self.changeBtnClickBlock(QWBtnType_next);
//}

#pragma mark - 完成
-(UIButton *)confirmButton
{
    if (!_confirmButton) {
        _confirmButton =[UIButton buttonWithType:UIButtonTypeCustom];
        [_confirmButton setTitle:@"提交" forState:UIControlStateNormal];
        [_confirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _confirmButton.titleLabel.font = [UIFont systemFontOfSize:14];
        _confirmButton.backgroundColor = [UIColor lightGrayColor];
        _confirmButton.enabled = NO;
        [_confirmButton addTarget:self action:@selector(confirmButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        _confirmButton.layer.cornerRadius = 6;
        _confirmButton.layer.masksToBounds = YES;
        
        [self.buttonView addSubview:_confirmButton];
        
        [_confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.buttonView);
            make.bottom.equalTo(self.buttonView);
            make.right.equalTo(self.buttonView).offset(-40);
            make.width.equalTo(self.buttonView.mas_width).multipliedBy(0.25);
        }];
    }
    return _confirmButton;
}
-(void)confirmButtonClick:(UIButton *)sender
{
    if(self.changeBtnClickBlock)self.changeBtnClickBlock(QWBtnType_confirm);
}


#pragma mark - 按钮状态改变
- (void)changeLastStepBtnState:(BOOL)isEnable{
    self.lastStepButton.backgroundColor = isEnable?Color_Main_Green:[UIColor lightGrayColor];
    self.lastStepButton.enabled = isEnable;
}

- (void)changeConfirmBtnState:(BOOL)isEnable{
    self.confirmButton.backgroundColor = isEnable?Color_Main_Green:[UIColor lightGrayColor];
    self.confirmButton.enabled = isEnable;
}


//- (void)changeBtnState:(NSInteger)cur_num withAll:(NSInteger)all_num{
//    self.lastStepButton.backgroundColor = [UIColor lightGrayColor];
////    self.nextStepButton.backgroundColor = [UIColor lightGrayColor];
//    self.confirmButton.backgroundColor = [UIColor lightGrayColor];
//    
//    self.lastStepButton.enabled = NO;
////    self.nextStepButton.enabled = NO;
//    self.confirmButton.enabled = NO;
//    
//    if(cur_num > 0){
//        self.lastStepButton.backgroundColor = Color_Main_Green;
//        self.lastStepButton.enabled = YES;
//    }
//    
//    
//    
////    if(cur_num >= 0 && cur_num< all_num-1){
////        self.nextStepButton.backgroundColor = Color_Main_Green;
////        self.nextStepButton.enabled = YES;
////    }
//    
//    if(cur_num == all_num-1){
//        self.confirmButton.backgroundColor = Color_Main_Green;
//        self.confirmButton.enabled = YES;
//    }
//}

@end
