//
//  QWSymptomCell.m
//  assistant
//
//  Created by qunai on 2023/7/20.
//

#import "QWSymptomCell.h"

@implementation QWSymptomCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];
        [self bgView];
        [self nameLabel];
    }
    return self;
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.top.equalTo(self.contentView).offset(1);
            make.bottom.equalTo(self.contentView).offset(-1);
            make.height.mas_offset(56.0);
        }];
    }
    return _bgView;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font =[UIFont systemFontOfSize:17];
        [self.bgView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView).offset(8);
            make.right.equalTo(self.bgView);
            make.top.equalTo(self.bgView);
            make.bottom.equalTo(self.bgView);
            
        }];
    }
    return _nameLabel;
}

- (void)changeUIForSelState:(BOOL)isel{
    self.bgView.backgroundColor = isel?Color_Main_Green:[UIColor whiteColor];
    self.nameLabel.textColor = isel?[UIColor whiteColor]:[UIColor blackColor];
}

@end
