//
//  QWBaseInfoHeaderView.m
//  assistant
//
//  Created by qunai on 2023/7/18.
//

#import "QWBaseInfoHeaderView.h"
@interface QWBaseInfoHeaderView()
{
    UIImageView *_leftImageView;
    UIImageView *_rightImageView;
}

@end
@implementation QWBaseInfoHeaderView
- (instancetype)initWithReuseIdentifier:(nullable NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if(self){
//        self.backgroundColor = [UIColor whiteColor];
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self nameLabel];
        [self leftImageView];
        [self rightImageView];
    }
    return self;
}

#pragma mark - name
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont boldSystemFontOfSize:20];
        _nameLabel.textColor = Color_Main_Green;
        [self.contentView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.centerY.equalTo(self);
        }];
    }
    return _nameLabel;
}

- (UIImageView *)leftImageView{
    if(!_leftImageView){
        _leftImageView = [[UIImageView alloc]init];
        _leftImageView.backgroundColor = Color_Line_Gray;
        [self.contentView addSubview:_leftImageView];
        
        [_leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_nameLabel.mas_left).offset(-15);
            make.left.mas_offset(40);
            make.centerY.equalTo(self.contentView);
            make.height.mas_equalTo(1);
        }];
    }
    return _leftImageView;
}

- (UIImageView *)rightImageView{
    if(!_rightImageView){
        _rightImageView = [[UIImageView alloc]init];
        _rightImageView.backgroundColor = Color_Line_Gray;
        [self.contentView addSubview:_rightImageView];
        
        [_rightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameLabel.mas_right).offset(15);
            make.right.mas_offset(-40);
            make.centerY.equalTo(self.contentView);
            make.height.mas_equalTo(1);
        }];
    }
    return _rightImageView;
}

@end
