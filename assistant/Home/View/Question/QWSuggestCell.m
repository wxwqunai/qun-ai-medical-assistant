//
//  QWSuggestCell.m
//  assistant
//
//  Created by qunai on 2023/7/25.
//

#import "QWSuggestCell.h"

@implementation QWSuggestCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self baseInfoView];
        
        [self nameLabel];
        [self suggestLabel];
    }
    return self;
}

#pragma mark - view
- (UIView *)baseInfoView{
    if(!_baseInfoView){
        _baseInfoView = [[UIView alloc]init];
//        _baseInfoView.backgroundColor = [UIColor colorFromHexString:@"#AFEEEE"];
        [self.contentView addSubview:_baseInfoView];
        
        [_baseInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(14.0);
            make.top.equalTo(self.contentView).offset(4.0);
            make.right.equalTo(self.contentView).offset(-14.0);
            make.bottom.equalTo(self.contentView).offset(-4.0);
        }];
    
    }
    return _baseInfoView;
}

#pragma mark - 姓名
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont boldSystemFontOfSize:18];
        _nameLabel.textColor = [UIColor blackColor];
        [self.baseInfoView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.baseInfoView).offset(8.0);
            make.top.equalTo(self.baseInfoView).offset(4.0);
            make.right.equalTo(self.baseInfoView).offset(-8.0);
        }];
    }
    return _nameLabel;
}

#pragma mark - 建议
- (UILabel *)suggestLabel{
    if(!_suggestLabel){
        _suggestLabel = [[UILabel alloc]init];
        _suggestLabel.font = [UIFont systemFontOfSize:17];
        _suggestLabel.textColor = [UIColor blackColor];
        _suggestLabel.numberOfLines = 0;
        [self.baseInfoView addSubview:_suggestLabel];
        
        [_suggestLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.baseInfoView).offset(8.0);
            make.right.equalTo(self.baseInfoView).offset(-8.0);
            make.top.equalTo(self.nameLabel.mas_bottom).offset(2);
            make.bottom.equalTo(self.baseInfoView).offset(-4);
        }];
    }
    return _suggestLabel;
}
#pragma mark - 数据

@end
