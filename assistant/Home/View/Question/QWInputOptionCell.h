//
//  QWInputOptionCell.h
//  assistant
//
//  Created by qunai on 2023/7/20.
//

#import "QWBaseTableViewCell.h"
#import "QWQuestModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWInputOptionCell : QWBaseTableViewCell<UITextViewDelegate>
@property (nonatomic,strong) UITextView *importInfoView;
@property (nonatomic, strong) UILabel *placeholderLabel;
@property (nonatomic,strong) QWOptionModel *optionModel;
@end

NS_ASSUME_NONNULL_END
