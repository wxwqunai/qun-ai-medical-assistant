//
//  QWQuesAgeCell.m
//  assistant
//
//  Created by qunai on 2023/7/19.
//

#import "QWQuesAgeCell.h"

@implementation QWQuesAgeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self datePicker];        
    }
    return self;
}

- (UIDatePicker *)datePicker{
    if(!_datePicker){
        _datePicker = [[UIDatePicker alloc]init];
        _datePicker.datePickerMode = UIDatePickerModeDate;
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        formatter.dateFormat = @"YYYY-MM-dd";
        _datePicker.date = [formatter dateFromString:@"1980-01-01"];
        _datePicker.minimumDate = [formatter dateFromString:@"1900-01-01"];
        _datePicker.maximumDate = [NSDate date];
        [self.contentView addSubview:_datePicker];
        
        [_datePicker mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.centerY.equalTo(self.contentView);
            make.top.bottom.equalTo(self.contentView);
            make.height.mas_offset(60);
        }];
    }
    return _datePicker;
}

@end
