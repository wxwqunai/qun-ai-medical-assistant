//
//  QWOptionConfirmView.m
//  assistant
//
//  Created by qunai on 2023/7/14.
//

#import "QWOptionConfirmView.h"
@interface QWOptionConfirmView()
@property (nonatomic, strong) UIButton *confirmbutton;

@end
@implementation QWOptionConfirmView

- (instancetype)initWithReuseIdentifier:(nullable NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if(self){
//        self.backgroundColor = [UIColor whiteColor];
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self confirmbutton];
    }
    return self;
}


-(UIButton *)confirmbutton
{
    if (!_confirmbutton) {
        _confirmbutton =[UIButton buttonWithType:UIButtonTypeCustom];
        [_confirmbutton setTitle:@"确定" forState:UIControlStateNormal];
        [_confirmbutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _confirmbutton.titleLabel.font = [UIFont systemFontOfSize:16];
        _confirmbutton.backgroundColor = Color_Main_Green;
        [_confirmbutton addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        _confirmbutton.layer.cornerRadius = 6;
        _confirmbutton.layer.masksToBounds = YES;
        
        [self addSubview:_confirmbutton];
        
        [_confirmbutton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.width.equalTo(self).multipliedBy(0.7);
            make.top.equalTo(self).offset(44);
            make.bottom.equalTo(self).offset(-10);
        }];
    }
    return _confirmbutton;
}
-(void)buttonClick:(UIButton *)sender
{
    if(self.confirmClickBlock)self.confirmClickBlock();
}



@end
