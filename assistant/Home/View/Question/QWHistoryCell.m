//
//  QWHistoryCell.m
//  assistant
//
//  Created by qunai on 2023/7/21.
//

#import "QWHistoryCell.h"
@interface QWHistoryCell()
@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UIView * userInfoView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *suggestLabel;
@property (nonatomic, strong) UILabel *timeLabel;

@property (nonatomic, strong) UIView *btnsView;
@property (nonatomic, strong) UIButton *checkSuggestBtn;
@property (nonatomic, strong) UIButton *giveSuggestBtn;
@end
@implementation QWHistoryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
      
        
        [self bgView];
        [self userInfoView];
        [self nameLabel];
        [self suggestLabel];
        [self timeLabel];
        
        [self btnsView];
        [self checkSuggestBtn];
        [self giveSuggestBtn];
    }
    return self;
}


- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = Color_Main_Green;
        _bgView.layer.cornerRadius = 8.0;
        _bgView.layer.masksToBounds = YES;
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(14);
//            make.right.equalTo(self.btnsView.mas_left).offset(-4);
            make.top.equalTo(self.contentView).offset(4);
            make.bottom.equalTo(self.contentView).offset(-4);
            make.height.mas_greaterThanOrEqualTo(80.0);
        }];
    }
    return _bgView;
}

#pragma mark - 用户基本信息
- (UIView *)userInfoView{
    if(!_userInfoView){
        _userInfoView = [[UIView alloc]init];
        _userInfoView.backgroundColor = [UIColor clearColor];
        [self.bgView addSubview:_userInfoView];
        
        [_userInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView).offset(8.0);
            make.right.equalTo(self.bgView).offset(-8.0);
            make.top.equalTo(self.bgView).offset(8.0);
        }];
    }
    return _userInfoView;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.textColor = [UIColor whiteColor];
        _nameLabel.font = [UIFont boldSystemFontOfSize:18];;
//        _nameLabel.text = @"性别：男";
        [self.userInfoView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.userInfoView);
            make.top.equalTo(self.userInfoView);
            make.right.equalTo(self.userInfoView.mas_centerX);
            
        }];
    }
    return _nameLabel;
}

- (UILabel *)suggestLabel{
    if(!_suggestLabel){
        _suggestLabel = [self customLabel];
        _suggestLabel.numberOfLines = 0;
//        _suggestLabel.text = @"年龄：33";
        [self.userInfoView addSubview:_suggestLabel];
        
        [_suggestLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.nameLabel.mas_bottom).offset(8.0);
            make.left.equalTo(self.userInfoView);
            make.right.equalTo(self.userInfoView);
            
        }];
    }
    return _suggestLabel;
}

#pragma mark - 时间
- (UILabel *)timeLabel{
    if(!_timeLabel){
        _timeLabel = [self customLabel];
//        _timeLabel.text = @"2023-07-24 13:22";
        [self.bgView addSubview:_timeLabel];
        
        [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.suggestLabel.mas_bottom).offset(8.0);
            make.right.equalTo(self.bgView).offset(-8.0);
            make.bottom.equalTo(self.bgView);
        }];
    }
    return _timeLabel;
}

#pragma mark - 查看诊断建议、给出诊断建议
- (UIView *)btnsView{
    if(!_btnsView){
        _btnsView = [[UIView alloc]init];
        _btnsView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_btnsView];
        
        [_btnsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView.mas_right).offset(4.0);
            make.right.equalTo(self.contentView).offset(-14);
            make.top.equalTo(self.bgView);
            make.bottom.equalTo(self.bgView);
            make.width.mas_offset(80.0);
        }];
    }
    return _btnsView;
}

//查看诊断建议
- (UIButton *)checkSuggestBtn{
    if(!_checkSuggestBtn){
        _checkSuggestBtn =[self customButton];
        [_checkSuggestBtn setTitle:@"查看\n诊断\n建议" forState:UIControlStateNormal];
        [_checkSuggestBtn addTarget:self action:@selector(checkSuggestBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.btnsView addSubview:_checkSuggestBtn];
        
        [_checkSuggestBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.btnsView);
            make.left.equalTo(self.btnsView);
            make.right.equalTo(self.btnsView);
            make.bottom.equalTo(self.btnsView.mas_centerY).offset(-2);
        }];
    }
    return _checkSuggestBtn;
}

- (void)checkSuggestBtnClick{
    if(self.checkSuggestBlock)self.checkSuggestBlock();
}

//给出诊断建议
- (UIButton *)giveSuggestBtn{
    if(!_giveSuggestBtn){
        _giveSuggestBtn = [self customButton];
        [_giveSuggestBtn setTitle:@"给出\n建议" forState:UIControlStateNormal];
        [_giveSuggestBtn addTarget:self action:@selector(giveSuggestBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.btnsView addSubview:_giveSuggestBtn];
        
        [_giveSuggestBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.btnsView.mas_centerY).offset(2);
            make.left.equalTo(self.btnsView);
            make.right.equalTo(self.btnsView);
            make.bottom.equalTo(self.btnsView);
        }];
    }
    return _giveSuggestBtn;
}
- (void)giveSuggestBtnClick{
    if(self.giveSuggestBlock)self.giveSuggestBlock();
}


#pragma mark - 公共
- (UILabel *)customLabel{
    UILabel *label = [[UILabel alloc]init];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont systemFontOfSize:18];
    return label;
}
- (UIButton *)customButton{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.titleLabel.numberOfLines = 0;
    button.titleLabel.font = [UIFont systemFontOfSize:18];
    button.layer.cornerRadius = 8.0;
    button.layer.masksToBounds = YES;
    button.backgroundColor = [UIColor colorFromHexString:@"#6591CD"];
    return button;
}

#pragma mark -因此给出建议按钮
- (void)hidGiveSuggestBtn{
    [self.checkSuggestBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.btnsView);
        make.left.equalTo(self.btnsView);
        make.right.equalTo(self.btnsView);
        make.bottom.equalTo(self.btnsView.mas_bottom);
    }];

    [self.giveSuggestBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.btnsView.mas_bottom);
        make.left.equalTo(self.btnsView);
        make.right.equalTo(self.btnsView);
        make.bottom.equalTo(self.btnsView);
    }];
    self.giveSuggestBtn.hidden = YES;
}

#pragma mark - 数据
- (void)setModel:(QWQuesAianswerListModel *)model{
    _model = model;
    _nameLabel.text = _model.user_name;
    
    _suggestLabel.text = IsStringEmpty(_model.ai_suggest1)?@"尚无明确建议":_model.ai_suggest1;
    
    _timeLabel.text = _model.answer_time;
}

- (void)setIsHiddenBtns:(BOOL)isHiddenBtns{
    _isHiddenBtns = isHiddenBtns;
    _btnsView.hidden = _isHiddenBtns;
    [_btnsView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.bgView.mas_right).offset(0.0);
        make.right.equalTo(self.contentView).offset(-14);
        make.top.equalTo(self.bgView);
        make.bottom.equalTo(self.bgView);
        make.width.mas_offset(0.0);
    }];
}










@end
