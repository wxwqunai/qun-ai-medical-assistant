//
//  QWHistoryCell.h
//  assistant
//
//  Created by qunai on 2023/7/21.
//

#import "QWBaseTableViewCell.h"
#import "QWQuestModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef void (^CheckSuggestBlock)(void);
typedef void (^GiveSuggestBlock)(void);

@interface QWHistoryCell : QWBaseTableViewCell
@property (nonatomic,copy)CheckSuggestBlock checkSuggestBlock;
@property (nonatomic,copy)GiveSuggestBlock giveSuggestBlock;
@property (nonatomic,copy) QWQuesAianswerListModel *model;

@property (nonatomic, assign) BOOL isHiddenBtns;
- (void)hidGiveSuggestBtn;
@end

NS_ASSUME_NONNULL_END
