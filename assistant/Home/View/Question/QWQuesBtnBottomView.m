//
//  QWQuesBtnBottomView.m
//  assistant
//
//  Created by qunai on 2023/7/18.
//

#import "QWQuesBtnBottomView.h"
@interface QWQuesBtnBottomView()
@property (nonatomic, strong) UIView *buttonView;
@property (nonatomic, strong) UIButton *confirmButton;
@end
@implementation QWQuesBtnBottomView

- (instancetype)init{
    self = [super init];
    if(self){
        [self buttonView];
        [self confirmButton];
    }
    return self;
}

- (UIView *)buttonView{
    if(!_buttonView){
        _buttonView = [[UIView alloc]init];
        _buttonView.backgroundColor =[UIColor clearColor];
        [self addSubview:_buttonView];
        
        [_buttonView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.top.equalTo(self);
            make.bottom.equalTo(self).offset(-[UIDevice safeDistanceBottom]-10);
        }];
    }
    return _buttonView;
}

#pragma mark - 完成
-(UIButton *)confirmButton
{
    if (!_confirmButton) {
        _confirmButton =[UIButton buttonWithType:UIButtonTypeCustom];
        [_confirmButton setTitle:@"下一步" forState:UIControlStateNormal];
        [_confirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _confirmButton.titleLabel.font = [UIFont systemFontOfSize:18];
        _confirmButton.backgroundColor = Color_Main_Green;
        [_confirmButton addTarget:self action:@selector(confirmButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        _confirmButton.layer.cornerRadius = 6;
        _confirmButton.layer.masksToBounds = YES;
        
        [self.buttonView addSubview:_confirmButton];
        
        [_confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.buttonView);
            make.width.equalTo(self.buttonView.mas_width).multipliedBy(0.8);
            make.height.equalTo(self.buttonView);
        }];
    }
    return _confirmButton;
}
-(void)confirmButtonClick:(UIButton *)sender
{
    if(self.confirmClickBlock)self.confirmClickBlock();
}

- (void)setBtnName:(NSString *)btnName{
    _btnName = btnName;
    [_confirmButton setTitle:_btnName forState:UIControlStateNormal];
}

//- (void)fullBottm{
//    [self.buttonView mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.equalTo(self);
//        make.top.equalTo(self);
//        make.bottom.equalTo(self);
//    }];
//    self.buttonView.backgroundColor = Color_Main_Green;
//
//    [self.confirmButton mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.equalTo(self);
//        make.top.equalTo(self);
//        make.bottom.equalTo(self).offset(-[UIDevice safeDistanceBottom]);
//    }];
//    self.confirmButton.backgroundColor = [UIColor clearColor];
//}

@end
