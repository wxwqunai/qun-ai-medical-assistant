//
//  QWQuestResultCell.h
//  assistant
//
//  Created by qunai on 2023/7/24.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWQuestResultCell : QWBaseTableViewCell
@property (nonatomic, strong) UILabel *nameLabel;
//@property (nonatomic, strong) UILabel *desLabel;
@end

NS_ASSUME_NONNULL_END
