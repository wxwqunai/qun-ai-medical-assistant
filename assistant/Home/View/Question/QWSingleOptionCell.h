//
//  QWSingleOptionCell.h
//  assistant
//
//  Created by qunai on 2023/7/14.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWSingleOptionCell : QWBaseTableViewCell
@property (nonatomic, strong) UIButton *circleButton;
@property (nonatomic, copy) NSString *nameStr;
@property (nonatomic, assign) BOOL isSeleted;
@end

NS_ASSUME_NONNULL_END
