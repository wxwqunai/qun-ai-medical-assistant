//
//  QWResultBaseInfoCell.m
//  assistant
//
//  Created by qunai on 2023/8/23.
//

#import "QWResultBaseInfoCell.h"
@interface QWResultBaseInfoCell()
@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@end
@implementation QWResultBaseInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
      
        
        [self bgView];
        [self nameLabel];
        [self timeLabel];
    }
    return self;
}


- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor clearColor];
//        _bgView.layer.cornerRadius = 8.0;
//        _bgView.layer.masksToBounds = YES;
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(14);
            make.right.equalTo(self.contentView).offset(-14);
            make.top.equalTo(self.contentView).offset(4);
            make.bottom.equalTo(self.contentView).offset(-4);
            make.height.mas_greaterThanOrEqualTo(50.0);
        }];
    }
    return _bgView;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.font = [UIFont boldSystemFontOfSize:18];;
        [self.bgView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView);
            make.centerY.equalTo(self.bgView);
            make.right.equalTo(self.bgView.mas_centerX);
            
        }];
    }
    return _nameLabel;
}

#pragma mark - 时间
- (UILabel *)timeLabel{
    if(!_timeLabel){
        _timeLabel = [self customLabel];
//        _timeLabel.text = @"2023-07-24 13:22";
        [self.bgView addSubview:_timeLabel];
        
        [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.bgView);
            make.centerY.equalTo(self.bgView);
        }];
    }
    return _timeLabel;
}

#pragma mark - 公共
- (UILabel *)customLabel{
    UILabel *label = [[UILabel alloc]init];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont systemFontOfSize:18];
    return label;
}

#pragma mark - 数据
- (void)setModel:(QWQuesAianswerListModel *)model{
    _model = model;
    _nameLabel.text = _model.user_name;
        
    _timeLabel.text = _model.answer_time;
}











@end
