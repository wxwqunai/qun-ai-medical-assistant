//
//  QWQuesSexCell.m
//  assistant
//
//  Created by qunai on 2023/7/18.
//

#import "QWQuesSexCell.h"

@implementation QWQuesSexCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self maleBtn];
        [self femaleBtn];
    }
    return self;
}

- (UIButton *)maleBtn{
    if(!_maleBtn){
        _maleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_maleBtn setTitle:@"男" forState:UIControlStateNormal];
        _maleBtn.titleLabel.font = [UIFont systemFontOfSize:20];
        [_maleBtn setTitleColor:[UIColor colorFromHexString:@"#767676"] forState:UIControlStateNormal];
        _maleBtn.backgroundColor = [UIColor colorFromHexString:@"#EEF0F3"];
        _maleBtn.layer.cornerRadius = 6.0;
        _maleBtn.layer.masksToBounds = YES;
        [_maleBtn addTarget:self action:@selector(maleBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_maleBtn];
        
        [_maleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_centerX).offset(-10);
            make.top.equalTo(self.contentView).offset(8.0);
            make.bottom.equalTo(self.contentView).offset(-8.0);
            make.width.equalTo(self.contentView.mas_width).multipliedBy(0.3);
            make.height.equalTo(_maleBtn.mas_width).multipliedBy(0.5);
        }];
    }
    
    return _maleBtn;
}

- (void)maleBtnClick{
    [_maleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _maleBtn.backgroundColor = [UIColor colorFromHexString:@"#485DFB"];
    
    [_femaleBtn setTitleColor:[UIColor colorFromHexString:@"#767676"] forState:UIControlStateNormal];
    _femaleBtn.backgroundColor = [UIColor colorFromHexString:@"#EEF0F3"];
}

- (UIButton *)femaleBtn{
    if(!_femaleBtn){
        _femaleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_femaleBtn setTitle:@"女" forState:UIControlStateNormal];
        _femaleBtn.titleLabel.font = [UIFont systemFontOfSize:20];
        [_femaleBtn setTitleColor:[UIColor colorFromHexString:@"#767676"] forState:UIControlStateNormal];
        _femaleBtn.backgroundColor = [UIColor colorFromHexString:@"#EEF0F3"];
        _femaleBtn.layer.cornerRadius = 6.0;
        _femaleBtn.layer.masksToBounds = YES;
        [_femaleBtn addTarget:self action:@selector(femaleBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_femaleBtn];
        
        [_femaleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_centerX).offset(10);
            make.top.equalTo(self.contentView).offset(8.0);
            make.bottom.equalTo(self.contentView).offset(-8.0);
            make.width.equalTo(self.contentView.mas_width).multipliedBy(0.3);
            make.height.equalTo(_femaleBtn.mas_width).multipliedBy(0.5);
        }];
    }
    
    return _femaleBtn;
}

- (void)femaleBtnClick{
    [_maleBtn setTitleColor:[UIColor colorFromHexString:@"#767676"] forState:UIControlStateNormal];
    _maleBtn.backgroundColor = [UIColor colorFromHexString:@"#EEF0F3"];
    
    [_femaleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _femaleBtn.backgroundColor = [UIColor colorFromHexString:@"#485DFB"];
}

@end
