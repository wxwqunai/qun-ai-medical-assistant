//
//  QWAiFileCell.h
//  assistant
//
//  Created by qunai on 2023/9/1.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN
typedef void (^FileChooseOpenBlock)(void);
typedef void (^FileDeleteBlock)(void);
typedef void (^OpenFileBlock)(void);

@interface QWAiFileCell : QWBaseTableViewCell
@property (nonatomic, strong) UIViewController *superVC;
@property (nonatomic, strong) UILabel *fileTypeLabel; //文件类型
@property (nonatomic, copy) FileChooseOpenBlock fileChooseOpenBlock;
@property (nonatomic, copy) FileDeleteBlock fileDeleteBlock;
@property (nonatomic, copy) OpenFileBlock openFileBlock;


@property (nonatomic, strong) NSDictionary *dataDic;
@end

NS_ASSUME_NONNULL_END
