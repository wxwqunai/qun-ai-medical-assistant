//
//  QWAiCaseInputViewCell.m
//  assistant
//
//  Created by qunai on 2023/8/31.
//

#import "QWAiCaseInputViewCell.h"
@interface QWAiCaseInputViewCell()<UITextViewDelegate>
@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, strong) UILabel *placeholderLabel;
@end
@implementation QWAiCaseInputViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        [self bgView];
        [self nameLabel];
        [self textView];
    }
    return self;
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(14.0);
            make.right.equalTo(self.contentView).offset(-14.0);
            make.top.equalTo(self.contentView).offset(4.0);
            make.bottom.equalTo(self.contentView).offset(-4.0);
        }];

    }
    return _bgView;
}

#pragma mark -
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.text = @"";
        _nameLabel.font = [UIFont systemFontOfSize:18];
        [_bgView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView);
            make.right.equalTo(_bgView);
            make.top.equalTo(_bgView);
        }];
    }
    return _nameLabel;
}

-(UITextView *)textView
{
    if (!_textView) {
        _textView =[[UITextView alloc]init];
        _textView.text = @"";
        _textView.returnKeyType=UIReturnKeyDone;
        _textView.delegate=self;
        _textView.font=[UIFont fontWithName:@"PingFangSC-Medium" size:15];
        _textView.textColor=[UIColor colorFromHexString:@"#707070"];
        _textView.backgroundColor=[UIColor clearColor];
//        _textView.scrollEnabled = NO;
        
        _textView.layer.borderWidth=1;
        _textView.layer.borderColor=Color_Main_Green.CGColor;
        
        [_bgView addSubview:_textView];
        
        [_textView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView);
            make.right.equalTo(_bgView);
            make.top.equalTo(_nameLabel.mas_bottom).offset(8);
            make.height.mas_offset(100);
            make.bottom.equalTo(_bgView);
        }];
        
        _placeholderLabel =[[UILabel alloc]init];
        _placeholderLabel.text = @"请输入";
        _placeholderLabel.hidden = !IsStringEmpty(_textView.text);
        _placeholderLabel.font = [UIFont systemFontOfSize:15];
        _placeholderLabel.textColor = [UIColor colorFromHexString:@"#CBCBCB"];
        [_textView addSubview:_placeholderLabel];
        
        [_placeholderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(4);
            make.top.mas_offset(8);
        }];
    }
    return _textView;
}

#pragma mark- UITextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if(textView == _textView) _placeholderLabel.hidden = YES;
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    if(textView == _textView) _placeholderLabel.hidden = !IsStringEmpty(textView.text);
    return YES;
}

- (void)setNameStr:(NSString *)nameStr{
    _nameStr = nameStr;
    
    _nameLabel.text = [NSString stringWithFormat:@"%@：",_nameStr];
    
    _placeholderLabel.text = [NSString stringWithFormat:@"请输入%@",_nameStr];
}

- (void)setInputStr:(NSString *)inputStr{
    _inputStr = inputStr;
    
    _textView.text = _inputStr;
    
    _placeholderLabel.hidden = !IsStringEmpty(_textView.text);
}
@end
