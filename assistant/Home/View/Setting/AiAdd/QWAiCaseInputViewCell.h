//
//  QWAiCaseInputViewCell.h
//  assistant
//
//  Created by qunai on 2023/8/31.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWAiCaseInputViewCell : QWBaseTableViewCell

@property (nonatomic, copy) NSString *nameStr;

@property (nonatomic, copy) NSString *inputStr;
@end

NS_ASSUME_NONNULL_END
