//
//  QWAiFileHeaderView.m
//  assistant
//
//  Created by qunai on 2023/8/31.
//

#import "QWAiFileHeaderView.h"
@interface QWAiFileHeaderView()
@property (nonatomic, strong) UIView *bgView;
@end

@implementation QWAiFileHeaderView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        [self bgView];
        
        [self operationLabel];
        [self typeLabel];
        [self fileLabel];
    }
    return self;
}
- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.layer.borderWidth = 0.5;
        _bgView.layer.borderColor = Color_Main_Green.CGColor;
        [self addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(2.0);
            make.right.equalTo(self).offset(-2.0);
            make.top.equalTo(self);
            make.bottom.equalTo(self);
        }];
    }
    return _bgView;
}

#pragma mark - 文件类型
- (UILabel *)typeLabel{
    if(!_typeLabel){
        _typeLabel = [self customLabel];
        _typeLabel.text = @"文件类型";
        [_bgView addSubview:_typeLabel];
        
        [_typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
            make.right.equalTo(_operationLabel.mas_left).multipliedBy(0.35);
        }];
        
    }
    return _typeLabel;
}

#pragma mark - 文件
- (UILabel *)fileLabel{
    if(!_fileLabel){
        
        UIImageView *leftImgeView = [[UIImageView alloc]init];
        leftImgeView.backgroundColor = Color_Main_Green;
        [_bgView addSubview:leftImgeView];
        [leftImgeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_typeLabel.mas_right);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
            make.width.mas_offset(1.0);
        }];
        
        UIImageView *rightImgeView = [[UIImageView alloc]init];
        rightImgeView.backgroundColor = Color_Main_Green;
        [_bgView addSubview:rightImgeView];
        [rightImgeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_operationLabel.mas_left);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
            make.width.mas_offset(1.0);
        }];
        
        
        _fileLabel = [self customLabel];
        _fileLabel.text = @"文件";
        [_bgView addSubview:_fileLabel];
        
        [_fileLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(leftImgeView.mas_right);
            make.right.equalTo(rightImgeView.mas_left);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
        }];
    }
    return _fileLabel;
}

#pragma mark - 操作
- (UILabel *)operationLabel{
    if(!_operationLabel){
        _operationLabel = [self customLabel];
        _operationLabel.text = @"操作";
        [_bgView addSubview:_operationLabel];
        
        [_operationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_bgView);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
            make.width.mas_equalTo(80.0);
        }];
    }
    return _operationLabel;
}

#pragma mark - 公共
- (UILabel *)customLabel{
    UILabel *label = [[UILabel alloc]init];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont systemFontOfSize:18];
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor colorFromHexString:@"#1E90FF"];
    return label;
}

@end

