//
//  QWAiAddSubitemCell.m
//  assistant
//
//  Created by qunai on 2023/9/1.
//

#import "QWAiAddSubitemCell.h"
@interface QWAiAddSubitemCell()<UITextViewDelegate>
@property (nonatomic, strong) UIImageView *lineImageView;
@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UITextView *judgeTextView;
@property (nonatomic, strong) UILabel *judgePlaceholderLabel;

@property (nonatomic, strong) UITextView *resultTextView;
@property (nonatomic, strong) UILabel *resultPlaceholderLabel;

@end
@implementation QWAiAddSubitemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self lineImageView];
        [self subBtn];
        [self addBtn];
        
        [self bgView];
        
        [self judgeTextView];
        [self resultTextView];
    }
    return self;
}

-(UIImageView *)lineImageView{
    if(!_lineImageView){
        _lineImageView= [[UIImageView alloc]init];
        _lineImageView.backgroundColor = Color_Line_Gray;
        [self.contentView addSubview:_lineImageView];
        
        [_lineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.height.mas_offset(1.0);
        }];
    }
    return _lineImageView;
}

#pragma mark - bgView
- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.layer.borderColor = Color_Main_Green.CGColor;
        _bgView.layer.borderWidth = 0.5;
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(14.0);
            make.right.equalTo(self.addBtn.mas_left).offset(-8.0);
            make.top.equalTo(self.contentView).offset(4.0);
            make.bottom.equalTo(self.contentView).offset(-5.0);
        }];
    }
    return _bgView;
}

-(UITextView *)judgeTextView
{
    if (!_judgeTextView) {
        _judgeTextView =[[UITextView alloc]init];
        _judgeTextView.text = @"";
        _judgeTextView.returnKeyType=UIReturnKeyDone;
        _judgeTextView.delegate=self;
        _judgeTextView.font=[UIFont fontWithName:@"PingFangSC-Medium" size:15];
        _judgeTextView.textColor=[UIColor colorFromHexString:@"#707070"];
        _judgeTextView.backgroundColor=[UIColor clearColor];
//        _judgeTextView.scrollEnabled = NO;
        
        _judgeTextView.layer.borderWidth=1;
        _judgeTextView.layer.borderColor=Color_Main_Green.CGColor;
        
        [_bgView addSubview:_judgeTextView];
        
       
        //检查判断项目
        UILabel *nameLabel = [[UILabel alloc]init];
        nameLabel.text = @"检查判断项目：";
        nameLabel.font = [UIFont systemFontOfSize:18];
        [_bgView addSubview:nameLabel];
        
        
        //占位符
        _judgePlaceholderLabel =[[UILabel alloc]init];
        _judgePlaceholderLabel.text = @"请输入检查判断项目";
        _judgePlaceholderLabel.hidden = !IsStringEmpty(_judgeTextView.text);
        _judgePlaceholderLabel.font = [UIFont systemFontOfSize:15];
        _judgePlaceholderLabel.textColor = [UIColor colorFromHexString:@"#CBCBCB"];
        [_judgeTextView addSubview:_judgePlaceholderLabel];
        
        
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView).offset(10.0);
            make.right.equalTo(_bgView);
            make.top.equalTo(_bgView).offset(4.0);
        }];
        
        [_judgeTextView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(nameLabel.mas_left);
            make.right.equalTo(_bgView).offset(-10.0);
            make.top.equalTo(nameLabel.mas_bottom).offset(8);
            make.height.mas_offset(50);
        }];
        
        [_judgePlaceholderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(4);
            make.top.mas_offset(8);
        }];
    }
    return _judgeTextView;
}

-(UITextView *)resultTextView
{
    if (!_resultTextView) {
        _resultTextView =[[UITextView alloc]init];
        _resultTextView.text = @"";
        _resultTextView.returnKeyType=UIReturnKeyDone;
        _resultTextView.delegate=self;
        _resultTextView.font=[UIFont fontWithName:@"PingFangSC-Medium" size:15];
        _resultTextView.textColor=[UIColor colorFromHexString:@"#707070"];
        _resultTextView.backgroundColor=[UIColor clearColor];
//        _resultTextView.scrollEnabled = NO;
        
        _resultTextView.layer.borderWidth=1;
        _resultTextView.layer.borderColor=Color_Main_Green.CGColor;
        
        [_bgView addSubview:_resultTextView];
        
       
        //检查判断项目
        UILabel *nameLabel = [[UILabel alloc]init];
        nameLabel.text = @"当前病症表现：";
        nameLabel.font = [UIFont systemFontOfSize:18];
        [_bgView addSubview:nameLabel];
        
        
        //占位符
        _resultPlaceholderLabel =[[UILabel alloc]init];
        _resultPlaceholderLabel.text = @"请输入当前病症表现";
        _resultPlaceholderLabel.hidden = !IsStringEmpty(_resultTextView.text);
        _resultPlaceholderLabel.font = [UIFont systemFontOfSize:15];
        _resultPlaceholderLabel.textColor = [UIColor colorFromHexString:@"#CBCBCB"];
        [_resultTextView addSubview:_resultPlaceholderLabel];
        
        
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView).offset(10.0);
            make.right.equalTo(_bgView);
            make.top.equalTo(_judgeTextView.mas_bottom).offset(8.0);
        }];
        
        [_resultTextView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(nameLabel.mas_left);
            make.right.equalTo(_bgView).offset(-10.0);
            make.top.equalTo(nameLabel.mas_bottom).offset(8);
            make.height.mas_offset(50);
            make.bottom.equalTo(_bgView).offset(-4.0);
        }];
        
        [_resultPlaceholderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(4);
            make.top.mas_offset(8);
        }];
    }
    return _resultTextView;
}

#pragma mark- UITextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if(textView == _judgeTextView) _judgePlaceholderLabel.hidden = YES;
    if(textView == _resultTextView) _resultPlaceholderLabel.hidden = YES;
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    if(textView == _judgeTextView) _judgePlaceholderLabel.hidden = !IsStringEmpty(textView.text);
    if(textView == _resultTextView) _resultPlaceholderLabel.hidden = !IsStringEmpty(textView.text);
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if(textView == _judgeTextView) self.model.judgment_text = textView.text;
    if(textView == _resultTextView) self.model.result_text = textView.text;
}

#pragma mark - 加
- (UIButton *)addBtn{
    if(!_addBtn){
        _addBtn = [self customButton];
        [_addBtn setTitle:@"+" forState:UIControlStateNormal];
        [_addBtn addTarget:self action:@selector(addBtnClcik) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_addBtn];
        
        [_addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.subBtn.mas_left).offset(-4.0);
            make.centerY.equalTo(self.contentView);
            make.height.mas_offset(24);
            make.width.mas_offset(24);
        }];
        
    }
    return _addBtn;
}
- (void)addBtnClcik{
    if(self.addOrSubControlBlock)self.addOrSubControlBlock(@"+");
}

#pragma mark - 减
- (UIButton *)subBtn{
    if(!_subBtn){
        _subBtn = [self customButton];
        [_subBtn setTitle:@"-" forState:UIControlStateNormal];
        [_subBtn addTarget:self action:@selector(subBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_subBtn];
        
        [_subBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-14.0);
            make.centerY.equalTo(self.contentView);
            make.height.mas_offset(24);
            make.width.mas_offset(24);
        }];
        
    }
    return _subBtn;
}
- (void)subBtnClick{
    if(self.addOrSubControlBlock)self.addOrSubControlBlock(@"-");
}

#pragma mark - 公共
- (UILabel *)customLabel{
    UILabel *label = [[UILabel alloc]init];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont systemFontOfSize:15];
    label.numberOfLines = 0;
    return label;
}

- (UIButton *)customButton{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.layer.borderWidth = 1.0;
    button.layer.borderColor = Color_Main_Green.CGColor;
    button.layer.cornerRadius = 12.0;
    button.layer.masksToBounds = YES;
    [button setTitleColor:Color_Main_Green forState:UIControlStateNormal];
    button.titleLabel.font =[UIFont systemFontOfSize:17];
    return button;
}

- (UIButton *)customButton_choose{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitleColor:[UIColor colorFromHexString:@"#4169E1"] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:17];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//    button.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
//    button.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 4);
    button.titleLabel.numberOfLines = 0;
    return button;
}

#pragma mark - 数据加载
- (void)setModel:(QWQuesAiAddSubitemModel *)model{
    _model = model;
    
    _judgeTextView.text = _model.judgment_text;
    _resultTextView.text = _model.result_text;
    
     _judgePlaceholderLabel.hidden = !IsStringEmpty(_model.judgment_text);
    _resultPlaceholderLabel.hidden = !IsStringEmpty(_model.result_text);
}


@end
