//
//  QWAiHeaderView.h
//  assistant
//
//  Created by qunai on 2023/8/24.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWAiHeaderView : UIView
@property (nonatomic, strong) UILabel *numLabel; //编号
@property (nonatomic, strong) UILabel *caseLabel; //病例
@property (nonatomic, strong) UILabel *conclusionLabel; //结论
@end

NS_ASSUME_NONNULL_END
