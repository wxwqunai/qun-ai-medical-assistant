//
//  QWAiCell.m
//  assistant
//
//  Created by qunai on 2023/8/24.
//

#import "QWAiCell.h"
@interface QWAiCell()
@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UIView *numView;
@property (nonatomic, strong) UIView *caseView;
@property (nonatomic, strong) UIView *conclusionView;

@property (nonatomic, strong) UIButton *caseBtn;
@property (nonatomic, strong) UIButton *conclusionBtn;
@end

@implementation QWAiCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        [self bgView];
        
        [self numView];
        [self caseView];
        [self conclusionView];

    }
    return self;
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(2.0);
            make.right.equalTo(self.contentView).offset(-2.0);
            make.top.equalTo(self.contentView).offset(1.0);
            make.bottom.equalTo(self.contentView).offset(-1.0);
            
        }];
    }
    return _bgView;
}

#pragma mark - 编号
- (UIView *)numView{
    if(!_numView){
        _numView = [self customView];
        [_bgView addSubview:_numView];
        
        [_numView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
            make.width.mas_equalTo(80.0);
        }];
        
        _numLabel = [self customLabel];
        _numLabel.textAlignment = NSTextAlignmentCenter;
        _numLabel.text = @"";
        [_numView addSubview:_numLabel];
        
        [_numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_numView).offset(2.0);
            make.bottom.equalTo(_numView).offset(-2.0);
            make.left.equalTo(_numView).offset(2.0);
            make.right.equalTo(_numView).offset(-2.0);
        }];
        
    }
    return _numView;
}

#pragma mark - 病例
- (UIView *)caseView{
    if(!_caseView){
        _caseView = [self customView];
        [_bgView addSubview:_caseView];
        
        [_caseView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_numView.mas_right).offset(2.0);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
            make.width.equalTo(_bgView.mas_width).multipliedBy(0.5).offset(-40-2);
        }];
        
        _caseBtn= [self customButton];
        [_caseBtn setTitle:@"xxx病例" forState:UIControlStateNormal];
        [_caseBtn addTarget:self action:@selector(caseBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_caseView addSubview:_caseBtn];
        
        [_caseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_caseView);
        }];
    }
    return _caseView;
}
- (void)caseBtnClick{
    [SWHUDUtil hideHudViewWithMessageSuperView:self.superVC.view withMessage:@"维护中！"];
}

#pragma mark - 结论
- (UIView *)conclusionView{
    if(!_conclusionView){
        _conclusionView = [self customView];
        [_bgView addSubview:_conclusionView];
        
        [_conclusionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_caseView.mas_right).offset(2.0);
            make.right.equalTo(_bgView).offset(-2.0);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
        }];
        
        _conclusionBtn= [self customButton];
        [_conclusionBtn setTitle:@"xxx结论" forState:UIControlStateNormal];
        [_conclusionBtn addTarget:self action:@selector(conclusionBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_conclusionView addSubview:_conclusionBtn];
        
        [_conclusionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_conclusionView);
        }];
    }
    return _conclusionView;
}

- (void)conclusionBtnClick{
    [SWHUDUtil hideHudViewWithMessageSuperView:self.superVC.view withMessage:@"维护中！"];
}


#pragma mark - 公共
- (UIView *)customView{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor colorFromHexString:@"#E9ECF6"];
    return view;
}

- (UILabel *)customLabel{
    UILabel *label = [[UILabel alloc]init];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont systemFontOfSize:15];
    label.numberOfLines = 0;
    return label;
}

- (UIButton *)customButton{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitleColor:[UIColor colorFromHexString:@"#4169E1"] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:15];
    return button;
}


@end
