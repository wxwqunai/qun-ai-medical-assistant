//
//  QWAiHeaderView.m
//  assistant
//
//  Created by qunai on 2023/8/24.
//

#import "QWAiHeaderView.h"
@interface QWAiHeaderView()
@property (nonatomic, strong) UIView *bgView;
@end
@implementation QWAiHeaderView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        [self bgView];
        
        [self numLabel];
        [self caseLabel];
        [self conclusionLabel];
    }
    return self;
}
- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        [self addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(2.0);
            make.right.equalTo(self).offset(-2.0);
            make.top.equalTo(self);
            make.bottom.equalTo(self);
        }];
    }
    return _bgView;
}

#pragma mark - 编号
- (UILabel *)numLabel{
    if(!_numLabel){
        _numLabel = [self customLabel];
        _numLabel.text = @"编号";
        [_bgView addSubview:_numLabel];
        
        [_numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
            make.width.mas_equalTo(80.0);
        }];
        
    }
    return _numLabel;
}



#pragma mark - 病例
- (UILabel *)caseLabel{
    if(!_caseLabel){
        _caseLabel = [self customLabel];
        _caseLabel.text = @"病例";
        [_bgView addSubview:_caseLabel];
        
        [_caseLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_numLabel.mas_right).offset(2.0);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
            make.width.equalTo(_bgView.mas_width).multipliedBy(0.5).offset(-40-2);
        }];
    }
    return _caseLabel;
}


#pragma mark - 结论
- (UILabel *)conclusionLabel{
    if(!_conclusionLabel){
        _conclusionLabel = [self customLabel];
        _conclusionLabel.text = @"结论";
        [_bgView addSubview:_conclusionLabel];
        
        [_conclusionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_caseLabel.mas_right).offset(2.0);
            make.right.equalTo(_bgView).offset(-2.0);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
        }];
    }
    return _conclusionLabel;
}



#pragma mark - 公共
- (UILabel *)customLabel{
    UILabel *label = [[UILabel alloc]init];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont systemFontOfSize:18];
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor colorFromHexString:@"#CFD6EC"];
    return label;
}


@end
