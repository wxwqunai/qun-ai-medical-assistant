//
//  QWAiCaseInputCell.m
//  assistant
//
//  Created by qunai on 2023/8/31.
//

#import "QWAiCaseInputCell.h"
@interface QWAiCaseInputCell()<UITextFieldDelegate>
@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UIView *idView;
@property (nonatomic, strong) UIView *nameView;
@property (nonatomic, strong) UIView *sexView;
@property (nonatomic, strong) UIView *phoneView;
@property (nonatomic, strong) UIView *ageView;
@property (nonatomic, strong) UIView *heigntView;
@property (nonatomic, strong) UIView *weightView;
@property (nonatomic, strong) UIView *addressView;
@end

@implementation QWAiCaseInputCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        [self bgView];
        
        [self idView];
        
        [self nameView];
        [self sexView];

        [self phoneView];
        [self ageView];
        
        [self heigntView];
        [self weightView];
        
        [self addressView];
    }
    return self;
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(14.0);
            make.right.equalTo(self.contentView).offset(-14.0);
            make.top.equalTo(self.contentView).offset(4.0);
            make.bottom.equalTo(self.contentView).offset(-4.0);
        }];

    }
    return _bgView;
}

#pragma mark - 用户id
- (UIView *)idView{
    if(!_idView){
        _idView = [self itemView:@"用户ID"];
        [_bgView addSubview:_idView];
        [_idView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView);
            make.right.equalTo(_bgView);
            make.top.equalTo(_bgView);
        }];
    }
    return _idView;
}

#pragma mark - 姓名
- (UIView *)nameView{
    if(!_nameView){
        _nameView = [self itemView:@"姓名"];
        [_bgView addSubview:_nameView];
        [_nameView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView);
            make.right.equalTo(_bgView.mas_centerX).offset(-2.0);
            make.top.equalTo(_idView.mas_bottom).offset(8.0);
        }];
    }
    return _nameView;
}

#pragma mark - 性别
- (UIView *)sexView{
    if(!_sexView){
        _sexView = [self itemView:@"性别"];
        [_bgView addSubview:_sexView];
        [_sexView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_bgView);
            make.left.equalTo(_bgView.mas_centerX).offset(2.0);
            make.top.equalTo(_nameView.mas_top);
        }];
    }
    return _sexView;
}

#pragma mark - 手机号
- (UIView *)phoneView{
    if(!_phoneView){
        _phoneView = [self itemView:@"手机号"];
        [_bgView addSubview:_phoneView];
        [_phoneView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView);
            make.right.equalTo(_bgView.mas_centerX).offset(-2.0);
            make.top.equalTo(_nameView.mas_bottom).offset(8.0);
        }];
    }
    return _phoneView;
}

#pragma mark - 年龄
- (UIView *)ageView{
    if(!_ageView){
        _ageView = [self itemView:@"年龄"];
        [_bgView addSubview:_ageView];
        [_ageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_bgView);
            make.left.equalTo(_bgView.mas_centerX).offset(2.0);
            make.top.equalTo(_phoneView.mas_top);
        }];
    }
    return _ageView;
}

#pragma mark - 身高
- (UIView *)heigntView{
    if(!_heigntView){
        _heigntView = [self itemView:@"身高"];
        [_bgView addSubview:_heigntView];
        [_heigntView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView);
            make.right.equalTo(_bgView.mas_centerX).offset(-2.0);
            make.top.equalTo(_phoneView.mas_bottom).offset(8.0);
        }];
    }
    return _heigntView;
}

#pragma mark - 体重
- (UIView *)weightView{
    if(!_weightView){
        _weightView = [self itemView:@"体重"];
        [_bgView addSubview:_weightView];
        [_weightView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_bgView);
            make.left.equalTo(_bgView.mas_centerX).offset(2.0);
            make.top.equalTo(_heigntView.mas_top);
        }];
    }
    return _weightView;
}

#pragma mark - 地址
- (UIView *)addressView{
    if(!_addressView){
        _addressView = [self itemView:@"地址"];
        [_bgView addSubview:_addressView];
        [_addressView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView);
            make.right.equalTo(_bgView);
            make.top.equalTo(_heigntView.mas_bottom).offset(8.0);
            make.bottom.equalTo(_bgView);
        }];
    }
    return _addressView;
}


#pragma mark - UITextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
}


- (UIView *)itemView:(NSString *)labelStr {
    UIView *itemView = [[UIView alloc]init];
    
    UITextField *textField = [self customTextField];
    textField.placeholder = [NSString stringWithFormat:@"请输入%@",labelStr];
    [itemView addSubview:textField];
    
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(itemView);
        make.top.equalTo(itemView);
        make.bottom.equalTo(itemView);
    }];
    
    
    UILabel *label = [[UILabel alloc]init];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont systemFontOfSize:18];
    label.text = [NSString stringWithFormat:@"%@：",labelStr];
    [itemView addSubview:label];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(itemView);
        make.top.equalTo(itemView);
        make.bottom.equalTo(itemView);
        make.right.equalTo(textField.mas_left).offset(-4.0);
        make.height.mas_offset(40);
    }];
    [label sizeToFit];
    
    return itemView;
}



#pragma mark - 公共

- (UITextField *)customTextField{
    UITextField *textField= [[UITextField alloc]init];
//        textField.textColor =[UIColor colorFromHexString:@"#A2A2A2"];
    textField.borderStyle = UITextBorderStyleNone;
    textField.font =[UIFont systemFontOfSize:16];
//    textField.textAlignment = NSTextAlignmentCenter;
    textField.delegate = self;
    
    //边框
    textField.layer.borderColor = Color_Main_Green.CGColor;
    textField.layer.borderWidth = 1;
    
    //左边距
    textField.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, 0)];
    textField.leftViewMode = UITextFieldViewModeAlways;
    
    //清除样式
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    return textField;
}

@end
