//
//  QWAiFileHeaderView.h
//  assistant
//
//  Created by qunai on 2023/8/31.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWAiFileHeaderView : UITableViewHeaderFooterView
@property (nonatomic, strong) UILabel *typeLabel; //文件类型
@property (nonatomic, strong) UILabel *fileLabel; //文件
@property (nonatomic, strong) UILabel *operationLabel; //操作
@end

NS_ASSUME_NONNULL_END
