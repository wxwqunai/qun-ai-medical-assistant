//
//  QWAiCell.h
//  assistant
//
//  Created by qunai on 2023/8/24.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWAiCell : QWBaseTableViewCell
@property (nonatomic, strong) UILabel *numLabel;
@property (nonatomic, strong) UIViewController *superVC;

@end

NS_ASSUME_NONNULL_END
