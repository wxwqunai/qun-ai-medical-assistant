//
//  QWAiFileCell.m
//  assistant
//
//  Created by qunai on 2023/9/1.
//

#import "QWAiFileCell.h"
@interface QWAiFileCell()
@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UIView *fileTypeView;
@property (nonatomic, strong) UIView *fileView;
@property (nonatomic, strong) UIView *operationView;

@property (nonatomic, strong) UIButton *fileBrn; //文件
@property (nonatomic, strong) UIButton *deleteBtn; //操作-删除
@property (nonatomic, strong) UIButton *addBtn; //操作-添加
@end
@implementation QWAiFileCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        [self bgView];
        
        [self operationView];
        [self fileTypeView];
        [self fileView];

    }
    return self;
}


- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.layer.borderWidth = 0.5;
        _bgView.layer.borderColor = Color_Main_Green.CGColor;
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(2.0);
            make.right.equalTo(self.contentView).offset(-2.0);
            make.top.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            
        }];
    }
    return _bgView;
}

#pragma mark - 文件类型
- (UIView *)fileTypeView{
    if(!_fileTypeView){
        _fileTypeView = [self customView];
        [_bgView addSubview:_fileTypeView];
        
        [_fileTypeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
            make.right.equalTo(_operationView.mas_left).multipliedBy(0.35);
        }];
        
        
        _fileTypeLabel = [self customLabel];
        _fileTypeLabel.textAlignment = NSTextAlignmentCenter;
        _fileTypeLabel.text = @"";
        [_fileTypeView addSubview:_fileTypeLabel];
        
        [_fileTypeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_fileTypeView).offset(2.0);
            make.bottom.equalTo(_fileTypeView).offset(-2.0);
            make.left.equalTo(_fileTypeView).offset(2.0);
            make.right.equalTo(_fileTypeView).offset(-2.0);
        }];
        
    }
    return _fileTypeView;
}

#pragma mark - 文件
- (UIView *)fileView{
    if(!_fileView){
        //分割线-左
        UIImageView *leftImgeView = [[UIImageView alloc]init];
        leftImgeView.backgroundColor = Color_Main_Green;
        [_bgView addSubview:leftImgeView];
        [leftImgeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_fileTypeView.mas_right);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
            make.width.mas_offset(1.0);
        }];
        //分割线-右
        UIImageView *rightImgeView = [[UIImageView alloc]init];
        rightImgeView.backgroundColor = Color_Main_Green;
        [_bgView addSubview:rightImgeView];
        [rightImgeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_operationView.mas_left);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
            make.width.mas_offset(1.0);
        }];
        
        
        _fileView = [self customView];
        [_bgView addSubview:_fileView];
        
        [_fileView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(leftImgeView.mas_right);
            make.right.equalTo(rightImgeView.mas_left);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
        }];
        
        _fileBrn= [self customButton];
        [_fileBrn addTarget:self action:@selector(fileBrnClick) forControlEvents:UIControlEventTouchUpInside];
        [_fileView addSubview:_fileBrn];
        
        [_fileBrn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_fileView).offset(2.0);
            make.bottom.equalTo(_fileView).offset(-2.0);
            make.left.equalTo(_fileView).offset(2.0);
            make.right.equalTo(_fileView).offset(-2.0);
        }];
    }
    return _fileView;
}
- (void)fileBrnClick{
    if(self.openFileBlock)self.openFileBlock();
}

#pragma mark - 操作
- (UIView *)operationView{
    if(!_operationView){
        _operationView = [self customView];
        [_bgView addSubview:_operationView];
        
        [_operationView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_bgView);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
            make.width.mas_equalTo(80.0);
        }];
        
        _addBtn= [UIButton buttonWithType:UIButtonTypeCustom];
        [_addBtn setTitle:@"添加" forState:UIControlStateNormal];
        [_addBtn setTitleColor:[UIColor colorFromHexString:@"#4169E1"] forState:UIControlStateNormal];
        
        _addBtn.titleLabel.font = [UIFont systemFontOfSize:18];
        [_addBtn addTarget:self action:@selector(addBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_operationView addSubview:_addBtn];
        
        [_addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_operationView);
            make.left.equalTo(_operationView);
            make.right.equalTo(_operationView);
            make.bottom.equalTo(_operationView.mas_centerY);
        }];
        
        _deleteBtn= [UIButton buttonWithType:UIButtonTypeCustom];
        [_deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
        [_deleteBtn setTitleColor:[UIColor colorFromHexString:@"#4169E1"] forState:UIControlStateNormal];
        
        _deleteBtn.titleLabel.font = [UIFont systemFontOfSize:18];
        [_deleteBtn addTarget:self action:@selector(deleteBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_operationView addSubview:_deleteBtn];
        
        [_deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_operationView.mas_centerY);
            make.left.equalTo(_operationView);
            make.right.equalTo(_operationView);
            make.bottom.equalTo(_operationView);
        }];
    }
    return _operationView;
}
- (void)addBtnClick{
    if(self.fileChooseOpenBlock)self.fileChooseOpenBlock();
}
- (void)deleteBtnClick{
    if(self.fileDeleteBlock)self.fileDeleteBlock();
}


 

#pragma mark - 公共
- (UIView *)customView{
    UIView *view = [[UIView alloc]init];
//    view.backgroundColor = [UIColor colorFromHexString:@"#E9ECF6"];
    return view;
}

- (UILabel *)customLabel{
    UILabel *label = [[UILabel alloc]init];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont systemFontOfSize:16];
    label.numberOfLines = 0;
    return label;
}

- (UIButton *)customButton{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitleColor:[UIColor colorFromHexString:@"#4169E1"] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:16];
    button.titleLabel.numberOfLines = 0;
    return button;
}


#pragma mark - 数据
- (void)setDataDic:(NSDictionary *)dataDic{
    _dataDic = dataDic;
    
    _fileTypeLabel.text = _dataDic[@"fileTypeName"];
    
    [_fileBrn setTitle:_dataDic[@"fileName"] forState:UIControlStateNormal];

}

@end
