//
//  QWQuesSettingCell.m
//  assistant
//
//  Created by qunai on 2023/8/17.
//

#import "QWQuesSettingCell.h"

@implementation QWQuesSettingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        [self baseInfoView];
        [self nameLabel];

    }
    return self;
}

#pragma mark - view
- (UIView *)baseInfoView{
    if(!_baseInfoView){
        _baseInfoView = [[UIView alloc]init];
        _baseInfoView.backgroundColor = Color_Main_Green;
        _baseInfoView.layer.cornerRadius = 6.0;
        _baseInfoView.layer.masksToBounds = YES;
        [self.contentView addSubview:_baseInfoView];
        
        [_baseInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(14.0);
            make.top.equalTo(self.contentView).offset(2.0);
            make.right.equalTo(self.contentView).offset(-14.0);
            make.bottom.equalTo(self.contentView).offset(-2.0);
            make.height.mas_greaterThanOrEqualTo(80.0);
        }];
    
    }
    return _baseInfoView;
}

#pragma mark - 姓名
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont boldSystemFontOfSize:24];
        _nameLabel.textColor = [UIColor whiteColor];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        [_baseInfoView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_baseInfoView);
            make.right.equalTo(_baseInfoView);
            make.centerY.equalTo(_baseInfoView);
        }];
    }
    return _nameLabel;
}

@end
