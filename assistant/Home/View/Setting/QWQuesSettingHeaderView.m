//
//  QWQuesSettingHeaderView.m
//  assistant
//
//  Created by qunai on 2023/8/16.
//

#import "QWQuesSettingHeaderView.h"

@interface QWQuesSettingHeaderView()
@property (nonatomic, strong) UIButton *moreButton;
@end

@implementation QWQuesSettingHeaderView
- (instancetype)initWithReuseIdentifier:(nullable NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if(self){
//        self.backgroundColor = [UIColor whiteColor];
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self iconImageView];
        [self nameLabel];
        [self moreButton];
    }
    return self;
}

#pragma mark - iconImageView
- (UIImageView *)iconImageView{
    if(!_iconImageView){
        _iconImageView = [[UIImageView alloc]init];
        _iconImageView.tintColor = [UIColor colorFromHexString:@"#4169E1"];
        [self addSubview:_iconImageView];
        
        [_iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(20.0);
            make.centerY.equalTo(self);
        }];
    }
    return _iconImageView;
}

#pragma mark - name
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont boldSystemFontOfSize:20];
        _nameLabel.textColor = [UIColor colorFromHexString:@"#4169E1"];
        [self addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_iconImageView.mas_right).offset(6);
            make.centerY.equalTo(self);
        }];
    }
    return _nameLabel;
}

#pragma mark - 按钮-更多
- (UIButton *)moreButton{
    if(!_moreButton){
        _moreButton =[UIButton buttonWithType:UIButtonTypeCustom];
        [_moreButton setTitle:@"更多" forState:UIControlStateNormal];
        _moreButton.hidden = YES;
        [_moreButton setImage:[UIImage imageNamed:@"next_arrow.png"] forState:UIControlStateNormal];
        
        [_moreButton setTitleColor:[UIColor colorFromHexString:@"#515151"] forState:UIControlStateNormal];
        _moreButton.titleLabel.font =[UIFont systemFontOfSize:18];
        
        [_moreButton setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
        _moreButton.imageEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);

        
        [_moreButton addTarget:self action:@selector(moreButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_moreButton];
        
        [_moreButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.right.equalTo(self).offset(-14.0);
        }];
    }
    return _moreButton;
}
-(void)moreButtonClick:(UIButton *)sender{
    NSLog(@"分享");
    if(self.moreClickBlock)self.moreClickBlock();
}


@end
