//
//  QWRobotAddConclusionCell.m
//  assistant
//
//  Created by qunai on 2023/8/25.
//

#import "QWRobotAddConclusionCell.h"
@interface QWRobotAddConclusionCell()<UITextFieldDelegate, UITextViewDelegate>
@property (nonatomic, strong) UILabel *tipsLabel;

@property (nonatomic, strong) UIImageView *lineImageView;
@property (nonatomic, strong) UIView *numView;

@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UILabel *wLabel; //系数
@property (nonatomic, strong) UITextField *wtextField;

@property (nonatomic, strong) UITextView *suggestView;//结论
@property (nonatomic,strong) UILabel *placeholderLabel;

@end
@implementation QWRobotAddConclusionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self tipsLabel];
        
        [self lineImageView];
        [self numView];

        [self bgView];
        [self wLabel];
        [self wtextField];
        [self suggestView];
        
        [self subBtn];
        [self addBtn];
    }
    return self;
}

#pragma mark - 提示（第一个cell展示，其他隐藏）
- (UILabel *)tipsLabel{
    if(!_tipsLabel){
        _tipsLabel = [self customLabel];
        _tipsLabel.textColor = [UIColor colorFromHexString:@"#FF6347"];
        _tipsLabel.font = [UIFont systemFontOfSize:14];
        [self.contentView addSubview:_tipsLabel];

        [_tipsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView);
            make.left.equalTo(self.contentView).offset(48);
            make.right.equalTo(self.contentView);
        }];
    }
    return _tipsLabel;
}

#pragma mark - 条件num
- (UIView *)numView{
    if(!_numView){
        _numView = [[UIView alloc]init];
        [self.contentView addSubview:_numView];
        [_numView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(14);
            make.top.equalTo(self.tipsLabel.mas_bottom);
            make.bottom.equalTo(self.contentView);
            make.width.mas_equalTo(25.0);
        }];
        
        UIImageView *lineImageView = [[UIImageView alloc]init];
        lineImageView.backgroundColor = Color_Line_Gray;
        [_numView addSubview:lineImageView];
        
        [lineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_numView);
            make.bottom.equalTo(_numView);
            make.right.equalTo(_numView);
            make.width.mas_offset(1.0);
        }];
        
        _numLabel =  [[UILabel alloc]init];
        _numLabel.textColor = [UIColor blackColor];
        _numLabel.font = [UIFont systemFontOfSize:17];
        _numLabel.numberOfLines = 0;
        _numLabel.textAlignment = NSTextAlignmentCenter;
        _numLabel.text = @"";
        [_numView addSubview:_numLabel];
         
        [_numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_numView);
            make.right.equalTo(lineImageView.mas_left).offset(-4.0);
            make.top.equalTo(_numView).offset(4.0);
            make.bottom.equalTo(_numView).offset(-4.0);
        }];
        
    }
    return _numView;
}

-(UIImageView *)lineImageView{
    if(!_lineImageView){
        _lineImageView= [[UIImageView alloc]init];
        _lineImageView.backgroundColor = Color_Line_Gray;
        [self.contentView addSubview:_lineImageView];
        
        [_lineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.height.mas_offset(1.0);
        }];
    }
    return _lineImageView;
}

#pragma mark - bgView
- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.numView.mas_right).offset(8.0);
            make.right.equalTo(self.addBtn.mas_left).offset(-8.0);
            make.top.equalTo(self.tipsLabel.mas_bottom).offset(8.0);
            make.bottom.equalTo(self.contentView).offset(-8.0);
            
        }];
    }
    return _bgView;
}



#pragma mark - 系数
- (UILabel *)wLabel{
    if(!_wLabel){
        _wLabel = [self customLabel];
        _wLabel.font = [UIFont systemFontOfSize:16];
        _wLabel.text = @"系数：";
        [_bgView addSubview:_wLabel];

        [_wLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_bgView);
            make.left.equalTo(_bgView);
            make.height.mas_offset(20);
        }];

    }
    return _wLabel;
}
#pragma mark - 系数-输入
- (UITextField *)wtextField{
    if(!_wtextField){
        _wtextField = [[UITextField alloc]init];
        _wtextField.placeholder =@"请输入系数（例：0.1）";
//        _wtextField.textColor =[UIColor colorFromHexString:@"#A2A2A2"];
        _wtextField.borderStyle = UITextBorderStyleNone;
        _wtextField.keyboardType = UIKeyboardTypeDecimalPad;
        _wtextField.font =[UIFont systemFontOfSize:16];
        _wtextField.textAlignment = NSTextAlignmentCenter;
        _wtextField.delegate = self;
        
        //边框
        _wtextField.layer.borderColor = Color_Main_Green.CGColor;
        _wtextField.layer.borderWidth = 1;
        
        //左边距
        _wtextField.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, 0)];
        _wtextField.leftViewMode = UITextFieldViewModeAlways;
        
        //清除样式
//        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        [_bgView addSubview:_wtextField];
        
        
        [_wtextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_wLabel);
            make.left.equalTo(_wLabel.mas_right);
            make.right.mas_lessThanOrEqualTo(_bgView.mas_right);
//            make.height.mas_equalTo(50);
        }];
    }
    return _wtextField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    self.model.w = textField.text;
}

#pragma mark - 结论 - 输入
-(UITextView *)suggestView
{
    if (!_suggestView) {
        _suggestView =[[UITextView alloc]init];
        _suggestView.returnKeyType=UIReturnKeyDone;
        _suggestView.delegate=self;
        _suggestView.font=[UIFont fontWithName:@"PingFangSC-Medium" size:15];
        _suggestView.textColor=[UIColor colorFromHexString:@"#707070"];
        _suggestView.backgroundColor=[UIColor clearColor];
//        _suggestView.scrollEnabled = NO;
        
        _suggestView.layer.borderWidth=1;
        _suggestView.layer.borderColor=Color_Main_Green.CGColor;
        
        [_bgView addSubview:_suggestView];
        
        [_suggestView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView);
            make.right.equalTo(_bgView);
            make.top.equalTo(_wtextField.mas_bottom).offset(8.0);
            make.height.mas_offset(80);
            make.bottom.equalTo(_bgView);
        }];
        
        _placeholderLabel =[[UILabel alloc]init];
        _placeholderLabel.text = @"请输入结论或建议";
        _placeholderLabel.hidden = !IsStringEmpty(self.model.suggest);
        _placeholderLabel.font = [UIFont systemFontOfSize:15];
        _placeholderLabel.textColor = [UIColor colorFromHexString:@"#CBCBCB"];
        [_suggestView addSubview:_placeholderLabel];
        
        [_placeholderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(4);
            make.top.mas_offset(8);
        }];
    }
    return _suggestView;
}
#pragma mark- UITextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if(textView == _suggestView) _placeholderLabel.hidden = YES;
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    if(textView == _suggestView) _placeholderLabel.hidden = !IsStringEmpty(textView.text);
    return YES;
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    self.model.suggest = textView.text;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if( [ @"\n" isEqualToString: text]){
        //你的响应方法
        [self endEditing:YES];
        return NO;
    }
    
    if (range.length + range.location > textView.text.length) {
        return NO;
    }
    
    return YES;
}


#pragma mark - 加
- (UIButton *)addBtn{
    if(!_addBtn){
        _addBtn = [self customButton];
        [_addBtn setTitle:@"+" forState:UIControlStateNormal];
        [_addBtn addTarget:self action:@selector(addBtnClcik) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_addBtn];
        
        [_addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.subBtn.mas_left).offset(-4.0);
            make.centerY.equalTo(self.contentView);
            make.height.mas_offset(24);
            make.width.mas_offset(24);
        }];
        
    }
    return _addBtn;
}
- (void)addBtnClcik{
    if(self.addOrSubControlBlock)self.addOrSubControlBlock(@"+");
}

#pragma mark - 减
- (UIButton *)subBtn{
    if(!_subBtn){
        _subBtn = [self customButton];
        [_subBtn setTitle:@"-" forState:UIControlStateNormal];
        [_subBtn addTarget:self action:@selector(subBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_subBtn];
        
        [_subBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-14.0);
            make.centerY.equalTo(self.contentView);
            make.height.mas_offset(24);
            make.width.mas_offset(24);
        }];
        
    }
    return _subBtn;
}
- (void)subBtnClick{
    if(self.addOrSubControlBlock)self.addOrSubControlBlock(@"-");
}

#pragma mark - 公共
- (UILabel *)customLabel{
    UILabel *label = [[UILabel alloc]init];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont systemFontOfSize:15];
    label.numberOfLines = 0;
    return label;
}

- (UIButton *)customButton{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.layer.borderWidth = 1.0;
    button.layer.borderColor = Color_Main_Green.CGColor;
    button.layer.cornerRadius = 12.0;
    button.layer.masksToBounds = YES;
    [button setTitleColor:Color_Main_Green forState:UIControlStateNormal];
    button.titleLabel.font =[UIFont systemFontOfSize:17];
    return button;
}

- (UIButton *)customButton_choose{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitleColor:[UIColor colorFromHexString:@"#4169E1"] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:17];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//    button.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
//    button.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 4);
//    button.titleLabel.numberOfLines = 0;
    return button;
}

#pragma mark - 数据加载
- (void)setModel:(QWAiConclusionModel *)model{
    _model = model;
    
    _wtextField.text = _model.w;
    
    _suggestView.text = _model.suggest;
    _placeholderLabel.hidden = !IsStringEmpty(_model.suggest);
}

- (void)setIsHiddenTips:(BOOL)isHiddenTips{
    _isHiddenTips = isHiddenTips;
    
    _tipsLabel.hidden =_isHiddenTips;
    _tipsLabel.text = _isHiddenTips?@"":@"当系数大于0，表示有利于某结论\n当系数小于0，表示排除、不支持某结论";

}


@end
