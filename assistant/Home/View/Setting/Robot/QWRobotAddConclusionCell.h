//
//  QWRobotAddConclusionCell.h
//  assistant
//
//  Created by qunai on 2023/8/25.
//

#import "QWBaseTableViewCell.h"
#import "QWQuestModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef void (^AddOrSubControlBlock)(NSString *control);
@interface QWRobotAddConclusionCell : QWBaseTableViewCell
@property (nonatomic, assign) BOOL isHiddenTips;
@property (nonatomic, strong) UILabel *numLabel;
@property (nonatomic, strong) UIButton *addBtn; //加
@property (nonatomic, strong) UIButton *subBtn; //减
@property (nonatomic, copy) AddOrSubControlBlock addOrSubControlBlock;

@property (nonatomic, strong) QWAiConclusionModel *model;

@end

NS_ASSUME_NONNULL_END
