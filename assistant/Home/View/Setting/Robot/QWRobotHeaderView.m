//
//  QWRobotHeaderView.m
//  assistant
//
//  Created by qunai on 2023/8/24.
//

#import "QWRobotHeaderView.h"
@interface QWRobotHeaderView()
@property (nonatomic, strong) UIView *bgView;
@end

@implementation QWRobotHeaderView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        [self bgView];
        
        [self operationLabel];
        [self conditionLabel];
        [self conclusionLabel];
    }
    return self;
}
- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        [self addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(2.0);
            make.right.equalTo(self).offset(-2.0);
            make.top.equalTo(self);
            make.bottom.equalTo(self);
        }];
    }
    return _bgView;
}

#pragma mark - 依据
- (UILabel *)conditionLabel{
    if(!_conditionLabel){
        _conditionLabel = [self customLabel];
        _conditionLabel.text = @"依据";
        [_bgView addSubview:_conditionLabel];
        
        [_conditionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
            make.right.equalTo(_operationLabel.mas_left).multipliedBy(0.5).offset(-1.0);
        }];
        
    }
    return _conditionLabel;
}

#pragma mark - 结论
- (UILabel *)conclusionLabel{
    if(!_conclusionLabel){
        _conclusionLabel = [self customLabel];
        _conclusionLabel.text = @"结论";
        [_bgView addSubview:_conclusionLabel];
        
        [_conclusionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_operationLabel.mas_left).multipliedBy(0.5).offset(1.0);
            make.right.equalTo(_operationLabel.mas_left).offset(-2.0);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
        }];
    }
    return _conclusionLabel;
}

#pragma mark - 操作
- (UILabel *)operationLabel{
    if(!_operationLabel){
        _operationLabel = [self customLabel];
        _operationLabel.text = @"操作";
        [_bgView addSubview:_operationLabel];
        
        [_operationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_bgView);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
            make.width.mas_equalTo(80.0);
        }];
    }
    return _operationLabel;
}






#pragma mark - 公共
- (UILabel *)customLabel{
    UILabel *label = [[UILabel alloc]init];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont systemFontOfSize:18];
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor colorFromHexString:@"#CFD6EC"];
    return label;
}

@end
