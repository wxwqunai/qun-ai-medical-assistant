//
//  QWRobotHeaderView.h
//  assistant
//
//  Created by qunai on 2023/8/24.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWRobotHeaderView : UIView
@property (nonatomic, strong) UILabel *conditionLabel; //依据
@property (nonatomic, strong) UILabel *conclusionLabel; //结论
@property (nonatomic, strong) UILabel *operationLabel; //操作
@end

NS_ASSUME_NONNULL_END
