//
//  QWRobotCell.h
//  assistant
//
//  Created by qunai on 2023/8/24.
//

#import "QWBaseTableViewCell.h"
#import "QWQuestModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWRobotCell : QWBaseTableViewCell

@property (nonatomic, strong) QWQuesAiDiagnosticRulesModel *model;
@property (nonatomic, strong) UIViewController *superVC;
@end

NS_ASSUME_NONNULL_END
