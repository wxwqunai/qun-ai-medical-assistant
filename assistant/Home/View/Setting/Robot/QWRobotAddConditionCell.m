//
//  QWRobotAddConditionCell.m
//  assistant
//
//  Created by qunai on 2023/8/25.
//

#import "QWRobotAddConditionCell.h"
@interface QWRobotAddConditionCell()<UITextFieldDelegate>
@property (nonatomic, strong) UIImageView *lineImageView;
@property (nonatomic, strong) UIView *numView;

@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIButton *titleNumBtn;
@property (nonatomic, strong) UILabel *timuLabel;

@property (nonatomic, strong) UILabel *diseaseLabel;
@property (nonatomic, strong) UITextField *textField;

@end
@implementation QWRobotAddConditionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self lineImageView];
        [self numView];

        [self bgView];
        [self titleNumBtn];
        
        [self timuLabel];
        [self diseaseLabel];
        [self textField];
        
        [self subBtn];
        [self addBtn];
    }
    return self;
}

#pragma mark - 条件num
- (UIView *)numView{
    if(!_numView){
        _numView = [[UIView alloc]init];
        [self.contentView addSubview:_numView];
        [_numView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(14);
            make.top.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            make.width.mas_equalTo(25.0);
        }];
        
        UIImageView *lineImageView = [[UIImageView alloc]init];
        lineImageView.backgroundColor = Color_Line_Gray;
        [_numView addSubview:lineImageView];
        
        [lineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_numView);
            make.bottom.equalTo(_numView);
            make.right.equalTo(_numView);
            make.width.mas_offset(1.0);
        }];
        
        
        
        _numLabel =  [[UILabel alloc]init];
        _numLabel.textColor = [UIColor blackColor];
        _numLabel.font = [UIFont systemFontOfSize:17];
        _numLabel.numberOfLines = 0;
        _numLabel.textAlignment = NSTextAlignmentCenter;
        _numLabel.text = @"";
        [_numView addSubview:_numLabel];
         
        [_numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_numView);
            make.right.equalTo(lineImageView.mas_left).offset(-4.0);
            make.top.equalTo(_numView).offset(4.0);
            make.bottom.equalTo(_numView).offset(-4.0);
        }];
        
    }
    return _numView;
}

-(UIImageView *)lineImageView{
    if(!_lineImageView){
        _lineImageView= [[UIImageView alloc]init];
        _lineImageView.backgroundColor = Color_Line_Gray;
        [self.contentView addSubview:_lineImageView];
        
        [_lineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.height.mas_offset(1.0);
        }];
    }
    return _lineImageView;
}

#pragma mark - bgView
- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.numView.mas_right);
            make.right.equalTo(self.addBtn.mas_left);
            make.top.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            
        }];
    }
    return _bgView;
}

#pragma mark - 题目-选择
- (UIButton *)titleNumBtn{
    if(!_titleNumBtn){
        _titleNumBtn = [self customButton_choose];
        [_titleNumBtn setTitle:@"请选择依据 >" forState:UIControlStateNormal];
        [_titleNumBtn addTarget:self action:@selector(titleNumBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_bgView addSubview:_titleNumBtn];
        
        [_titleNumBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_bgView).offset(4.0);
            make.left.equalTo(_bgView).offset(8.0);
            make.right.equalTo(_bgView).offset(-8.0);
        }];
    }
    return _titleNumBtn;
}
- (void)titleNumBtnClick{
    if(self.topicNumChooseBlock)self.topicNumChooseBlock();
}

#pragma mark - 题
- (UILabel *)timuLabel{
    if(!_timuLabel){
        _timuLabel = [self customLabel];
        _timuLabel.textAlignment = NSTextAlignmentLeft;
        [_bgView addSubview:_timuLabel];
        
        [_timuLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_titleNumBtn.mas_bottom);
            make.left.equalTo(self.titleNumBtn.mas_left).offset(20.0);
            make.right.equalTo(_bgView).offset(-8.0);
        }];
        
    }
    return _timuLabel;
}


#pragma mark - 病情表现
- (UILabel *)diseaseLabel{
    if(!_diseaseLabel){
        _diseaseLabel = [self customLabel];
        _diseaseLabel.font = [UIFont systemFontOfSize:16];
        _diseaseLabel.text = @"病情表现：";
        [_bgView addSubview:_diseaseLabel];

        [_diseaseLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_timuLabel.mas_bottom).offset(4.0);
            make.left.equalTo(_bgView).offset(8.0);
            make.height.mas_offset(20);
            make.bottom.equalTo(_bgView).offset(-8.0);
        }];

    }
    return _diseaseLabel;
}
#pragma mark - 病情表现选择
- (UITextField *)textField{
    if(!_textField){
        _textField = [[UITextField alloc]init];
        _textField.placeholder =@"请输入（例：A）";
//        _textField.textColor =[UIColor colorFromHexString:@"#A2A2A2"];
        _textField.borderStyle = UITextBorderStyleNone;
        _textField.font =[UIFont systemFontOfSize:16];
        _textField.textAlignment = NSTextAlignmentCenter;
        _textField.delegate = self;
        
        //边框
        _textField.layer.borderColor = Color_Main_Green.CGColor;
        _textField.layer.borderWidth = 1;
        
        //左边距
        _textField.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, 0)];
        _textField.leftViewMode = UITextFieldViewModeAlways;
        
        //清除样式
//        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        [_bgView addSubview:_textField];
        
        
        [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_diseaseLabel);
            make.left.equalTo(_diseaseLabel.mas_right);
            make.right.mas_lessThanOrEqualTo(_bgView.mas_right);
//            make.height.mas_equalTo(50);
        }];
    }
    return _textField;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{

}
//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
//    if([textField.text isEqualToString:@"A"]){
//        return YES;
//    }
//    return NO;
//}
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
//    if([textField.text isEqualToString:@"A"]){
//        return YES;
//    }
//    return NO;
//}

#pragma mark - 加
- (UIButton *)addBtn{
    if(!_addBtn){
        _addBtn = [self customButton];
        [_addBtn setTitle:@"+" forState:UIControlStateNormal];
        [_addBtn addTarget:self action:@selector(addBtnClcik) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_addBtn];
        
        [_addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.subBtn.mas_left).offset(-4.0);
            make.centerY.equalTo(self.contentView);
            make.height.mas_offset(24);
            make.width.mas_offset(24);
        }];
        
    }
    return _addBtn;
}
- (void)addBtnClcik{
    if(self.addOrSubControlBlock)self.addOrSubControlBlock(@"+");
}

#pragma mark - 减
- (UIButton *)subBtn{
    if(!_subBtn){
        _subBtn = [self customButton];
        [_subBtn setTitle:@"-" forState:UIControlStateNormal];
        [_subBtn addTarget:self action:@selector(subBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_subBtn];
        
        [_subBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-14.0);
            make.centerY.equalTo(self.contentView);
            make.height.mas_offset(24);
            make.width.mas_offset(24);
        }];
        
    }
    return _subBtn;
}
- (void)subBtnClick{
    if(self.addOrSubControlBlock)self.addOrSubControlBlock(@"-");
}

#pragma mark - 公共
- (UILabel *)customLabel{
    UILabel *label = [[UILabel alloc]init];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont systemFontOfSize:15];
    label.numberOfLines = 0;
    return label;
}

- (UIButton *)customButton{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.layer.borderWidth = 1.0;
    button.layer.borderColor = Color_Main_Green.CGColor;
    button.layer.cornerRadius = 12.0;
    button.layer.masksToBounds = YES;
    [button setTitleColor:Color_Main_Green forState:UIControlStateNormal];
    button.titleLabel.font =[UIFont systemFontOfSize:17];
    return button;
}

- (UIButton *)customButton_choose{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitleColor:[UIColor colorFromHexString:@"#4169E1"] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:17];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//    button.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
//    button.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 4);
    button.titleLabel.numberOfLines = 0;
    return button;
}

#pragma mark - 数据加载
- (void)setModel:(QWQuesAitimuListModel *)model{
    _model = model;
    
    [_titleNumBtn setTitle:[NSString stringWithFormat:@"第%ld题：%@ >",[_model.q_id integerValue]+1,_model.q_title] forState:UIControlStateNormal];
    
    NSString *__block timuStr = @"";
    [_model.options enumerateObjectsUsingBlock:^(QWOptionModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if(idx == 0){//病情表现
            _textField.text =obj.num;
        }
        
        timuStr = [timuStr stringByAppendingFormat:@"%@%@：%@",
                   idx == 0?@"":@"\n",
                   obj.num,
                   obj.content];
    }];
    _timuLabel.text = timuStr;
    
}


@end
