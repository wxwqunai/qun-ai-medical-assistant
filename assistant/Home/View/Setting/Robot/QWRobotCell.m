//
//  QWRobotCell.m
//  assistant
//
//  Created by qunai on 2023/8/24.
//

#import "QWRobotCell.h"
@interface QWRobotCell()
@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UIView *conditionView;
@property (nonatomic, strong) UIView *conclusionView;
@property (nonatomic, strong) UIView *operationView;

@property (nonatomic, strong) UILabel *conditionLabel; //依据
@property (nonatomic, strong) UILabel *conclusionLabel; //结论
@property (nonatomic, strong) UIButton *operationBtn; //操作
@end
@implementation QWRobotCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        [self bgView];
        
        [self operationView];
        [self conditionView];
        [self conclusionView];

    }
    return self;
}


- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(2.0);
            make.right.equalTo(self.contentView).offset(-2.0);
            make.top.equalTo(self.contentView).offset(1.0);
            make.bottom.equalTo(self.contentView).offset(-1.0);
            
        }];
    }
    return _bgView;
}

#pragma mark - 依据
- (UIView *)conditionView{
    if(!_conditionView){
        _conditionView = [self customView];
        [_bgView addSubview:_conditionView];
        
        [_conditionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
            make.right.equalTo(_operationView.mas_left).multipliedBy(0.5).offset(-1.0);
        }];
        
        
        _conditionLabel = [self customLabel];
        _conditionLabel.text = @"";
        [_conditionView addSubview:_conditionLabel];
        
        [_conditionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_conditionView).offset(2.0);
            make.bottom.equalTo(_conditionView).offset(-2.0);
            make.left.equalTo(_conditionView).offset(2.0);
            make.right.equalTo(_conditionView).offset(-2.0);
        }];
        
    }
    return _conditionView;
}

#pragma mark - 结论
- (UIView *)conclusionView{
    if(!_conclusionView){
        _conclusionView = [self customView];
        [_bgView addSubview:_conclusionView];
        
        [_conclusionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_operationView.mas_left).multipliedBy(0.5).offset(1.0);
            make.right.equalTo(_operationView.mas_left).offset(-2.0);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
        }];
        
        _conclusionLabel = [self customLabel];
        _conclusionLabel.text = @"";
        [_conclusionView addSubview:_conclusionLabel];
        
        [_conclusionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_conclusionView).offset(2.0);
            make.bottom.equalTo(_conclusionView).offset(-2.0);
            make.left.equalTo(_conclusionView).offset(2.0);
            make.right.equalTo(_conclusionView).offset(-2.0);
        }];
    }
    return _conclusionView;
}

#pragma mark - 操作
- (UIView *)operationView{
    if(!_operationView){
        _operationView = [self customView];
        [_bgView addSubview:_operationView];
        
        [_operationView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_bgView);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView);
            make.width.mas_equalTo(80.0);
        }];
        
        _operationBtn= [UIButton buttonWithType:UIButtonTypeCustom];
        [_operationBtn setTitle:@"删除" forState:UIControlStateNormal];
        [_operationBtn setTitleColor:[UIColor colorFromHexString:@"#4169E1"] forState:UIControlStateNormal];
        
        _operationBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [_operationBtn addTarget:self action:@selector(operationBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_operationView addSubview:_operationBtn];
        
        [_operationBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_operationView);
        }];
    }
    return _operationView;
}

- (void)operationBtnClick{
    [SWHUDUtil hideHudViewWithMessageSuperView:self.superVC.view withMessage:@"维护中！"];
}

#pragma mark - 公共
- (UIView *)customView{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor colorFromHexString:@"#E9ECF6"];
    return view;
}

- (UILabel *)customLabel{
    UILabel *label = [[UILabel alloc]init];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont systemFontOfSize:15];
    label.numberOfLines = 0;
    return label;
}

#pragma mark - 数据

- (void)setModel:(QWQuesAiDiagnosticRulesModel *)model{
    _model = model;
    
    //依据
    NSString * __block conditionStr = @"";
    [_model.ai_condition enumerateObjectsUsingBlock:^(QWQuesARconditionModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if(obj){
            conditionStr = [conditionStr stringByAppendingFormat:@"%@%@%@%@",
                            idx == 0 ||IsStringEmpty(obj.q_id)?@"":@"\n\n",
                            !IsStringEmpty(obj.q_id)?[NSString stringWithFormat:@"%ld题：",[obj.q_id integerValue]+1]:@"",
                            !IsStringEmpty(obj.q_title)?obj.q_title:@"",
                            !IsStringEmpty(obj.answer_select_text)?[NSString stringWithFormat:@"\n-选项：%@",obj.answer_select_text]:@""
            ];
        }
    }];
    _conditionLabel.text = conditionStr;
    
    
    //结论
    NSString * __block conclusionStr = @"";
    [_model.ai_conclusion enumerateObjectsUsingBlock:^(QWAiConclusionModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if(obj){
            conclusionStr = [conclusionStr stringByAppendingFormat:@"%@%@%@",
                             idx == 0||IsStringEmpty(obj.suggest)?@"":@"\n\n",
                             !IsStringEmpty(obj.w)?[NSString stringWithFormat:@"%.0f%%",[obj.w floatValue]*100]:@"",
                             !IsStringEmpty(obj.suggest)?obj.suggest:@""
            ];
        }
    }];
    _conclusionLabel.text = conclusionStr;
    
}


@end
