//
//  QWQuesSettingHeaderView.h
//  assistant
//
//  Created by qunai on 2023/8/16.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^MoreClickBlock)(void);
@interface QWQuesSettingHeaderView : UITableViewHeaderFooterView
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic,copy)MoreClickBlock moreClickBlock;
@end

NS_ASSUME_NONNULL_END
