//
//  QWQuesSettingCell.h
//  assistant
//
//  Created by qunai on 2023/8/17.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWQuesSettingCell : QWBaseTableViewCell
@property (nonatomic, strong) UIView *baseInfoView;
@property (nonatomic, strong) UILabel *nameLabel;
@end

NS_ASSUME_NONNULL_END
