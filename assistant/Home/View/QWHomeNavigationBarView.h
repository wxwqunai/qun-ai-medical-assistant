//
//  QWHomeNavigationBarView.h
//  assistant
//
//  Created by qunai on 2024/3/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWHomeNavigationBarView : UIView
@property (nonatomic,strong) UIView *leftView;
//@property (nonatomic,strong) UIImageView *logoImageView;
//@property (nonatomic,strong) UILabel *titleLabel;

@property (nonatomic,strong) UIView *rightView;
@property (nonatomic,strong) UIButton *newsBtn;
//@property (nonatomic,strong) UIButton *scansBtn;

@property (nonatomic,copy) void (^newsOpenBlock)(void);
@end

NS_ASSUME_NONNULL_END
