//
//  QASearchView.h
//  assistant
//
//  Created by kevin on 2023/4/28.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^SearchCompleteBlock)(NSString *searchStr);

@interface QASearchView : UIView

@property (nonatomic,copy)SearchCompleteBlock searchCompleteBlock;


@end

NS_ASSUME_NONNULL_END
