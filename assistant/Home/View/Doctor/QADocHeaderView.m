//
//  QADocHeaderView.m
//  assistant
//
//  Created by kevin on 2023/4/27.
//

#import "QADocHeaderView.h"
@interface QADocHeaderView()
@property (nonatomic, strong) UIImageView *lineImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UIButton *moreButton;
@end

@implementation QADocHeaderView

- (instancetype)initWithReuseIdentifier:(nullable NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if(self){
//        self.backgroundColor = [UIColor whiteColor];
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self lineImageView];
        [self nameLabel];
        [self moreButton];
    }
    return self;
}

#pragma mark - lineImage
- (UIImageView *)lineImageView{
    if(!_lineImageView){
        _lineImageView = [[UIImageView alloc]init];
        _lineImageView.backgroundColor = Color_Main_Green;
        [self addSubview:_lineImageView];
        
        [_lineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(14.0);
            make.centerY.equalTo(self);
            make.height.equalTo(self).multipliedBy(1.2/3.0);
            make.width.mas_equalTo(3.0);
        }];
    }
    return _lineImageView;
}

#pragma mark - name
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont boldSystemFontOfSize:17];
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.text = @"群爱名医";
        [self addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_lineImageView.mas_right).offset(14);
            make.centerY.equalTo(self);
        }];
    }
    return _nameLabel;
}

#pragma mark - 按钮-更多
- (UIButton *)moreButton{
    if(!_moreButton){
        _moreButton =[UIButton buttonWithType:UIButtonTypeCustom];
        [_moreButton setTitle:@"更多" forState:UIControlStateNormal];
        [_moreButton setImage:[UIImage imageNamed:@"next_arrow.png"] forState:UIControlStateNormal];
        
        [_moreButton setTitleColor:[UIColor colorFromHexString:@"#515151"] forState:UIControlStateNormal];
        _moreButton.titleLabel.font =[UIFont systemFontOfSize:16];
        
        [_moreButton setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
        _moreButton.imageEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);

        
        [_moreButton addTarget:self action:@selector(moreButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_moreButton];
        
        [_moreButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.right.equalTo(self).offset(-14.0);
        }];
    }
    return _moreButton;
}
-(void)moreButtonClick:(UIButton *)sender{
    NSLog(@"分享");
    if(self.moreClickBlock)self.moreClickBlock();
}


@end
