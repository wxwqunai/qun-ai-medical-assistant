//
//  QASearchView.m
//  assistant
//
//  Created by kevin on 2023/4/28.
//

#import "QASearchView.h"

@interface QASearchView()<UISearchBarDelegate>
@property (nonatomic,strong) UISearchBar *searchBar;
@end

@implementation QASearchView

- (instancetype)init{
    self = [super init];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        [self searchBar];
    }
    return self;
}

#pragma mark-搜索
-(UISearchBar *)searchBar
{
    if(!_searchBar){
        UIView *titleView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 60)];
        titleView.backgroundColor = [UIColor clearColor];
//        titleView.layer.cornerRadius = 8;
//        titleView.clipsToBounds = YES;
        [self addSubview:titleView];
        
        _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0,titleView.frame.size.width, titleView.frame.size.height)];
        [_searchBar setBackgroundImage:[UIImage new]];
        [titleView addSubview:_searchBar];
        
        _searchBar.tintColor = Color_Black;
        _searchBar.placeholder = @"请输入姓名/科室/医院搜索";
        _searchBar.delegate = self;
//        [_searchBar setBackgroundColor:[UIColor clearColor]];
//        _searchBar.barTintColor = Color_TableView_Gray;
    }
    return _searchBar;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if(IsStringEmpty(searchText)){
        if(self.searchCompleteBlock)self.searchCompleteBlock(searchText);
    }
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSString *input_word = searchBar.text;
    if(self.searchCompleteBlock)self.searchCompleteBlock(input_word);
}

@end
