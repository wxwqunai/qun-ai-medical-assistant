//
//  QADocHeaderView.h
//  assistant
//
//  Created by kevin on 2023/4/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^MoreClickBlock)(void);

@interface QADocHeaderView : UITableViewHeaderFooterView
@property (nonatomic,copy)MoreClickBlock moreClickBlock;
@end

NS_ASSUME_NONNULL_END
