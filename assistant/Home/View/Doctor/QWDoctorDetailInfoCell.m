//
//  QWDoctorDetailInfoCell.m
//  assistant
//
//  Created by kevin on 2023/5/18.
//

#import "QWDoctorDetailInfoCell.h"
@interface QWDoctorDetailInfoCell()
@property (nonatomic, strong) UIImageView *headPicImageView;

@property (nonatomic, strong) UIView *baseInfoView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *companyLabel;
@property (nonatomic, strong) UILabel *levelLabel;

@property (nonatomic, strong) UILabel *phoneLabel;
@property (nonatomic, strong) UILabel *weixinLabel;


@property (nonatomic, strong) UIView *introView;
@property (nonatomic, strong) UILabel *goodAtLabel;
@property (nonatomic, strong) UILabel *personalLabel;

@property (nonatomic, strong) UIButton *consultationButton;
@end

@implementation QWDoctorDetailInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];

        [self baseInfoView];
        [self headPicImageView];
        [self introView];
    }
    return self;
}

#pragma mark - 头像
- (UIImageView *)headPicImageView{
    if(!_headPicImageView){
        _headPicImageView = [[UIImageView alloc]init];
        _headPicImageView.backgroundColor = [UIColor lightGrayColor];
        _headPicImageView.layer.cornerRadius = 45;
        _headPicImageView.layer.masksToBounds = YES;
        [self.contentView addSubview:_headPicImageView];
        
        [_headPicImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(15);
            make.right.equalTo(self.baseInfoView.mas_left).offset(-15);
            make.centerY.equalTo(self.baseInfoView);
            make.width.mas_equalTo(90);
            make.height.mas_equalTo(90);
        }];
    }
    return _headPicImageView;
}
#pragma mark - view
- (UIView *)baseInfoView{
    if(!_baseInfoView){
        _baseInfoView = [[UIView alloc]init];
        _baseInfoView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_baseInfoView];
        
        [_baseInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(20.0);
            make.right.equalTo(self.contentView).offset(-10.0);
            make.height.mas_greaterThanOrEqualTo(90);
        }];
    
        [self nameLabel];
        [self levelLabel];

        [self companyLabel];
        
        [self phoneLabel];
        [self weixinLabel];

    }
    return _baseInfoView;
}

#pragma mark - 姓名
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont boldSystemFontOfSize:20];
        _nameLabel.textColor = [UIColor blackColor];
        [_baseInfoView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_baseInfoView);
            make.top.equalTo(_baseInfoView).offset(5.0);
        }];
    }
    return _nameLabel;
}

#pragma mark - 级别
- (UILabel *)levelLabel{
    if(!_levelLabel){
        _levelLabel = [[UILabel alloc]init];
        _levelLabel.font = [UIFont systemFontOfSize:14];
        _levelLabel.textColor = [UIColor colorFromHexString:@"#1F2020"];
        [_baseInfoView addSubview:_levelLabel];
        
        [_levelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameLabel.mas_right).offset(20.0);
            make.centerY.equalTo(_nameLabel);
        }];
    }
    return _levelLabel;
}

#pragma mark - 医院
- (UILabel *)companyLabel{
    if(!_companyLabel){
        _companyLabel = [[UILabel alloc]init];
        _companyLabel.numberOfLines = 2;
        _companyLabel.font = [UIFont systemFontOfSize:16];
        _companyLabel.textColor = [UIColor colorFromHexString:@"#1F2020"];
        [_baseInfoView addSubview:_companyLabel];
        
        [_companyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_nameLabel.mas_bottom).offset(6.0);
            make.left.equalTo(_baseInfoView);
//            make.centerY.equalTo(_baseInfoView);
            make.right.equalTo(_baseInfoView);
        }];
    }
    return _companyLabel;
}


#pragma mark - 电话
- (UILabel *)phoneLabel{
    if(!_phoneLabel){
        _phoneLabel = [[UILabel alloc]init];
        _phoneLabel.numberOfLines = 2;
        _phoneLabel.font = [UIFont systemFontOfSize:14];
        _phoneLabel.textColor = [UIColor colorFromHexString:@"#1F2020"];
        [_baseInfoView addSubview:_phoneLabel];
        [_phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_companyLabel.mas_bottom).offset(6.0);
            make.left.equalTo(_baseInfoView);
            make.right.equalTo(_baseInfoView.mas_centerX);
            make.bottom.equalTo(_baseInfoView);
        }];
    }
    return _phoneLabel;
}

#pragma mark - 微信
- (UILabel *)weixinLabel{
    if(!_weixinLabel){
        _weixinLabel = [[UILabel alloc]init];
        _weixinLabel.hidden = YES;
        _weixinLabel.numberOfLines = 2;
        _weixinLabel.font = [UIFont systemFontOfSize:14];
        _weixinLabel.textColor = [UIColor colorFromHexString:@"#1F2020"];
        [_baseInfoView addSubview:_weixinLabel];
        
        [_weixinLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_phoneLabel.mas_right).offset(8.0);
            make.centerY.equalTo(_phoneLabel);
            make.right.equalTo(_baseInfoView);
        }];
    }
    return _weixinLabel;
}


#pragma mark - view
- (UIView *)introView{
    if(!_introView){
        _introView = [[UIView alloc]init];
        _introView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_introView];
        
        [_introView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(15.0);
            make.right.equalTo(self.contentView).offset(-10.0);
            make.top.equalTo(_baseInfoView.mas_bottom).offset(8.0);
            make.bottom.equalTo(self.contentView);
        }];
        
        [self goodAtLabel];
        [self personalLabel];
        [self consultationButton];
    }
    return _introView;
}
#pragma mark - 擅长方向
- (UILabel *)goodAtLabel{
    if(!_goodAtLabel){
        UILabel *nameLabel = [[UILabel alloc]init];
        nameLabel.text = @"擅长方向：";
        nameLabel.font = [UIFont systemFontOfSize:14];
        nameLabel.textColor = [UIColor colorFromHexString:@"#1F2020"];
        [_introView addSubview:nameLabel];
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_introView);
            make.top.equalTo(_introView);
            make.width.mas_equalTo(70);
        }];
        
        
        _goodAtLabel = [[UILabel alloc]init];
        _goodAtLabel.numberOfLines = 0;
        _goodAtLabel.font = [UIFont systemFontOfSize:14];
        _goodAtLabel.textColor = [UIColor colorFromHexString:@"#1F2020"];
        [_introView addSubview:_goodAtLabel];

        [_goodAtLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(nameLabel.mas_right);
            make.right.equalTo(_introView);
            make.top.equalTo(nameLabel.mas_top);
            make.height.mas_greaterThanOrEqualTo(14.0);
        }];
    }
    return _goodAtLabel;
}

#pragma mark - 个人简介
- (UILabel *)personalLabel{
    if(!_personalLabel){
        UILabel *nameLabel = [[UILabel alloc]init];
        nameLabel.text = @"个人简介：";
        nameLabel.font = [UIFont systemFontOfSize:14];
        nameLabel.textColor = [UIColor colorFromHexString:@"#1F2020"];
        [_introView addSubview:nameLabel];
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_introView);
            make.top.equalTo(_goodAtLabel.mas_bottom).offset(8.0);
            make.width.mas_equalTo(70);
        }];
        
        
        _personalLabel = [[UILabel alloc]init];
        _personalLabel.numberOfLines = 0;
        _personalLabel.font = [UIFont systemFontOfSize:14];
        _personalLabel.textColor = [UIColor colorFromHexString:@"#1F2020"];
        [_introView addSubview:_personalLabel];

        [_personalLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(nameLabel.mas_right);
            make.right.equalTo(_introView);
            make.top.equalTo(nameLabel.mas_top);
            make.height.mas_greaterThanOrEqualTo(14.0);
        }];
    }
    return _personalLabel;
}


#pragma mark - 咨询
- (UIButton *)consultationButton{
    if(!_consultationButton){
        _consultationButton =[UIButton buttonWithType:UIButtonTypeCustom];
        
        [_consultationButton setTitle:@"咨询：" forState:UIControlStateNormal];
        [_consultationButton setImage:[UIImage imageNamed:@"doc_chat.png"] forState:UIControlStateNormal];
        
        [_consultationButton setTitleColor:[UIColor colorFromHexString:@"#1F2020"] forState:UIControlStateNormal];
        _consultationButton.titleLabel.font =[UIFont systemFontOfSize:14];
        
        [_consultationButton setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
        _consultationButton.imageEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);


        [_consultationButton addTarget:self action:@selector(addAttentionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [_introView addSubview:_consultationButton];

        [_consultationButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_introView);
            make.top.equalTo(_personalLabel.mas_bottom).offset(8.0);
            make.bottom.equalTo(_introView);
        }];
    }
    return _consultationButton;
}
-(void)addAttentionButtonClick:(UIButton *)sender{
    NSLog(@"咨询");
    if(self.chatBlock)self.chatBlock();
}

#pragma mark - 数据
- (void)setDetailModel:(QADoctorDetailModel *)detailModel
{
    _detailModel = detailModel;
    
    //头像
    NSString *headStr = [QWM_BASE_API_URL stringByAppendingFormat:@"/bokeupload%@", _detailModel.photo];
    [_headPicImageView sd_setImageWithURL:[NSURL URLWithString:headStr] placeholderImage:[UIImage imageNamed:@"contact_default_avatar"]];
    
    //姓名
    _nameLabel.text = _detailModel.name;
    
    //级别
    _levelLabel.text = _detailModel.level;
    
    //医院 科室
//    _companyLabel.text = [NSString stringWithFormat:@"%@  %@",_detailModel.companyId,_detailModel.officeId];
    _companyLabel.text = _detailModel.companyId;
    
    //电话
    _phoneLabel.text = _detailModel.officeId;
//    if(!IsStringEmpty(_detailModel.phone) && _detailModel.phone.length>7){
//        NSString *numberString = [_detailModel.phone stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
//        _phoneLabel.text = [@"电话：" stringByAppendingFormat:@"%@",numberString];
//    }else{
//        _phoneLabel.text = @"电话：****";
//    }
    
    //微信
    _weixinLabel.text = [@"微信：" stringByAppendingFormat:@"%@",IsStringEmpty(_detailModel.wechat)?@"****":_detailModel.wechat];
    
    //擅长方法
    _goodAtLabel.text = _detailModel.major;
    
    //个人简介
    _personalLabel.text = _detailModel.resume;

}

@end
