//
//  QWDoctorDetailInfoCell.h
//  assistant
//
//  Created by kevin on 2023/5/18.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN
typedef void (^ChatBlock)(void);

@interface QWDoctorDetailInfoCell : QWBaseTableViewCell
@property (nonatomic, strong) QADoctorDetailModel *detailModel;
@property (nonatomic,copy)ChatBlock chatBlock;

@end

NS_ASSUME_NONNULL_END
