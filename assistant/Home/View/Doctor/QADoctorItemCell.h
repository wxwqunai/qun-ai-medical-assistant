//
//  QADoctorItemCell.h
//  assistant
//
//  Created by kevin on 2023/4/27.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QADoctorItemCell : QWBaseTableViewCell
@property (nonatomic,strong) QADoctorModel* model;
@end

NS_ASSUME_NONNULL_END
