//
//  QWFuncMenusCell.h
//  assistant
//
//  Created by qunai on 2023/10/9.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN
typedef void (^BtnsClickBlock)(NSString *selStr);

@interface QWFuncMenusCell : QWBaseTableViewCell
@property (nonatomic,copy) BtnsClickBlock btnsClickBlock;
@property (nonatomic, copy) NSString *unreadIMCount;
@end

NS_ASSUME_NONNULL_END
