//
//  QWProductMeetingCell.m
//  assistant
//
//  Created by qunai on 2024/1/9.
//

#import "QWProductMeetingCell.h"
@interface QWProductMeetingCell()
@property (nonatomic, strong) UIView *bgView;
@end
@implementation QWProductMeetingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self bgView];
        [self nameLabel];
        [self desLabel];
        
        [self newsButton];
        [self scheduleButton];
    }
    return self;
}
- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_bgView];
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(10.0);
            make.right.equalTo(self.contentView).offset(-10.0);
            make.top.equalTo(self.contentView).offset(2.0);
            make.bottom.equalTo(self.contentView).offset(-2.0);
        }];
    }
    return _bgView;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont systemFontOfSize:15 weight:(UIFontWeightLight)];
        _nameLabel.textColor = [UIColor blueberryColor];
        _nameLabel.numberOfLines = 3;
        [self.bgView addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView).offset(4.0);
            make.right.equalTo(self.bgView).offset(-4.0);
            make.top.equalTo(self.bgView).offset(4.0);
        }];
    }
    return _nameLabel;
}

- (UILabel *)desLabel{
    if(!_desLabel){
        _desLabel = [[UILabel alloc]init];
        _desLabel.font = [UIFont systemFontOfSize:13 weight:(UIFontWeightLight)];
        _desLabel.numberOfLines = 0;
        [self.bgView addSubview:_desLabel];
        [_desLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView).offset(4.0);
            make.right.equalTo(self.bgView).offset(-4.0);
            make.top.equalTo(self.nameLabel.mas_bottom).offset(4);
        }];
    }
    return _desLabel;
}

#pragma mark - 新闻
- (UIButton *)newsButton{
    if(!_newsButton){
        _newsButton= [self customButton];
        [_newsButton setTitle:@"新闻" forState:UIControlStateNormal];
        [_newsButton addTarget:self action:@selector(newsButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [self.bgView addSubview:_newsButton];
        
        [_newsButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView).offset(4.0);
            make.top.equalTo(self.desLabel.mas_bottom).offset(6.0);
            make.width.mas_equalTo(80);
            make.height.mas_equalTo(22);
            make.bottom.equalTo(self.bgView).offset(-4.0);
        }];
    }
    return _newsButton;
}
- (void)newsButtonClick{
    if(self.newsOnBlock)self.newsOnBlock();
}
#pragma mark - 日程
- (UIButton *)scheduleButton{
    if(!_scheduleButton){
        _scheduleButton= [self customButton];
        [_scheduleButton setTitle:@"日程" forState:UIControlStateNormal];
        [_scheduleButton addTarget:self action:@selector(scheduleButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [self.bgView addSubview:_scheduleButton];
        
        [_scheduleButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.newsButton.mas_right).offset(6.0);
            make.top.equalTo(self.newsButton.mas_top);
            make.width.equalTo(self.newsButton.mas_width);
            make.bottom.equalTo(self.newsButton.mas_bottom);
        }];
    }
    return _scheduleButton;
}
- (void)scheduleButtonClick{
    if(self.newsOnBlock)self.newsOnBlock();
}

- (UIButton *)customButton{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitleColor:Color_Main_Green forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    button.layer.cornerRadius = 4.0;
    button.layer.masksToBounds = YES;
    button.layer.borderColor = Color_Main_Green.CGColor;
    button.layer.borderWidth = 1.0;
    return button;
}

@end
