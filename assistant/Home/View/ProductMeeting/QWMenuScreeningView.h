//
//  QWMenuScreeningView.h
//  assistant
//
//  Created by qunai on 2024/1/9.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWMenuScreeningView : UIView
-(void)menuScreeningViewDismiss;

@property (nonatomic, copy) void (^menuSelectedOnBlock)(NSString *dateStr, NSString *city);

@end

NS_ASSUME_NONNULL_END
