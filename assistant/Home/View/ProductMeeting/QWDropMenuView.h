//
//  QWDropMenuView.h
//  assistant
//
//  Created by qunai on 2024/1/9.
//

#import <UIKit/UIKit.h>

#define kWidth [UIScreen mainScreen].bounds.size.width
#define kHeight [UIScreen mainScreen].bounds.size.height

NS_ASSUME_NONNULL_BEGIN

@class QWDropMenuView;
@protocol DropMenuViewDelegate <NSObject>

-(void)dropMenuView:(QWDropMenuView *)view didSelectName:(NSArray *)strArr;

@end


@interface QWDropMenuView : UIView
@property (nonatomic, weak) id<DropMenuViewDelegate> delegate;

/** 箭头变化 */
@property (nonatomic, strong) UIView *arrowView;


/**
  控件设置

 @param view 提供控件 位置信息
 @param tableNum 显示TableView数量
 @param arr 使用数据
 */
-(void)creatDropView:(UIView *)view withShowTableNum:(NSInteger)tableNum withData:(NSArray *)arr;

/** 视图消失 */
- (void)dismiss;
@end

NS_ASSUME_NONNULL_END
