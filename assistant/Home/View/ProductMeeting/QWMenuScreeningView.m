//
//  QWMenuScreeningView.m
//  assistant
//
//  Created by qunai on 2024/1/9.
//

#import "QWMenuScreeningView.h"
#import "QWDropMenuView.h"

@interface QWMenuScreeningView ()<DropMenuViewDelegate>

@property (nonatomic, strong) UIButton *oneLinkageButton;
@property (nonatomic, strong) UIButton *twoLinkageButton;

@property (nonatomic, strong) QWDropMenuView *oneLinkageDropMenu;
@property (nonatomic, strong) QWDropMenuView *twoLinkageDropMenu;

@property (nonatomic, strong) NSArray *addressArr;
@property (nonatomic, strong) NSArray *categoriesArr;


@end

@implementation QWMenuScreeningView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        self.oneLinkageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.oneLinkageButton.frame = CGRectMake(0, 0, kWidth/2, frame.size.height);
        [self setUpButton:self.oneLinkageButton withText:@"日期"];
        
        self.oneLinkageDropMenu = [[QWDropMenuView alloc] init];
        self.oneLinkageDropMenu.arrowView = self.oneLinkageButton.imageView;
        self.oneLinkageDropMenu.delegate = self;
        
        
        
        self.twoLinkageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.twoLinkageButton.frame = CGRectMake(kWidth/2, 0, kWidth/2, frame.size.height);
        [self setUpButton:self.twoLinkageButton withText:@"地址"];
        
        self.twoLinkageDropMenu = [[QWDropMenuView alloc] init];
        self.twoLinkageDropMenu.arrowView = self.twoLinkageButton.imageView;
        self.twoLinkageDropMenu.delegate = self;
        
        
        
        /** 最下面横线 */
        UIView *horizontalLine = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 0.6, kWidth, 0.5)];
        horizontalLine.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.000];
        [self addSubview:horizontalLine];
        
        
    }
    return self;
}



#pragma mark - 按钮点击推出菜单 (并且其他的菜单收起)
-(void)clickButton:(UIButton *)button{

   
    if (button == self.oneLinkageButton) {
        
        [self.twoLinkageDropMenu dismiss];
        
        [self.oneLinkageDropMenu creatDropView:self withShowTableNum:2 withData:self.categoriesArr];
        
    }else if (button == self.twoLinkageButton){
        
        [self.oneLinkageDropMenu dismiss];
    
        [self.twoLinkageDropMenu creatDropView:self withShowTableNum:3 withData:self.addressArr];
    
    }
}



#pragma mark - 筛选菜单消失
-(void)menuScreeningViewDismiss{
    
    [self.oneLinkageDropMenu dismiss];
    [self.twoLinkageDropMenu dismiss];
}


#pragma mark - 协议实现
-(void)dropMenuView:(QWDropMenuView *)view didSelectName:(NSArray *)strArr{
    __block NSString * mutableStr = @"";
    [strArr enumerateObjectsUsingBlock:^(NSString *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        mutableStr = [mutableStr stringByAppendingString:obj];
    }];
    
    if (view == self.oneLinkageDropMenu) {
     
        [self.oneLinkageButton setTitle:[mutableStr containsString:@"全部"]?@"全部":mutableStr forState:UIControlStateNormal];
        [self buttonEdgeInsets:self.oneLinkageButton];
        
    }else if (view == self.twoLinkageDropMenu){
    
        [self.twoLinkageButton setTitle:[mutableStr containsString:@"全部"]?@"全部":mutableStr forState:UIControlStateNormal];
        [self buttonEdgeInsets:self.twoLinkageButton];
    }
    
    NSString *oneBtnText = self.oneLinkageButton.titleLabel.text;
    NSString *twoBtnText = self.twoLinkageButton.titleLabel.text;
    
    NSString *dateStr = [oneBtnText isEqualToString:@"日期"]?@"":oneBtnText;
    NSString *cityStr = [twoBtnText isEqualToString:@"地址"]?@"":twoBtnText;
    
    if(self.menuSelectedOnBlock)self.menuSelectedOnBlock(dateStr, cityStr);
    
}


#pragma mark - 设置Button
-(void)setUpButton:(UIButton *)button withText:(NSString *)str{
    
    [button addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button];
    [button setTitle:str forState:UIControlStateNormal];
    button.titleLabel.font =  [UIFont systemFontOfSize:11];
    button.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [button setTitleColor:[UIColor colorWithWhite:0.3 alpha:1.000] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"downarr"] forState:UIControlStateNormal];
    
    [self buttonEdgeInsets:button];
    
    UIView *verticalLine = [[UIView alloc]init];
    verticalLine.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    [button addSubview:verticalLine];
    verticalLine.frame = CGRectMake(button.frame.size.width - 0.5, 3, 0.5, 30);
}

-(void)buttonEdgeInsets:(UIButton *)button{
    
    [button setTitleEdgeInsets:UIEdgeInsetsMake(0, -button.imageView.bounds.size.width + 2, 0, button.imageView.bounds.size.width + 10)];
    [button setImageEdgeInsets:UIEdgeInsetsMake(0, button.titleLabel.bounds.size.width + 10, 0, -button.titleLabel.bounds.size.width + 2)];
    
}




#pragma mark - 懒加载
-(NSArray *)addressArr{

    if (_addressArr == nil) {
        
        NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"address.plist" ofType:nil]];
        
        _addressArr = dic[@"address"];
    }
    
    return _addressArr;
}

-(NSArray *)categoriesArr{

    if (_categoriesArr == nil) {
        
        _categoriesArr = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"categories.plist" ofType:nil]];
    }
    
    return _categoriesArr;
}

@end
