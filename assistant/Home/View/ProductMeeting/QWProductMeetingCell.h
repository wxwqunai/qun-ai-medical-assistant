//
//  QWProductMeetingCell.h
//  assistant
//
//  Created by qunai on 2024/1/9.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWProductMeetingCell : QWBaseTableViewCell
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *desLabel;

@property (nonatomic,strong) UIButton *newsButton;
@property (nonatomic,strong) UIButton *scheduleButton;


@property (nonatomic,copy) void(^newsOnBlock)(void);
@end

NS_ASSUME_NONNULL_END
