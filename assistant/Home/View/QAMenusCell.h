//
//  QAMenusCell.h
//  assistant
//
//  Created by kevin on 2023/4/28.
//

#import "QWBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN
typedef void (^MenuItemClickBlock)(NSDictionary *data);

@interface QAMenusCell : QWBaseTableViewCell
@property (nonatomic,copy) MenuItemClickBlock menuItemClickBlock;

@end

NS_ASSUME_NONNULL_END
