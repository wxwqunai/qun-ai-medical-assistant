//
//  QWOptionInputCell.m
//  assistant
//
//  Created by qunai on 2023/7/26.
//

#import "QWOptionInputCell.h"

@implementation QWOptionInputCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self subBtn];
        [self addBtn];
        [self textField];
        [self nameLabel];
    }
    return self;
}

#pragma mark - 选项
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont systemFontOfSize:16];
        [self.contentView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(14.0);
            make.centerY.equalTo(self.contentView);
            make.right.equalTo(self.textField.mas_left);
            make.width.mas_offset(60);
        }];
    }
    return _nameLabel;
}

- (UITextField *)textField{
    if(!_textField){
        _textField = [[UITextField alloc]init];
        _textField.placeholder =@"请输入内容";
        _textField.text = self.optionModel.content;
//        _textField.textColor =[UIColor colorFromHexString:@"#A2A2A2"];
        _textField.borderStyle = UITextBorderStyleNone;
        _textField.font =[UIFont systemFontOfSize:16];
        _textField.delegate = self;
        
        //边框
        _textField.layer.borderColor = [UIColor colorFromHexString:@"#A2A2A2"].CGColor;
        _textField.layer.borderWidth = 1;
        
        //左边距
        _textField.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, 0)];
        _textField.leftViewMode = UITextFieldViewModeAlways;
        
        //清除样式
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        [self.contentView addSubview:_textField];
        
        [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(4.0);
            make.right.equalTo(self.addBtn.mas_left).offset(-4.0);
            make.bottom.equalTo(self.contentView).offset(-4.0);
            make.height.mas_offset(40.0);
        }];
    }
    return _textField;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    self.optionModel.content = textField.text;
}

#pragma mark - 加
- (UIButton *)addBtn{
    if(!_addBtn){
        _addBtn = [self customButton];
        [_addBtn setTitle:@"+" forState:UIControlStateNormal];
        [_addBtn addTarget:self action:@selector(addBtnClcik) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_addBtn];
        
        [_addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.subBtn.mas_left).offset(-4.0);
            make.centerY.equalTo(self.contentView);
            make.height.mas_offset(24);
            make.width.mas_offset(24);
        }];
        
    }
    return _addBtn;
}
- (void)addBtnClcik{
    if(self.optionControl)self.optionControl(QWOptionControl_add);
}

#pragma mark - 减
- (UIButton *)subBtn{
    if(!_subBtn){
        _subBtn = [self customButton];
        [_subBtn setTitle:@"-" forState:UIControlStateNormal];
        [_subBtn addTarget:self action:@selector(subBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_subBtn];
        
        [_subBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-14.0);
            make.centerY.equalTo(self.contentView);
            make.height.mas_offset(24);
            make.width.mas_offset(24);
        }];
        
    }
    return _subBtn;
}
- (void)subBtnClick{
    if(self.optionControl)self.optionControl(QWOptionControl_sub);
}

- (UIButton *)customButton{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.layer.borderWidth = 1.0;
    button.layer.borderColor = Color_Main_Green.CGColor;
    button.layer.cornerRadius = 12.0;
    button.layer.masksToBounds = YES;
    [button setTitleColor:Color_Main_Green forState:UIControlStateNormal];
    button.titleLabel.font =[UIFont systemFontOfSize:17];
    return button;
}


#pragma mark - 加载数据
- (void)setOptionModel:(QWAddOptionModel *)optionModel{
    _optionModel = optionModel;
    _textField.text = _optionModel.content;

}
@end
