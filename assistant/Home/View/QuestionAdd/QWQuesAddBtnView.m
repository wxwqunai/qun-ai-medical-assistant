//
//  QWQuesAddBtnView.m
//  assistant
//
//  Created by qunai on 2023/7/26.
//

#import "QWQuesAddBtnView.h"

@implementation QWQuesAddBtnView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        [self addBtn];
    }
    return self;
}

- (UIButton *)addBtn{
    if(!_addBtn){
        _addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _addBtn.layer.borderWidth = 1.0;
        _addBtn.layer.borderColor = Color_Main_Green.CGColor;
        [_addBtn setTitle:@"添加题目" forState:UIControlStateNormal];
        [_addBtn setTitleColor:Color_Main_Green forState:UIControlStateNormal];
        _addBtn.titleLabel.font =[UIFont systemFontOfSize:17];
        [_addBtn addTarget:self action:@selector(addBtnClcik) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_addBtn];
        
        [_addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(14.0);
            make.right.equalTo(self).offset(-14.0);
            make.top.equalTo(self).offset(4.0);
            make.bottom.equalTo(self).offset(-4.0);
            make.height.mas_offset(44);
        }];
        
    }
    return _addBtn;
}
- (void)addBtnClcik{
    if(self.addQuestBlock)self.addQuestBlock();
}

@end
