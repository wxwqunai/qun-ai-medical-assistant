//
//  QWQuesAddTipsView.h
//  assistant
//
//  Created by qunai on 2023/7/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWQuesAddTipsView : UIView
@property (nonatomic, strong) UILabel *symptomLabel;
@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, copy) NSString *symptomsStr;
@end

NS_ASSUME_NONNULL_END
