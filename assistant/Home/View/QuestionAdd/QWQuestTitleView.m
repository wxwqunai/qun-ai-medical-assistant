//
//  QWQuestTitleView.m
//  assistant
//
//  Created by qunai on 2023/7/26.
//

#import "QWQuestTitleView.h"
@interface QWQuestTitleView()<UITextViewDelegate>
@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) UILabel *placeholderLabel;
@property (nonatomic, strong) NSArray *typeArr;
@end

@implementation QWQuestTitleView
- (instancetype)initWithReuseIdentifier:(nullable NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if(self){
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.typeArr = @[@"单选",@"多选",@"填空"];
        
        [self bgView];
        
        [self topView];
        [self numLabel];
        [self nameLabel];
        [self typeBtn];
        [self deleteBtn];
        
        [self importInfoView];
        

    }
    return self;
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(14.0);
            make.right.mas_offset(-14.0);
            make.top.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
        }];
    }
    return _bgView;
}

- (UIView *)topView{
    if(!_topView){
        _topView = [[UIView alloc]init];
        _topView.backgroundColor = [UIColor clearColor];
        [self.bgView addSubview:_topView];
        
        [_topView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView);
            make.right.equalTo(self.bgView);
            make.top.equalTo(self.bgView);
            make.height.mas_offset(40.0);
        }];
    }
    return _topView;
}

#pragma mark - num
- (UILabel *)numLabel{
    if(!_numLabel){
        _numLabel = [[UILabel alloc]init];
        _numLabel.font = [UIFont boldSystemFontOfSize:18];
        [self.topView addSubview:_numLabel];
        
        [_numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.topView).offset(4);
            make.centerY.equalTo(self.topView);
        }];
    }
    return _numLabel;
}
#pragma mark - name
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont boldSystemFontOfSize:18];
        _nameLabel.text = @"标题";
        [self.topView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.numLabel.mas_right).offset(4);
            make.centerY.equalTo(self.topView);
        }];
    }
    return _nameLabel;
}

#pragma mark - 类型-单选、多选、填空
- (UIButton *)typeBtn{
    if(!_typeBtn){
        _typeBtn =[UIButton buttonWithType:UIButtonTypeCustom];
        _typeBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_typeBtn setImage:[UIImage imageNamed:@"ic_arrow_down.png"] forState:UIControlStateNormal];
        [_typeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [_typeBtn setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
        _typeBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
        
        _typeBtn.layer.borderWidth=1;
        _typeBtn.layer.borderColor=[UIColor colorFromHexString:@"#CBCBCB"].CGColor;
        
        [_typeBtn addTarget:self action:@selector(typeBtnClcik) forControlEvents:UIControlEventTouchUpInside];
        [self.topView addSubview:_typeBtn];
        
        [_typeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.topView);
            make.left.equalTo(self.nameLabel.mas_right).offset(20.0);
            make.height.mas_equalTo(30);
            make.width.mas_equalTo(110);
        }];
    }
    return _typeBtn;
}
- (void)typeBtnClcik{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (NSString *value in self.typeArr) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:value style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.questModel.type = value;
            [self.typeBtn setTitle:value forState:UIControlStateNormal];
            if(self.changeQuestTypeBlock)self.changeQuestTypeBlock();
        }];
    //    [action setValue:[UIColor blackColor] forKey:@"_titleTextColor"];
        [alertController addAction:action];
    }
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style: UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
//    [cancelAction  setValue:[UIColor redColor] forKey:@"_titleTextColor"];
    [alertController addAction:cancelAction];
    alertController.modalPresentationStyle = 0;
    [self.superVC presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - 删除
- (UIButton *)deleteBtn{
    if(!_deleteBtn){
        _deleteBtn =[UIButton buttonWithType:UIButtonTypeCustom];
        [_deleteBtn setImage:[UIImage imageNamed:@"QWDelete.png"] forState:UIControlStateNormal];
        [_deleteBtn addTarget:self action:@selector(deleteBtnClcik) forControlEvents:UIControlEventTouchUpInside];
        [self.topView addSubview:_deleteBtn];
        
        [_deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.topView);
            make.right.equalTo(self.topView).offset(-4.0);
            make.height.mas_equalTo(20);
        }];
    }
    return _deleteBtn;
}
- (void)deleteBtnClcik{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"是否确定删除？" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *sureAction = [UIAlertAction actionWithTitle:@"确定" style: UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(self.deleteQuestBlock)self.deleteQuestBlock();
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style: UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
//    [cancelAction  setValue:[UIColor redColor] forKey:@"_titleTextColor"];
    [alertController addAction:sureAction];
    [alertController addAction:cancelAction];
//    alertController.modalPresentationStyle = 0;
    [self.superVC presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - 输入框
-(UITextView *)importInfoView
{
    if (!_importInfoView) {
        _importInfoView =[[UITextView alloc]init];
        _importInfoView.returnKeyType=UIReturnKeyDone;
        _importInfoView.delegate=self;
        _importInfoView.font=[UIFont systemFontOfSize:15];
        _importInfoView.textColor=[UIColor blackColor];
        _importInfoView.backgroundColor=[UIColor clearColor];
//        _importInfoView.scrollEnabled = NO;
        
        _importInfoView.layer.borderWidth=1;
        _importInfoView.layer.borderColor=[UIColor colorFromHexString:@"#CBCBCB"].CGColor;
        
        [self.bgView addSubview:_importInfoView];
        
        [_importInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView).offset(4);
            make.right.equalTo(self.bgView).offset(-4);
            make.top.equalTo(self.topView.mas_bottom).offset(4);
            make.bottom.equalTo(_bgView).offset(-4);
            make.height.mas_equalTo(60.0);
        }];
        
        
        _placeholderLabel =[[UILabel alloc]init];
        _placeholderLabel.text = @"请输入标题";
        _placeholderLabel.font = [UIFont systemFontOfSize:15];
        _placeholderLabel.textColor = [UIColor colorFromHexString:@"#CBCBCB"];
        [_importInfoView addSubview:_placeholderLabel];
        
        [_placeholderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(4);
            make.top.mas_offset(8);
        }];
    }
    return _importInfoView;
}

#pragma mark- UITextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    _placeholderLabel.hidden = YES;
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    self.questModel.title = textView.text;
    _placeholderLabel.hidden = !IsStringEmpty(textView.text);
    return YES;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if( [ @"\n" isEqualToString: text]){
        //你的响应方法
        [self endEditing:YES];
        return NO;
    }
    
    if (range.length + range.location > textView.text.length) {
        return NO;
    }
    
//    NSUInteger length = textView.text.length + text.length - range.length;
    
    
    return YES;
}

- (void)setQuestModel:(QWAddQuestionModel *)questModel{
    _questModel = questModel;
    [_typeBtn setTitle:_questModel.type forState:UIControlStateNormal];
    _importInfoView.text = _questModel.title;
    _placeholderLabel.hidden = !IsStringEmpty(_importInfoView.text);
}

@end
