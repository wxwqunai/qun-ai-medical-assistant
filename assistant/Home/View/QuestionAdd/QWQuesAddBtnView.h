//
//  QWQuesAddBtnView.h
//  assistant
//
//  Created by qunai on 2023/7/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^AddQuestBlock)(void);

@interface QWQuesAddBtnView : UIView
@property (nonatomic, strong) UIButton *addBtn;
@property (nonatomic,copy)AddQuestBlock addQuestBlock;
@end

NS_ASSUME_NONNULL_END
