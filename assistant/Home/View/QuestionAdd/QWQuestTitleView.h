//
//  QWQuestTitleView.h
//  assistant
//
//  Created by qunai on 2023/7/26.
//

#import <UIKit/UIKit.h>
#import "QWQuestModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef void (^DeleteQuestBlock)(void);
typedef void (^ChangeQuestTypeBlock)(void);

@interface QWQuestTitleView : UITableViewHeaderFooterView
@property (nonatomic,copy)DeleteQuestBlock deleteQuestBlock;
@property (nonatomic,copy)ChangeQuestTypeBlock changeQuestTypeBlock;

@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UILabel *numLabel;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic,strong) UITextView *importInfoView;

@property (nonatomic, strong) UIButton *typeBtn;
@property (nonatomic, strong) UIButton *deleteBtn;

@property (nonatomic, strong) UIViewController *superVC;

@property (nonatomic, strong) QWAddQuestionModel *questModel;

@end

NS_ASSUME_NONNULL_END
