//
//  QWQuesAddTipsView.m
//  assistant
//
//  Created by qunai on 2023/7/26.
//

#import "QWQuesAddTipsView.h"

@implementation QWQuesAddTipsView

- (instancetype)init{
    self = [super init];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        [self symptomLabel];
        [self nameLabel];
    }
    return self;
}

#pragma mark - 已选症状
- (UILabel *)symptomLabel{
    if(!_symptomLabel){
        _symptomLabel = [[UILabel alloc]init];
        _symptomLabel.font = [UIFont boldSystemFontOfSize:15];
//        _symptomLabel.textColor = [UIColor redColor];
        _symptomLabel.numberOfLines = 0;
        [self addSubview:_symptomLabel];
        
        [_symptomLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(14);
            make.right.equalTo(self).offset(-14);
            make.top.equalTo(self).offset(4);
        }];
    }
    return _symptomLabel;
}

#pragma mark - name
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont systemFontOfSize:13];
        _nameLabel.textColor = [UIColor redColor];
        _nameLabel.text = @"更复杂的诊断过程，请联系技术人员定制修改。";
        _nameLabel.numberOfLines = 0;
        [self addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(14);
            make.right.equalTo(self).offset(-14);
            make.top.equalTo(self.symptomLabel.mas_bottom).offset(4);
            make.bottom.equalTo(self).offset(-4);
        }];
    }
    return _nameLabel;
}

- (void)setSymptomsStr:(NSString *)symptomsStr{
    _symptomsStr = symptomsStr;
    
    if(!IsStringEmpty(_symptomsStr)){
        _symptomLabel.text = [NSString stringWithFormat:@"已选症状：%@",_symptomsStr];
    }
    
    [_symptomLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(IsStringEmpty(_symptomsStr)?0:4);
    }];
}

@end
