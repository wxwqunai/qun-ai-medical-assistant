//
//  QWOptionInputCell.h
//  assistant
//
//  Created by qunai on 2023/7/26.
//

#import "QWBaseTableViewCell.h"
#import "QWQuestModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger, QWOptionControl) {
    QWOptionControl_add = 0,
    QWOptionControl_sub,
};

typedef void (^OptionControl)(QWOptionControl control);

@interface QWOptionInputCell : QWBaseTableViewCell<UITextFieldDelegate>
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UITextField *textField;

@property (nonatomic, strong) UIButton *addBtn; //加
@property (nonatomic, strong) UIButton *subBtn; //减

@property (nonatomic,copy) OptionControl optionControl;

@property (nonatomic, strong) QWAddOptionModel *optionModel; //减

@end

NS_ASSUME_NONNULL_END
