//
//  QWScrollTextCell.h
//  assistant
//
//  Created by qunai on 2023/10/9.
//

#import "QWBaseTableViewCell.h"
#import "XWVerticalScrollText.h"

NS_ASSUME_NONNULL_BEGIN

@interface QWScrollTextCell : QWBaseTableViewCell
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIImageView *newsImageView;
@property (nonatomic, strong) XWVerticalScrollText *verScrollText;

@property (nonatomic, strong) NSArray *noticeArr;

@property(nonatomic,copy)void(^handlerTitleClickCallBack)(NSInteger index);
@end

NS_ASSUME_NONNULL_END
