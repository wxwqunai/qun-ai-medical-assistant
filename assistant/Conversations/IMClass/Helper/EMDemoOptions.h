//
//  EMDemoOptions.h
//  ChatDemo-UI3.0
//
//  Created by XieYajie on 2018/12/17.
//  Copyright © 2018 XieYajie. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString * _Nullable kOptions_Appkey = @"Appkey";
static NSString * _Nullable kOptions_ApnsCertname = @"ApnsCertname";
static NSString * _Nullable kOptions_HttpsOnly = @"HttpsOnly";

static NSString * _Nullable kOptions_SpecifyServer = @"SpecifyServer";
static NSString * _Nullable kOptions_IMPort = @"IMPort";
static NSString * _Nullable kOptions_IMServer = @"IMServer";
static NSString * _Nullable kOptions_RestServer = @"RestServer";

static NSString * _Nullable kOptions_AutoAcceptGroupInvitation = @"AutoAcceptGroupInvitation";
static NSString * _Nullable kOptions_AutoTransMsgFile = @"AutoTransferMessageAttachments";
static NSString * _Nullable kOptions_AutoDownloadThumb = @"AutoDownloadThumbnail";
static NSString * _Nullable kOptions_DeleteChatExitGroup = @"DeleteChatExitGroup";
static NSString * _Nullable kOptions_SortMessageByServerTime = @"SortMessageByServerTime";
static NSString * _Nullable kOptions_PriorityGetMsgFromServer = @"PriorityGetMsgFromServer";

static NSString * _Nullable kOptions_AutoLogin = @"AutoLogin";
static NSString * _Nullable kOptions_LoggedinUsername = @"LoggedinUsername";
static NSString * _Nullable kOptions_LoggedinPassword = @"LoggedinPassword";

static NSString * _Nullable kOptions_ChatTyping = @"ChatTyping";
static NSString * _Nullable kOptions_AutoDeliveryAck = @"AutoDeliveryAck";

static NSString * _Nullable kOptions_OfflineHangup = @"OfflineHangup";

static NSString * _Nullable kOptions_ShowCallInfo = @"ShowCallInfo";
static NSString * _Nullable kOptions_UseBackCamera = @"UseBackCamera";

static NSString * _Nullable kOptions_IsReceiveNewMsgNotice = @"IsReceiveNewMsgNotice";
static NSString * _Nullable kOptions_WillRecord = @"WillRecord";
static NSString * _Nullable kOptions_WillMergeStrem = @"WillMergeStrem";
static NSString * _Nullable kOptions_EnableConsoleLog = @"enableConsoleLog";

static NSString * _Nullable kOptions_LocationAppkeyArray = @"LocationAppkeyArray";

static NSString * _Nullable kOptions_IsSupportWechatMiniProgram = @"IsSupportMiniProgram";
static NSString * _Nullable kOptions_EnableCustomAudioData = @"EnableCustomAudioData";
static NSString * _Nullable kOptions_CustomAudioDataSamples = @"CustomAudioDataSamples";
static NSString * _Nullable kOptions_IsCustomServer = @"IsCustomServer";
static NSString * _Nullable kOptions_IsFirstLaunch = @"IsFirstLaunch";
static NSString * _Nullable kOptions_TranslateLanguage = @"TranslateLanguage";
static NSString * _Nullable kOptions_isDevelopMode = @"isDevelopMode";

NS_ASSUME_NONNULL_BEGIN

@class EMOptions;
@interface EMDemoOptions : NSObject <NSCoding, NSCopying>

@property (nonatomic, copy) NSString *appkey;
@property (nonatomic, copy) NSString *apnsCertName;
@property (nonatomic, assign) BOOL usingHttpsOnly;
@property (nonatomic) BOOL specifyServer;
@property (nonatomic, assign) int chatPort;
@property (nonatomic, copy) NSString *chatServer;
@property (nonatomic, copy) NSString *restServer;
@property (nonatomic) BOOL isAutoAcceptGroupInvitation;
@property (nonatomic) BOOL isAutoTransferMessageAttachments;
@property (nonatomic) BOOL isAutoDownloadThumbnail;
@property (nonatomic) BOOL isSortMessageByServerTime;
@property (nonatomic) BOOL isPriorityGetMsgFromServer;
@property (nonatomic) BOOL isAutoLogin;
@property (nonatomic, strong) NSString *loggedInUsername;
@property (nonatomic, strong) NSString *loggedInPassword;
@property (nonatomic) BOOL isChatTyping;
@property (nonatomic) BOOL isAutoDeliveryAck;
@property (nonatomic) BOOL isOfflineHangup;
@property (nonatomic) BOOL isShowCallInfo;
@property (nonatomic) BOOL isUseBackCamera;
@property (nonatomic) BOOL isReceiveNewMsgNotice;
@property (nonatomic) BOOL willRecord;
@property (nonatomic) BOOL willMergeStrem;
@property (nonatomic) BOOL enableConsoleLog;
@property (nonatomic) BOOL enableCustomAudioData;
@property (nonatomic) int  customAudioDataSamples;
@property (nonatomic) BOOL isSupportWechatMiniProgram;
@property (nonatomic) BOOL isCustomServer;
@property (nonatomic) BOOL isFirstLaunch;
@property (nonatomic, strong) NSMutableArray *locationAppkeyArray;
@property (nonatomic, strong) NSString* language;
@property (nonatomic) BOOL isDevelopMode;


+ (instancetype)sharedOptions;

+ (void)reInitAndSaveServerOptions;

+ (void)updateAndSaveServerOptions:(NSDictionary *)aDic;

- (void)archive;

- (EMOptions *)toOptions;

@end

NS_ASSUME_NONNULL_END
