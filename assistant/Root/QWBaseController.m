//
//  QWBaseController.m
//  qwm
//
//  Created by kevin on 2023/3/23.
//

#import "QWBaseController.h"

@interface QWBaseController ()
@property (nonatomic,strong) UIView *lineView;
@end

@implementation QWBaseController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    
    //解决机型不同不能全屏问题
    self.navigationController.navigationBar.translucent = NO;
    [self setExtendedLayoutIncludesOpaqueBars:YES];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
//     [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    //解决机型不同不能全屏问题
    self.navigationController.navigationBar.translucent = YES;
    [self setExtendedLayoutIncludesOpaqueBars:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor =[UIColor whiteColor];
    
    UIImageView *navBarHairlineImageView = [self findHairlineImageViewUnder:self.navigationController.navigationBar];
        // 隐藏导航栏下的黑线
        navBarHairlineImageView.hidden = YES;
    
    [self customNavigationBar];
    
    [self leftBarButtonItem];//返回

}

#pragma mark- 自定义导航栏
-(void)customNavigationBar
{
    [self customNavigationBarWithBgColor:Color_Main_Green withTitleColor:[UIColor whiteColor]];
}

-(void)customNavigationBarWithBgColor:(UIColor *)bgcolor withTitleColor:(UIColor *)titleColor{
    //导航栏背景色
    UIColor *navBarColor = bgcolor;
    if (@available(iOS 15.0, *)) {
        UINavigationBarAppearance *appearance = [[UINavigationBarAppearance alloc] init];
        appearance.backgroundColor = navBarColor;
        appearance.backgroundEffect = nil;
        appearance.titleTextAttributes = @{NSForegroundColorAttributeName:titleColor};
        self.navigationController.navigationBar.scrollEdgeAppearance = appearance;
        self.navigationController.navigationBar.standardAppearance = appearance;
    }else{
        [self.navigationController.navigationBar setBackgroundImage:[self imageWithColor:navBarColor] forBarMetrics:UIBarMetricsDefault];
        //设置导航栏文字颜色
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    }
    self.navigationController.navigationBar.tintColor = titleColor;
}


#pragma mark- 导航栏左
-(UIBarButtonItem *)leftBarButtonItem
{
    if (!_leftBarButtonItem) {
        _leftBarButtonItem= [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"WnavBack.png"] style:UIBarButtonItemStylePlain target:self action:@selector(navLeftBaseButtonClick)];
        _leftBarButtonItem.imageInsets=UIEdgeInsetsMake(0, -10, 0, 0);
        self.navigationItem.leftBarButtonItem =_leftBarButtonItem;
    }
    return _leftBarButtonItem;
}
#pragma mark - 导航栏左 - 隐藏\显示
- (void)setIsHiddenLeftBarButtonItem:(BOOL)isHiddenLeftBarButtonItem
{
    _isHiddenLeftBarButtonItem = isHiddenLeftBarButtonItem;
    
    if (@available(iOS 16.0, *)) {
        [_leftBarButtonItem setHidden:_isHiddenLeftBarButtonItem];
    } else {
//            self.navigationItem.leftBarButtonItem = nil;
        _leftBarButtonItem.image = _isHiddenLeftBarButtonItem?[UIImage new]:[UIImage imageNamed:@"WnavBack.png"];

    }
}
#pragma mark-导航栏右
-(UIBarButtonItem *)rightBarButtonItem
{
    if (!_rightBarButtonItem) {
        _rightBarButtonItem= [[UIBarButtonItem alloc]init];
        _rightBarButtonItem.style=UIBarButtonItemStylePlain;
        [_rightBarButtonItem setTintColor:[UIColor whiteColor]];
//        [_rightBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[ZBCOMMETH customFontWithName:FONTPingFangSCRegular size:15],NSFontAttributeName, nil] forState:UIControlStateNormal];
        _rightBarButtonItem.action=@selector(navRightBaseButtonClick);
        _rightBarButtonItem.target=self;
        self.navigationItem.rightBarButtonItem =_rightBarButtonItem;
    }
    return _rightBarButtonItem;
}
#pragma mark-导航条左边按钮
-(void)navLeftBaseButtonClick
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark-导航条右边按钮
-(void)navRightBaseButtonClick
{
}

#pragma mark- 通过颜色来生成一个纯色图片
- (UIImage*)imageWithColor:(UIColor*)color{
    CGRect rect = CGRectMake(0.0f, 0.0f, 100.0f, 99.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);

    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
}

#pragma mark- 寻找导航栏下的黑线
- (UIImageView *)findHairlineImageViewUnder:(UIView *)view {
    if ([view isKindOfClass:UIImageView.class] && view.bounds.size.height <= 1.0) {
        return (UIImageView *)view;
    }
    for (UIView *subview in view.subviews) {
        UIImageView *imageView = [self findHairlineImageViewUnder:subview];
        if (imageView) {
            return imageView;
        }
    }
    return nil;
}

#pragma mark - 返回到某个模块一级界面
- (void)turnToTabVCAtIndex:(NSUInteger)index
{
    if (index != self.self.navigationController.tabBarController.selectedIndex) {
        self.navigationController.tabBarController.selectedIndex = index;
        [self.navigationController popToRootViewControllerAnimated:YES];

    }else{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}


#pragma mark - alert
- (void)showAlert:(NSString *)title
          message:(NSString *)message
          success:(void (^ __nullable)(UIAlertAction *action))sucHandler
          failure:(void (^ __nullable)(UIAlertAction *action))failHandler{
    [self showAlert:title message:message defAction:@"" canAction:@"" success:sucHandler failure:failHandler];
}
- (void)showAlert:(NSString *)title
          message:(NSString *)message
        defAction:(NSString  *)defActTitle
        canAction:(NSString  *)canActTitle
          success:(void (^ __nullable)(UIAlertAction *action))sucHandler
          failure:(void (^ __nullable)(UIAlertAction *action))failHandler{
    
    defActTitle = IsStringEmpty(defActTitle)?@"确定":defActTitle;
    canActTitle = IsStringEmpty(canActTitle)?@"取消":canActTitle;
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:defActTitle style:UIAlertActionStyleDefault handler:sucHandler];
//    [action setValue:[UIColor blackColor] forKey:@"_titleTextColor"];
    [alertController addAction:action];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:canActTitle style: UIAlertActionStyleCancel handler:failHandler];
    [cancelAction  setValue:[UIColor redColor] forKey:@"_titleTextColor"];
    [alertController addAction:cancelAction];
    alertController.modalPresentationStyle = 0;
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
