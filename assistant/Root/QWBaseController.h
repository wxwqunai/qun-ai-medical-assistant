//
//  QWBaseController.h
//  qwm
//
//  Created by kevin on 2023/3/23.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWBaseController : UIViewController
@property (nonatomic,strong) UIBarButtonItem * _Nullable leftBarButtonItem;
@property (nonatomic,strong) UIBarButtonItem * _Nullable rightBarButtonItem;

@property (nonatomic,assign) BOOL isHiddenLeftBarButtonItem;

#pragma mark- 自定义导航栏
-(void)customNavigationBarWithBgColor:(UIColor *)bgcolor withTitleColor:(UIColor *)titleColor;

#pragma mark - 返回到某个模块一级界面
- (void)turnToTabVCAtIndex:(NSUInteger)index;

- (void)showAlert:(NSString *)title
          message:(NSString *)message
          success:(void (^ __nullable)(UIAlertAction *action))sucHandler
          failure:(void (^ __nullable)(UIAlertAction *action))failHandler;

- (void)showAlert:(NSString *)title
          message:(NSString *)message
        defAction:(NSString *)defActTitle
        canAction:(NSString *)canActTitle
          success:(void (^ __nullable)(UIAlertAction *action))sucHandler
          failure:(void (^ __nullable)(UIAlertAction *action))failHandler;

@end

NS_ASSUME_NONNULL_END
