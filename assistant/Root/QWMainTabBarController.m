//
//  QWMainTabBarController.m
//  qwm
//
//  Created by kevin on 2023/3/23.
//

#import "QWMainTabBarController.h"
#import "QWNavgationController.h"
#import "QWHomeController.h"
#import "QWConversationsController.h"
#import "QWMallHomeController.h"

#import "EMRemindManager.h"

@interface QWMainTabBarController ()<UITabBarControllerDelegate, EMChatManagerDelegate, EaseIMKitManagerDelegate>
@property (nonatomic) BOOL isViewAppear;
@property (nonatomic, strong) QWConversationsController *conversationsVC;
@property (nonatomic, strong) QWHomeController *homeVC;
@end

@implementation QWMainTabBarController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.isViewAppear = YES;
    [self _loadConversationTabBarItemBadge];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.isViewAppear = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UITabBar appearance] setTintColor:Color_Main_Green]; // 修改系统默认的tabBarItem的颜色
    [self setTabbarBackGround];
    [self fetchStartData];
    
    [self initUI];
    [self huanXinUnReadMessageInit];//要在页面加载后
}

- (void)initUI {
    
    self.delegate = self;
    
    NSArray *titles = @[@"首页", @"会诊", @"商城",@"消息", @"我的"];
    
    NSArray *className = @[
        @"QWHomeController",
//        @"QWConsultationController",
        @"QWConsultationWebController",
        @"QWMallHomeController",
        @"QWConversationsController",
        @"QWMineController"
    ];
    NSArray *defaultImageNames = @[
        @"qw_tab_home",
        @"qw_tab_quan",
        @"qw_tab_mall",
        @"qw_tab_chat",
        @"qw_tab_me"
    ];
    NSArray *selectedImageNames = @[
        @"qw_tab_home",
        @"qw_tab_quan",
        @"qw_tab_mall",
        @"qw_tab_chat",
        @"qw_tab_me"
    ];
    
    
//    UIColor *def_color = [UIColor colorFromHexString:@"#DEDEDE"];
//    UIColor *sel_color = [UIColor colorFromHexString:@"#333333"];
    
    for (NSInteger i = 0; i < titles.count; i++) {
        UIViewController *viewController = [[NSClassFromString([className objectAtIndex:i]) alloc] init];
        if(i == 0) self.homeVC = (QWHomeController*)viewController;
        if(i == 2) self.conversationsVC = (QWConversationsController*)viewController;
        QWNavgationController *navController  =[[QWNavgationController alloc]initWithRootViewController:viewController];

        UIImage *image =[UIImage imageNamed:defaultImageNames[i]];
        UIImage *selectimage =[UIImage imageNamed:selectedImageNames[i]];
        viewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:titles[i] image:image selectedImage:selectimage];
//        [viewController.tabBarItem setTitleTextAttributes:[NSDictionary
//                                               dictionaryWithObjectsAndKeys:sel_color,
//                                               NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
//        [viewController.tabBarItem setTitleTextAttributes:[NSDictionary
//                                               dictionaryWithObjectsAndKeys:def_color,
//                                               NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
        [self addChildViewController:navController];
    }
    
    
}

#pragma mark - 背景颜色
- (void)setTabbarBackGround{
    UIImage *tabbarImage = [self imageWithColor:[UIColor whiteColor]];
    self.tabBar.backgroundImage = tabbarImage;
    self.tabBar.shadowImage = tabbarImage;
}

- (UIImage *)imageWithColor:(UIColor *)color

{
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    [color setFill];
    UIRectFill(rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;

}
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    NSLog(@"item.title1===%@",item.title);
}

#pragma mark UITabBarController  Delegate
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    NSLog(@"item.title2===%@---%@",[tabBarController class],[viewController class]);
    return YES;
}



#pragma mark - 启动数据
- (void)fetchStartData{
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[QWCommonMethod Instance] fetchDataGuideWay];
    });
 
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [self fetchMenusOnMePage];
    });
}
#pragma mark - 我的-菜单数据
- (void)fetchMenusOnMePage{
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_MENUS_ON_ME_PAGE argument:@{}];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        if(success && result != nil ){
            AppProfile.meMenus = [QWMeMenus mj_objectWithKeyValues:result];
        }
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
    
    }];
}



#pragma mark - 消息模块 - 环信未读消息
- (void)huanXinUnReadMessageInit{
    //监听消息接收，主要更新会话tabbaritem的badge
    [[EMClient sharedClient].chatManager addDelegate:self delegateQueue:nil];
    [EaseIMKitManager.shared addDelegate:self];
}

#pragma mark - EMChatManagerDelegate

- (void)messagesDidReceive:(NSArray *)aMessages
{
    for (EMChatMessage *msg in aMessages) {
        [EMRemindManager remindMessage:msg];
        if (msg.body.type == EMMessageBodyTypeText && [((EMTextMessageBody *)msg.body).text isEqualToString:EMCOMMUNICATE_CALLINVITE]) {
            //通话邀请
            EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:msg.conversationId type:EMConversationTypeGroupChat createIfNotExist:YES];
            if ([((EMTextMessageBody *)msg.body).text isEqualToString:EMCOMMUNICATE_CALLINVITE]) {
                [conversation deleteMessageWithId:msg.messageId error:nil];
                continue;
            }
        }
        
        [EMRemindManager remindMessage:msg];
    }
    
    if (self.isViewAppear) {
        [self _loadConversationTabBarItemBadge];
    }
    
}

//　收到已读回执
- (void)messagesDidRead:(NSArray *)aMessages
{
    [self _loadConversationTabBarItemBadge];
}

- (void)conversationListDidUpdate:(NSArray *)aConversationList
{
    [self _loadConversationTabBarItemBadge];
}

- (void)onConversationRead:(NSString *)from to:(NSString *)to
{
    [self _loadConversationTabBarItemBadge];
}

#pragma mark - EaseIMKitManagerDelegate

- (void)conversationsUnreadCountUpdate:(NSInteger)unreadCount
{
    __weak typeof(self) weakself = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        weakself.conversationsVC.tabBarItem.badgeValue = unreadCount > 0 ? @(MIN(unreadCount, 99)).stringValue : nil;
        weakself.homeVC.unreadIMCount = unreadCount > 0 ? @(MIN(unreadCount, 99)).stringValue : nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:QWHomeVC_TableView_reload object:nil];
    });
    [EMRemindManager updateApplicationIconBadgeNumber:MIN(unreadCount, 99)];
}

#pragma mark - Private

- (void)_loadConversationTabBarItemBadge
{
    NSArray *conversations = [[EMClient sharedClient].chatManager getAllConversations];
    NSInteger unreadCount = 0;
    for (EMConversation *conversation in conversations) {
        if ([[[EMClient sharedClient].pushManager noPushUIds] containsObject:conversation.conversationId]) {//单聊免打扰会话
            continue;
        }
        if ([[[EMClient sharedClient].pushManager noPushGroups] containsObject:conversation.conversationId]) {//群聊免打扰会话
            continue;
        }
        unreadCount += conversation.unreadMessagesCount;
    }
    
    self.conversationsVC.tabBarItem.badgeValue = unreadCount > 0 ? @(MIN(unreadCount, 99)).stringValue : nil;
    self.homeVC.unreadIMCount = unreadCount > 0 ? @(MIN(unreadCount, 99)).stringValue : nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:QWHomeVC_TableView_reload object:nil];
    [EMRemindManager updateApplicationIconBadgeNumber:MIN(unreadCount, 99)];
}

@end
