//
//  XWLogUtil.h
//  assistant
//
//  Created by kevin on 2023/12/13.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

// 封装日志模块
typedef NS_ENUM(NSInteger, XWLogLevel) {
    XWLogLevelInfo,
    XWLogLevelError,
    XWLogLevelDebug
};

@interface XWLogUtil : NSObject

+ (void)log:(NSString *)message;

+ (void)log:(NSString *)message level:(XWLogLevel)level;

@end

NS_ASSUME_NONNULL_END
