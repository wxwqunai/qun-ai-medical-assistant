//
//  QWAppProfile.m
//  qwm
//
//  Created by kevin on 2023/3/26.
//

#import "QWAppProfile.h"
#import "QWTipsAlertController.h"

@implementation QWAppProfile
static QWAppProfile *_sharedInstance = nil;
static dispatch_once_t once_token = 0;
+(instancetype)sharedInstance {
    dispatch_once(&once_token, ^{
        if (_sharedInstance == nil) {
            _sharedInstance = [[super allocWithZone:NULL] init] ;
        }
    });
    return _sharedInstance;
}

#pragma mark -登录定时器
-(SWLoginTimeModel *)timeModel
{
    if (!_timeModel) {
        _timeModel =[[SWLoginTimeModel alloc]init];
    }
    return _timeModel;
}

#pragma mark - 是否已登录
- (BOOL)isLogined{
    if (!_qaUserInfo && IsStringEmpty(_qaUserInfo.phone) && IsStringEmpty(_qaUserInfo.password)) {
        return false;
    }
    return true;
}
- (BOOL)isLoginedAlert{
    if (![self isLogined]) {
        QWTipsAlertController *activityVC =[[QWTipsAlertController alloc]init];
        activityVC.tipType = TipsAlertType_loginNot;
        activityVC.modalPresentationStyle=UIModalPresentationOverFullScreen;
        [[QWCommonMethod Instance].getCurrentVC presentViewController:activityVC animated:NO completion:nil];
        return false;
    }
    return true;
}

#pragma mark - 保存／获取/修改/清除 用户信息 缓存
//-(QWqaUserModel *)qaUserInfo
//{
//    if (!_qaUserInfo) {
//        _qaUserInfo =[[QWqaUserModel alloc]init];
//    }
//    return _qaUserInfo;
//}
-(void)saveUserDefault:(QWqaUserModel *)userInfo
{
    [LKDBHelper clearTableData:[QWqaUserModel class]];
    [userInfo saveToDB];
}
-(QWqaUserModel *)GetUserInfo
{
    return [QWqaUserModel searchSingleWithWhere:nil orderBy:nil];
}
-(void)chageUserInfo:(QWqaUserModel *)userInfo
{
    [QWqaUserModel updateToDB:userInfo where:nil];
}
-(void)clearUserInfo
{
    if(_qaUserInfo){
        [LKDBHelper clearTableData:[QWqaUserModel class]];
        [LKDBHelper deleteToDB:_qaUserInfo];
        _qaUserInfo=nil;
    }
}

#pragma mark - 视频会议-保存/回去
-(void)saveVideoMeeting:(QWAgoraUserInfoModel *)bookingModel{
    [bookingModel saveToDB];
}
-(NSMutableArray *)GetBookingModelArr{
    NSMutableArray *arr = [QWAgoraUserInfoModel searchWithWhere:nil orderBy:@"rowid desc" offset:0 count:0];
    return arr;
}
-(void)chageBookingModel:(QWAgoraUserInfoModel *)bookingModel{
    BOOL isUpDate= [QWAgoraUserInfoModel updateToDB:bookingModel where:@{@"channelName":bookingModel.channelName}];
    NSLog(@"是否更新成功:%@",isUpDate>0?@"是":@"否");
}
-(void)deleteBookingModel:(QWAgoraUserInfoModel *)bookingModel{
    [[LKDBHelper getUsingLKDBHelper]  deleteWithClass:[QWAgoraUserInfoModel class] where:@{@"channelName":bookingModel.channelName}];
}
-(void)clearBookingModel{
    [LKDBHelper clearTableData:[QWAgoraUserInfoModel class]];
}


@end
