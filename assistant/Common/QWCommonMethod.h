//
//  QWCommonMethod.h
//  qwm
//
//  Created by kevin on 2023/3/26.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWCommonMethod : NSObject
+(instancetype)Instance;



#pragma mark -showAlert提示
-(void)ShowAlert:(NSString*)title Msg:(NSString*)msg;

#pragma mark - 当前版本号
- (NSString *)currentVersion;

#pragma mark - 获取当前屏幕显示的viewcontroller
- (UIViewController *)getCurrentVC;

#pragma mark- 时间转换格式
-(NSString *)changeDateToString:(NSDate *)date withFormatter:(NSString *)formatterStr;
#pragma mark - 当前时间戳
- (NSString *)nowTimeInterval;

#pragma mark- 计算当前时间在哪个节气间
-(NSString *)calculateThisYearSolarTerms;

#pragma mark- 计算当前时间在哪个时辰-修改时辰状态
-(NSString *)calculateOneOfTheTwelveHourPeriods;

#pragma mark - 跳转 外部浏览器/内部safari浏览器
- (void)openAppExternalBrowser:(NSString *)urlStr;

#pragma mark - 分享 - 链接/文件下载
- (void)sharedFromSystem:(NSString * __nullable)title
                 withUrl:(NSURL* __nullable)url
               withImage:(UIImage * __nullable)image
                 success:(void (^)(void))success
                 failure:(void (^)(void))failure;

#pragma mark - 判断字符串是否包含中文
- (BOOL)isHasChineseWithStr:(NSString *)strFrom;

#pragma mark- 跳转 手机app：软件
- (void)openSystemAppFile;
#pragma mark- 系统设置
- (void)openSystemSetting;

#pragma mark - 数据来源-请求判断
- (void)fetchDataGuideWay;

#pragma mark - 本地数据
- (id)fetchBundleFile:(NSString *)fileName withType:(NSString *)type;

- (UIView *)mas_HorizontalBisection:(NSMutableArray *)viewArr
                        withNum:(NSInteger)num
               withFixedSpacing:(CGFloat)fixedSpacing
                    leadSpacing:(CGFloat)leadSpacing
                        tailSpacing:(CGFloat)tailSpacing;

//alert
- (void)showAlert:(NSString *)title
         message:(NSString *)message
         success:(void (^ __nullable)(UIAlertAction *action))sucHandler
          failure:(void (^ __nullable)(UIAlertAction *action))failHandler;
+ (NSDictionary *)parameterWithURL:(NSString *)urlStr;

@end

NS_ASSUME_NONNULL_END
