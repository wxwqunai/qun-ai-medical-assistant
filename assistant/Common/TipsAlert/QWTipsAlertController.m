//
//  QWTipsAlertController.m
//  qwm
//
//  Created by kevin on 2023/3/28.
//

#import "QWTipsAlertController.h"
#import "QWLoginNotTipsView.h"
#import "QWLoginController.h"

@interface QWTipsAlertController ()<UIGestureRecognizerDelegate>
@property (nonatomic, strong) QWLoginNotTipsView *loginNotView;
@end

@implementation QWTipsAlertController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    
    if(self.tipType == TipsAlertType_loginNot){
        [self loginNotView];
    }
    
    if(self.tipType == TipsAlertType_checkCode){
        [self checkCodeView];
    }
    
    if(self.tipType == TipsAlertType_datePicker_date || self.tipType == TipsAlertType_datePicker_countDownTimer){
        [self addGestureRecognizer];
        [self datePickerView];
    }
}
#pragma mark - 登录提示
- (QWLoginNotTipsView *)loginNotView{
    if(!_loginNotView){
        _loginNotView = [[QWLoginNotTipsView alloc]init];
        __weak __typeof(self) weakSelf = self;
        _loginNotView.forkBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf dismissViewControllerAnimated:NO completion:^{
                QWLoginController *loginVC =[[QWLoginController alloc]init];
                [loginVC setHidesBottomBarWhenPushed:YES];
                [[[QWCommonMethod Instance] getCurrentVC].navigationController pushViewController:loginVC animated:YES];
            }];
        };
        [self.view addSubview:_loginNotView];
        
        [_loginNotView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.view);
            make.centerY.equalTo(self.view);
            make.width.equalTo(self.view).offset(-50);
        }];
    }
    return _loginNotView;
}

#pragma mark - 验证码
- (QWCheckCodeView *)checkCodeView{
    if(!_checkCodeView){
        _checkCodeView = [[QWCheckCodeView alloc]init];
        __weak __typeof(self) weakSelf = self;
        _checkCodeView.forkBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf dismissViewControllerAnimated:NO completion:nil];
        };
        [self.view addSubview:_checkCodeView];
        
        [_checkCodeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.view);
            make.centerY.equalTo(self.view);
            make.width.equalTo(self.view).offset(-50);
        }];
    }
    return _checkCodeView;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

#pragma mark - 日期选择
- (QWDatePickerView *)datePickerView{
    if(!_datePickerView){
        _datePickerView = [[QWDatePickerView alloc]init];
        _datePickerView.isShowDate = self.tipType == TipsAlertType_datePicker_date;
        [self.view addSubview:_datePickerView];
        [_datePickerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.view);
        }];
        
        __weak __typeof(self) weakSelf = self;
        _datePickerView.closeBlock = ^{
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf dismissViewControllerAnimated:NO completion:nil];
        };
    }
    return _datePickerView;
}




#pragma mark - 手势 -其他区域点击关闭
- (void)addGestureRecognizer{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeTap:)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
}

#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    UIView *touchView;
    if(self.tipType == TipsAlertType_loginNot) touchView = self.loginNotView;
    if(self.tipType == TipsAlertType_checkCode) touchView = self.checkCodeView;
    if(self.tipType == TipsAlertType_datePicker_date || self.tipType == TipsAlertType_datePicker_countDownTimer) touchView = self.datePickerView;
   
    if ([touch.view isDescendantOfView:touchView]) {
        return NO;
    }
    return YES;
}
#pragma mark - 手势 - 打开/关闭控制页面
- (void)closeTap:(UIGestureRecognizer *)ges{
    if (ges.state == UIGestureRecognizerStateEnded ) {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

@end
