//
//  QWDatePickerView.m
//  assistant
//
//  Created by qunai on 2024/1/11.
//

#import "QWDatePickerView.h"
@interface QWDatePickerView()
@property (nonatomic,strong) UIView *bgView;
@property (nonatomic,strong) UIView *topView;
@property (nonatomic,strong) UIButton *closeBtn;
@property (nonatomic,strong) UIButton *sureBtn;
@property (nonatomic,strong) UILabel *titleLabel;

@property (nonatomic,strong) UIView *dateShowView;
@property (nonatomic, strong) QWDateView *dateView;
@property (nonatomic, strong) QWCountDownTimerView *countDownTimerView;

@end
@implementation QWDatePickerView

- (instancetype)init
{
    self =[super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self bgView];
        [self topView];
        [self closeBtn];
        [self sureBtn];
        [self titleLabel];
        
        [self dateShowView];
        
    }
    return self;
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        [self addSubview:_bgView];
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.top.equalTo(self);
            make.bottom.equalTo(self).offset(-[UIDevice safeDistanceBottom]);
        }];
    }
    return _bgView;
}

#pragma mark - 关闭、标题、确定
- (UIView *)topView{
    if(!_topView){
        _topView = [[UIView alloc]init];
        [self.bgView addSubview:_topView];
        [_topView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView);
            make.right.equalTo(self.bgView);
            make.top.equalTo(self.bgView);
            make.height.mas_equalTo(40.0);
        }];
    }
    return _topView;
}

- (UIButton *)closeBtn{
    if(!_closeBtn){
        _closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeBtn setImage:[UIImage imageNamed:@"WCFork"] forState:UIControlStateNormal];
        [_closeBtn addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.topView addSubview:_closeBtn];
        [_closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.topView).offset(14.0);
            make.top.equalTo(self.topView);
            make.bottom.equalTo(self.topView);
            make.width.mas_equalTo(40);
        }];
    }
    return _closeBtn;
}

- (UIButton *)sureBtn{
    if(!_sureBtn){
        _sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_sureBtn setImage:[UIImage imageNamed:@"WCTick"] forState:UIControlStateNormal];
        [_sureBtn addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.topView addSubview:_sureBtn];
        [_sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.topView).offset(-14.0);
            make.top.equalTo(self.topView);
            make.bottom.equalTo(self.topView);
            make.width.mas_equalTo(40);
        }];
    }
    return _sureBtn;
}

- (void)buttonClick:(UIButton *)sender{
    if(self.closeBlock)self.closeBlock();

    NSDate *date = self.isShowDate?self.dateView.datePicker.date:self.countDownTimerView.datePicker.date;
    if(self.sureBlock)self.sureBlock(date);
}

#pragma mark - 时间选择
- (void)setIsShowDate:(BOOL)isShowDate{
    _isShowDate = isShowDate;
    if(_isShowDate){
        [self dateView];
    }else{
        [self countDownTimerView];
    }
}
- (QWDateView *)dateView{
    if(!_dateView){
        _dateView = [[QWDateView alloc]init];
        [self.bgView addSubview:_dateView];
        
        [_dateView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView);
            make.right.equalTo(self.bgView);
            make.top.equalTo(self.topView.mas_bottom);
            make.bottom.equalTo(self.bgView);
        }];
    }
    return _dateView;
}

- (QWCountDownTimerView *)countDownTimerView{
    if(!_countDownTimerView){
        _countDownTimerView = [[QWCountDownTimerView alloc]init];
        [self.bgView addSubview:_countDownTimerView];
        
        [_countDownTimerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView);
            make.right.equalTo(self.bgView);
            make.top.equalTo(self.topView.mas_bottom);
            make.bottom.equalTo(self.bgView);
        }];
    }
    return _countDownTimerView;
}

@end


@implementation QWDateView
- (instancetype)init{
    self = [super init];
    if(self){
        [self datePicker];

    }
    return self;
}

#pragma mark - 时间选择
- (UIDatePicker *)datePicker{
    if(!_datePicker){
        _datePicker = [[UIDatePicker alloc]init];
        _datePicker.datePickerMode = UIDatePickerModeDateAndTime;
        _datePicker.date = [NSDate date];
        _datePicker.minimumDate = [NSDate date];
        if (@available(iOS 13.4, *)){
            _datePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];//新发现这里不会根据系统的语言变了
            _datePicker.preferredDatePickerStyle = UIDatePickerStyleWheels;
        }
        [self addSubview:_datePicker];
        [_datePicker mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return _datePicker;
}

@end

@implementation QWCountDownTimerView
- (instancetype)init{
    self = [super init];
    if(self){
        [self datePicker];

    }
    return self;
}

#pragma mark - 时间选择
- (UIDatePicker *)datePicker{
    if(!_datePicker){
        _datePicker = [[UIDatePicker alloc]init];
        _datePicker.datePickerMode = UIDatePickerModeCountDownTimer;
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        formatter.dateFormat = @"HH:mm";
        _datePicker.date = [formatter dateFromString:@"00:30"];
        if (@available(iOS 13.4, *)){
            _datePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];//新发现这里不会根据系统的语言变了
            _datePicker.preferredDatePickerStyle = UIDatePickerStyleWheels;
        }
        [self addSubview:_datePicker];
        [_datePicker mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return _datePicker;
}

@end
