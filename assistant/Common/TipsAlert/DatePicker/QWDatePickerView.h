//
//  QWDatePickerView.h
//  assistant
//
//  Created by qunai on 2024/1/11.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWDatePickerView : UIView
@property (nonatomic,copy) void (^sureBlock)(NSDate *date);
@property (nonatomic,copy) void (^closeBlock)(void);

@property (nonatomic, assign) BOOL isShowDate;
@end


@interface QWDateView : UIView
@property (nonatomic,strong) UIDatePicker *datePicker;
@end

@interface QWCountDownTimerView : UIView
@property (nonatomic,strong) UIDatePicker *datePicker;
@end


NS_ASSUME_NONNULL_END

 
