//
//  QWTipsAlertController.h
//  qwm
//
//  Created by kevin on 2023/3/28.
//

#import <UIKit/UIKit.h>
#import "QWCheckCodeView.h"
#import "QWDatePickerView.h"

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger, TipsAlertType)
{
    
    TipsAlertType_defalut = 0,
    TipsAlertType_loginNot,   //未登录
    TipsAlertType_checkCode,  //检查验证码
    TipsAlertType_datePicker_date,  //日期选择
    TipsAlertType_datePicker_countDownTimer
};

@interface QWTipsAlertController : UIViewController

@property (nonatomic, assign) TipsAlertType tipType;

@property (nonatomic, strong) QWCheckCodeView *checkCodeView;
@property (nonatomic, strong) QWDatePickerView *datePickerView;

@end

NS_ASSUME_NONNULL_END
