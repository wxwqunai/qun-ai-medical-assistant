//
//  QWLoginNotTipsView.m
//  qwm
//
//  Created by kevin on 2023/3/28.
//

#import "QWLoginNotTipsView.h"

@interface QWLoginNotTipsView()
@property (nonatomic, strong) UILabel *tishiLabel;
@property (nonatomic, strong) UIButton *forkButtun;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIButton *sureButtun;
@end

@implementation QWLoginNotTipsView

- (instancetype)init
{
    self =[super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.layer.cornerRadius = 6;
        self.layer.masksToBounds = YES;
        
        [self tishiLabel];
        [self forkButtun];
        
        [self contentLabel];
        [self sureButtun];
        
    }
    return self;
}

- (UILabel *)tishiLabel{
    if(!_tishiLabel){
        _tishiLabel = [[UILabel alloc]init];
        _tishiLabel.text =@"提示";
        _tishiLabel.font =[UIFont systemFontOfSize:20];
        
        [self addSubview:_tishiLabel];
        
        [_tishiLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(20);
            make.left.mas_equalTo(20);
        }];
    }
    return _tishiLabel;
}

-(UIButton *)forkButtun
{
    if (!_forkButtun) {
        _forkButtun =[UIButton buttonWithType:UIButtonTypeCustom];
        [_forkButtun setImage:[UIImage imageNamed:@"WCFork.png"] forState:UIControlStateNormal];
        [_forkButtun addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_forkButtun];
        
        [_forkButtun mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_tishiLabel);
            make.right.mas_equalTo(0);
            make.width.mas_equalTo(50);
            make.height.mas_equalTo(50);
        }];
    }
    return _forkButtun;
}
-(void)buttonClick:(UIButton *)sender
{
    if(self.forkBlock){
        self.forkBlock();
    }
}


- (UILabel *)contentLabel{
    if(!_contentLabel){
        _contentLabel = [[UILabel alloc]init];
        _contentLabel.text =@"您需要登录后才可以使用该功能。";
        _contentLabel.numberOfLines = 0;
        _contentLabel.textColor =[UIColor colorFromHexString:@"#A2A2A2"];
        _contentLabel.font =[UIFont systemFontOfSize:18];
        
        [self addSubview:_contentLabel];
        
        [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_tishiLabel.mas_bottom).offset(20);
            make.left.mas_equalTo(20);
            make.right.mas_equalTo(-20);
        }];
    }
    return _contentLabel;
}

- (UIButton *)sureButtun
{
    if (!_sureButtun) {
        _sureButtun =[UIButton buttonWithType:UIButtonTypeCustom];
        [_sureButtun setTitle:@"确定" forState:UIControlStateNormal];
        [_sureButtun setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _sureButtun.titleLabel.font = [UIFont systemFontOfSize:20];
        _sureButtun.backgroundColor = Color_Main_Green;
        [_sureButtun addTarget:self action:@selector(sureButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        _sureButtun.layer.cornerRadius = 6;
        _sureButtun.layer.masksToBounds = YES;
        
        [self addSubview:_sureButtun];
        
        [_sureButtun mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(20);
            make.right.equalTo(self).offset(-20);
            make.top.equalTo(_contentLabel.mas_bottom).offset(40);
            make.height.mas_equalTo(55);
            make.bottom.mas_equalTo(-20);
        }];
    }
    return _sureButtun;
}
-(void)sureButtonClick:(UIButton *)sender
{
    if(self.forkBlock) self.forkBlock();
}

@end
