//
//  QWCommonMethod.m
//  qwm
//
//  Created by kevin on 2023/3/26.
//

#import "QWCommonMethod.h"
#import "QWMainTabBarController.h"
#import "QWCLWrapper.h"
#import "JieqiDataModel.h"
#import <SafariServices/SafariServices.h>

@implementation QWCommonMethod
static QWCommonMethod *_sharedInstance = nil;
static dispatch_once_t once_token = 0;
+(instancetype)Instance {
    dispatch_once(&once_token, ^{
        if (_sharedInstance == nil) {
            _sharedInstance = [[super allocWithZone:NULL] init] ;
        }
    });
    return _sharedInstance;
}


#pragma mark -showAlert提示
-(void)ShowAlert:(NSString*)title Msg:(NSString*)msg
{
    UIAlertView* alert=[[UIAlertView alloc] initWithTitle:title
                                                  message:msg
                                                 delegate:self
                                        cancelButtonTitle:@"确定"
                                        otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - 当前版本号
- (NSString *)currentVersion{
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    return version;
}

#pragma mark - 获取当前屏幕显示的viewcontroller
//- (UIViewController *)getCurrentVC {
//    UIViewController *result = nil;
//
//    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
//    if (window.windowLevel != UIWindowLevelNormal) {
//        NSArray *windows = [[UIApplication sharedApplication] windows];
//        for(UIWindow * tmpWin in windows) {
//            if (tmpWin.windowLevel == UIWindowLevelNormal) {
//                window = tmpWin;
//                break;
//            }
//        }
//    }
//
//    if ([window subviews].count ==0 )return nil;
//    UIView *frontView = [[window subviews] objectAtIndex:0];
//    id nextResponder = [frontView nextResponder];
//
//    if ([nextResponder isKindOfClass:[UIViewController class]]) {
//        result = nextResponder;
//    } else {
//        result = window.rootViewController;
//    }
//
//    /*
//     *  在此判断返回的视图是不是你的根视图--我的根视图是tabbar
//     */
//    if ([result isKindOfClass:[QWMainTabBarController class]]) {
//        QWMainTabBarController *mainTabBarVC = (QWMainTabBarController *)result;
//        result = [mainTabBarVC selectedViewController];
//        result = [result.childViewControllers lastObject];
//    }
//
//    return result;
//}

// 获取当前显示的 UIViewController
- (UIViewController *)getCurrentVC {
    //获得当前活动窗口的根视图
    UIViewController *vc = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIViewController *currentShowingVC = [self findCurrentShowingViewControllerFrom:vc];
    return currentShowingVC;
}
- (UIViewController *)findCurrentShowingViewControllerFrom:(UIViewController *)vc
{
    // 递归方法 Recursive method
    UIViewController *currentShowingVC;
    if ([vc presentedViewController]) {
        // 当前视图是被presented出来的
        UIViewController *nextRootVC = [vc presentedViewController];
        currentShowingVC = [self findCurrentShowingViewControllerFrom:nextRootVC];
 
    } else if ([vc isKindOfClass:[UITabBarController class]]) {
        // 根视图为UITabBarController
        UIViewController *nextRootVC = [(UITabBarController *)vc selectedViewController];
        currentShowingVC = [self findCurrentShowingViewControllerFrom:nextRootVC];
 
    } else if ([vc isKindOfClass:[UINavigationController class]]){
        // 根视图为UINavigationController
        UIViewController *nextRootVC = [(UINavigationController *)vc visibleViewController];
        currentShowingVC = [self findCurrentShowingViewControllerFrom:nextRootVC];
 
    } else {
        // 根视图为非导航类
        currentShowingVC = vc;
    }
 
    return currentShowingVC;
}

#pragma mark- 时间转换格式
-(NSString *)changeDateToString:(NSDate *)date withFormatter:(NSString *)formatterStr
{
    NSString *timeStr =@"";
    if (date) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = formatterStr;
        timeStr = [dateFormatter stringFromDate:date];
    }
    return timeStr;
}

#pragma mark - 当前时间戳
- (NSString *)nowTimeInterval {
    // 现在的时间戳
    
    // 获取当前时间0秒后的时间
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0];
    // *1000 是精确到毫秒，不乘就是精确到秒
    NSTimeInterval time = [date timeIntervalSince1970]*1000;
    NSString *timeStr = [NSString stringWithFormat:@"%.0f", time];
    return timeStr;
}

#pragma mark- 计算当前时间在哪个节气间
-(NSString *)calculateThisYearSolarTerms
{
    NSDate *nowDate=[NSDate date];
    NSString *year =[self changeDateToString:nowDate withFormatter:@"YYYY"];
    NSMutableArray *arr = [CLWrapper calculateAllJieqiByYear:[year intValue] fromJieqiId:0]; //21 是立春，详见 calendartool.h 中的定义
    
    JieqiDataModel* currentjdModel;
    for (int i=0; i<arr.count; i++) {
        JieqiDataModel* jdModel=arr[i];
        NSDate *laterDate = [jdModel.jieqiDate laterDate:nowDate];
        //当前时间处于两个节气之间
        if ([laterDate isEqualToDate:jdModel.jieqiDate]) {
            if (i==0) {
                currentjdModel=arr[arr.count-1];
                currentjdModel.numOfAllJieqi=arr.count;
            }else
            {
                currentjdModel=arr[i-1];
                currentjdModel.numOfAllJieqi=i;
            }
            return currentjdModel.jieqiName;
        }
    }
    return nil;
}

#pragma mark- 计算当前时间在哪个时辰-修改时辰状态
-(NSString *)calculateOneOfTheTwelveHourPeriods
{
    NSInteger hourNum =[[self changeDateToString:[NSDate date] withFormatter:@"H"] integerValue];
    
    NSInteger currentHourNum;
    if (hourNum%2==0) {
        currentHourNum=hourNum/2;
    }else
    {
        currentHourNum=hourNum/2+1;
    }
    if (currentHourNum>=12) {
        currentHourNum=0;
    }
    
    NSArray *arr =[[NSArray alloc]initWithObjects:
                            @"子时",@"丑时",@"寅时",@"卯时",
                            @"辰时",@"巳时",@"午时",@"未时",
                            @"申时",@"酉时",@"戌时",@"亥时",nil];
    NSString *showStr;
    if (currentHourNum<arr.count) {
        showStr=arr[currentHourNum];
    }
    return showStr;
}

#pragma mark- 跳转 外部浏览器/内部safari浏览器
- (void)openAppExternalBrowser:(NSString *)urlStr {
    NSURL *url = [NSURL URLWithString:urlStr];
    if (url && [[UIApplication sharedApplication] canOpenURL:url]) {
//        SFSafariViewController *sf = [[SFSafariViewController alloc] initWithURL:url];
//        UIViewController *vc = [self getCurrentVC];
//        if (vc) {
//            [vc presentViewController:sf animated:YES completion:nil];
//        }
        
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
    }
}

#pragma mark - 分享 - 链接/文件下载
- (void)sharedFromSystem:(NSString * __nullable)title
                 withUrl:(NSURL* __nullable)url
               withImage:(UIImage * __nullable)image
                 success:(void (^)(void))success
                 failure:(void (^)(void))failure
{
    //在这里呢 如果想分享图片 就把图片添加进去  文字什么的通上
    NSMutableArray *activityItems = [[NSMutableArray alloc]init];
    if(!IsStringEmpty(title)) [activityItems addObject:title]; //分享的标题
    if(image) [activityItems addObject:image];//分享的图片
    if(url) [activityItems addObject:url];//分享的url
    
    if(activityItems.count == 0) if(failure)failure();
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc]initWithActivityItems:activityItems applicationActivities:nil];
     //不出现在活动项目
   activityVC.excludedActivityTypes = @[
        UIActivityTypePostToFacebook,
        UIActivityTypePostToTwitter,
        UIActivityTypePostToWeibo,
        UIActivityTypeMessage,
        UIActivityTypeMail,
        UIActivityTypePrint,
        UIActivityTypeCopyToPasteboard,
        UIActivityTypeAssignToContact,
        UIActivityTypeSaveToCameraRoll,
        UIActivityTypeAddToReadingList,
        UIActivityTypePostToFlickr,
        UIActivityTypePostToVimeo,
        UIActivityTypePostToTencentWeibo,
//        UIActivityTypeAirDrop,
        UIActivityTypeOpenInIBooks,
        UIActivityTypeMarkupAsPDF,
//             UIActivityTypeSharePlay,
//             UIActivityTypeCollaborationInviteWithLink,
//             UIActivityTypeCollaborationCopyLink,
   ];
   [[self getCurrentVC] presentViewController:activityVC animated:YES completion:nil];
    // 分享之后的回调
       activityVC.completionWithItemsHandler = ^(UIActivityType  _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
           if (completed) {
               //分享 成功
               if(success)success();
           } else  {
               //分享 取消
               if(failure)failure();
           }
       };
}

#pragma mark - 判断字符串是否包含中文
- (BOOL)isHasChineseWithStr:(NSString *)strFrom {
    for (int i=0; i<strFrom.length; i++) {
        NSRange range =NSMakeRange(i, 1);
        NSString * strFromSubStr=[strFrom substringWithRange:range];
        const char *cStringFromstr = [strFromSubStr UTF8String];
        if (strlen(cStringFromstr)==3) {
            //汉字
            return YES;
        } else if (strlen(cStringFromstr)==1) {
            //字母
        }

    }

    return NO;
}

#pragma mark- 跳转 手机app：软件
- (void)openSystemAppFile{
    [self openSystemUrl:@"shareddocuments://"];
}

#pragma mark- 系统设置
- (void)openSystemSetting{
    [self openSystemUrl:UIApplicationOpenSettingsURLString];
}

- (void)openSystemUrl:(NSString *)sysUrlStr{
    NSURL * url = [NSURL URLWithString:sysUrlStr];
    if([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
    }
}

#pragma mark - 数据来源-请求判断
- (void)fetchDataGuideWay{
    HXGetRequest * request = [[HXGetRequest alloc] initWithRequestUrl:URL_myjson_dev_config argument:@{}];
    [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
        NSLog(@"");
        NSArray *dataArr = (NSArray *)result;
        [[NSUserDefaults standardUserDefaults] setObject:dataArr forKey:NSUserDefaults_myjson_dev_config];
    } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
        //错误
        NSLog(@"%@",errorInfo);
    }];
}

#pragma mark - 本地数据
- (id)fetchBundleFile:(NSString *)fileName withType:(NSString *)type{
    NSString * filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:type];
    if(filePath){
        NSData *data = [NSData dataWithContentsOfFile:filePath];//获取指定路径的data文件
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        return dic;
    }
    return @{};
}

- (UIView *)mas_HorizontalBisection:(NSMutableArray *)viewArr
                        withNum:(NSInteger)num
               withFixedSpacing:(CGFloat)fixedSpacing
                    leadSpacing:(CGFloat)leadSpacing
                    tailSpacing:(CGFloat)tailSpacing{
    UIView *bgView = [[UIView alloc]init];
    //只有一个
    if(num ==1 && viewArr.count ==1){
        UIView *itemView = viewArr[0];
        [bgView addSubview:itemView];
        [itemView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(bgView).offset(leadSpacing);
            make.right.equalTo(bgView).offset(-tailSpacing);
            make.top.equalTo(bgView);
            make.bottom.equalTo(bgView);
        }];
        return bgView;
    }
    
    //将要展示的View 添加到数组内
    NSMutableArray *tolAry = [NSMutableArray new];
    for (UIView *itemView in viewArr) {
        [bgView addSubview:itemView];
        [tolAry addObject:itemView];
    }
    
    //补充数组，保证可以num等分
    if(viewArr.count%num>0){
        for (int i = 0; i< (num-viewArr.count%num); i ++) {
            UIView *emptyView = [UIView new];
            [bgView addSubview:emptyView];
            [tolAry addObject:emptyView];
        }
    }
  
    //manory方法
    [tolAry enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if(idx%num == 0){
            NSArray *rangeArr = [tolAry subarrayWithRange:NSMakeRange(idx, num)];
            //平方向控件间隔固定等间隔
            [rangeArr mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:fixedSpacing leadSpacing:leadSpacing tailSpacing:tailSpacing];
            [rangeArr mas_makeConstraints:^(MASConstraintMaker *make) {
                if(idx == 0){//第一行第一个
                    make.top.equalTo(bgView);
                }else{//每一行第一个
                    make.top.equalTo(((UIView *)tolAry[idx-num]).mas_bottom).offset(fixedSpacing);
                }
                //最后一行最后一个
                if(idx+num == tolAry.count) make.bottom.equalTo(bgView);
                make.height.mas_greaterThanOrEqualTo(10);
            }];
        }
    }];
    return bgView;
}

#pragma mark - alert
- (void)showAlert:(NSString *)title
          message:(NSString *)message
          success:(void (^ __nullable)(UIAlertAction *action))sucHandler
          failure:(void (^ __nullable)(UIAlertAction *action))failHandler{
    [self showAlert:title message:message defAction:@"" canAction:@"" success:sucHandler failure:failHandler];
}
- (void)showAlert:(NSString *)title
          message:(NSString *)message
        defAction:(NSString  *)defActTitle
        canAction:(NSString  *)canActTitle
          success:(void (^ __nullable)(UIAlertAction *action))sucHandler
          failure:(void (^ __nullable)(UIAlertAction *action))failHandler{
    
    defActTitle = IsStringEmpty(defActTitle)?@"确定":defActTitle;
    canActTitle = IsStringEmpty(canActTitle)?@"取消":canActTitle;
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:defActTitle style:UIAlertActionStyleDefault handler:sucHandler];
//    [action setValue:[UIColor blackColor] forKey:@"_titleTextColor"];
    [alertController addAction:action];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:canActTitle style: UIAlertActionStyleCancel handler:failHandler];
    [cancelAction  setValue:[UIColor redColor] forKey:@"_titleTextColor"];
    [alertController addAction:cancelAction];
    alertController.modalPresentationStyle = 0;
    [[self getCurrentVC] presentViewController:alertController animated:YES completion:nil];
}

/** 解析参数 */
+ (NSDictionary *)parameterWithURL:(NSString *)urlStr {
    
    NSArray *arr = [urlStr componentsSeparatedByString:@"?"];
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [arr enumerateObjectsUsingBlock:^(NSString *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if(idx == 1){
            NSArray *keyValues = [obj componentsSeparatedByString:@"&"];
            [keyValues enumerateObjectsUsingBlock:^(NSString *   _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSArray *arr = [obj componentsSeparatedByString:@"="];
                if(arr.count == 2) [dic setValue:arr[1] forKey:arr[0]];
            }];
        }
    }];
    return dic;
}

@end
