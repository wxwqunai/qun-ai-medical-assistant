//
//  QWUrls.h
//  qwm
//
//  Created by kevin on 2023/3/23.
//

#ifndef QWUrls_h
#define QWUrls_h



#ifdef DEBUG//测试环境
#define QWM_DOMAIN  @"gaiwo.belovedlive.com"
#define QWM_BASE_API_URL @"http://gaiwo.belovedlive.com:8098"
//#define QWM_DOMAIN  @"124.115.217.70"
//#define QWM_BASE_API_URL @"http://124.115.217.70:8098"
#define EaseIMAppKey_PROD  @"1145210608051818#medical-doctor" //环信key
//#define EaseIMAppKey_PROD  @"1133230426161487#coffeeinfo" //环信key

//MobPush消息推送
#define MOBAppKey @"390fcc7593c7c"
#define MOBAppSecret @"15c860235bbaf1a691b83f98ef0322ae"

#else//正式环境
#define QWM_DOMAIN  @"gaiwo.belovedlive.com"
#define QWM_BASE_API_URL @"http://gaiwo.belovedlive.com:8098"
#define EaseIMAppKey_PROD  @"1145210608051818#medical-doctor" //环信key
//#define EaseIMAppKey_PROD  @"1133230426161487#coffeeinfo" //环信key

//MobPush消息推送
#define MOBAppKey @"390fcc7593c7c"
#define MOBAppSecret @"15c860235bbaf1a691b83f98ef0322ae"

#endif

#define Bugly_ID  @"e5bcdce345" //腾讯bugly，崩溃日志记录

#define URL_Error @"网络异常，请重试"

/**
 *app第一个请求接口
 *所有其他接口请求前，要跟该接口数据对比，获取真正要请求的链接
 *
 */
#define URL_myjson_dev_config @"boke/resfile/debugReq/dev_config.json"
//app第一个请求数据缓存
#define NSUserDefaults_myjson_dev_config @"NSUserDefaults_myjson_dev_config"

/**登录**/
#define URL_login @"/boke/applogin_mlogin.do" //登录
#define URL_register @"/bokehall/user/submitRegister" //注册
#define URL_SEND_OTP @"/bokehall/user/sendmessage" //获取验证码
#define URL_RESET_PWD @"/bokehall/user/retrievePassword" //重置密码

/**首页**/
#define URL_DOCTOR_LIST @"/bokehall/all/doctorList" //医生列表
#define URL_BANNER @"/boke/resfile/banner_config.json" //banner
#define URL_DOCTOR_INFO QWM_BASE_API_URL@"/boke/user_findone.do" //医生详情
#define URL_GetUser_INFO @"/bokehall/user/getUser" //医生详情
#define URL_my_addAttention @"/bokehall/my/addAttention" //关注医生
#define URL_Home_Notice @"boke/resfile/debugReq/index_notice.json" //首页跑马灯

//医患管理
#define URL_Doctorpatient_Save @"/bokehall/doctorpatient/save" //医生详情-咨询/视频通话
#define URL_Doctorpatient @"/bokehall/doctorpatient" //医生/患者信息

/**智能诊断**/
#define URL_aianswer_start @"bokehall/aianswer/save" //开始诊断
#define URL_aitimu_list @"bokehall/aitimu/list" // 获取题目和选项-所有
#define URL_aitimu_get @"bokehall/aitimu/get" // 获取题目和选项-单个
#define URL_aianswerdetail_save @"bokehall/aianswerdetail/save" //提交答案
#define URL_aianswer_LIST @"bokehall/aianswer/list" //诊断历史中查看结果
#define URL_AiDiagnosticRules_getConclusion @"bokehall/AiDiagnosticRules/getConclusion" //获取机器诊断和专家专家的结论
#define URL_aianswerdetail_getOneUserPaper @"bokehall/aianswerdetail/getOneUserPaper"
//#define URL_aianswerdetail_list @"bokehall/aianswerdetail/list" //医生点击“查看答卷”
//#define URL_aianswer_update_rengongjianyi @"bokehall/aianswer/update_rengongjianyi" //医生点击“给出建议”
#define URL_AiDiagnosticRules_LIST @"/bokehall/AiDiagnosticRules/list" //查询所有诊断规则


/**资讯**/
#define URL_Health_List @"/bokehall/health/healthlist"
#define URL_Health_Detail @"/bokehall/health/detail" //资讯-详情-无用
#define URL_Resfile_Openqa @"/boke/resfile/openqa.json"
/**消息**/

/**我的**/
#define URL_FEEDBACK @"/boke/user_report.do" //后续反馈
#define URL_UPLOAD_IMAGE @"/bokehall/my/uploadImage" //头像上传
#define URL_UPDATE @"/bokehall/my/updateOne" //更改个人信息
#define URL_MENUS_ON_ME_PAGE @"/boke/resfile/meMenuAllConfig.json" //我的-菜单
#define URL_learning_meet QWM_BASE_API_URL@"/bokehall/learning/meet" //我的-学术预约-会议预告
#define URL_learning_findByDay @"/bokehall/learning/findByDay" //我的-学术预约-日历某一天
#define URL_learningSchedule_findAll @"/bokehall/learningSchedule/findAll" //学术预约-获取全部日历信息

/**分享**/
#define URL_share_config @"boke/resfile/debugReq/share_config.json"

/**通知消息**/
#define URL_updateByloginName @"/bokehall/user/updateByloginName" //通知传id

/*
 *聚合数据-https://www.juhe.cn/
 */
#define URL_JUHE_Healthtip @"http://apis.juhe.cn/fapigx/healthtip/query?key=e37b0428981b4c55edab6ae3a769b413" //健康小提示

//隐私协议
#define URL_Login_PrivacyService QWM_BASE_API_URL@"/boke/resfile/privacyServiceTermsBBS.html"
//服务协议
#define URL_Login_UserAgreement QWM_BASE_API_URL@"/boke/resfile/userAgreementBBS.html"


//保存医生列表
#define NSUserDefaults_doctorlist_keep_15 @"NSUserDefaults_doctorlist_keepFifteen"


#endif /* QWUrls_h */
