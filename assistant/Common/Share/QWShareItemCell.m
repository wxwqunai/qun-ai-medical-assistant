//
//  QWShareItemCell.m
//  assistant
//
//  Created by qunai on 2024/1/4.
//

#import "QWShareItemCell.h"

@implementation QWShareItemCell
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self logoImageView];
        [self nameLabel];
    }
    return self;
}

- (UIImageView *)logoImageView{
    if(!_logoImageView){
        _logoImageView = [[UIImageView alloc]init];
        _logoImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_logoImageView];
        [_logoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView);
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.height.mas_equalTo(13*4.0);
        }];
    }
    return _logoImageView;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont systemFontOfSize:13];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.top.equalTo(self.logoImageView.mas_bottom);
            make.bottom.equalTo(self.contentView);
        }];
    }
    return _nameLabel;
}

@end
