//
//  QWShareHomeController.m
//  assistant
//
//  Created by qunai on 2024/1/3.
//

#import "QWShareHomeController.h"
#import "QWShareItemCell.h"
#import "XWQRCodeGenerator.h"

@interface QWShareHomeController ()<UICollectionViewDelegate, UICollectionViewDataSource,UIGestureRecognizerDelegate>
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIButton *cancleBtn;
@property (strong, nonatomic) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *showArr;
@end

@implementation QWShareHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openMeetOptionsTap:)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];

    [self bgView];
    [self cancleBtn];
    [self collectionView];
    [self loadData];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self show];
    });
}

- (UIView *)bgView{
    if(!_bgView){
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view).offset(8.0);
            make.right.equalTo(self.view).offset(-8.0);
            make.top.equalTo(self.view.mas_bottom);
        }];
    }
    return _bgView;
}
#pragma mark - 分享到
- (UIButton *)cancleBtn{
    if(!_cancleBtn){
        _cancleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cancleBtn setTitle:@"取消" forState:UIControlStateNormal];
        [_cancleBtn setTitleColor:[UIColor skyBlueColor] forState:UIControlStateNormal];
        [_cancleBtn addTarget:self action:@selector(cancleBtnClick) forControlEvents:UIControlEventTouchUpInside];
        _cancleBtn.backgroundColor = [UIColor whiteColor];
        _cancleBtn.layer.cornerRadius = 6.0;
        _cancleBtn.layer.masksToBounds = YES;
        [self.bgView addSubview:_cancleBtn];
        
        [_cancleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView);
            make.right.equalTo(self.bgView);
            make.bottom.equalTo(self.bgView).offset([UIDevice safeDistanceBottom]>0?-[UIDevice safeDistanceBottom]:-10);
            make.height.mas_equalTo(40.0);
        }];
    }
    return _cancleBtn;
}
- (void)cancleBtnClick{
    [self close];
}

#pragma mark - 对集合视图进行布局
-(UICollectionView *)collectionView
{
    if (!_collectionView) {
        
        // 创建集合视图布局对象
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        // 设置最小的左右间距
        flowLayout.minimumInteritemSpacing = 0.0;
        // 设置最小的行间距（上下）
        flowLayout.minimumLineSpacing = 0.0;
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        // 设置边界范围
        flowLayout.sectionInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
        flowLayout.headerReferenceSize =CGSizeMake(0.0, 20.0);
        flowLayout.footerReferenceSize =CGSizeMake(0.0, 10.0);
        
        _collectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) collectionViewLayout:flowLayout];
        _collectionView.showsVerticalScrollIndicator=NO;
        _collectionView.showsHorizontalScrollIndicator=NO;
        _collectionView.backgroundColor=[UIColor whiteColor];
        
        [self.bgView addSubview:_collectionView];
        
        // 创建集合视图对象
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.alwaysBounceVertical=YES;
        _collectionView.alwaysBounceHorizontal=NO;
        _collectionView.scrollEnabled = NO;
        
        _collectionView.layer.cornerRadius = 6.0;
        _collectionView.layer.masksToBounds = YES;
        
        // 注册
        [_collectionView registerClass:[QWShareItemCell class] forCellWithReuseIdentifier:@"QWShareItemCell"];
        [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView);
            make.right.equalTo(self.bgView);
            make.top.equalTo(self.bgView);
            make.bottom.equalTo(self.cancleBtn.mas_top).offset(-10);
            make.height.mas_equalTo(kScreenWidth/5.0*2.5+40);
        }];
    }
    return _collectionView;
}
#pragma mark - UICollectionViewDelegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize showSize=CGSizeMake(CGRectGetWidth(collectionView.frame)/5.0, (CGRectGetHeight(collectionView.frame)-30)/2.0);
    return showSize;
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.showArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    QWShareItemCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"QWShareItemCell" forIndexPath:indexPath];
    NSDictionary *dic = self.showArr[indexPath.row];
    cell.logoImageView.image = [UIImage imageNamed:dic[@"logo"]];
    cell.nameLabel.text = dic[@"name"];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = self.showArr[indexPath.row];
    NSString *shareId = dic[@"shareId"];
    if([shareId isEqualToString:@"shareID_001"]){
        [self shareWeiXin];
        return;
    }else if([shareId isEqualToString:@"shareID_006"]){
        [self linkCopy];
    }else if([shareId isEqualToString:@"shareID_007"]){
        [self shareQRCode];
        return;
    }else{
        [SWHUDUtil hideHudViewWithMessageSuperView:self.view withMessage:@"开发中，敬请期待"];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self close];
    });
}

- (void)loadData{
    NSArray *arr = @[
        @{@"shareId":@"shareID_001",@"logo":@"share_weixin",@"name":@"微信好友"},
    @{@"shareId":@"shareID_002",@"logo":@"share_pengyou",@"name":@"朋友圈"},
    @{@"shareId":@"shareID_003",@"logo":@"share_qq",@"name":@"QQ"},
    @{@"shareId":@"shareID_004",@"logo":@"share_qqkongjian",@"name":@"QQ空间"},
    @{@"shareId":@"shareID_005",@"logo":@"share_weibo",@"name":@"新浪微博"},
    @{@"shareId":@"shareID_006",@"logo":@"share_copyLink",@"name":@"复制链接"},
    @{@"shareId":@"shareID_007",@"logo":@"share_qrcode",@"name":@"二维码"}];
    self.showArr = [[NSMutableArray alloc]initWithArray:arr];
    [self.collectionView reloadData];
}

#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isDescendantOfView:self.bgView]) {
        return NO;
    }
    return YES;
}
#pragma mark - 手势 - 打开/关闭控制页面
- (void)openMeetOptionsTap:(UIGestureRecognizer *)ges{
    if (ges.state == UIGestureRecognizerStateEnded ) {
        [self close];
    }
}

#pragma mark - 动画弹出
- (void)show{
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(8.0);
        make.right.equalTo(self.view).offset(-8.0);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
    // 告诉self.view约束需要更新
    [self.view setNeedsUpdateConstraints];
    // 调用此方法告诉self.view检测是否需要更新约束，若需要则更新，下面添加动画效果才起作用
    [self.view updateConstraintsIfNeeded];
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}
#pragma mark - 动画关闭
- (void)close{
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(8.0);
        make.right.equalTo(self.view).offset(-8.0);
        make.top.equalTo(self.view.mas_bottom);
    }];
    
    // 告诉self.view约束需要更新
    [self.view setNeedsUpdateConstraints];
    // 调用此方法告诉self.view检测是否需要更新约束，若需要则更新，下面添加动画效果才起作用
    [self.view updateConstraintsIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:NO completion:nil];
    }];
}

#pragma mark - 微信分享
- (void)shareWeiXin{
    [self dismissViewControllerAnimated:NO completion:^{
        [[QWCommonMethod Instance] sharedFromSystem:self.shareTitle withUrl:[NSURL URLWithString:self.shareUrl] withImage:[UIImage imageNamed:@"logo"] success:^{
        } failure:^{
        }];
    }];
}

#pragma mark - 链接拷贝
- (void)linkCopy{
    if(!IsStringEmpty(self.shareUrl)){
        [SWHUDUtil hideHudViewWithMessageSuperView:self.view withMessage:@"复制成功"];
        UIPasteboard *pboard = [UIPasteboard generalPasteboard];
        pboard.string = self.shareUrl;
    }
}

#pragma mark - 二维码分享
- (void)shareQRCode{
    if(IsStringEmpty(self.shareUrl))return;
    UIImage *image = [XWQRCodeGenerator QRCodeWithContentString:self.shareUrl size:200 centerLogo:[UIImage imageNamed:@"logo"]];
    [self dismissViewControllerAnimated:NO completion:^{
        [[QWCommonMethod Instance] sharedFromSystem:self.shareTitle withUrl:nil withImage:image success:^{
        } failure:^{
        }];
    }];
}

@end
