//
//  QWShareHomeController.h
//  assistant
//
//  Created by qunai on 2024/1/3.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QWShareHomeController : UIViewController

@property (nonatomic, copy) NSString *shareUrl;
@property (nonatomic, copy) NSString *shareTitle;

@end

NS_ASSUME_NONNULL_END
