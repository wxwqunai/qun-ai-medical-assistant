//
//  QWAppProfile.h
//  qwm
//
//  Created by kevin on 2023/3/26.
//

#import <Foundation/Foundation.h>
#import "LKDBHelper.h"//数据缓存
#import "QWLoginModel.h"
#import "QWHomeModel.h"
#import "QWMineModel.h"
#import "QWMeetingModel.h"

NS_ASSUME_NONNULL_BEGIN

#define AppProfile [QWAppProfile sharedInstance]

@interface QWAppProfile : NSObject
@property (nonatomic,strong) QWqaUserModel *qaUserInfo;//登录人信息
@property (nonatomic,strong) SWLoginTimeModel *timeModel;//注册定时器
@property (nonatomic,strong) QWMeMenus *meMenus;
+(instancetype)sharedInstance;

#pragma mark - 是否已登录
- (BOOL)isLogined;
- (BOOL)isLoginedAlert;

#pragma mark - 保存／获取 用户信息 缓存
-(void)saveUserDefault:(QWqaUserModel *)userInfo;
-(QWqaUserModel *)GetUserInfo;
-(void)chageUserInfo:(QWqaUserModel *)userInfo;
-(void)clearUserInfo;

#pragma mark - 视频会议-保存/回去
-(void)saveVideoMeeting:(QWAgoraUserInfoModel *)bookingModel;
-(NSMutableArray *)GetBookingModelArr;
-(void)chageBookingModel:(QWAgoraUserInfoModel *)bookingModel;
-(void)deleteBookingModel:(QWAgoraUserInfoModel *)bookingModel;
-(void)clearBookingModel;

@end

NS_ASSUME_NONNULL_END
