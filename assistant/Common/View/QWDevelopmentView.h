//
//  QWDevelopmentView.h
//  assistant
//
//  Created by qunai on 2023/8/25.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^LongPressBlock)(void);
@interface QWDevelopmentView : UIView
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic,copy) LongPressBlock longPressBlock;
@end

NS_ASSUME_NONNULL_END
