//
//  QWDevelopmentView.m
//  assistant
//
//  Created by qunai on 2023/8/25.
//

#import "QWDevelopmentView.h"

@implementation QWDevelopmentView

- (instancetype)init{
    self = [super init];
    if(self){
        self.layer.borderWidth = 1.0;
        self.layer.borderColor = Color_Main_Green.CGColor;
        self.layer.cornerRadius = 8.0;
        self.layer.masksToBounds = YES;
        
       //手势-长按
        UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
        longPressGesture.minimumPressDuration = 1.5; //长按1.5秒
        longPressGesture.numberOfTouchesRequired = 1;
        [self addGestureRecognizer:longPressGesture];
        
        [self nameLabel];
    }
    return self;
}

- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.numberOfLines = 0;
        _nameLabel.text = @"正在建设，敬请期待";
        _nameLabel.textColor = Color_Main_Green;
        _nameLabel.font = [UIFont systemFontOfSize:24];
        [self addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(30);
            make.right.equalTo(self).offset(-30);
            make.top.equalTo(self).offset(20);
            make.bottom.equalTo(self).offset(-20);
        }];
    }
    return _nameLabel;
}


- (void)longPress:(UILongPressGestureRecognizer *)press
{
    //state属性是所有手势父类提供的方法，用于记录手势的状态
    if (press.state == UIGestureRecognizerStateBegan) {
        //长按手势开始响应!
        if(self.longPressBlock)self.longPressBlock();
    } else if (press.state == UIGestureRecognizerStateChanged) {
        //长按手势状态发生改变!
    } else {
        //长按手势结束!
    }
}



@end
