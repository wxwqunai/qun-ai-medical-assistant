//
//  QWCopyrightInfoView.m
//  assistant
//
//  Created by qunai on 2023/9/22.
//

#import "QWCopyrightInfoView.h"
@interface QWCopyrightInfoView()
@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UILabel *contentLabel;
@end

@implementation QWCopyrightInfoView
- (instancetype)init{
    self = [super init];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        [self bgView];
        
        [self contentLabel];
    }
    return self;
}

#pragma mark - 内容
- (UIView *)bgView {
    if(!_bgView){
        _bgView  = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor clearColor];
        [self addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.bottom.equalTo(self);
        }];
    }
    return _bgView;
}

#pragma mark - 公司名称
- (UILabel *)contentLabel{
    if(!_contentLabel){
        _contentLabel = [self customLabel];
        _contentLabel.text = @"上海群爱智慧医疗\n客服热线400-610-9777\n公众号“群爱智慧医疗”";
        [_bgView addSubview:_contentLabel];
        
        [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bgView).offset(2.0);
            make.right.equalTo(_bgView).offset(-2.0);
            make.top.equalTo(_bgView);
            make.bottom.equalTo(_bgView).offset(-20);
        }];
    }
    return _contentLabel;
}

#pragma mark - 公共
- (UILabel *)customLabel{
    UILabel *label = [[UILabel alloc]init];
    label.numberOfLines = 0;
    label.textColor = [UIColor colorFromHexString:@"#9D9DA0"];
    label.font = [UIFont systemFontOfSize:14];
    label.textAlignment = NSTextAlignmentCenter;
    return label;
}

@end
