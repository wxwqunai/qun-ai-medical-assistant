//
//  QWComButtonView.m
//  assistant
//
//  Created by qunai on 2023/9/22.
//

#import "QWComButtonView.h"
@interface QWComButtonView()
@property (nonatomic, strong) UIButton *confirmButton;
@end
@implementation QWComButtonView
- (instancetype)init{
    self = [super init];
    if(self){
        [self loadAllView];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self loadAllView];
    }
    return self;
}
- (void)loadAllView{
    self.backgroundColor = [UIColor clearColor];
    self.isEnable = YES;
    [self confirmButton];
}

#pragma mark - 完成
-(UIButton *)confirmButton
{
    if (!_confirmButton) {
        _confirmButton =[UIButton buttonWithType:UIButtonTypeCustom];
        [_confirmButton setTitle:@"完成" forState:UIControlStateNormal];
        [_confirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _confirmButton.titleLabel.font = [UIFont systemFontOfSize:18];
        _confirmButton.backgroundColor = Color_Main_Green;
        [_confirmButton addTarget:self action:@selector(confirmButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        _confirmButton.layer.cornerRadius = 6;
        _confirmButton.layer.masksToBounds = YES;
        
        [self addSubview:_confirmButton];
        
        [_confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.width.equalTo(self.mas_width).multipliedBy(0.6);
            make.top.equalTo(self);
            make.bottom.equalTo(self);
        }];
    }
    return _confirmButton;
}
-(void)confirmButtonClick:(UIButton *)sender
{
    if(self.makeActionClickBlock)self.makeActionClickBlock();
}

- (void)setBtnName:(NSString *)btnName{
    _btnName = btnName;
    [_confirmButton setTitle:_btnName forState:UIControlStateNormal];
}

- (void)setIsEnable:(BOOL)isEnable{
    _isEnable = isEnable;
    _confirmButton.alpha = _isEnable?1:0.5;
}

@end
