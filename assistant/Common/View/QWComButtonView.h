//
//  QWComButtonView.h
//  assistant
//
//  Created by qunai on 2023/9/22.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^MakeActionClickBlock)(void);
@interface QWComButtonView : UIView
@property (nonatomic,copy) MakeActionClickBlock makeActionClickBlock;

@property (nonatomic, copy) NSString *btnName;
@property (nonatomic, assign) BOOL isEnable;
@end

NS_ASSUME_NONNULL_END
