//
//  WNavigationBarView.m
//  watermark
//
//  Created by qunai on 2023/11/22.
//

#import "WNavigationBarView.h"

@implementation WNavigationBarView

- (instancetype)init{
    self = [super init];
    if(self){
        self.frame = CGRectMake(0,0, kScreenWidth, [UIDevice navigationFullHeight]);
        self.backgroundColor = [UIColor clearColor];
        [self navBgView];
    
        [self backBtn];
        [self titleView];
        [self optionsView];
    }
    return self;
}

- (UIView *)navBgView{
    if(!_navBgView){
        _navBgView = [[UIView alloc]init];
        [self addSubview:_navBgView];
        
        [_navBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.top.equalTo(self).offset([UIDevice safeDistanceTop]);
            make.bottom.equalTo(self);
        }];
    }
    return _navBgView;
}

#pragma mark - 返回
- (UIButton *)backBtn{
    if(!_backBtn){
        _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backBtn setImage:[UIImage imageNamed:@"WnavBack"] forState:UIControlStateNormal];
        [_backBtn addTarget:self action:@selector(backBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
        _backBtn.tintColor = [UIColor whiteColor];
        [_navBgView addSubview:_backBtn];
        
        [_backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_navBgView).offset(14);
            make.top.equalTo(_navBgView);
            make.bottom.equalTo(_navBgView);
        }];
    }
    return _backBtn;
}
- (void)backBtnOnClick{
    UIViewController *currentVC = [[QWCommonMethod Instance] getCurrentVC];
    [currentVC.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 标题
- (UIView *)titleView{
    if(!_titleView){
        _titleView = [[UIView alloc]init];

        [_navBgView addSubview:_titleView];
        
        [_titleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_navBgView);
            make.bottom.equalTo(_navBgView);
            make.width.mas_lessThanOrEqualTo(_navBgView.mas_width).multipliedBy(0.5);
            make.centerX.equalTo(_navBgView);
        }];
        
    }
    return _titleView;
}

#pragma mark - 操作
- (UIView *)optionsView{
    if(!_optionsView){
        _optionsView = [[UIView alloc]init];
        [_navBgView addSubview:_optionsView];
        
        [_optionsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_navBgView).offset(-14);
            make.left.equalTo(_titleView.mas_right).offset(8);
            make.top.equalTo(_navBgView);
            make.bottom.equalTo(_navBgView);
        }];
    }
    return _optionsView;
}

@end
