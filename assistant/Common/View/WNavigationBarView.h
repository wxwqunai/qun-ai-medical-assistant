//
//  WNavigationBarView.h
//  watermark
//
//  Created by qunai on 2023/11/22.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WNavigationBarView : UIView
@property (nonatomic, strong) UIView *navBgView;

@property (nonatomic, strong) UIButton *backBtn;

@property (nonatomic, strong) UIView *titleView;
@property (nonatomic, strong) UIView *optionsView;
@end

NS_ASSUME_NONNULL_END
