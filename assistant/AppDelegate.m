//
//  AppDelegate.m
//  assistant
//
//  Created by kevin on 2023/4/12.
//

#import "AppDelegate.h"
#import "QWMainTabBarController.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import <Bugly/Bugly.h>
#import <MobPush/MobPush.h>
#import <MOBFoundation/MobSDK.h>
#import <MOBFoundation/MobSDK+Privacy.h>

//IM
#import "SingleCallController.h"
#import "ConferenceController.h"
#import "EMDemoOptions.h"
#import "EMNotificationHelper.h"
#import "EaseIMHelper.h"
#import "UserInfoStore.h"

@interface AppDelegate ()<UNUserNotificationCenterDelegate,EaseCallDelegate,EMLocalNotificationDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent; //状态栏颜色-白色

    [self initNotifications];
    [self initIMEMClient];
    [self initRootVC];//初始化页面
    [self addReachabilityNotification];//网络监控
    [self addIQKeyboard]; //调节键盘
    [self _registerRemoteNotification]; //注册推送
    [self initBugly]; //崩溃日志统计
    [self initMobPush];//推送
    
    return YES;
}

#pragma mark- Notification
-  (void)initNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginStateChangeNotification) name:QWLogin_Change_State object:nil];
}
//登录状态变化
- (void)loginStateChangeNotification{
    [self getRegistrationID];
}

#pragma mark- 进入前台
- (void)applicationDidBecomeActive:(UIApplication *)application {
    if ([AppProfile GetUserInfo]) {//每次返回前台获取用户信息
        AppProfile.qaUserInfo =[AppProfile GetUserInfo];
    }
}

#pragma mark- 初始化页面
- (void)initRootVC{
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = UIColor.whiteColor;
    QWMainTabBarController *tabBarVc = [[QWMainTabBarController alloc] init];
//    QWNavgationController *navgationController = [[QWNavgationController alloc] initWithRootViewController:tabBarVc];
    self.window.rootViewController = tabBarVc;
    [self.window makeKeyAndVisible];
}

#pragma mark- 网络监听
-(void)addReachabilityNotification{
//    // 监测网络情况
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(reachabilityChanged:)
//                                                 name: kReachabilityChangedNotification
//                                               object: nil];
    Reachability  *hostReach = [Reachability reachabilityWithHostName:@"www.apple.com"];//可以以多种形式初始化
    [hostReach startNotifier];//开始监听,会启动一个run loop
}

//- (void)reachabilityChanged:(NSNotification *)note {
//    // 连接改变
//    Reachability* curReach = [note object];
//    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
//    //处理连接改变后的情况
//    NetworkStatus status = [curReach currentReachabilityStatus];
//
//    if (status != NotReachable) {
//
//    }
//}

#pragma mark - 键盘库
- (void)addIQKeyboard{
    IQKeyboardManager *keyboardManager = [IQKeyboardManager sharedManager]; // 获取类库的单例变量
    keyboardManager.enable = YES; // 控制整个功能是否启用
    keyboardManager.shouldResignOnTouchOutside = YES; // 控制点击背景是否收起键盘
    keyboardManager.shouldToolbarUsesTextFieldTintColor = NO; // 控制键盘上的工具条文字颜色是否用户自定义
    keyboardManager.toolbarManageBehaviour = IQAutoToolbarBySubviews; // 有多个输入框时，可以通过点击Toolbar 上的“前一个”“后一个”按钮来实现移动到不同的输入框
    keyboardManager.enableAutoToolbar = NO; // 控制是否显示键盘上的工具条
    keyboardManager.shouldShowToolbarPlaceholder = NO; // 是否显示占位文字
    keyboardManager.placeholderFont = [UIFont boldSystemFontOfSize:17]; // 设置占位文字的字体
    keyboardManager.keyboardDistanceFromTextField = 10.0f; // 输入框距离键盘的距离
       //修改工具条字体颜色
    [IQKeyboardManager sharedManager].shouldToolbarUsesTextFieldTintColor = NO;
    [IQKeyboardManager sharedManager].toolbarTintColor = [UIColor grayColor];
}

#pragma mark- 初始化环信SDK
- (void)initIMEMClient{
    //注册登录状态监听
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginStateChange:) name:ACCOUNT_LOGIN_CHANGED object:nil];
    
    //一
    EMDemoOptions *demoOptions = [EMDemoOptions sharedOptions];
    [EaseIMKitManager initWithEMOptions:[demoOptions toOptions]];
    
    //二
    //初始化EaseIMHelper，注册 EMClient 监听
    [EaseIMHelper shareHelper];
    
    if (demoOptions.isAutoLogin){
        [[NSNotificationCenter defaultCenter] postNotificationName:ACCOUNT_LOGIN_CHANGED object:@(YES)];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:ACCOUNT_LOGIN_CHANGED object:@(NO)];
    }
}

- (void)loginStateChange:(NSNotification *)aNotif
{
//    UINavigationController *navigationController = nil;
    
    BOOL loginSuccess = [aNotif.object boolValue];
    if (loginSuccess) {//登录成功加载主窗口控制器
        [[EMClient sharedClient].pushManager getPushNotificationOptionsFromServerWithCompletion:^(EMPushOptions * _Nonnull aOptions, EMError * _Nonnull aError) {
            if (!aError) {
                [[EaseIMKitManager shared] cleanMemoryUndisturbMaps];
                [[NSNotificationCenter defaultCenter] postNotificationName:USER_PUSH_CONFIG_UPDATE object:nil];//更新用户重启App时，会话免打扰状态UI同步
            }
        }];
        [[EMClient sharedClient].groupManager getJoinedGroupsFromServerWithPage:0 pageSize:-1 needMemberCount:YES needRole:YES completion:^(NSArray<EMGroup *> * _Nullable aList, EMError * _Nullable aError) {
            if (!aError) {
                [[NSNotificationCenter defaultCenter] postNotificationName:GROUP_LIST_FETCHFINISHED object:nil];
            }
        }];
        
        
        [EMNotificationHelper shared];
        [SingleCallController sharedManager];
        [ConferenceController sharedManager];
        [[UserInfoStore sharedInstance] loadInfosFromLocal];
        EaseCallConfig* config = [[EaseCallConfig alloc] init];
        config.agoraAppId = @"15cb0d28b87b425ea613fc46f7c9f974";
        config.enableRTCTokenValidate = YES;

        [[EaseCallManager sharedManager] initWithConfig:config delegate:self];

    } else {//登录失败加载登录页面控制器

    }
}

#pragma mark - 通知推送-start
//- (void)applicationDidEnterBackground:(UIApplication *)application
//{
//    [[EMClient sharedClient] applicationDidEnterBackground:application];
//}

//- (void)applicationWillEnterForeground:(UIApplication *)application
//{
//    [[EMClient sharedClient] applicationWillEnterForeground:application];
//    
//    //程序进入前台时,清除角标，但不清空通知栏消息(开发者根据业务需求，自行调用)
//    //注意：不建议在进入后台通知(applicationDidEnterBackground:)中调用此方法，原因进入后台将角标清空结果无法通过网络同步到服务器
//    [MobPush clearBadge];
//}

- (void)applicationWillTerminate:(UIApplication *)application
{
    
}

// 将得到的deviceToken传给SDK
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[EMClient sharedClient] bindDeviceToken:deviceToken];
    });
}
// 注册deviceToken失败，此处失败，与环信SDK无关，一般是您的环境配置或者证书配置有误
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    EMAlertView *alertView = [[EMAlertView alloc]initWithTitle:NSLocalizedString(@"registefail", nil) message:error.description];
    [alertView show];
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
//    if (gMainController) {
//        [gMainController jumpToChatList];
//    }
    [[EMClient sharedClient] application:application didReceiveRemoteNotification:userInfo];
}

//#pragma mark - UNUserNotificationCenterDelegate

/*
// 如果用户在app设置了UNUserNotificationCenter的代理delegate 则需要实现以下两个方法并调用em的相关方法
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler
{
    [[EMLocalNotificationManager sharedManager] userNotificationCenter:center willPresentNotification:notification withCompletionHandler:completionHandler];
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler
{
    [[EMLocalNotificationManager sharedManager] userNotificationCenter:center didReceiveNotificationResponse:response withCompletionHandler:completionHandler];
}
 */

#pragma mark - EMLocalPushManagerDelegate
/*
 //如果自己设置通知方式，则通过下面方式修改
- (void)emuserNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler
{
    NSDictionary *userInfo = notification.request.content.userInfo;
    if ([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        NSLog(@"APNS userInfo : %@ : %@",userInfo);
    }else{
        NSLog(@"EaseMob userInfo : %@ \n ext : %@",userInfo,userInfo[@"ext"]);
    }
    completionHandler(UNNotificationPresentationOptionBadge|UNNotificationPresentationOptionSound|UNNotificationPresentationOptionAlert);//通知方式 可选badge，sound，alert 如果实现了这个代理方法，则必须有completionHandler回调
}

- (void)emuserNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler
{
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    if ([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        NSLog(@"APNS userInfo : %@ \n",userInfo);
    }else{
        NSLog(@"EaseMob userInfo : %@ \n ext : %@",userInfo,userInfo[@"ext"]);
    }
    completionHandler();//如果实现了这个代理方法 ，则必须有completionHandler回调
}
*/

//如果需要获取数据 只实现这一个代理方法即可
- (void)emGetNotificationMessage:(UNNotification *)notification state:(EMNotificationState)state
{
    NSDictionary *userInfo = notification.request.content.userInfo;
    if ([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        //apns推送
        NSLog(@"userInfo : %@",userInfo);
        [self pushDataToTestLog:[NSString stringWithFormat:@"notificationlog:type==%@ channel==%@ title==%@ \n userInfo===",(state == EMWillPresentNotification?@"arrive":@"click"),@"apns推送",notification.request.content.title] userInfo:userInfo];
    }else{
        //本地推送
        NSLog(@"userInfo : %@ \n ext : %@",userInfo,userInfo[@"ext"]);
        [self pushDataToTestLog:[NSString stringWithFormat:@"notificationlog:type===%@ channel===%@ title===%@ \n userInfo===",(state == EMWillPresentNotification?@"arrive":@"click"),@"环信在线推送",notification.request.content.title] userInfo:userInfo];
    }

    if (state == EMDidReceiveNotificationResponse) {
        //通知被点开
        NSLog(@"");
    }else{
        //即将展示通知
        NSLog(@"");
    }

}

//当应用收到环信推送透传消息时，此方法会被调用 注意这里是使用环信推送功能的透传消息
- (void)emDidRecivePushSilentMessage:(NSDictionary *)messageDic
{
    NSLog(@"emDidRecivePushSilentMessage : %@",messageDic);
    [self pushDataToTestLog:@"notificationlog:透传消息===" userInfo:messageDic];
}

-(void)pushDataToTestLog:(NSString*)keyStr userInfo:(NSDictionary*)userInfo
{
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:userInfo options:NSJSONWritingPrettyPrinted error:&parseError];
    NSString *str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [[EMClient sharedClient] log:[NSString stringWithFormat:@"%@%@",keyStr,userInfo]];
}


#pragma mark -注册远程通知-通知推送-end
- (void)_registerRemoteNotification
{
    UIApplication *application = [UIApplication sharedApplication];
    application.applicationIconBadgeNumber = 0;
    
    if (NSClassFromString(@"UNUserNotificationCenter")) {
        [[EMLocalNotificationManager sharedManager] launchWithDelegate:self];
        
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionAlert completionHandler:^(BOOL granted, NSError *error) {
            if (granted) {
#if !TARGET_IPHONE_SIMULATOR
                dispatch_async(dispatch_get_main_queue(), ^{
                    [application registerForRemoteNotifications];
                });
#endif
            }
        }];
        return;
    }
    
    if([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationType notificationTypes = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:notificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    
#if !TARGET_IPHONE_SIMULATOR
    if ([application respondsToSelector:@selector(registerForRemoteNotifications)]) {
        [application registerForRemoteNotifications];
    } else {
        UIRemoteNotificationType notificationTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert;
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:notificationTypes];
    }
#endif
}

#pragma mark - 腾讯bugly-崩溃日志
- (void)initBugly{
    BuglyConfig * config = [[BuglyConfig alloc] init];
    // 设置自定义日志上报的级别，默认不上报自定义日志
    config.reportLogLevel = BuglyLogLevelWarn;
    config.version = [EMClient sharedClient].version;
    config.deviceIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    config.unexpectedTerminatingDetectionEnable = true;
    [Bugly startWithAppId:Bugly_ID config:config];
}



- (void)callDidEnd:(NSString*)aChannelName reason:(EaseCallEndReason)aReason time:(int)aTm type:(EaseCallType)aCallType
{
    NSString* msg = @"";
    switch (aReason) {
        case EaseCallEndReasonHandleOnOtherDevice:
            msg = NSLocalizedString(@"otherDevice", nil);
            break;
        case EaseCallEndReasonBusy:
            msg = NSLocalizedString(@"remoteBusy", nil);
            break;
        case EaseCallEndReasonRefuse:
            msg = NSLocalizedString(@"refuseCall", nil);
            break;
        case EaseCallEndReasonCancel:
            msg = NSLocalizedString(@"cancelCall", nil);
            break;
        case EaseCallEndReasonRemoteCancel:
            msg = NSLocalizedString(@"callCancel", nil);
            break;
        case EaseCallEndReasonRemoteNoResponse:
            msg = NSLocalizedString(@"remoteNoResponse", nil);
            break;
        case EaseCallEndReasonNoResponse:
            msg = NSLocalizedString(@"noResponse", nil);
            break;
        case EaseCallEndReasonHangup:
            msg = [NSString stringWithFormat:NSLocalizedString(@"callendPrompt", nil),aTm];
            break;
        default:
            break;
    }
    if([msg length] > 0) {
        UIWindow *win = [[[UIApplication sharedApplication] windows] firstObject];
        [SWHUDUtil hideHudViewWithMessageSuperView:win withMessage:msg];
    }
}
// 多人音视频邀请按钮的回调
- (void)multiCallDidInvitingWithCurVC:(UIViewController*_Nonnull)vc excludeUsers:(NSArray<NSString*> *_Nullable)users ext:(NSDictionary *)aExt
{
    NSString* groupId = nil;
    if(aExt) {
        groupId = [aExt objectForKey:@"groupId"];
    }
    
    ConfInviteUsersViewController * confVC = nil;
    if([groupId length] == 0) {
        confVC = [[ConfInviteUsersViewController alloc] initWithType:ConfInviteTypeUser isCreate:NO excludeUsers:users groupOrChatroomId:nil];
    }else{
        confVC = [[ConfInviteUsersViewController alloc] initWithType:ConfInviteTypeGroup isCreate:NO excludeUsers:users groupOrChatroomId:groupId];
    }
    
    [confVC setDoneCompletion:^(NSArray *aInviteUsers) {
        for (NSString* strId in aInviteUsers) {
            EMUserInfo* info = [[UserInfoStore sharedInstance] getUserInfoById:strId];
            if(info && (info.avatarUrl.length > 0 || info.nickname.length > 0)) {
                EaseCallUser* user = [EaseCallUser userWithNickName:info.nickname image:[NSURL URLWithString:info.avatarUrl]];
                [[[EaseCallManager sharedManager] getEaseCallConfig] setUser:strId info:user];
            }
        }
        [[EaseCallManager sharedManager] startInviteUsers:aInviteUsers ext:aExt completion:nil];
    }];
    confVC.modalPresentationStyle = UIModalPresentationPopover;
    [vc presentViewController:confVC animated:NO completion:nil];
}
// 振铃时增加回调
- (void)callDidReceive:(EaseCallType)aType inviter:(NSString*_Nonnull)username ext:(NSDictionary*)aExt
{
    EMUserInfo* info = [[UserInfoStore sharedInstance] getUserInfoById:username];
    if(info && (info.avatarUrl.length > 0 || info.nickname.length > 0)) {
        EaseCallUser* user = [EaseCallUser userWithNickName:info.nickname image:[NSURL URLWithString:info.avatarUrl]];
        [[[EaseCallManager sharedManager] getEaseCallConfig] setUser:username info:user];
    }
}

// 异常回调
- (void)callDidOccurError:(EaseCallError *)aError
{
    
}

- (void)callDidRequestRTCTokenForAppId:(NSString *)aAppId channelName:(NSString *)aChannelName account:(NSString *)aUserAccount uid:(NSInteger)aAgoraUid
{
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config
                                                          delegate:nil
                                                     delegateQueue:[NSOperationQueue mainQueue]];

    NSString* strUrl = [NSString stringWithFormat:@"http://a1.easemob.com/token/rtcToken/v1?userAccount=%@&channelName=%@&appkey=%@",[EMClient sharedClient].currentUsername,aChannelName,[EMClient sharedClient].options.appkey];
    NSString*utf8Url = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    NSURL* url = [NSURL URLWithString:utf8Url];
    NSMutableURLRequest* urlReq = [[NSMutableURLRequest alloc] initWithURL:url];
    [urlReq setValue:[NSString stringWithFormat:@"Bearer %@",[EMClient sharedClient].accessUserToken ] forHTTPHeaderField:@"Authorization"];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:urlReq completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if(data) {
            NSDictionary* body = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            NSLog(@"%@",body);
            if(body) {
                NSString* resCode = [body objectForKey:@"code"];
                if([resCode isEqualToString:@"RES_0K"]) {
                    NSString* rtcToken = [body objectForKey:@"accessToken"];
                    NSNumber* uid = [body objectForKey:@"agoraUserId"];
                    [[EaseCallManager sharedManager] setRTCToken:rtcToken channelName:aChannelName uid:[uid unsignedIntegerValue]];
                }
            }
        }
        
        
    }];

    [task resume];
}

-(void)remoteUserDidJoinChannel:( NSString*_Nonnull)aChannelName uid:(NSInteger)aUid username:(NSString*_Nullable)aUserName
{
    if(aUserName.length > 0) {
        EMUserInfo* userInfo = [[UserInfoStore sharedInstance] getUserInfoById:aUserName];
        if(userInfo && (userInfo.avatarUrl.length > 0 || userInfo.nickname.length > 0)) {
            EaseCallUser* user = [EaseCallUser userWithNickName:userInfo.nickname image:[NSURL URLWithString:userInfo.avatarUrl]];
            [[[EaseCallManager sharedManager] getEaseCallConfig] setUser:aUserName info:user];
        }
    }else{
        [self _fetchUserMapsFromServer:aChannelName];
    }
}

- (void)callDidJoinChannel:(NSString*_Nonnull)aChannelName uid:(NSUInteger)aUid
{
    [self _fetchUserMapsFromServer:aChannelName];
}

- (void)_fetchUserMapsFromServer:(NSString*)aChannelName
{
    // 这里设置映射表，设置头像，昵称
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config
                                                          delegate:nil
                                                     delegateQueue:[NSOperationQueue mainQueue]];

    NSString* strUrl = [NSString stringWithFormat:@"http://a1.easemob.com/channel/mapper?userAccount=%@&channelName=%@&appkey=%@",[EMClient sharedClient].currentUsername,aChannelName,[EMClient sharedClient].options.appkey];
    NSString*utf8Url = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    NSURL* url = [NSURL URLWithString:utf8Url];
    NSMutableURLRequest* urlReq = [[NSMutableURLRequest alloc] initWithURL:url];
    [urlReq setValue:[NSString stringWithFormat:@"Bearer %@",[EMClient sharedClient].accessUserToken ] forHTTPHeaderField:@"Authorization"];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:urlReq completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if(data) {
            NSDictionary* body = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            NSLog(@"mapperBody:%@",body);
            if(body) {
                NSString* resCode = [body objectForKey:@"code"];
                if([resCode isEqualToString:@"RES_0K"]) {
                    NSString* channelName = [body objectForKey:@"channelName"];
                    NSDictionary* result = [body objectForKey:@"result"];
                    NSMutableDictionary<NSNumber*,NSString*>* users = [NSMutableDictionary dictionary];
                    for (NSString* strId in result) {
                        NSString* username = [result objectForKey:strId];
                        NSNumber* uId = [NSNumber numberWithInteger:[strId integerValue]];
                        [users setObject:username forKey:uId];
                        EMUserInfo* info = [[UserInfoStore sharedInstance] getUserInfoById:username];
                        if(info && (info.avatarUrl.length > 0 || info.nickname.length > 0)) {
                            EaseCallUser* user = [EaseCallUser userWithNickName:info.nickname image:[NSURL URLWithString:info.avatarUrl]];
                            [[[EaseCallManager sharedManager] getEaseCallConfig] setUser:username info:user];
                        }
                    }
                    [[EaseCallManager sharedManager] setUsers:users channelName:channelName];
                }
            }
        }
    }];

    [task resume];
}

#pragma mark - 消息推送 - MobPush
- (void)initMobPush{
    
    // 设置推送环境
#ifdef DEBUG
    [MobPush setAPNsForProduction:NO];
#else
    [MobPush setAPNsForProduction:YES];
#endif
    
    [MobSDK registerAppKey:MOBAppKey appSecret:MOBAppSecret];//注册
    
    //MobPush推送设置（获得角标、声音、弹框提醒权限）
    MPushNotificationConfiguration *configuration = [[MPushNotificationConfiguration alloc] init];
    configuration.types = MPushAuthorizationOptionsBadge | MPushAuthorizationOptionsSound | MPushAuthorizationOptionsAlert;
    [MobPush setupNotification:configuration];
    
//    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"NotAPNsShowForeground"])
//    {
//        // 设置后，应用在前台时不展示通知横幅、角标、声音。（iOS 10 以后有效，iOS 10 以前本来就不展示）
//        [MobPush setAPNsShowForegroundType:MPushAuthorizationOptionsNone];
//    }
    
    [self getRegistrationID];
    
    // 监听推送通知 MobPush整合了系统iOS10以上及以下不同方式获取推送统一通过此监听获取，开发者通过原始方式亦不影响。
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveMessage:) name:MobPushDidReceiveMessageNotification object:nil];
    
    // 注意：上传隐私协议接口，具体查看官方文档(http://www.mob.com/wiki/detailed?wiki=MobTechprivacypushios&id=136)
    [MobSDK uploadPrivacyPermissionStatus:YES onResult:^(BOOL success) {
        NSLog(@"-------------->上传结果：%d",success);
    }];
}

// 获取注册id（可与用户id绑定，实现向指定用户推送消息）
-  (void)getRegistrationID{
    [MobPush getRegistrationID:^(NSString *registrationID, NSError *error) {
        if(error == nil && [AppProfile isLogined] && !IsStringEmpty(AppProfile.qaUserInfo.phone) &&!IsStringEmpty(registrationID)){
            NSDictionary *dic = @{@"loginName":AppProfile.qaUserInfo.phone,@"rid":registrationID};
            HXPostRequest * request = [[HXPostRequest alloc] initWithRequestUrl:URL_updateByloginName argument:dic];
            [request startWithCompletionBlockWithSuccess:^(__kindof HXRequest *request, NSDictionary *result, BOOL success) {
                NSLog(@"定向推送传送registrationID成功");
            } failure:^(__kindof HXRequest *request, NSString *errorInfo) {
                NSLog(@"定向推送传送registrationID失败");
            }];
        }
        NSLog(@"registrationID = %@--error = %@", registrationID, error);//提供给服务端，可定向推送
    }];
}

// 收到推送通知回调
- (void)didReceiveMessage:(NSNotification *)notification
{
    MPushMessage *message = notification.object;
    
    NSLog(@"message:%@", message.convertDictionary);
    
    switch (message.messageType)
    {
        case MPushMessageTypeCustom:
        {// 自定义消息回调
            NSLog(@"自定义消息回调==%@",message.notification.body);
        }
            break;
        case MPushMessageTypeAPNs:
        {// APNs回调
//            [[[UIAlertView alloc] initWithTitle:message.notification.userInfo.description message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
        }
            break;
        case MPushMessageTypeLocal:
        {// 本地通知回调
            NSString *body = message.notification.body;
            NSString *title = message.notification.title;
            NSString *subtitle = message.notification.subTitle;
            NSInteger badge = message.notification.badge;
            NSString *sound = message.notification.sound;
            NSLog(@"收到本地通知:{\nbody:%@，\ntitle:%@,\nsubtitle:%@,\nbadge:%ld,\nsound:%@,\n}",body, title, subtitle, (long)badge, sound);
            //[[[UIAlertView alloc] initWithTitle:message.notification.userInfo.description message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
            
        }
            break;
        case MPushMessageTypeClicked:
        {// 点击通知回调
            
            // 测试
            if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
            { // 前台
                [self showAlertWithMessage:message];
            }
            else
            { // 后台
                [self pushVCWithMessage:message];
            }
        }
        default:
            break;
    }
}

- (void)showAlertWithMessage:(MPushMessage *)message
{
    NSString *url = message.notification.userInfo[@"url"];
    
//    if (url)
//    {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"收到推送"
//                                                            message:nil
//                                                           delegate:self
//                                                  cancelButtonTitle:@"取消"
//                                                  otherButtonTitles:@"查看", nil];
//        [alertView show];
//    }
}

- (void)pushVCWithMessage:(MPushMessage *)message
{
    NSString *url = message.notification.userInfo[@"url"];
    
    NSLog(@"pushVCWithMessage--url==%@",url);
}


@end
