//
//  SampleHandler.h
//  ScreenShareExtension
//
//  Created by qunai on 2024/2/29.
//

#import <ReplayKit/ReplayKit.h>
#import <AgoraReplayKitExtension/AgoraReplayKitExtension.h>

@interface SampleHandler : AgoraReplayKitHandler

@end
